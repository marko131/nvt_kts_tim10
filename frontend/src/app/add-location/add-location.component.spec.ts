import { async, ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';

import { AddLocationComponent } from './add-location.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule, By } from '@angular/platform-browser';
import { MatFormFieldModule, MatCardModule, MatInputModule, MatGridListModule, MatSnackBarModule } from '@angular/material';
import { LocationService } from '../_services/location.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LocationComponent } from '../location/location.component';
import { Routes } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { Location } from "@angular/common";

describe('AddLocationComponent', () => {
    let component: AddLocationComponent;
    let fixture: ComponentFixture<AddLocationComponent>;

    let locationService: any;
    let location: Location;

    beforeEach(async(() => {
        let locationServiceMock = {
            addLocation: jasmine.createSpy('addLocation').and.returnValue(of(Promise.resolve()))
        };

        const routes: Routes = [{ path: "locations", component: LocationComponent }];

        TestBed.configureTestingModule({
            declarations: [ AddLocationComponent, LocationComponent ],
            imports: [
                BrowserModule,
                FormsModule,
                ReactiveFormsModule,
                MatFormFieldModule,
                MatCardModule,
                MatInputModule,
                MatGridListModule,
                MatSnackBarModule,
                BrowserAnimationsModule,
                RouterTestingModule.withRoutes(routes)
            ],
            providers: [
                { provide: LocationService, useValue: locationServiceMock }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(AddLocationComponent);
        component = fixture.componentInstance;
        locationService = TestBed.get(LocationService);
        location = TestBed.get(Location);

        fixture.detectChanges();
    }));

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should bind data from input fields to addLocationForm', fakeAsync(() => {
        let name = fixture.debugElement.query(By.css("[formcontrolname='name']")).nativeElement;
        let address = fixture.debugElement.query(By.css("[formcontrolname='address']")).nativeElement;
        let latitude = fixture.debugElement.query(By.css("[formcontrolname='latitude']")).nativeElement;
        let longitude = fixture.debugElement.query(By.css("[formcontrolname='longitude']")).nativeElement;
        let picture = fixture.debugElement.query(By.css("[formcontrolname='picture']")).nativeElement;

        expect(name.value).not.toEqual('Srpsko narodno pozoriste');
        expect(address.value).not.toEqual('Pozorišni trg 1, Novi Sad 21000');
        expect(latitude.value).not.toEqual('45.255009');
        expect(longitude.value).not.toEqual('19.843016');
        expect(picture.value).not.toEqual('picture/path/snp.jpg');

        tick();

        name.value = 'Srpsko narodno pozoriste';
        address.value = 'Pozorišni trg 1, Novi Sad 21000';
        latitude.value = 45.255009;
        longitude.value = 19.843016;
        picture.value = 'picture/path/snp.jpg';

        name.dispatchEvent(new Event('input'));
        address.dispatchEvent(new Event('input'));
        latitude.dispatchEvent(new Event('input'));
        longitude.dispatchEvent(new Event('input'));
        picture.dispatchEvent(new Event('input'));

        expect(name.value).toEqual('Srpsko narodno pozoriste');
        expect(address.value).toEqual('Pozorišni trg 1, Novi Sad 21000');
        expect(latitude.value).toEqual('45.255009');
        expect(longitude.value).toEqual('19.843016');
        expect(picture.value).toEqual('picture/path/snp.jpg');
    }));

    it ('should add location and navigate to location list page', fakeAsync(() => {
        component.submit();
        expect(locationService.addLocation).toHaveBeenCalled();
        tick();
        
        fixture.detectChanges();
        expect(location.path()).toBe("/locations");
    }));

    it ('should display error Name is required', fakeAsync(() => {
        let name = fixture.debugElement.query(By.css("[formcontrolname='name']"));

        name.nativeElement.dispatchEvent(new Event('blur'));
        tick();
        fixture.detectChanges();

        expect(component.addLocationForm.invalid).toBe(true);

        let error = fixture.debugElement.query(By.css("mat-error"));
        expect(error.nativeElement.innerText).toEqual('Name is required');
    }));

    it ('should display error Maximum number of characters allowed is 255 for name field', fakeAsync(() => {
        let name = fixture.debugElement.query(By.css("[formcontrolname='name']"));
        name.nativeElement.value = "a".repeat(300);

        name.nativeElement.dispatchEvent(new Event('input'));
        tick();
        fixture.detectChanges();

        name.nativeElement.dispatchEvent(new Event('blur'));
        tick();
        fixture.detectChanges();

        expect(component.addLocationForm.invalid).toBe(true);

        let error = fixture.debugElement.query(By.css("mat-error"));
        expect(error.nativeElement.innerText).toEqual('Maximum number of characters allowed is 255');
    }));

    it ('should display error Address is required', fakeAsync(() => {
        let address = fixture.debugElement.query(By.css("[formcontrolname='address']"));

        address.nativeElement.dispatchEvent(new Event('blur'));
        tick();
        fixture.detectChanges();

        expect(component.addLocationForm.invalid).toBe(true);

        let error = fixture.debugElement.query(By.css("mat-error"));
        expect(error.nativeElement.innerText).toEqual('Address is required');
    }));

    it ('should display error Maximum number of characters allowed is 255 for address field', fakeAsync(() => {
        let address = fixture.debugElement.query(By.css("[formcontrolname='address']"));
        address.nativeElement.value = "a".repeat(300);

        address.nativeElement.dispatchEvent(new Event('input'));
        tick();
        fixture.detectChanges();

        address.nativeElement.dispatchEvent(new Event('blur'));
        tick();
        fixture.detectChanges();

        expect(component.addLocationForm.invalid).toBe(true);

        let error = fixture.debugElement.query(By.css("mat-error"));
        expect(error.nativeElement.innerText).toEqual('Maximum number of characters allowed is 255');
    }));

    it ('should display error Latitude is required', fakeAsync(() => {
        let latitude = fixture.debugElement.query(By.css("[formcontrolname='latitude']"));

        latitude.nativeElement.dispatchEvent(new Event('blur'));
        tick();
        fixture.detectChanges();

        expect(component.addLocationForm.invalid).toBe(true);

        let error = fixture.debugElement.query(By.css("mat-error"));
        expect(error.nativeElement.innerText).toEqual('Latitude is required');
    }));

    it ('should display error Minimum value is -90.000000 for latitude field', fakeAsync(() => {
        let latitude = fixture.debugElement.query(By.css("[formcontrolname='latitude']"));
        latitude.nativeElement.value = -1564;

        latitude.nativeElement.dispatchEvent(new Event('input'));
        tick();
        fixture.detectChanges();

        latitude.nativeElement.dispatchEvent(new Event('blur'));
        tick();
        fixture.detectChanges();

        expect(component.addLocationForm.invalid).toBe(true);

        let error = fixture.debugElement.query(By.css("mat-error"));
        expect(error.nativeElement.innerText).toEqual('Minimum value is -90.000000');
    }));

    it ('should display error Maximum value is 90.000000 for latitude field', fakeAsync(() => {
        let latitude = fixture.debugElement.query(By.css("[formcontrolname='latitude']"));
        latitude.nativeElement.value = 1564;

        latitude.nativeElement.dispatchEvent(new Event('input'));
        tick();
        fixture.detectChanges();

        latitude.nativeElement.dispatchEvent(new Event('blur'));
        tick();
        fixture.detectChanges();

        expect(component.addLocationForm.invalid).toBe(true);

        let error = fixture.debugElement.query(By.css("mat-error"));
        expect(error.nativeElement.innerText).toEqual('Maximum value is 90.000000');
    }));

    it ('should display error Longitude is required', fakeAsync(() => {
        let longitude = fixture.debugElement.query(By.css("[formcontrolname='longitude']"));

        longitude.nativeElement.dispatchEvent(new Event('blur'));
        tick();
        fixture.detectChanges();

        expect(component.addLocationForm.invalid).toBe(true);

        let error = fixture.debugElement.query(By.css("mat-error"));
        expect(error.nativeElement.innerText).toEqual('Longitude is required');
    }));

    it ('should display error Minimum value is -180.000000 for longitude field', fakeAsync(() => {
        let longitude = fixture.debugElement.query(By.css("[formcontrolname='longitude']"));
        longitude.nativeElement.value = -1564;

        longitude.nativeElement.dispatchEvent(new Event('input'));
        tick();
        fixture.detectChanges();

        longitude.nativeElement.dispatchEvent(new Event('blur'));
        tick();
        fixture.detectChanges();

        expect(component.addLocationForm.invalid).toBe(true);

        let error = fixture.debugElement.query(By.css("mat-error"));
        expect(error.nativeElement.innerText).toEqual('Minimum value is -180.000000');
    }));

    it ('should display error Maximum value is 180.000000 for longitude field', fakeAsync(() => {
        let longitude = fixture.debugElement.query(By.css("[formcontrolname='longitude']"));
        longitude.nativeElement.value = 1564;

        longitude.nativeElement.dispatchEvent(new Event('input'));
        tick();
        fixture.detectChanges();

        longitude.nativeElement.dispatchEvent(new Event('blur'));
        tick();
        fixture.detectChanges();

        expect(component.addLocationForm.invalid).toBe(true);

        let error = fixture.debugElement.query(By.css("mat-error"));
        expect(error.nativeElement.innerText).toEqual('Maximum value is 180.000000');
    }));

    it ('should display error Image is required', fakeAsync(() => {
        let image = fixture.debugElement.query(By.css("[formcontrolname='picture']"));
        image.nativeElement.dispatchEvent(new Event('blur'));
        tick();

        fixture.detectChanges();
        let error = fixture.debugElement.query(By.css("mat-error"));
        expect(error.nativeElement.innerText).toEqual('Image is required');
    }));

    it ('should be disabled Add location button when invalid form', fakeAsync(() => {
        let name = fixture.debugElement.query(By.css("[formcontrolname='name']"));

        name.nativeElement.dispatchEvent(new Event('blur'));
        tick();
        fixture.detectChanges();

        expect(component.addLocationForm.invalid).toBe(true);

        let button  = fixture.debugElement.query(By.css('button'));
        expect(button.nativeElement.disabled).toBe(true);
    }));
});
