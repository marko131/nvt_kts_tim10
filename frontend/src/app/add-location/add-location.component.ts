import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { LocationService } from "../_services/location.service";
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: "app-add-location",
  templateUrl: "./add-location.component.html",
  styleUrls: ["./add-location.component.scss"]
})
export class AddLocationComponent implements OnInit {
  addLocationForm: FormGroup;
  

  constructor(private locationService: LocationService,
              private router: Router,
              private snackBar: MatSnackBar) {
    this.addLocationForm = new FormGroup({
      name: new FormControl("", [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255)
      ]),
      address: new FormControl("", [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255)
      ]),
      latitude: new FormControl("", [
        Validators.required,
        Validators.min(-90),
        Validators.max(90),
        Validators.pattern("^([0-9]+(\.[0-9]?[0-9]?[0-9]?[0-9]?[0-9]?[0-9]?)?)")
      ]),
      longitude: new FormControl("", [
        Validators.required,
        Validators.min(-180),
        Validators.max(180),
        Validators.pattern("^([0-9]+(\.[0-9]?[0-9]?[0-9]?[0-9]?[0-9]?[0-9]?)?)")
      ]),
      picture: new FormControl("", [Validators.required])
    });
  }

  ngOnInit() {}

  submit() {
    this.locationService.addLocation(this.addLocationForm.value).subscribe(
      response => {
        console.log(response)
        this.router.navigate(["/locations"])
      },
      errorResponse => {
        this.snackBar.open(errorResponse.error.message, "", { duration: 15000 });
      }
    );
  }
}
