import { NgModule, Component } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { LoginComponent } from "./login/login.component";
import { AuthGuard } from "./_helpers/auth.guard";
import { RegisterComponent } from "./register/register.component";
import { EventComponent } from "./event/event.component";
import { ProfileComponent } from "./profile/profile.component";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { EventDetailsComponent } from "./event-details/event-details.component";
import { CreateEventComponent } from "./create-event/create-event.component";
import { EditEventComponent } from "./edit-event/edit-event.component";
import { AddLocationComponent } from "./add-location/add-location.component";
import { LocationComponent } from "./location/location.component";
import { EditLocationComponent } from "./edit-location/edit-location.component";
import { AddLayoutComponent } from "./add-layout/add-layout.component";
import { LayoutListComponent } from "./layout-list/layout-list.component";
import { LayoutComponent } from "./layout/layout.component";

const routes: Routes = [
  { path: "login", component: LoginComponent },
  { path: "register", component: RegisterComponent },
  { path: "profile", component: ProfileComponent, canActivate: [AuthGuard] },
  { path: "event/:id", component: EventDetailsComponent },
  { path: "event", component: EventComponent },
  {
    path: "add-event",
    component: CreateEventComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "add-location",
    component: AddLocationComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "edit-event/:id",
    component: EditEventComponent,
    canActivate: [AuthGuard]
  },
  { path: "locations", component: LocationComponent, canActivate: [AuthGuard] },
  {
    path: "edit-location/:id",
    component: EditLocationComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "location/:id/layout",
    component: LayoutListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "location/:id/add-layout",
    component: AddLayoutComponent,
    canActivate: [AuthGuard]
  },

  { path: "", redirectTo: "event", pathMatch: "full" },
  { path: "**", component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
