import { Component, OnInit } from "@angular/core";
import { LocationService } from "../_services/location.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { MatSnackBar } from '@angular/material';

@Component({
  selector: "app-edit-location",
  templateUrl: "./edit-location.component.html",
  styleUrls: ["./edit-location.component.scss"]
})
export class EditLocationComponent implements OnInit {
  editLocationForm: FormGroup;
  locationId: number;

  constructor(
    private locationService: LocationService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar
  ) {
    this.editLocationForm = new FormGroup({
      name: new FormControl("", [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255)
      ]),
      address: new FormControl("", [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255)
      ]),
      picture: new FormControl("", [Validators.required])
    });
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.locationId = +params["id"];
      this.locationService.getById(this.locationId).subscribe(response => {
        this.editLocationForm.get("name").setValue(response.name);
        this.editLocationForm.get("address").setValue(response.address);
        this.editLocationForm.get("picture").setValue(response.picture);
      });
    });
  }

  submit() {
    this.locationService
      .update(this.locationId, this.editLocationForm.value)
      .subscribe(
        response => {
          console.log(response)
          this.router.navigate(["/locations"])
        },
        errorResponse => {
          this.snackBar.open(errorResponse.error.message, "", { duration: 15000 });
        }
      );
  }
}
