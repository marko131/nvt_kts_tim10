import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";
import { AuthService } from "../_services/auth.service";
import { Router } from "@angular/router";
import { MyErrorStateMatcher } from "../_helpers/myErrorStateMatcher";
import { MatSnackBar } from "@angular/material";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.scss"]
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  matcher = new MyErrorStateMatcher();

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private _snackBar: MatSnackBar
  ) {
    this.registerForm = new FormGroup(
      {
        firstName: new FormControl("", Validators.required),
        lastName: new FormControl("", Validators.required),
        email: new FormControl("", [Validators.required, Validators.email]),
        password1: new FormControl("", [
          Validators.required,
          Validators.minLength(6)
        ]),
        password2: new FormControl("", [
          Validators.required,
          Validators.minLength(6)
        ])
      },
      { validators: this.checkPasswords }
    );
  }

  ngOnInit() {}

  register() {
    const formValue = this.registerForm.value;

    if (this.registerForm.valid) {
      this.authService
        .register(
          formValue.firstName,
          formValue.lastName,
          formValue.email,
          formValue.password1,
          formValue.password2
        )
        .subscribe(
          response => {
            this._snackBar.open(response, "", {
              duration: 2000
            });
            this.router.navigate(["/login"]);
          },
          response => {
            try {
              let errorResponse = JSON.parse(response.error);
              this._snackBar.open(errorResponse.message, "", {
                duration: 2000
              });
            } catch (err) {
              alert(response.error);
            }
          }
        );
    }
  }

  checkPasswords(group: FormGroup) {
    let pass = group.controls.password1.value;
    let confirmPass = group.controls.password2.value;

    return pass === confirmPass ? null : { notSame: true };
  }
}
