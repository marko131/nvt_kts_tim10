import {
    async,
    ComponentFixture,
    TestBed,
    fakeAsync,
    tick
  } from "@angular/core/testing";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
  MatFormFieldModule,
  MatCardModule,
  MatInputModule,
  MatButtonModule,
  MatSnackBarModule,
  MatToolbar,
  MatToolbarModule
} from "@angular/material";
import { Routes } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";
import { Location } from "@angular/common";
import { RouterTestingModule } from "@angular/router/testing";
import { AllowedRoutes } from "../_services/allowedRoutes.service";
import { AuthService } from "../_services/auth.service";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { UserProfile } from "../_models/user";
import { of } from "rxjs";
import { By } from "@angular/platform-browser";
import { LoginComponent } from "../login/login.component";
import { PlatformModule } from "@angular/cdk/platform";
import { RegisterComponent } from './register.component';

describe("RegisterComponent", () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;

  let authService: any;

  beforeEach(async(() => {
    let authServiceMock = {
      login: jasmine
        .createSpy("register")
        .and.returnValue(
          of({firstName: "nikola", lastName: "nikolic", email: "email@gmail.com", password1: "password", password2:"password"})
        )
    };
    const routes: Routes = [{ path: "register", component: RegisterComponent }];
    TestBed.configureTestingModule({
      declarations: [RegisterComponent],
      imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatCardModule,
        MatToolbarModule,
        MatInputModule,
        MatSnackBarModule,
        HttpClientModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatFormFieldModule,
        FormsModule,
        PlatformModule,
        RouterTestingModule.withRoutes(routes)
      ],
      providers: [
        AllowedRoutes,
        { provide: AuthService, useValue: authServiceMock }
      ]
    }).compileComponents();
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    authService = TestBed.get(AuthService);
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should register", fakeAsync(() => {
    let firstName = fixture.debugElement.query(By.css("[formcontrolname='firstName']")).nativeElement;
    let lastName = fixture.debugElement.query(By.css("[formcontrolname='lastName']")).nativeElement;
    let email = fixture.debugElement.query(By.css("[formcontrolname='email']")).nativeElement;
    let password1 = fixture.debugElement.query(By.css("[formcontrolname='password1']")).nativeElement;
    let password2 = fixture.debugElement.query(By.css("[formcontrolname='password2']")).nativeElement;

    expect(firstName.value).not.toEqual("nikola");
    expect(lastName.value).not.toEqual("nikolic");
    expect(email.value).not.toEqual("email@gmail.com");
    expect(password1.value).not.toEqual("password");
    expect(password2.value).not.toEqual("password");
    tick();

    firstName.value = "nikola";
    lastName.value = "nikolic";
    email.value = "email@gmail.com";
    password1.value = "password";
    password2.value = "password";
    firstName.dispatchEvent(new Event('input'));
    lastName.dispatchEvent(new Event('input'));
    email.dispatchEvent(new Event('input'));
    password1.dispatchEvent(new Event('input'));
    password2.dispatchEvent(new Event('input'));
    
    expect(firstName.value).toEqual("nikola");
    expect(lastName.value).toEqual("nikolic");
    expect(email.value).toEqual("email@gmail.com");
    expect(password1.value).toEqual("password");
    expect(password2.value).toEqual("password");
  }));

  it('should display first name required', fakeAsync(() => {
    let firstName = fixture.debugElement.query(By.css("[formcontrolname='firstName']"));
    firstName.nativeElement.dispatchEvent(new Event('blur'));
    tick();
    fixture.detectChanges();

    expect(component.registerForm.invalid).toBe(true);

    let error = fixture.debugElement.query(By.css("mat-error"));
    expect(error.nativeElement.innerText).toEqual('First name is required');
  }));

  it('should display last name required', fakeAsync(() => {
    let lastName = fixture.debugElement.query(By.css("[formcontrolname='lastName']"));
    lastName.nativeElement.dispatchEvent(new Event('blur'));
    tick();
    fixture.detectChanges();

    expect(component.registerForm.invalid).toBe(true);

    let error = fixture.debugElement.query(By.css("mat-error"));
    expect(error.nativeElement.innerText).toEqual('Last name is required');
  }));

  it('should display email required', fakeAsync(() => {
    let email = fixture.debugElement.query(By.css("[formcontrolname='email']"));

    email.nativeElement.dispatchEvent(new Event('blur'));
    tick();
    fixture.detectChanges();

    expect(component.registerForm.invalid).toBe(true);

    let error = fixture.debugElement.query(By.css("mat-error"));
    expect(error.nativeElement.innerText).toEqual('Email is required');
  }));

  it('should display email not valid', fakeAsync(() => {
    let email = fixture.debugElement.query(By.css("[formcontrolname='email']"));
    email.nativeElement.value = "asdasdasd";
    email.nativeElement.dispatchEvent(new Event('input'));
    email.nativeElement.dispatchEvent(new Event('blur'));
    tick();
    fixture.detectChanges();

    expect(component.registerForm.invalid).toBe(true);

    let error = fixture.debugElement.query(By.css("mat-error"));
    expect(error.nativeElement.innerText).toEqual('Email is not valid');
  }));

  it('should display password required', fakeAsync(() => {
    let password1 = fixture.debugElement.query(By.css("[formcontrolname='password1']"));

    password1.nativeElement.dispatchEvent(new Event('blur'));
    tick();
    fixture.detectChanges();

    expect(component.registerForm.invalid).toBe(true);

    let error = fixture.debugElement.query(By.css("mat-error"));
    expect(error.nativeElement.innerText).toEqual('Password is required');
  }));

  it('should display password must be at least 6 characters long', fakeAsync(() => {
    let password1 = fixture.debugElement.query(By.css("[formcontrolname='password1']")).nativeElement;
    password1.value = "pass";
    password1.dispatchEvent(new Event('input'));
    password1.dispatchEvent(new Event('blur'));
    tick();
    fixture.detectChanges();

    expect(component.registerForm.invalid).toBe(true);

    let error = fixture.debugElement.query(By.css("mat-error"));
    expect(error.nativeElement.innerText).toEqual('Password must be at least 6 characters long');
  }));

  it('should display passwords do not match', fakeAsync(() => {
    let password1 = fixture.debugElement.query(By.css("[formcontrolname='password1']")).nativeElement;
    password1.value = "password1";
    let password2 = fixture.debugElement.query(By.css("[formcontrolname='password2']")).nativeElement;
    password2.value = "password2";
    password1.dispatchEvent(new Event('input'));
    password2.dispatchEvent(new Event('input'));
    password1.dispatchEvent(new Event('blur'));
    tick();
    fixture.detectChanges();

    expect(component.registerForm.invalid).toBe(true);

    let error = fixture.debugElement.query(By.css("mat-error"));
    expect(error.nativeElement.innerText).toEqual('Passwords do not match');
  }));
})
