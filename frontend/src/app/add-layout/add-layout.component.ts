import { Component, OnInit, ViewChild } from "@angular/core";
import { FormArray, FormGroup, FormControl, FormBuilder, Validators, Form } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Layout } from '../_models/layout';
import { SeatingLayout } from '../_models/seating-layout';
import { StandingArea } from '../_models/standing-area';
import { SeatingArea } from '../_models/seating-area';
import { LayoutService } from '../_services/layout.service';

@Component({
  selector: "app-add-layout",
  templateUrl: "./add-layout.component.html",
  styleUrls: ["./add-layout.component.scss"]
})
export class AddLayoutComponent implements OnInit {
  locationId: number;

  middleSection: string;
  eastSection: string;
  westSection: string;
  northSection: string;
  southSection: string;

  seatsLayoutM: SeatingLayout;
  seatsLayoutE: SeatingLayout;
  seatsLayoutW: SeatingLayout;
  seatsLayoutN: SeatingLayout;
  seatsLayoutS: SeatingLayout;

  layoutPositions = [
    {value: 'M', viewValue: 'Middle'},
    {value: 'E', viewValue: 'East'},
    {value: 'W', viewValue: 'West'},
    {value: 'N', viewValue: 'North'},
    {value: 'S', viewValue: 'South'}
  ];

  selectedLayoutPosition: string;

  layoutFG: FormGroup;
  seatingAreaFG: FormGroup;
  standingAreaFG: FormGroup;

  standingAreas: StandingArea[];
  seatingAreas: SeatingArea[];
  finalLayout: Layout;

  constructor(private activatedRoute: ActivatedRoute,
              private formBuilder: FormBuilder,
              private layoutService: LayoutService,
              private router: Router) {
  }

  ngOnInit() {
    this.middleSection = "stage";
    this.northSection = "empty";
    this.southSection = "empty";
    this.eastSection = "empty";
    this.westSection = "empty";

    this.seatsLayoutM = new SeatingLayout();
    this.seatsLayoutE = new SeatingLayout();
    this.seatsLayoutW = new SeatingLayout();
    this.seatsLayoutN = new SeatingLayout();
    this.seatsLayoutS = new SeatingLayout();

    this.activatedRoute.params.subscribe(params => {
      this.locationId = +params["id"];
    });

    this.standingAreas = [];
    this.seatingAreas = [];

    this.layoutFG = this.formBuilder.group({
      layoutName: new FormControl("", [
        Validators.required
      ]),
      stagePosition: new FormControl("", [
        Validators.required
      ])
    });

    this.seatingAreaFG = this.formBuilder.group({
      rowNum: new FormControl("", [
        Validators.required,
        Validators.pattern("[0-9]*"),
        Validators.min(1),
        Validators.max(7)
      ]),
      seatNum: new FormControl("", [
        Validators.required,
        Validators.pattern("[0-9]*"),
        Validators.min(1),
        Validators.max(7)
      ]),
      layoutPosition: new FormControl("", [
        Validators.required
      ])
    });

    this.standingAreaFG = this.formBuilder.group({
      maxNumber: new FormControl("", [
        Validators.required,
        Validators.pattern("[0-9]*"),
        Validators.min(1)
      ]),
      layoutPosition: new FormControl("", [
        Validators.required
      ])
    });
  }

  onNext() : void {
    console.log(this.layoutFG.value)
    let selectedPosition = this.layoutFG.controls['stagePosition'].value;

    this.middleSection = "empty";

    switch (selectedPosition) {
      case "M": {
        this.middleSection = "stage";
        break;
      }
      case "E": {
        this.eastSection = "stage";
        break;
      }
      case "W": {
        this.westSection = "stage";
        break;
      }
      case "N": {
        this.northSection = "stage";
        break;
      }
      case "S": {
        this.southSection = "stage";
        break;
      }
    }

    let index = this.layoutPositions.findIndex(pos => pos.value === selectedPosition);
    this.layoutPositions.splice(index, 1);
  }

  addSeatingArea(): void {
    console.log(this.seatingAreaFG.value)
    let selectedPosition = this.seatingAreaFG.controls['layoutPosition'].value;

    switch (selectedPosition) {
      case "M": {
        this.middleSection = "seatingArea";
        this.seatsLayoutM.totalRows = this.seatingAreaFG.controls['rowNum'].value;
        this.seatsLayoutM.seatsPerRow = this.seatingAreaFG.controls['seatNum'].value;
        this.seatsLayoutM.seatNaming = 'rowType';
        this.seatsLayoutM.booked = [];
        break;
      }
      case "E": {
        this.eastSection = "seatingArea";
        this.seatsLayoutE.totalRows = this.seatingAreaFG.controls['rowNum'].value;
        this.seatsLayoutE.seatsPerRow = this.seatingAreaFG.controls['seatNum'].value;
        this.seatsLayoutE.seatNaming = 'rowType';
        this.seatsLayoutE.booked = [];
        break;
      }
      case "W": {
        this.westSection = "seatingArea";
        this.seatsLayoutW.totalRows = this.seatingAreaFG.controls['rowNum'].value;
        this.seatsLayoutW.seatsPerRow = this.seatingAreaFG.controls['seatNum'].value;
        this.seatsLayoutW.seatNaming = 'rowType';
        this.seatsLayoutW.booked = [];
        break;
      }
      case "N": {
        this.northSection = "seatingArea";
        this.seatsLayoutN.totalRows = this.seatingAreaFG.controls['rowNum'].value;
        this.seatsLayoutN.seatsPerRow = this.seatingAreaFG.controls['seatNum'].value;
        this.seatsLayoutN.seatNaming = 'rowType';
        this.seatsLayoutN.booked = [];
        break;
      }
      case "S": {
        this.southSection = "seatingArea";
        this.seatsLayoutS.totalRows = this.seatingAreaFG.controls['rowNum'].value;
        this.seatsLayoutS.seatsPerRow = this.seatingAreaFG.controls['seatNum'].value;
        this.seatsLayoutS.seatNaming = 'rowType';
        this.seatsLayoutS.booked = [];
        break;
      }
    }

    let index = this.layoutPositions.findIndex(pos => pos.value === selectedPosition);
    this.layoutPositions.splice(index, 1);

    let seatingArea = new SeatingArea();
    seatingArea.rowNum = this.seatingAreaFG.controls['rowNum'].value;
    seatingArea.seatNum = this.seatingAreaFG.controls['seatNum'].value;
    seatingArea.layoutPosition = this.seatingAreaFG.controls['layoutPosition'].value;
    this.seatingAreas.push(seatingArea)

    this.seatingAreaFG.reset();
  }

  addStandingArea(): void {
    console.log(this.standingAreaFG.value)
    let selectedPosition = this.standingAreaFG.controls['layoutPosition'].value;

    switch (selectedPosition) {
      case "M": {
        this.middleSection = "standingArea";
        break;
      }
      case "E": {
        this.eastSection = "standingArea";
        break;
      }
      case "W": {
        this.westSection = "standingArea";
        break;
      }
      case "N": {
        this.northSection = "standingArea";
        break;
      }
      case "S": {
        this.southSection = "standingArea";
        break;
      }
    }

    let index = this.layoutPositions.findIndex(pos => pos.value === selectedPosition);
    this.layoutPositions.splice(index, 1);

    let standingArea = new StandingArea();
    standingArea.maxNumber = this.standingAreaFG.controls['maxNumber'].value;
    standingArea.layoutPosition = this.standingAreaFG.controls['layoutPosition'].value;
    this.standingAreas.push(standingArea)

    this.standingAreaFG.reset();
  }

  submit(): void {
    this.finalLayout = new Layout();
    this.finalLayout.locationId = this.locationId;
    this.finalLayout.layoutName = this.layoutFG.controls['layoutName'].value;
    this.finalLayout.stagePosition = this.layoutFG.controls['stagePosition'].value;
    this.finalLayout.sittingAreas = this.seatingAreas;
    this.finalLayout.standingAreas = this.standingAreas;

    console.log(this.finalLayout);

    this.layoutService.addLayout(this.finalLayout).subscribe(
      response => {
        console.log(response)
        this.router.navigate(["/location/" + this.locationId + "/layout"])
      },
      errorResponse => console.log(errorResponse)
    );
  }

  reset(): void {
    this.layoutFG.reset();
    this.seatingAreaFG.reset();
    this.standingAreaFG.reset();

    this.layoutPositions = [
      {value: 'M', viewValue: 'Middle'},
      {value: 'E', viewValue: 'East'},
      {value: 'W', viewValue: 'West'},
      {value: 'N', viewValue: 'North'},
      {value: 'S', viewValue: 'South'}
    ];

    this.middleSection = "stage";
    this.northSection = "";
    this.southSection = "";
    this.eastSection = "";
    this.westSection = "";

    this.seatsLayoutM = new SeatingLayout();
    this.seatsLayoutE = new SeatingLayout();
    this.seatsLayoutW = new SeatingLayout();
    this.seatsLayoutN = new SeatingLayout();
    this.seatsLayoutS = new SeatingLayout();

    this.seatingAreas = [];
    this.standingAreas = [];
  }
}
