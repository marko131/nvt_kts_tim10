import {
    async,
    ComponentFixture,
    TestBed,
    fakeAsync,
    tick
  } from "@angular/core/testing";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
  MatFormFieldModule,
  MatCardModule,
  MatInputModule,
  MatButtonModule,
  MatSnackBarModule,
  MatToolbar,
  MatToolbarModule
} from "@angular/material";
import { Routes } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";
import { Location } from "@angular/common";
import { RouterTestingModule } from "@angular/router/testing";
import { AllowedRoutes } from "../_services/allowedRoutes.service";
import { AuthService } from "../_services/auth.service";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { UserProfile } from "../_models/user";
import { of } from "rxjs";
import { By } from "@angular/platform-browser";
import { LoginComponent } from "../login/login.component";
import { PlatformModule } from "@angular/cdk/platform";

describe("LoginComponent", () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  let authService: any;

  beforeEach(async(() => {
    let authServiceMock = {
      login: jasmine
        .createSpy("login")
        .and.returnValue(
          of({email: "email@gmail.com", password: "password"})
        )
    };
    const routes: Routes = [{ path: "login", component: LoginComponent }];
    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatCardModule,
        MatToolbarModule,
        MatInputModule,
        MatSnackBarModule,
        HttpClientModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatFormFieldModule,
        FormsModule,
        PlatformModule,
        RouterTestingModule.withRoutes(routes)
      ],
      providers: [
        AllowedRoutes,
        { provide: AuthService, useValue: authServiceMock }
      ]
    }).compileComponents();
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    authService = TestBed.get(AuthService);
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should login", fakeAsync(() => {
    let email = fixture.debugElement.query(By.css("[formcontrolname='email']"))
      .nativeElement;
    let password = fixture.debugElement.query(
      By.css("[formcontrolname='password']")
    ).nativeElement;

    expect(email.value).not.toEqual("email@gmail.com");
    expect(password.value).not.toEqual("password");
    tick();
    email.value = "email@gmail.com";
    password.value = "password";
    email.dispatchEvent(new Event('input'));
    password.dispatchEvent(new Event('input'));
    
    expect(email.value).toEqual("email@gmail.com");
    expect(password.value).toEqual("password");
  }));

  it('should display required email', fakeAsync(() => {
    let email = fixture.debugElement.query(By.css("[formcontrolname='email']"));

    email.nativeElement.dispatchEvent(new Event('blur'));
    tick();
    fixture.detectChanges();

    expect(component.form.invalid).toBe(true);

    let error = fixture.debugElement.query(By.css("mat-error"));
    expect(error.nativeElement.innerText).toEqual('Email is required');
  }));

  it('should display is not valid email', fakeAsync(() => {
    let email = fixture.debugElement.query(By.css("[formcontrolname='email']")).nativeElement;
    email.value = "asd";
    email.dispatchEvent(new Event('input'));
    tick();
    fixture.detectChanges();

    email.dispatchEvent(new Event('blur'));
    tick();
    fixture.detectChanges();

    expect(component.form.invalid).toBe(true);

    let error = fixture.debugElement.query(By.css("mat-error"));
    expect(error.nativeElement.innerText).toEqual('Email is not valid');
  }));

  it('should display password required', fakeAsync(() => {
    let password = fixture.debugElement.query(By.css("[formcontrolname='password']"));

    password.nativeElement.dispatchEvent(new Event('blur'));
    tick();
    fixture.detectChanges();

    expect(component.form.invalid).toBe(true);

    let error = fixture.debugElement.query(By.css("mat-error"));
    expect(error.nativeElement.innerText).toEqual('Password is required');
  }));
})
