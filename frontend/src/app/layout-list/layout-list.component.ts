import { Component, OnInit } from "@angular/core";
import { LocationService } from "../_services/location.service";
import { ActivatedRoute, Router } from "@angular/router";
import { SeatingLayout } from "../_models/seating-layout";
import { RenderLayout } from "../_models/render-layout";
import { MatSnackBar } from "@angular/material";

@Component({
  selector: "app-layout-list",
  templateUrl: "./layout-list.component.html",
  styleUrls: ["./layout-list.component.scss"]
})
export class LayoutListComponent implements OnInit {
  locationId: number;
  layouts: RenderLayout[] = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private locationService: LocationService,
    private _snackBar: MatSnackBar,
    private router: Router
  ) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.locationId = +params["id"];
    });

    this.locationService.getLayoutsForLocation(this.locationId).subscribe(
      response => {
        if (!response.length) {
          let snackBarRef = this._snackBar.open(
            "No available layouts",
            "Add layout",
            {
              duration: 5000
            }
          );
          snackBarRef
            .onAction()
            .subscribe(() =>
              this.router.navigate(["/location", this.locationId, "add-layout"])
            );
        }
        response.forEach(layout => {
          let l = {
            id: layout.id,
            name: layout.layoutName,
            middleSection: "",
            eastSection: "",
            westSection: "",
            northSection: "",
            southSection: "",
            middleSectionId: 0,
            eastSectionId: 0,
            westSectionId: 0,
            northSectionId: 0,
            southSectionId: 0,
            seatsLayoutM: new SeatingLayout(),
            seatsLayoutN: new SeatingLayout(),
            seatsLayoutS: new SeatingLayout(),
            seatsLayoutE: new SeatingLayout(),
            seatsLayoutW: new SeatingLayout()
          };

          switch (layout.stagePosition) {
            case "M": {
              l.middleSection = "STAGE";
              break;
            }
            case "E": {
              l.eastSection = "STAGE";
              break;
            }
            case "W": {
              l.westSection = "STAGE";
              break;
            }
            case "N": {
              l.northSection = "STAGE";
              break;
            }
            case "S": {
              l.southSection = "STAGE";
              break;
            }
          }

          layout.standingAreas.forEach(standingArea => {
            switch (standingArea.layoutPosition) {
              case "M": {
                l.middleSection = String(standingArea.maxNumber);
                l.middleSectionId = standingArea.id;
                break;
              }
              case "E": {
                l.eastSection = String(standingArea.maxNumber);
                l.eastSectionId = standingArea.id;
                break;
              }
              case "W": {
                l.westSection = String(standingArea.maxNumber);
                l.westSectionId = standingArea.id;
                break;
              }
              case "N": {
                l.northSection =  String(standingArea.maxNumber);
                l.northSectionId = standingArea.id;
                break;
              }
              case "S": {
                l.southSection = String(standingArea.maxNumber);
                l.southSectionId = standingArea.id;
                break;
              }
            }
          });

          layout.sittingAreas.forEach(sittingArea => {
            switch (sittingArea.layoutPosition) {
              case "M": {
                l.middleSection = "seatingArea";
                l.middleSectionId = sittingArea.id;
                l.seatsLayoutM.totalRows = sittingArea.numRows;
                l.seatsLayoutM.seatsPerRow = sittingArea.numSeats;
                l.seatsLayoutM.seatNaming = "rowType";
                l.seatsLayoutM.booked = [];
                break;
              }
              case "E": {
                l.eastSection = "seatingArea";
                l.eastSectionId = sittingArea.id;
                l.seatsLayoutE.totalRows = sittingArea.numRows;
                l.seatsLayoutE.seatsPerRow = sittingArea.numSeats;
                l.seatsLayoutE.seatNaming = "rowType";
                l.seatsLayoutE.booked = [];
                break;
              }
              case "W": {
                l.westSection = "seatingArea";
                l.westSectionId = sittingArea.id;
                l.seatsLayoutW.totalRows = sittingArea.numRows;
                l.seatsLayoutW.seatsPerRow = sittingArea.numSeats;
                l.seatsLayoutW.seatNaming = "rowType";
                l.seatsLayoutW.booked = [];
                break;
              }
              case "N": {
                l.northSection = "seatingArea";
                l.northSectionId = sittingArea.id;
                l.seatsLayoutN.totalRows = sittingArea.numRows;
                l.seatsLayoutN.seatsPerRow = sittingArea.numSeats;
                l.seatsLayoutN.seatNaming = "rowType";
                l.seatsLayoutN.booked = [];
                break;
              }
              case "S": {
                l.southSection = "seatingArea";
                l.southSectionId = sittingArea.id;
                l.seatsLayoutS.totalRows = sittingArea.numRows;
                l.seatsLayoutS.seatsPerRow = sittingArea.numSeats;
                l.seatsLayoutS.seatNaming = "rowType";
                l.seatsLayoutS.booked = [];
                break;
              }
            }
          });

          this.layouts.push(l);
        });
      },
      errorResponse => console.log(errorResponse)
    );
  }
}
