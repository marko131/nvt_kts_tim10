import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { DatePipe } from "@angular/common";
import { EventService } from "../_services/event.service";
import { Event } from "../_models/event";
import * as jwt_decode from "jwt-decode";

@Component({
  selector: "app-event",
  templateUrl: "./event.component.html",
  styleUrls: ["./event.component.scss"]
})
export class EventComponent implements OnInit {
  breakpoint: number;
  searchEventsForm: FormGroup;
  categoryList: Array<string>;
  eventList: Array<Event>;
  isAdmin;

  constructor(private http: HttpClient, private eventService: EventService) {
    this.searchEventsForm = new FormGroup({
      name: new FormControl(),
      location: new FormControl(),
      categories: new FormControl(),
      dateBegin: new FormControl(),
      dateEnd: new FormControl()
    });
    this.eventList = new Array<Event>();
    this.categoryList = [
      "CONCERT",
      "FESTIVAL",
      "THEATER",
      "SPORT",
      "ENTERTAINMENT",
      "OTHER"
    ];
    let token = localStorage.getItem("token");
    if (!token) this.isAdmin = false;
    else {
      this.isAdmin = false;
      let decodedToken = jwt_decode(token);
      decodedToken.roles.forEach(role => {
        if (role.authority === "ROLE_ADMIN") {
          this.isAdmin = true;
        }
      });
    }
  }

  search() {
    let name = this.searchEventsForm.get("name").value;
    let location = this.searchEventsForm.get("location").value;
    let categories =
      this.searchEventsForm.get("categories").value === null
        ? []
        : this.searchEventsForm.get("categories").value;
    let dateBegin = this.searchEventsForm.get("dateBegin").value;
    let dateEnd = this.searchEventsForm.get("dateEnd").value;
    //let page = this.searchEventsForm.get("page").value;

    this.eventService
      .searchEvents(name, location, categories, dateBegin, dateEnd, 0)
      .subscribe(
        response => {
          this.eventList = response;
        },
        errorResponse => console.log(errorResponse)
      );
  }

  ngOnInit() {
    if (window.innerWidth >= 1900) this.breakpoint = 3;
    else if (window.innerWidth >= 1300) this.breakpoint = 2;
    else this.breakpoint = 1;
  }
  onResize(event) {
    if (window.innerWidth >= 1900) this.breakpoint = 3;
    else if (window.innerWidth >= 1300) this.breakpoint = 2;
    else this.breakpoint = 1;
  }
}
