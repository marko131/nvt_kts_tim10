import {
  async,
  ComponentFixture,
  TestBed,
  fakeAsync,
  tick
} from "@angular/core/testing";

import { ProfileComponent } from "./profile.component";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
  MatFormFieldModule,
  MatCardModule,
  MatInputModule,
  MatButtonModule,
  MatSnackBarModule,
  MatTabsModule,
  MatToolbar,
  MatToolbarModule,
  MatCheckboxModule,
  MatIconModule,
  MatPaginatorModule,
  MatTableModule,
  MatDialogModule
} from "@angular/material";
import { Routes } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";
import { Location } from "@angular/common";
import { RouterTestingModule } from "@angular/router/testing";
import { AllowedRoutes } from "../_services/allowedRoutes.service";
import { AuthService } from "../_services/auth.service";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { UserProfile } from "../_models/user";
import { of } from "rxjs";
import { By } from "@angular/platform-browser";
import { LoginComponent } from "../login/login.component";
import { PlatformModule } from "@angular/cdk/platform";
import { ReservationsComponent } from "../reservations/reservations.component";
import { TicketsComponent } from "../tickets/tickets.component";
import { SeatPipe } from "../_helpers/seatNum.pipe";
import { TicketService } from "../_services/ticket.service";
import { Reservation } from "../_models/reservation.model";

describe("ProfileComponent", () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;

  let authService: any;
  let location: Location;

  beforeEach(async(() => {
    let authServiceMock = {
      profile: jasmine
        .createSpy("profile")
        .and.returnValue(
          of(new UserProfile("user@gmail.com", "firstName", "lastName"))
        ),
      updateProfile: jasmine
        .createSpy("updateProfile")
        .and.returnValue(
          of(new UserProfile("user@gmail.com", "firstName", "lastName"))
        ),
      logout: jasmine.createSpy("logout"),
      isAdmin: jasmine.createSpy("isAdmin")
    };
    let ticketServiceMock = {
      getReservations: jasmine
        .createSpy("getReservations")
        .and.returnValue(of(new Array<Reservation>())),
      getTickets: jasmine
        .createSpy("getTickets")
        .and.returnValue(of(new Array<Reservation>()))
    };
    const routes: Routes = [{ path: "login", component: LoginComponent }];
    TestBed.configureTestingModule({
      declarations: [
        ProfileComponent,
        LoginComponent,
        ReservationsComponent,
        TicketsComponent,
        SeatPipe
      ],
      imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatCardModule,
        MatInputModule,
        MatSnackBarModule,
        HttpClientModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatFormFieldModule,
        FormsModule,
        PlatformModule,
        MatTabsModule,
        MatToolbarModule,
        MatCheckboxModule,
        MatIconModule,
        MatPaginatorModule,
        MatTableModule,
        MatDialogModule,
        RouterTestingModule.withRoutes(routes)
      ],
      providers: [
        AllowedRoutes,
        SeatPipe,
        TicketService,
        { provide: AuthService, useValue: authServiceMock },
        { provide: TicketService, useValue: ticketServiceMock }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    authService = TestBed.get(AuthService);
    location = TestBed.get(Location);
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should fetch user profile", fakeAsync(() => {
    expect(authService.profile).toHaveBeenCalled();
    tick();

    //Return value from service method
    expect(component.userProfile.value).toEqual({
      email: "user@gmail.com",
      firstName: "firstName",
      lastName: "lastName"
    });
    fixture.detectChanges();
    tick();

    //HTML input value
    let email = fixture.debugElement.query(By.css("[formcontrolname='email']"))
      .nativeElement;
    let firstName = fixture.debugElement.query(
      By.css("[formcontrolname='firstName']")
    ).nativeElement;
    let lastName = fixture.debugElement.query(
      By.css("[formcontrolname='lastName']")
    ).nativeElement;
    expect(email.value).toEqual("user@gmail.com");
    expect(firstName.value).toEqual("firstName");
    expect(lastName.value).toEqual("lastName");
  }));

  it("should bind data from edit fields to user profile", fakeAsync(() => {
    //Get values from HTML inputs
    let email = fixture.debugElement.query(By.css("[formcontrolname='email']"))
      .nativeElement;
    let firstName = fixture.debugElement.query(
      By.css("[formcontrolname='firstName']")
    ).nativeElement;
    let lastName = fixture.debugElement.query(
      By.css("[formcontrolname='lastName']")
    ).nativeElement;
    email.value = "new_email@gmail.com";
    firstName.value = "new_first_name";
    lastName.value = "new_last_name";

    //Change input events
    email.dispatchEvent(newEvent("input"));
    firstName.dispatchEvent(newEvent("input"));
    lastName.dispatchEvent(newEvent("input"));

    //Expect HTML input values to change
    expect(component.userProfile.value).toEqual({
      email: "new_email@gmail.com",
      firstName: "new_first_name",
      lastName: "new_last_name"
    });
  }));

  it("should update profile", fakeAsync(() => {
    component.updateProfile();
    expect(authService.updateProfile).toHaveBeenCalled();
    tick();
    fixture.detectChanges();
    expect(location.path()).toBe("/login");
  }));

  it("should display error for required password", fakeAsync(() => {
    fixture.debugElement
      .query(By.css("#mat-tab-label-0-1"))
      .nativeElement.click();

    let password1 = fixture.debugElement.query(By.css("#mat-input-9"));
    password1.nativeElement.dispatchEvent(new Event("blur"));
    tick();
    fixture.detectChanges();
    let error = fixture.debugElement.query(By.css("#mat-error-8"));
    expect(error.nativeElement.innerText).toEqual("Password is required");
  }));

  it("should display error for short password", fakeAsync(() => {
    fixture.debugElement
      .query(By.css("#mat-tab-label-0-1"))
      .nativeElement.click();

    let password1 = fixture.debugElement.query(By.css("#mat-input-9"));
    let password2 = fixture.debugElement.query(By.css("#mat-input-10"));
    password1.nativeElement.value = "11";
    password2.nativeElement.value = "11";
    password1.nativeElement.dispatchEvent(newEvent("input"));
    password2.nativeElement.dispatchEvent(newEvent("input"));
    expect(component.userPassword.invalid).toBe(true);
    tick();
    fixture.detectChanges();
    password1.nativeElement.dispatchEvent(new Event("blur"));
    tick();
    fixture.detectChanges();
    let error = fixture.debugElement.query(By.css("#mat-error-8"));
    expect(error.nativeElement.innerText).toEqual(
      "Password must be at least 6 characters long"
    );
  }));

  it("should display error for long password", fakeAsync(() => {
    fixture.debugElement
      .query(By.css("#mat-tab-label-0-1"))
      .nativeElement.click();
    let password1 = fixture.debugElement.query(By.css("#mat-input-9"));
    console.log(password1);
    password1.nativeElement.value = "password".repeat(50);
    password1.nativeElement.dispatchEvent(newEvent("input"));
    tick();
    fixture.detectChanges();
    password1.nativeElement.dispatchEvent(new Event("blur"));
    tick();
    fixture.detectChanges();
    let error = fixture.debugElement.query(By.css("#mat-error-8"));
    expect(error.nativeElement.innerText).toEqual(
      "Password can be maximum 255 characters long"
    );
  }));

  it("should display error for different passwords", fakeAsync(() => {
    let password1 = fixture.debugElement.query(By.css("#mat-input-9"))
      .nativeElement;
    let password2 = fixture.debugElement.query(By.css("#mat-input-10"))
      .nativeElement;
    password1.value = "111111";
    password2.value = "1111111";
    password1.dispatchEvent(newEvent("input"));
    password2.dispatchEvent(newEvent("input"));
    expect(component.userPassword.invalid).toBe(true);
    tick();
    fixture.detectChanges();
    let error = fixture.debugElement.query(By.css("#mat-error-9"));
    expect(error.nativeElement.innerText).toEqual("Passwords do not match");
  }));

  it("should navigate to login page after logout", fakeAsync(() => {
    component.logout();
    tick();
    fixture.detectChanges();
    expect(location.path()).toBe("/login");
  }));

  function newEvent(eventName: string, bubbles = false, cancelable = false) {
    let evt = document.createEvent("CustomEvent"); // MUST be 'CustomEvent'
    evt.initCustomEvent(eventName, bubbles, cancelable, null);
    return evt;
  }
});
