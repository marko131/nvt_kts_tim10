import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { AllowedRoutes } from "../_services/allowedRoutes.service";
import { AuthService } from "../_services/auth.service";
import { UserProfile } from "../_models/user";
import {
  FormControl,
  Validators,
  FormGroup,
  FormBuilder
} from "@angular/forms";
import { MyErrorStateMatcher } from "../_helpers/myErrorStateMatcher";
import {
  MatSnackBar,
  MatPaginator,
  MatTableDataSource
} from "@angular/material";
import { Reservation } from "../_models/reservation.model";
import { TicketService } from "../_services/ticket.service";

@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.scss"]
})
export class ProfileComponent implements OnInit {
  userProfile: FormGroup;
  userPassword: FormGroup;

  matcher = new MyErrorStateMatcher();

  isAdmin: boolean;

  constructor(
    private router: Router,
    private allowedRoutes: AllowedRoutes,
    private authService: AuthService,
    private snackBar: MatSnackBar
  ) {
    this.isAdmin = authService.isAdmin();

    this.userProfile = new FormGroup({
      email: new FormControl("", [Validators.required, Validators.email]),
      firstName: new FormControl("", Validators.required),
      lastName: new FormControl("", Validators.required)
    });
    this.userPassword = new FormGroup(
      {
        password1: new FormControl("", [
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(255)
        ]),
        password2: new FormControl("", [
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(255)
        ])
      },
      { validators: this.checkPasswords }
    );
  }

  ngOnInit() {
    this.authService.profile().subscribe(
      response => {
        this.userProfile.get("email").setValue(response.email);
        this.userProfile.get("firstName").setValue(response.firstName);
        this.userProfile.get("lastName").setValue(response.lastName);
      },
      errorResponse => {
        console.log(errorResponse);
        localStorage.removeItem("token");
        this.allowedRoutes.updateRoutes();
      }
    );
  }

  updateProfile() {
    if (this.userProfile.valid) {
      this.authService.updateProfile(this.userProfile.value).subscribe(
        response => {
          this.snackBar.open("Profile updated successfully", "", {
            duration: 2000
          });
          this.logout();
        },
        errorResponse => {
          this.snackBar.open(errorResponse, "", {
            duration: 2000
          });
        }
      );
    }
  }

  changePassword() {
    if (this.userPassword.valid) {
      this.authService.changePassword(this.userPassword.value).subscribe(
        response => {
          this.snackBar.open("Profile updated successfully", "", {
            duration: 2000
          });
          this.logout();
        },

        errorResponse => {
          this.snackBar.open(errorResponse, "", {
            duration: 2000
          });
        }
      );
    }
  }

  logout() {
    localStorage.removeItem("token");
    this.router.navigate(["/login"]);
    this.allowedRoutes.updateRoutes();
  }

  checkPasswords(group: FormGroup) {
    // here we have the 'passwords' group
    let pass = group.controls.password1.value;
    let confirmPass = group.controls.password2.value;

    return pass === confirmPass ? null : { notSame: true };
  }
}
