import { Component, OnInit } from "@angular/core";
import { LocationService } from "../_services/location.service";
import { Location } from "../_models/location";
@Component({
  selector: "app-location",
  templateUrl: "./location.component.html",
  styleUrls: ["./location.component.scss"]
})
export class LocationComponent implements OnInit {
  locations: Array<Location>;
  constructor(private locationService: LocationService) {}

  ngOnInit() {
    this.locationService.getAll().subscribe(
      response => (this.locations = response),
      errorResponse => console.log(errorResponse)
    );
  }
}
