import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { RenderLayout } from "../_models/render-layout";
import { Layout } from "../_models/layout";
import { element } from "protractor";

@Injectable()
export class PriceListTicketService {
  constructor(private http: HttpClient) {}

  reserveTicketStandingArea(priceListId: number, numberOfTickets: number) {
    return this.http.post(
      `${environment.apiUrl}/api/standingAreaReserve`,
      {
        priceListId: priceListId,
        numberOfTickets: numberOfTickets
      },
      { responseType: "text" }
    );
  }
  getLayoutByPriceListId(priceListId: number) {
    return this.http.get<any>(
      `${environment.apiUrl}/api/pricelist/${priceListId}/sittingAreaLayout`
    );
  }
  reserveTicketSittingArea(priceListId: number, selectedSeats: string[]) {
    const seats = selectedSeats.map(element =>
      this.convertSeatCharToNum(element)
    );
    return this.http.post(
      `${environment.apiUrl}/api/sittingAreaReserve`,
      {
        priceListId: priceListId,
        sittingAreaTicketDTOList: seats
      },
      { responseType: "text" }
    );
  }
  buyTicketSittingArea(
    priceListId: number,
    selectedSeats: string[],
    orderID: string
  ) {
    const seats = selectedSeats.map(element =>
      this.convertSeatCharToNum(element)
    );
    return this.http.post(
      `${environment.apiUrl}/api/sittingAreaBuy`,
      {
        orderID: orderID,
        reserveSittingAreaDTO: {
          priceListId: priceListId,
          sittingAreaTicketDTOList: seats
        }
      },
      { responseType: "text" }
    );
  }
  convertSeatCharToNum(seat: string): any {
    let rowNum = +seat.charAt(0);
    let col = seat.charCodeAt(1) - 64;
    return { rowNum: rowNum, seatNum: col };
  }
}
