import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { UserProfile } from "../_models/user";
import { ChangePassword } from "../_models/changePassword";
import * as jwt_decode from "jwt-decode";
@Injectable()
export class AuthService {
  constructor(private http: HttpClient) {}

  login(email: string, password: string) {
    return this.http.post(
      `${environment.apiUrl}/api/login`,
      { email: email, password: password },
      { responseType: "text" }
    );
  }

  register(
    firstName: string,
    lastName: string,
    email: string,
    password1: string,
    password2: string
  ) {
    return this.http.post(
      `${environment.apiUrl}/api/register`,
      {
        name: firstName,
        lastName: lastName,
        email: email,
        password1: password1,
        password2: password2
      },
      { responseType: "text" }
    );
  }

  profile() {
    return this.http.get<UserProfile>(`${environment.apiUrl}/api/profile`);
  }

  updateProfile(user: UserProfile) {
    console.log(user);
    return this.http.put<UserProfile>(
      `${environment.apiUrl}/api/updateProfile`,
      { email: user.email, firstName: user.firstName, lastName: user.lastName }
    );
  }

  changePassword(changePassword: ChangePassword) {
    console.log(changePassword);
    return this.http.put<UserProfile>(`${environment.apiUrl}/api/reset`, {
      password1: changePassword.password1,
      password2: changePassword.password2
    });
  }

  isAdmin() {
    let isAdmin = false;
    let token = localStorage.getItem("token");
    let decodedToken = jwt_decode(token);
    let current_time = new Date().getTime() / 1000;
    if (current_time > decodedToken.exp) {
      return isAdmin;
    }
    decodedToken.roles.forEach(role => {
      if (role.authority === "ROLE_ADMIN") {
        isAdmin = true;
      }
    });
    return isAdmin;
  }
}
