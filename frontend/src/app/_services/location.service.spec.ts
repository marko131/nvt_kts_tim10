import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { LocationService } from './location.service';
import { environment } from 'src/environments/environment';

describe('LocationsService', () => {
    let service: LocationService;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [LocationService]
        })

        service = TestBed.get(LocationService);
        httpMock = TestBed.get(HttpTestingController);
    });

    afterEach(() => {
        httpMock.verify();
    });

    it('should be created', () => {
        const service: LocationService = TestBed.get(LocationService);
        expect(service).toBeTruthy();
    });

    it ('getAll() should return an array of all locations', () => {
        const dummyLocations =  [
            {
                id: 1,
                name: 'SPENS',
                address: 'Sutjeska 2, Novi Sad 21000',
                latitude: 45.246937,
                longitude: 19.845355,
                picture: 'picture/path/spens.jpg'
            },
            {
                id: 2,
                name: 'Srpsko narodno pozoriste',
                address: 'Pozorišni trg 1, Novi Sad 21000',
                latitude: 45.255009, 
                longitude: 19.843016,
                picture: 'picture/path/snp.jpg'
            }
        ];

        service.getAll().subscribe(locations => {
            expect(locations.length).toBe(2);
            expect(locations).toEqual(dummyLocations);
        })

        const req = httpMock.expectOne(`${environment.apiUrl}/api/location`);
        expect(req.request.method).toBe("GET");
        req.flush(dummyLocations);
    });

    it ('addLocation() should return added location', () => {
        const testLocation = {
            name: 'Srpsko narodno pozoriste',
            address: 'Pozorišni trg 1, Novi Sad 21000',
            latitude: 45.255009, 
            longitude: 19.843016,
            picture: 'picture/path/snp.jpg'
        };

        service.addLocation(testLocation)
            .subscribe(location => {
                expect(location.name).toBe('Srpsko narodno pozoriste');
                expect(location.address).toBe('Pozorišni trg 1, Novi Sad 21000');
                expect(location.latitude).toBe(45.255009);
                expect(location.longitude).toBe(19.843016);
                expect(location.picture).toBe('picture/path/snp.jpg');
            });

        const req = httpMock.expectOne(`${environment.apiUrl}/api/location`);
        expect(req.request.method).toEqual('POST');

        req.flush(testLocation);
    });
});
