import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { Layout } from "../_models/layout";

@Injectable()
export class LayoutService {
  constructor(private http: HttpClient) {}

  getAll() {
    return this.http.get<Array<any>>(`${environment.apiUrl}/api/layout`);
  }

  addLayout(layout: Layout) {
    return this.http.post<Layout>(`${environment.apiUrl}/api/layout`, {
      locationId: layout.locationId,
      stagePosition: layout.stagePosition,
      layoutName: layout.layoutName,
      sittingAreas: layout.sittingAreas,
      standingAreas: layout.standingAreas
    });
  }

  getSittingAreas(layoutId: number) {
    return this.http.get<Array<any>>(
      `${environment.apiUrl}/api/layout/${layoutId}/sittingAreas`
    );
  }
  getStangingAreas(layoutId: number) {
    return this.http.get<Array<any>>(
      `${environment.apiUrl}/api/layout/${layoutId}/standingAreas`
    );
  }
}
