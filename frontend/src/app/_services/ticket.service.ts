import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Reservation } from "../_models/reservation.model";
import { environment } from "src/environments/environment";

@Injectable()
export class TicketService {
  constructor(private http: HttpClient) {}

  getReservations() {
    return this.http.get<Array<Reservation>>(
      `${environment.apiUrl}/api/ticket/reservations`
    );
  }

  getTickets() {
    return this.http.get<Array<Reservation>>(
      `${environment.apiUrl}/api/ticket/user-tickets`
    );
  }

  cancelReservation(ticketId: number) {
    return this.http.put(
      `${environment.apiUrl}/api/ticket/${ticketId}/cancel`,
      {},
      { responseType: "text" }
    );
  }
}
