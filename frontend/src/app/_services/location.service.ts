import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { Location } from "../_models/location";
@Injectable()
export class LocationService {
  constructor(private http: HttpClient) {}

  addLocation(location: Location) {
    return this.http.post<Location>(`${environment.apiUrl}/api/location`, {
      name: location.name,
      address: location.address,
      latitude: location.latitude,
      longitude: location.longitude,
      picture: location.picture
    });
  }

  getAll() {
    return this.http.get<Array<Location>>(`${environment.apiUrl}/api/location`);
  }

  getById(id: number) {
    return this.http.get<Location>(`${environment.apiUrl}/api/location/${id}`);
  }

  update(id: number, location: Location) {
    return this.http.put<Location>(`${environment.apiUrl}/api/location/${id}`, {
      name: location.name,
      address: location.address,
      picture: location.picture
    });
  }
  getLayoutsForLocation(locationId: number) {
    return this.http.get<Array<any>>(
      `${environment.apiUrl}/api/location/${locationId}/layouts`
    );
  }
}
