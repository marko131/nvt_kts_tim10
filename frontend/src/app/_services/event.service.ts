import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { DatePipe } from "@angular/common";
import { Event } from "../_models/event";
import { PriceList } from "../_models/priceList.model";
@Injectable()
export class EventService {
  constructor(private http: HttpClient, private datePipe: DatePipe) {}

  searchEvents(
    name: string,
    location: string,
    categories: Array<string>,
    dateBegin: Date,
    dateEnd: Date,
    page: number
  ) {
    let params = new HttpParams();
    if (name) params = params.set("name", name);
    if (location) params = params.set("location", location);

    if (dateBegin)
      params = params.set(
        "begin",
        this.datePipe.transform(dateBegin, "dd-MM-yyyy")
      );
    if (dateEnd)
      params = params.set(
        "end",
        this.datePipe.transform(dateEnd, "dd-MM-yyyy")
      );
    categories.forEach(
      category => (params = params.append("eventCategories", category))
    );
    //params = params.set("page", page.toString());

    return this.http.get<Array<Event>>(
      `${environment.apiUrl}/api/event/search_events`,
      {
        params: params
      }
    );
  }
  createEvent(createEventDTO: any) {
    return this.http.post(`${environment.apiUrl}/api/event`, createEventDTO);
  }
  getById(id: number) {
    return this.http.get<Event>(`${environment.apiUrl}/api/event/${id}`);
  }
  updateEvent(eventId: number, event) {
    return this.http.put<Event>(`${environment.apiUrl}/api/event/${eventId}`, {
      name: event.name,
      eventCategory: event.eventCategory,
      description: event.description,
      picture: event.picture
    });
  }
  getPriceList(eventId: number) {
    return this.http.get<Array<PriceList>>(
      `${environment.apiUrl}/api/event/${eventId}/pricelists`
    );
  }
}
