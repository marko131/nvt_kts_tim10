export class Location {
  id?: number;
  name: string;
  address: string;
  latitude: number;
  longitude: number;
  picture: string;
}

