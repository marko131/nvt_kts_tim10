export class Reservation {
  ticketId: number;
  timestamp: Date;
  rowNum?: number;
  seatNum?: number;
  eventName: string;
  date: Date;
  location: string;
  price: number;
}
