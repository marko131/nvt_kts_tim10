export class Event {
  id: number;
  name: string;
  eventCategory: string;
  description: string;
  picture: string;
  priceLists: Array<any>;
}
