export class SeatingLayout {
  totalRows: Number;
  seatsPerRow: Number;
  seatNaming: String;
  booked: String[];
  unavailable: String[];
}
