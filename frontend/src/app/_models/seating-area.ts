export class SeatingArea {
    rowNum: number;
    seatNum: number;
    layoutPosition: string;
}