import { SeatingLayout } from "./seating-layout";

export class RenderLayout {
  id: number;
  name: string;
  middleSection?: string;
  eastSection?: string;
  westSection?: string;
  northSection?: string;
  southSection?: string;
  eastSectionId?: number;
  westSectionId?: number;
  northSectionId?: number;
  southSectionId?: number;
  seatsLayoutM: SeatingLayout;
  seatsLayoutN: SeatingLayout;
  seatsLayoutS: SeatingLayout;
  seatsLayoutE: SeatingLayout;
  seatsLayoutW: SeatingLayout;
}
