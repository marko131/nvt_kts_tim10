import { SeatingArea } from './seating-area';
import { StandingArea } from './standing-area';

export class Layout {
    locationId: number;
    layoutName: string;
    stagePosition: string;
    sittingAreas: SeatingArea[];
    standingAreas: StandingArea[];
}




