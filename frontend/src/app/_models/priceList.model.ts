export class PriceList {
  id: number;
  date: string;
  areaType: string;
  areaId: number;
  price: number;
  position: string;
}
