import { Component, OnInit, Input } from "@angular/core";
import { SeatingLayout } from '../_models/seating-layout';
import { RenderLayout } from '../_models/render-layout';

@Component({
  selector: "layout",
  templateUrl: "./layout.component.html",
  styleUrls: ["./layout.component.scss"]
})
export class LayoutComponent implements OnInit {
  @Input() layout: RenderLayout;

  constructor() {
  }

  ngOnInit() {
  }
}
