import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { EventService } from "../_services/event.service";
import { Event } from "../_models/event";
import { PriceList } from "../_models/priceList.model";
import { MatDialog, MAT_DIALOG_SCROLL_STRATEGY } from "@angular/material";
import { SelectTicketDialogComponent } from "../select-ticket-dialog/select-ticket-dialog.component";

@Component({
  selector: "app-event-details",
  templateUrl: "./event-details.component.html",
  styleUrls: ["./event-details.component.scss"]
})
export class EventDetailsComponent implements OnInit {
  event: Event;
  displayedColumns: Array<string>;
  priceList: Array<PriceList>;

  constructor(
    private activatedRoute: ActivatedRoute,
    private eventService: EventService,
    private router: Router,
    public dialog: MatDialog
  ) {
    this.event = new Event();
    this.displayedColumns = [
      "date",
      "areaType",
      "areaPosition",
      "price",
      "select"
    ];
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      let id = +params["id"];
      this.eventService.getById(id).subscribe(
        response => {
          this.event = response;
        },
        errorResponse => console.log(errorResponse)
      );

      this.eventService.getPriceList(id).subscribe(
        response => {
          this.priceList = response;
        },
        errorResponse => {
          console.log(errorResponse);
        }
      );
    });
  }
  selectPriceList(priceList: PriceList) {
    const dialogRef = this.dialog.open(SelectTicketDialogComponent, {
      data: {
        priceListId: priceList.id,
        areaType: priceList.areaType,
        areaId: priceList.areaId,
        price: priceList.price,
        position: priceList.position
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log("The dialog was closed");
    });
  }
}
