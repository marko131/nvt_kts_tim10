import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { LoginComponent } from "./login/login.component";
import { AuthService } from "./_services/auth.service";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  MatToolbarModule,
  MatButtonToggleModule,
  MatTabsModule,
  MatSnackBarModule,
  MatIconModule,
  MatExpansionModule,
  MatSelectModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MAT_DATE_LOCALE,
  MatCardModule,
  MatListModule,
  MatGridListModule,
  MatTableModule,
  MatStepperModule,
  MatDialogModule,
  MatPaginatorModule,
  MatCheckboxModule
} from "@angular/material";
import { RegisterComponent } from "./register/register.component";
import { EventComponent } from "./event/event.component";
import { ProfileComponent } from "./profile/profile.component";
import { NavbarComponent } from "./navbar/navbar.component";
import { AllowedRoutes } from "./_services/allowedRoutes.service";
import { JwtInterceptor } from "./_helpers/jwt.interceptor";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { DatePipe } from "@angular/common";
import { EventService } from "./_services/event.service";
import { EventDetailsComponent } from "./event-details/event-details.component";
import { CreateEventComponent } from "./create-event/create-event.component";
import { LayoutService } from "./_services/layout.service";
import { EditEventComponent } from "./edit-event/edit-event.component";
import { AddLocationComponent } from "./add-location/add-location.component";
import { LocationService } from "./_services/location.service";
import { LocationComponent } from "./location/location.component";
import { EditLocationComponent } from "./edit-location/edit-location.component";
import { LayoutComponent } from "./layout/layout.component";
import { AddLayoutComponent } from "./add-layout/add-layout.component";
import { SeatingLayoutComponent } from "./seating-layout/seating-layout.component";
import { LayoutListComponent } from "./layout-list/layout-list.component";
import { AreaTypePipe } from "./_helpers/area-type.pipe";
import { SelectTicketDialogComponent } from "./select-ticket-dialog/select-ticket-dialog.component";
import { PriceListTicketService } from "./_services/priceListTicket.service";
import { TicketService } from "./_services/ticket.service";
import { SeatPipe } from "./_helpers/seatNum.pipe";
import { ReservationsComponent } from "./reservations/reservations.component";
import { TicketsComponent } from "./tickets/tickets.component";
import { QrCodeDialogComponent } from "./qr-code-dialog/qr-code-dialog.component";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    EventComponent,
    ProfileComponent,
    NavbarComponent,
    PageNotFoundComponent,
    EventDetailsComponent,
    CreateEventComponent,
    EditEventComponent,
    AddLocationComponent,
    LocationComponent,
    EditLocationComponent,
    LayoutComponent,
    AddLayoutComponent,
    SeatingLayoutComponent,
    LayoutListComponent,
    AreaTypePipe,
    SeatPipe,
    SelectTicketDialogComponent,
    ReservationsComponent,
    TicketsComponent,
    QrCodeDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatToolbarModule,
    MatButtonToggleModule,
    MatTabsModule,
    MatSnackBarModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatExpansionModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCardModule,
    MatListModule,
    MatGridListModule,
    MatTableModule,
    MatStepperModule,
    MatDialogModule,
    MatPaginatorModule,
    MatCheckboxModule
  ],
  providers: [
    AuthService,
    AllowedRoutes,
    DatePipe,
    EventService,
    LayoutService,
    LocationService,
    PriceListTicketService,
    TicketService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: MAT_DATE_LOCALE, useValue: "en-GB" },
    AreaTypePipe,
    SeatPipe
  ],
  entryComponents: [SelectTicketDialogComponent, QrCodeDialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule {}
