import { Component, OnInit, Inject } from "@angular/core";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

@Component({
  selector: "app-qr-code-dialog",
  templateUrl: "./qr-code-dialog.component.html",
  styleUrls: ["./qr-code-dialog.component.scss"]
})
export class QrCodeDialogComponent {
  ticketId: number;
  link: string;
  constructor(
    public dialogRef: MatDialogRef<QrCodeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.ticketId = data.id;
    this.link = `https://chart.googleapis.com/chart?cht=qr&chs=500x500&chl=${this.ticketId}`;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
