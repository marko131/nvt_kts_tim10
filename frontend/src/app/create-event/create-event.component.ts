import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormControl,
  Validators,
  FormControlName,
  FormArray
} from "@angular/forms";
import { LayoutService } from "../_services/layout.service";
import { EventService } from "../_services/event.service";
import { DatePipe } from "@angular/common";
import { create } from "domain";
import { Layout } from "../_models/layout";
import { LocationService } from "../_services/location.service";
import { MatSelectChange, MatSnackBar } from "@angular/material";

@Component({
  selector: "app-create-event",
  templateUrl: "./create-event.component.html",
  styleUrls: ["./create-event.component.scss"]
})
export class CreateEventComponent implements OnInit {
  createEventForm: FormGroup;
  priceListForm: FormGroup;
  priceLists: FormArray;

  categoryList: Array<string>;
  layoutList: Array<any>;
  locationList: Array<any>;
  sittingAreaList: Array<any>;
  standingAreaList: Array<any>;

  constructor(
    private layoutService: LayoutService,
    private locationService: LocationService,
    private eventService: EventService,
    private datePipe: DatePipe,
    private snackBar: MatSnackBar
  ) {
    this.createEventForm = new FormGroup({
      name: new FormControl("", [Validators.required]),
      eventCategory: new FormControl("", [Validators.required]),
      description: new FormControl("", [Validators.required]),
      picture: new FormControl("", [Validators.required]),
      daysReservationIsValid: new FormControl("", [
        Validators.required,
        Validators.min(1)
      ]),
      locationId: new FormControl("", [Validators.required]),
      layoutId: new FormControl("", [Validators.required]),
      reservationsAvailable: new FormControl("", [Validators.required]),
      maxReservationNumber: new FormControl("", [Validators.required, Validators.min(1)])
    });
    this.priceListForm = new FormGroup({
      priceLists: new FormArray([])
    });
    this.categoryList = [
      "CONCERT",
      "FESTIVAL",
      "THEATER",
      "SPORT",
      "ENTERTAINMENT",
      "OTHER"
    ];
    this.layoutList = new Array<any>();
    this.locationList = new Array<any>();
    this.sittingAreaList = new Array<any>();
    this.standingAreaList = new Array<any>();

    this.priceLists = new FormArray([]);
  }
  createPriceList(): FormGroup {
    return new FormGroup({
      date: new FormControl("", [Validators.required]),
      areaType: new FormControl("", [Validators.required]),
      areaId: new FormControl("", [Validators.required]),
      price: new FormControl("", [Validators.required, Validators.min(0)])
    });
  }

  addPriceList(): void {
    this.priceLists = this.priceListForm.get("priceLists") as FormArray;
    this.priceLists.push(this.createPriceList());
  }
  removePriceList(i: number) {
    try {
      if (this.priceLists.length == 1) return;
      this.priceLists.removeAt(i);
    } catch (error) {
      console.log(error);
    }
  }
  submit() {
    let createEventDTO = JSON.parse(JSON.stringify(this.createEventForm.value));
    let priceListDTO = JSON.parse(JSON.stringify(this.priceListForm.value));

    createEventDTO.reservationsAvailable = this.datePipe.transform(
      createEventDTO.reservationsAvailable,
      "dd-MM-yyyy"
    );
    priceListDTO.priceLists.forEach(element => {
      element.date = this.datePipe.transform(element.date, "dd-MM-yyyy");
    });
    createEventDTO = {
      ...createEventDTO,
      ...priceListDTO
    };

    if (this.createEventForm.valid)
      this.eventService.createEvent(createEventDTO).subscribe(
        response =>
          this.snackBar.open("Event successfully created", "", {
            duration: 2000
          }),
        errorResponse => console.log(errorResponse)
      );
  }
  onLocationChange(event: MatSelectChange) {
    this.locationService
      .getLayoutsForLocation(event.value)
      .subscribe(response => (this.layoutList = response));
  }
  onAreaChange(event: MatSelectChange) {
    if (!this.createEventForm.get("layoutId")) return;
    if (event.value === 0) {
      this.layoutService
        .getSittingAreas(this.createEventForm.get("layoutId").value)
        .subscribe(
          response => {
            this.sittingAreaList = response;
          },
          errorResponse => console.log(errorResponse)
        );
    } else if (event.value === 1) {
      this.layoutService
        .getStangingAreas(this.createEventForm.get("layoutId").value)
        .subscribe(
          response => {
            this.standingAreaList = response;
          },
          errorResponse => console.log(errorResponse)
        );
    }
  }

  ngOnInit() {
    this.addPriceList();

    this.layoutService.getAll().subscribe(
      response => {
        this.layoutList = response;
      },
      errorResponse => console.log(errorResponse)
    );
    this.locationService.getAll().subscribe(
      response => (this.locationList = response),
      errorResponse => console.log(errorResponse)
    );
  }
}
