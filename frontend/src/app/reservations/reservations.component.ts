import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import {
  MatTableDataSource,
  MatPaginator,
  MatSnackBar
} from "@angular/material";
import { Reservation } from "../_models/reservation.model";
import { TicketService } from "../_services/ticket.service";
import { SelectionModel } from "@angular/cdk/collections";
import { environment } from "src/environments/environment";
import { element } from "protractor";

declare const paypal: any;

@Component({
  selector: "reservations",
  templateUrl: "./reservations.component.html",
  styleUrls: ["./reservations.component.scss"]
})
export class ReservationsComponent implements OnInit {
  myReservations: MatTableDataSource<Reservation>;
  displayedColumns: Array<string>;
  selection = new SelectionModel<Reservation>(true, []);
  totalPrice: number;
  @ViewChild("paypalButtonContainer", null) paypalContainer: ElementRef;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private ticketService: TicketService,
    private _snackBar: MatSnackBar
  ) {
    this.myReservations = new MatTableDataSource<Reservation>([]);
    this.displayedColumns = [
      "select",
      "eventName",
      "location",
      "date",
      "rowNum",
      "seatNum",
      "timestamp",
      "price",
      "cancel"
    ];
  }

  ngOnInit() {
    this.ticketService.getReservations().subscribe(
      response => {
        this.myReservations = new MatTableDataSource<Reservation>(response);
        this.myReservations.paginator = this.paginator;
      },
      errorResponse => console.log(errorResponse)
    );
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.myReservations.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.myReservations.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(index: number, row?: Reservation): string {
    if (!row) {
      return `${this.isAllSelected() ? "select" : "deselect"} all`;
    }
    return `${
      this.selection.isSelected(row) ? "deselect" : "select"
    } row ${index + 1}`;
  }

  cancelReservation(ticketId: number, index: number) {
    this.ticketService.cancelReservation(ticketId).subscribe(
      response => {
        this._snackBar.open(response, "", {
          duration: 2000
        });
        this.myReservations.data.splice(index, 1);
        this.myReservations._updateChangeSubscription();
      },
      errorResponse => console.log(errorResponse)
    );
  }

  renderPayPal() {
    if (this.paypalContainer.nativeElement.innerHTML) return;
    let totalPrice = 0;
    this.selection.selected.forEach(element => (totalPrice += element.price));
    const reservedTickets = this.selection.selected.map(
      element => element.ticketId
    );
    if (totalPrice === 0 || reservedTickets.length === 0) return;
    paypal
      .Buttons({
        createOrder: (data, actions) => {
          return actions.order.create({
            purchase_units: [
              {
                amount: {
                  value: totalPrice
                }
              }
            ]
          });
        },
        onApprove: (data, actions) => {
          return actions.order.capture().then(details => {
            this._snackBar.open(
              "Transaction completed by " + details.payer.name.given_name,
              "",
              {
                duration: 2000
              }
            );
            return fetch(
              `${environment.apiUrl}/api/ticket/buyReservedTickets`,
              {
                method: "post",
                headers: {
                  "content-type": "application/json",
                  "X-Auth-Token": localStorage.getItem("token")
                },
                body: JSON.stringify({
                  orderID: data.orderID,
                  reservedTickets: reservedTickets
                })
              }
            ).then(response => {
              //window.location.reload();
            });
          });
        }
      })
      .render(this.paypalContainer.nativeElement);
  }
}
