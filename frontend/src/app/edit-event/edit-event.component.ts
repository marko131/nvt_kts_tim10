import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { FormGroup, FormControl, Validators, FormArray } from "@angular/forms";
import { EventService } from "../_services/event.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-edit-event",
  templateUrl: "./edit-event.component.html",
  styleUrls: ["./edit-event.component.scss"]
})
export class EditEventComponent implements OnInit {
  editEventForm: FormGroup;
  addPriceListsForm: FormGroup;
  priceLists: FormArray;

  categoryList: Array<string>;
  layoutList: Array<string>;
  eventId: number;

  constructor(
    private eventService: EventService,
    private activatedRoute: ActivatedRoute
  ) {
    this.editEventForm = new FormGroup({
      name: new FormControl("", [Validators.required]),
      eventCategory: new FormControl("", [Validators.required]),
      description: new FormControl("", [Validators.required]),
      picture: new FormControl("", [Validators.required])
    });
    this.addPriceListsForm = new FormGroup({
      priceLists: new FormArray([])
    });

    this.categoryList = [
      "CONCERT",
      "FESTIVAL",
      "THEATER",
      "SPORT",
      "ENTERTAINMENT",
      "OTHER"
    ];
    this.layoutList = new Array<string>();
    this.priceLists = new FormArray([]);
    this.addPriceList();
  }
  createPriceList(): FormGroup {
    return new FormGroup({
      date: new FormControl("", [Validators.required]),
      areaType: new FormControl("", [Validators.required]),
      areaId: new FormControl("", [Validators.required]),
      price: new FormControl("", [Validators.required, Validators.min(0)])
    });
  }
  addPriceList(): void {
    this.priceLists = this.addPriceListsForm.get("priceLists") as FormArray;
    this.priceLists.push(this.createPriceList());
  }
  removePriceList(i: number) {
    try {
      if (this.priceLists.length == 1) return;
      this.priceLists.removeAt(i);
    } catch (error) {
      console.log(error);
    }
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      let id = +params["id"];
      this.eventService.getById(id).subscribe(
        response => {
          this.editEventForm.get("name").setValue(response.name);
          this.editEventForm
            .get("eventCategory")
            .setValue(response.eventCategory);
          this.editEventForm.get("description").setValue(response.description);
          this.editEventForm.get("picture").setValue(response.picture);
          this.eventId = +response.id;
        },
        errorResponse => console.log(errorResponse)
      );
    });
  }

  submit() {
    this.eventService
      .updateEvent(this.eventId, this.editEventForm.value)
      .subscribe(
        response => console.log(response),
        errorResponse => console.log(errorResponse)
      );
  }

  updatePriceList() {
    console.log(this.addPriceListsForm.value);
  }
}
