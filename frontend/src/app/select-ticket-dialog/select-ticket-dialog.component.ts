import {
  Component,
  OnInit,
  Inject,
  ViewChild,
  ElementRef
} from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from "@angular/material";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { PriceListTicketService } from "../_services/priceListTicket.service";
import { RenderLayout } from "../_models/render-layout";
import { SeatingLayout } from "../_models/seating-layout";
import { environment } from "src/environments/environment";
import { AllowedRoutes } from "../_services/allowedRoutes.service";
import { AuthService } from "../_services/auth.service";

declare const paypal: any;

@Component({
  selector: "app-select-ticket-dialog",
  templateUrl: "./select-ticket-dialog.component.html",
  styleUrls: ["./select-ticket-dialog.component.scss"]
})
export class SelectTicketDialogComponent implements OnInit {
  priceListId: number;
  standingAreaForm: FormGroup;
  areaType: string;
  areaId: number;
  price: number;
  layout: RenderLayout;
  position: string;
  selectedSeats: Array<string>;
  isAdmin: boolean;
  totalPrice: number;
  @ViewChild("paypalButtonContainer", null) paypalContainer: ElementRef;
  constructor(
    private priceListTicketService: PriceListTicketService,
    private _snackBar: MatSnackBar,
    private authService: AuthService,
    public dialogRef: MatDialogRef<SelectTicketDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.standingAreaForm = new FormGroup({
      numberOfTickets: new FormControl("", [
        Validators.required,
        Validators.min(1)
      ])
    });
    this.isAdmin = this.authService.isAdmin();
    this.priceListId = data.priceListId;
    this.areaId = data.areaId;
    this.areaType = data.areaType;
    this.price = data.price;
    this.position = data.position;
    this.totalPrice = 0;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  reserveTicketStandingArea() {
    if (!this.standingAreaForm.valid) return;
    this.priceListTicketService
      .reserveTicketStandingArea(
        this.priceListId,
        this.standingAreaForm.get("numberOfTickets").value
      )
      .subscribe(
        response => {
          this._snackBar.open(response, "", { duration: 2000 });
          this.dialogRef.close();
        },
        errorResponse => {
          console.log(errorResponse);
          this._snackBar.open(JSON.parse(errorResponse.error).message, "", {
            duration: 2000
          });
          this.dialogRef.close();
        }
      );
  }

  ngOnInit() {
    if (this.areaType === "SITTING_AREA") {
      this.priceListTicketService
        .getLayoutByPriceListId(this.priceListId)
        .subscribe(layout => {
          let l = {
            id: layout.id,
            name: layout.layoutName,
            middleSection: "M",
            eastSection: "E",
            westSection: "W",
            northSection: "N",
            southSection: "S",
            seatsLayoutM: new SeatingLayout(),
            seatsLayoutN: new SeatingLayout(),
            seatsLayoutS: new SeatingLayout(),
            seatsLayoutE: new SeatingLayout(),
            seatsLayoutW: new SeatingLayout()
          };

          switch (layout.stagePosition) {
            case "M": {
              l.middleSection = "stage";
              break;
            }
            case "E": {
              l.eastSection = "stage";
              break;
            }
            case "W": {
              l.westSection = "stage";
              break;
            }
            case "N": {
              l.northSection = "stage";
              break;
            }
            case "S": {
              l.southSection = "stage";
              break;
            }
          }

          layout.sittingAreas.forEach(sittingArea => {
            switch (sittingArea.layoutPosition) {
              case "M": {
                l.middleSection = "seatingArea";
                l.seatsLayoutM.totalRows = sittingArea.numRows;
                l.seatsLayoutM.seatsPerRow = sittingArea.numSeats;
                l.seatsLayoutM.seatNaming = "rowType";
                l.seatsLayoutM.booked = [];
                l.seatsLayoutM.unavailable = layout.bookedTickets.map(element =>
                  this.convertSeatNumToChar(element.rowNum, element.seatNum)
                );
                break;
              }
              case "E": {
                l.eastSection = "seatingArea";
                l.seatsLayoutE.totalRows = sittingArea.numRows;
                l.seatsLayoutE.seatsPerRow = sittingArea.numSeats;
                l.seatsLayoutE.seatNaming = "rowType";
                l.seatsLayoutE.booked = [];
                l.seatsLayoutE.unavailable = layout.bookedTickets.map(element =>
                  this.convertSeatNumToChar(element.rowNum, element.seatNum)
                );
                break;
              }
              case "W": {
                l.westSection = "seatingArea";
                l.seatsLayoutW.totalRows = sittingArea.numRows;
                l.seatsLayoutW.seatsPerRow = sittingArea.numSeats;
                l.seatsLayoutW.seatNaming = "rowType";
                l.seatsLayoutW.booked = [];
                l.seatsLayoutW.unavailable = layout.bookedTickets.map(element =>
                  this.convertSeatNumToChar(element.rowNum, element.seatNum)
                );
                break;
              }
              case "N": {
                l.northSection = "seatingArea";
                l.seatsLayoutN.totalRows = sittingArea.numRows;
                l.seatsLayoutN.seatsPerRow = sittingArea.numSeats;
                l.seatsLayoutN.seatNaming = "rowType";
                l.seatsLayoutN.booked = [];
                l.seatsLayoutN.unavailable = layout.bookedTickets.map(element =>
                  this.convertSeatNumToChar(element.rowNum, element.seatNum)
                );
                break;
              }
              case "S": {
                l.southSection = "seatingArea";
                l.seatsLayoutS.totalRows = sittingArea.numRows;
                l.seatsLayoutS.seatsPerRow = sittingArea.numSeats;
                l.seatsLayoutS.seatNaming = "rowType";
                l.seatsLayoutS.booked = [];
                l.seatsLayoutS.unavailable = layout.bookedTickets.map(element =>
                  this.convertSeatNumToChar(element.rowNum, element.seatNum)
                );
                break;
              }
            }
          });
          this.layout = l;
        });
    }
  }
  ngAfterViewInit() {
    if (this.authService.isAdmin()) return;
    paypal
      .Buttons({
        createOrder: (data, actions) => {
          if (this.areaType === "STANDING_AREA") {
            return actions.order.create({
              purchase_units: [
                {
                  amount: {
                    value:
                      this.price *
                      this.standingAreaForm.get("numberOfTickets").value
                  }
                }
              ]
            });
          } else if (this.areaType === "SITTING_AREA") {
            let selected;
            if (this.position === "M")
              selected = this.layout.seatsLayoutM.booked;
            if (this.position === "S")
              selected = this.layout.seatsLayoutS.booked;
            if (this.position === "N")
              selected = this.layout.seatsLayoutN.booked;
            if (this.position === "W")
              selected = this.layout.seatsLayoutW.booked;
            if (this.position === "E")
              selected = this.layout.seatsLayoutE.booked;
            return actions.order.create({
              purchase_units: [
                {
                  amount: {
                    value: this.price * selected.length
                  }
                }
              ]
            });
          }
        },
        onApprove: (data, actions) => {
          return actions.order.capture().then(details => {
            this._snackBar.open(
              "Transaction completed by " + details.payer.name.given_name,
              "",
              {
                duration: 2000
              }
            );
            // Call your server to save the transaction
            if (this.areaType === "STANDING_AREA")
              return fetch(`${environment.apiUrl}/api/standingAreaBuy`, {
                method: "post",
                headers: {
                  "content-type": "application/json",
                  "X-Auth-Token": localStorage.getItem("token")
                },
                body: JSON.stringify({
                  orderID: data.orderID,
                  reserveStandingAreaDTO: {
                    priceListId: this.priceListId,
                    numberOfTickets: this.standingAreaForm.get(
                      "numberOfTickets"
                    ).value
                  }
                })
              });
            if (this.areaType === "SITTING_AREA")
              this.priceListTicketService
                .buyTicketSittingArea(
                  this.priceListId,
                  this.selectedSeats,
                  data.orderID
                )
                .subscribe(
                  response => {
                    this._snackBar.open(response, "", { duration: 2000 });
                    this.dialogRef.close();
                  },
                  errorResponse => {
                    console.log(errorResponse);
                    this._snackBar.open(
                      JSON.parse(errorResponse.error).message,
                      "",
                      {
                        duration: 2000
                      }
                    );
                    this.dialogRef.close();
                  }
                );
          });
        }
      })
      .render(this.paypalContainer.nativeElement);
  }

  getSelected(selectedSeats) {
    this.selectedSeats = selectedSeats;
    this.totalPrice = selectedSeats.length * this.price;
  }

  reserveTicketSittingArea() {
    if (!this.selectedSeats) return;
    if (this.selectedSeats.length === 0) return;
    this.priceListTicketService
      .reserveTicketSittingArea(this.priceListId, this.selectedSeats)
      .subscribe(
        response => {
          this._snackBar.open(response, "", { duration: 2000 });
          this.dialogRef.close();
        },
        errorResponse => {
          console.log(errorResponse);
          this._snackBar.open(JSON.parse(errorResponse.error).message, "", {
            duration: 2000
          });
          this.dialogRef.close();
        }
      );
  }

  convertSeatNumToChar(row: number, col: number): string {
    return row.toString() + String.fromCharCode(64 + col);
  }
}
