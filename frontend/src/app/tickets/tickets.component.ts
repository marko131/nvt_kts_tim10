import { Component, OnInit, ViewChild } from "@angular/core";
import {
  MatTableDataSource,
  MatPaginator,
  MatSnackBar,
  MatDialog
} from "@angular/material";
import { Reservation } from "../_models/reservation.model";
import { TicketService } from "../_services/ticket.service";
import { QrCodeDialogComponent } from "../qr-code-dialog/qr-code-dialog.component";

@Component({
  selector: "tickets",
  templateUrl: "./tickets.component.html",
  styleUrls: ["./tickets.component.scss"]
})
export class TicketsComponent implements OnInit {
  myTickets: MatTableDataSource<Reservation>;
  displayedColumns: Array<string>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(
    private ticketService: TicketService,
    private _snackBar: MatSnackBar,
    public dialog: MatDialog
  ) {
    this.myTickets = new MatTableDataSource<Reservation>([]);
    this.displayedColumns = [
      "eventName",
      "location",
      "date",
      "rowNum",
      "seatNum",
      "timestamp",
      "price",
      "qr"
    ];
  }

  ngOnInit() {
    this.ticketService.getTickets().subscribe(
      response => {
        this.myTickets = new MatTableDataSource<Reservation>(response);
        this.myTickets.paginator = this.paginator;
      },
      errorResponse => console.log(errorResponse)
    );
  }
  showQR(ticketId: number) {
    const dialogRef = this.dialog.open(QrCodeDialogComponent, {
      data: { id: ticketId }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log("The dialog was closed");
    });
  }
}
