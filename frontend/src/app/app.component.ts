import { Component } from "@angular/core";
import { AllowedRoutes } from "./_services/allowedRoutes.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "frontend";
}
