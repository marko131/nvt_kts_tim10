import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "seatNumPipe" })
export class SeatPipe implements PipeTransform {
  transform(value: number): string {
    if (!value) return "";
    return String.fromCharCode(64 + value);
  }
}
