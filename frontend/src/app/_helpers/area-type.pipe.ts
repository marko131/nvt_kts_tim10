import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "areaTypePipe" })
export class AreaTypePipe implements PipeTransform {
  transform(areaType: string): string {
    const replace = areaType.replace("_", " ");
    const capitalize =
      replace.charAt(0).toUpperCase() + replace.slice(1).toLowerCase();
    return capitalize;
  }
}
