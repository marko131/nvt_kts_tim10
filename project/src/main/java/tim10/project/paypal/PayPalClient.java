package tim10.project.paypal;

import com.paypal.core.PayPalEnvironment;
import com.paypal.core.PayPalHttpClient;

public class PayPalClient {

    /**
     *Set up the PayPal Java SDK environment with PayPal access credentials.
     *This sample uses SandboxEnvironment. In production, use LiveEnvironment.
     */
    private PayPalEnvironment environment = new PayPalEnvironment.Sandbox(
            "Ab5Vij3-bPRd78LaJgVwKjF4tkVkPsirHCHb2c1CqECWhcErOGIBRTeANlqCZbh27udpM05p3jRR0pl3",
            "ECNNkZbVAeDJxkacjBiSt2i-amdqcG20C1oGtIQgfoYHZLMpENjuS1aSiNEmBo8Pc66CVdvbVjCCm8wn");

    /**
     *PayPal HTTP client instance with environment that has access
     *credentials context. Use to invoke PayPal APIs.
     */
    PayPalHttpClient client = new PayPalHttpClient(environment);

    /**
     *Method to get client object
     *
     *@return PayPalHttpClient client
     */
    public PayPalHttpClient client() {
        return this.client;
    }
}