package tim10.project.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import tim10.project.model.Ticket;
import tim10.project.model.User;
import tim10.project.service.*;

import javax.annotation.security.PermitAll;
import java.util.List;

@RestController
public class TestController {

    @Autowired
    private EventService eventService;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    private LocationService locationService;

    @Autowired LayoutService layoutService;

    @Autowired
    TicketService ticketService;

    @PermitAll()
    @GetMapping("/database/init")
    public void initDatabase(){

    }

    @GetMapping("/database/admin")
    public void registerAdmin(){
        User user = new User("admin", "admin", "admin@gmail.com", "password", true, null);
        userDetailsService.createUser(user);
    }


}
