package tim10.project.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import tim10.project.model.Ticket;
import tim10.project.model.TicketSittingArea;
import tim10.project.model.TicketStatus;
import tim10.project.model.User;
import tim10.project.paypal.GetOrder;
import tim10.project.service.MailService;
import tim10.project.service.TicketService;
import tim10.project.service.UserDetailsServiceImpl;
import tim10.project.service.exceptions.AlreadySoldException;
import tim10.project.service.exceptions.CancelReservationException;
import tim10.project.web.dto.paypal.SittingAreaOrderDTO;
import tim10.project.web.dto.paypal.StandingAreaOrderDTO;
import tim10.project.web.dto.sittingArea.ReserveSittingAreaDTO;
import tim10.project.web.dto.standingArea.ReserveStandingAreaDTO;
import tim10.project.web.dto.ticket.BuyReservedTicketDTO;
import tim10.project.web.dto.ticket.ReservationDTO;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class TicketController {

    @Autowired
    private TicketService ticketService;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    private MailService mailService;

    @Autowired
    private GetOrder getOrder;


    @PreAuthorize("hasRole('USER')")
    @PostMapping("/api/standingAreaReserve")
    public ResponseEntity<String> reserveTicket(@RequestBody ReserveStandingAreaDTO reserveStandingAreaDTO) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        User user = userDetailsService.findUserByEmail(securityContext.getAuthentication().getName());
        ticketService.reserveStandingAreaTickets(reserveStandingAreaDTO.getPriceListId(), reserveStandingAreaDTO.getNumberOfTickets(), user);
        return new ResponseEntity<String>("Successful reservation", HttpStatus.OK);
    }

    @PreAuthorize("hasRole('USER')")
    @PostMapping("/api/sittingAreaReserve")
    public ResponseEntity<String> reserveSittingTicket(@RequestBody ReserveSittingAreaDTO reserveSittingAreaDTO) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        User user = userDetailsService.findUserByEmail(securityContext.getAuthentication().getName());
        ticketService.reserveSittingAreaTickets(reserveSittingAreaDTO, user);
        return new ResponseEntity<String>("Successful reservation", HttpStatus.OK);
    }

    @PreAuthorize("hasRole('USER')")
    @PostMapping("/api/standingAreaBuy")
    public ResponseEntity<String> buyStandingTicket(@RequestBody StandingAreaOrderDTO orderDTO) throws IOException, MessagingException {
        boolean completed = getOrder.getOrder(orderDTO.getOrderID());
        if (completed) {
            SecurityContext securityContext = SecurityContextHolder.getContext();
            User user = userDetailsService.findUserByEmail(securityContext.getAuthentication().getName());
            ticketService.buyStandingAreaTickets(orderDTO.getReserveStandingAreaDTO().getPriceListId(), orderDTO.getReserveStandingAreaDTO().getNumberOfTickets(), user);
        }
        return new ResponseEntity<String>("Successful purchase", HttpStatus.OK);
    }

    @PreAuthorize("hasRole('USER')")
    @PostMapping("/api/sittingAreaBuy")
    public ResponseEntity<String> buySittingTicket(@RequestBody SittingAreaOrderDTO sittingAreaOrderDTO) throws IOException, MessagingException {
        boolean completed = getOrder.getOrder(sittingAreaOrderDTO.getOrderID());
        if (completed) {
            SecurityContext securityContext = SecurityContextHolder.getContext();
            User user = userDetailsService.findUserByEmail(securityContext.getAuthentication().getName());
            ticketService.buySittingAreaTickets(sittingAreaOrderDTO.getReserveSittingAreaDTO(), user);
        }
        return new ResponseEntity<String>("Successful purchase", HttpStatus.OK);
    }

    @PreAuthorize("hasRole('USER')")
    @PostMapping("/api/ticket/buyReservedTickets")
    public ResponseEntity<String> buyReservedTickets(@RequestBody BuyReservedTicketDTO buyReservedTicketDTO) throws IOException {
        boolean completed = getOrder.getOrder(buyReservedTicketDTO.getOrderID());
        if (completed){
            SecurityContext securityContext = SecurityContextHolder.getContext();
            User user = userDetailsService.findUserByEmail(securityContext.getAuthentication().getName());
            ticketService.buyReservedTickets(buyReservedTicketDTO.getReservedTickets(), user);
        }
        return new ResponseEntity<>("Successful purchase", HttpStatus.OK);
    }

    @GetMapping("/api/ticket")
    public List<Ticket> getAllTickets() {
        return ticketService.getAllTickets();
    }

    @GetMapping("/api/ticket/{id}")
    public Ticket getTicketById(@PathVariable("id") Integer id) {
        return ticketService.getTicketById(id);
    }

    @PreAuthorize("hasRole('USER')")
    @PutMapping("api/ticket/{id}/cancel")
    public ResponseEntity<String> cancelReservation(@PathVariable("id") Integer id) {
        Ticket ticket = ticketService.getTicketById(id);
        if (ticket.getTicketStatus() == TicketStatus.IS_SOLD) throw new CancelReservationException("Ticket is bought");
        ticket.setUser(null);
        ticket.setTicketStatus(TicketStatus.AVAILABLE);
        ticket.setTimestamp(null);
        ticketService.updateTicket(id, ticket);
        return new ResponseEntity<String>("Reservation successfully canceled", HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/api/ticket/{id}")
    public void deleteTicket(@PathVariable("id") Integer id) {
        ticketService.deleteTicket(id);
    }

    @PreAuthorize("hasRole('USER')")
    @PutMapping("/api/ticket/{id}/reserve")
    public Ticket reserveTicket(@PathVariable("id") Integer id) {
        Ticket ticket = ticketService.getTicketById(id);
        SecurityContext securityContext = SecurityContextHolder.getContext();
        User user = userDetailsService.findUserByEmail(securityContext.getAuthentication().getName());
        ticket.setTicketStatus(TicketStatus.IS_RESERVED);
        ticket.setTimestamp(new Date());
        ticket.setUser(user);
        mailService.sendMail(user, "Successful reservation", "Your ticket reservation was successful!");
        return ticketService.updateTicket(id, ticket);
    }

    @PreAuthorize("hasRole('USER')")
    @PutMapping("/api/ticket/{id}/buy")
    public Ticket buyTicket(@PathVariable("id") Integer id) {
        Ticket ticket = ticketService.getTicketById(id);
        SecurityContext securityContext = SecurityContextHolder.getContext();
        User user = userDetailsService.findUserByEmail(securityContext.getAuthentication().getName());
        ticket.setTicketStatus(TicketStatus.IS_SOLD);
        ticket.setTimestamp(new Date());
        ticket.setUser(user);
        mailService.sendMail(user, "Successful purchase", String.format("<html><body><img src='https://chart.googleapis.com/chart?cht=qr&chs=500x500&chl=%d'></body></html>", id));
        return ticketService.updateTicket(id, ticket);
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping("/api/ticket/reservations")
    public List<ReservationDTO> getReservedTicketForUser() {
        List<ReservationDTO> reservationDTOList = new ArrayList<>();
        SecurityContext securityContext = SecurityContextHolder.getContext();
        User user = userDetailsService.findUserByEmail(securityContext.getAuthentication().getName());
        List<Ticket> tickets = ticketService.getReservedTicketForUser(user);
        tickets.forEach(ticket -> {
            ReservationDTO reservationDTO = new ReservationDTO();
            reservationDTO.setTicketId(ticket.getId());
            reservationDTO.setTimestamp(ticket.getTimestamp());
            reservationDTO.setDate(ticket.getPriceList().getDate());
            if (ticket instanceof TicketSittingArea) {
                reservationDTO.setRowNum(((TicketSittingArea) ticket).getRowNum());
                reservationDTO.setSeatNum(((TicketSittingArea) ticket).getSeatNum());
            }
            reservationDTO.setEventName(ticket.getPriceList().getEvent().getName());
            reservationDTO.setLocation(ticket.getPriceList().getEvent().getLayout().getLocation().getName());
            reservationDTO.setPrice(ticket.getPriceList().getPrice());
            reservationDTOList.add(reservationDTO);
        });
        return reservationDTOList;
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping("/api/ticket/user-tickets")
    public List<ReservationDTO> getPurchasedTicketForUser() {
        List<ReservationDTO> reservationDTOList = new ArrayList<>();
        SecurityContext securityContext = SecurityContextHolder.getContext();
        User user = userDetailsService.findUserByEmail(securityContext.getAuthentication().getName());
        List<Ticket> tickets = ticketService.getPurchasedTicketForUser(user);
        tickets.forEach(ticket -> {
            ReservationDTO reservationDTO = new ReservationDTO();
            reservationDTO.setTicketId(ticket.getId());
            reservationDTO.setTimestamp(ticket.getTimestamp());
            reservationDTO.setDate(ticket.getPriceList().getDate());
            if (ticket instanceof TicketSittingArea) {
                reservationDTO.setRowNum(((TicketSittingArea) ticket).getRowNum());
                reservationDTO.setSeatNum(((TicketSittingArea) ticket).getSeatNum());
            }
            reservationDTO.setEventName(ticket.getPriceList().getEvent().getName());
            reservationDTO.setLocation(ticket.getPriceList().getEvent().getLayout().getLocation().getName());
            reservationDTO.setPrice(ticket.getPriceList().getPrice());
            reservationDTOList.add(reservationDTO);
        });
        return reservationDTOList;
    }
}
