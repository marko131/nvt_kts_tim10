package tim10.project.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tim10.project.model.*;
import tim10.project.service.EventService;
import tim10.project.service.LayoutService;
import tim10.project.service.SittingAreaService;
import tim10.project.service.StandingAreaService;
import tim10.project.web.dto.event.CreateEventDTO;
import tim10.project.web.dto.event.ReturnEventDTO;
import tim10.project.web.dto.event.UpdateEventDTO;
import tim10.project.web.dto.priceList.ReturnPriceListDTO;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class EventController {

    @Autowired
    private EventService eventService;

    @Autowired
    private LayoutService layoutService;

    @Autowired
    private StandingAreaService standingAreaService;

    @Autowired
    private SittingAreaService sittingAreaService;

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/api/event")
    public Event createEvent(@Valid @RequestBody CreateEventDTO createEventDTO) {
        Layout layout = layoutService.getLayoutById(createEventDTO.getLayoutId());
        Event event = new Event(createEventDTO.getName(), createEventDTO.getEventCategory(), createEventDTO.getDescription(),
                createEventDTO.getPicture(), createEventDTO.getDaysReservationIsValid(), createEventDTO.getReservationsAvailable(), createEventDTO.getMaxReservationNumber(),
                new ArrayList<>(), layout);
        List<PriceList> priceLists = createEventDTO.getPriceLists().stream().map(priceListDTO -> new PriceList(priceListDTO.getDate(), priceListDTO.getAreaType(),
                priceListDTO.getAreaId(), priceListDTO.getPrice(), event, new ArrayList<>())).collect(Collectors.toList());
        event.setPriceLists(priceLists);
        return eventService.createEvent(event);
    }


    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/api/event")
    public List<Event> getAllEvents(@RequestParam(required = false) Integer page) {
        Integer pageNumber = page == null ? 0 : page;
        return eventService.getAllEvents(pageNumber);
    }

    @GetMapping("/api/event/{id}")
    public ReturnEventDTO getEventById(@PathVariable("id") Integer id) {
        return new ReturnEventDTO(eventService.getEventById(id));
    }



    @GetMapping("/api/event/search_events")
    public List<ReturnEventDTO> searchEvents(@RequestParam(required = false) String name, @RequestParam(required = false) List<EventCategory> eventCategories, @RequestParam(required = false) @DateTimeFormat(pattern = "dd-MM-yyyy") Date begin, @RequestParam(required = false) @DateTimeFormat(pattern = "dd-MM-yyyy") Date end, @RequestParam(required = false) String location, @RequestParam(required = false) Integer page){
        Integer pageNumber = page == null ? 0 : page;
        List<Event> events = eventService.search(name, eventCategories, begin, end, location, pageNumber);
        List<ReturnEventDTO> eventDTOList = new ArrayList<>();
        events.forEach(event -> {eventDTOList.add(new ReturnEventDTO(event));});
        return eventDTOList;
    }

    @GetMapping("/api/event/{id}/pricelists")
    public List<ReturnPriceListDTO> getPriceListsByEventId(@PathVariable("id") Integer id) {
        List<PriceList> priceLists = eventService.getPriceListsByEventId(id);
        List<ReturnPriceListDTO> returnPriceListDTOList = new ArrayList<>();
        priceLists.forEach(priceList -> {
            if (priceList.getAreaType() == AreaType.STANDING_AREA) returnPriceListDTOList.add(new ReturnPriceListDTO(priceList, standingAreaService.getStandingAreaById(priceList.getAreaId()).getLayoutPosition()));
            else returnPriceListDTOList.add(new ReturnPriceListDTO(priceList, sittingAreaService.getSittingAreaById(priceList.getAreaId()).getLayoutPosition()));
            });
        return returnPriceListDTOList;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/api/event/{id}")
    public void deleteEvent(@PathVariable("id") Integer id) {
        eventService.deleteEvent(id);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/api/event/{id}")
    public ReturnEventDTO updateEvent(@PathVariable("id") Integer id, @Valid @RequestBody UpdateEventDTO updateEventDTO) {
        Event event = eventService.getEventById(id);
        event.setName(updateEventDTO.getName());
        event.setDescription(updateEventDTO.getDescription());
        event.setEventCategory(updateEventDTO.getEventCategory());
        event.setPicture(updateEventDTO.getPicture());
        return new ReturnEventDTO(eventService.updateEvent(id, event));
    }

}
