package tim10.project.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tim10.project.model.Layout;
import tim10.project.model.SittingArea;
import tim10.project.service.LayoutService;
import tim10.project.service.SittingAreaService;
import tim10.project.web.dto.sittingArea.CreateSittingAreaDTO;
import tim10.project.web.dto.sittingArea.UpdateSittingAreaDTO;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class SittingAreaController {
    @Autowired
    private SittingAreaService sittingAreaService;

    @Autowired
    private LayoutService layoutService;

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/api/sittingArea")
    public SittingArea createSittingArea(@Valid @RequestBody CreateSittingAreaDTO createSittingAreaDTO) {
        Layout layout = layoutService.getLayoutById(createSittingAreaDTO.getLayoutId());
        SittingArea sittingArea = new SittingArea(createSittingAreaDTO.getRows(), createSittingAreaDTO.getColls(), layout, createSittingAreaDTO.getLayoutPosition());
        return sittingAreaService.createSittingArea(sittingArea);
    }

    @GetMapping("/api/sittingArea/{id}")
    public SittingArea getSittingAreaById(@PathVariable("id") Integer id) {
        return sittingAreaService.getSittingAreaById(id);
    }

    @GetMapping("/api/sittingArea")
    public List<SittingArea> getAllSittingAreas() {
        return sittingAreaService.getAllSittingAreas();
    }

    @GetMapping("/api/sittingArea/{id}/layout")
    public List<SittingArea> getSittingAreasByLayoutId(@PathVariable("id") Integer id) {
        return sittingAreaService.getSittingAreasByLayoutId(id);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/api/sittingArea/{id}")
    public SittingArea updateSittingArea(@PathVariable("id") Integer id, @Valid @RequestBody UpdateSittingAreaDTO updateSittingAreaDTO){
        SittingArea sittingArea = new SittingArea();
        sittingArea.setNumSeats(updateSittingAreaDTO.getColls());
        sittingArea.setNumRows(updateSittingAreaDTO.getRows());
        return sittingAreaService.updateSittingArea(id, sittingArea);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/api/sittingArea/{id}")
    public void deleteSittingArea(@PathVariable("id") Integer id){
        sittingAreaService.deleteSittingArea(id);
    }


}
