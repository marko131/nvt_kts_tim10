package tim10.project.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tim10.project.model.Layout;
import tim10.project.model.StandingArea;
import tim10.project.service.LayoutService;
import tim10.project.service.StandingAreaService;
import tim10.project.web.dto.standingArea.CreateStandingAreaDTO;
import tim10.project.web.dto.standingArea.UpdateStandingAreaDTO;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class StandingAreaController {

    @Autowired
    private StandingAreaService standingAreaService;

    @Autowired
    private LayoutService layoutService;

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/api/standingArea")
    public StandingArea createStandingArea(@Valid @RequestBody CreateStandingAreaDTO createStandingAreaDTO){
        Layout layout = layoutService.getLayoutById(createStandingAreaDTO.getLayoutId());
        StandingArea standingArea = new StandingArea(createStandingAreaDTO.getMaxNumber(), layout, createStandingAreaDTO.getLayoutPosition());
        return standingAreaService.createStandingArea(standingArea);
    }

    @GetMapping("/api/standingArea/{id}")
    public StandingArea getStandingAreaById(@PathVariable("id") Integer id){
        return standingAreaService.getStandingAreaById(id);
    }

    @GetMapping("/api/standingArea")
    public List<StandingArea> getStandingAreas(){
        return standingAreaService.getAllStandingAreas();
    }

    @GetMapping("/api/standingArea/{id}/layout")
    public List<StandingArea> getSittingAreasByLayoutId(@PathVariable("id") Integer id) {
        return standingAreaService.getStandingAreasByLayoutId(id);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/api/standingArea/{id}")
    public StandingArea updateStandingArea(@PathVariable("id") Integer id, @Valid @RequestBody UpdateStandingAreaDTO updateStandingAreaDTO){
        StandingArea standingArea = new StandingArea();
        standingArea.setMaxNumber(updateStandingAreaDTO.getMaxNumber());
        return standingAreaService.updateStandingArea(id, standingArea);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/api/standingArea/{id}")
    public void deleteStandingArea(@PathVariable("id") Integer id){
        standingAreaService.deleteStandingArea(id);
    }
}
