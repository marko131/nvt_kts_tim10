package tim10.project.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tim10.project.model.Layout;
import tim10.project.model.Location;
import tim10.project.service.LocationService;
import tim10.project.web.dto.location.CreateLocationDTO;
import tim10.project.web.dto.location.UpdateLocationDTO;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class LocationController {

    @Autowired
    private LocationService locationService;

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/api/location")
    public Location createLocation(@Valid @RequestBody CreateLocationDTO createLocationDTO) {
        Location location = new Location(createLocationDTO.getName(), createLocationDTO.getAddress(), createLocationDTO.getLatitude(), createLocationDTO.getLongitude(), createLocationDTO.getPicture(), new ArrayList<>());
        return locationService.createLocation(location);
    }

    @GetMapping("/api/location/{id}")
    public Location getLocationById(@PathVariable("id") Integer id) {
        return locationService.getLocationById(id);
    }

    @GetMapping("/api/location")
    public List<Location> getAllLocations() {
        return locationService.getAllLocations();
    }

    @GetMapping("/api/location/{id}/layouts")
    public List<Layout> getLayoutsByLocationId(@PathVariable("id") Integer id) {
        return locationService.getLayoutsByLocationId(id);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/api/location/{id}")
    public Location updateLocation(@PathVariable("id") Integer id, @Valid @RequestBody UpdateLocationDTO updateLocationDTO) {
        Location location = locationService.getLocationById(id);
        location.setName(updateLocationDTO.getName());
        location.setAddress(updateLocationDTO.getAddress());
        location.setPicture(updateLocationDTO.getPicture());
        return locationService.updateLocation(id, location);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/api/location/{id}")
    public void deleteLocation(@PathVariable("id") Integer id) {
        locationService.deleteLocation(id);
    }
}
