package tim10.project.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tim10.project.model.Layout;
import tim10.project.model.Location;
import tim10.project.model.SittingArea;
import tim10.project.model.StandingArea;
import tim10.project.service.LayoutService;
import tim10.project.service.LocationService;
import tim10.project.service.exceptions.NotFoundException;
import tim10.project.web.dto.layout.CreateLayoutDTO;
import tim10.project.web.dto.layout.UpdateLayoutDTO;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class LayoutController {

    @Autowired
    private LayoutService layoutService;

    @Autowired
    private LocationService locationService;

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/api/layout")
    public Layout createLayout(@Valid @RequestBody CreateLayoutDTO createLayoutDTO) {
        Location location = locationService.getLocationById(createLayoutDTO.getLocationId());
        if (location == null) throw new NotFoundException("Location not found");
        Layout layout = new Layout(new ArrayList<>(), new ArrayList<>(), createLayoutDTO.getLayoutName(), location, createLayoutDTO.getStagePosition());
        List<SittingArea> sittingAreas = createLayoutDTO.getSittingAreas().stream().map(sittingAreaDTO ->
                new SittingArea(sittingAreaDTO.getRowNum(), sittingAreaDTO.getSeatNum(), layout, sittingAreaDTO.getLayoutPosition())).collect(Collectors.toList());
        List<StandingArea> standingAreas = createLayoutDTO.getStandingAreas().stream().map(standingAreaDTO ->
                new StandingArea(standingAreaDTO.getMaxNumber(), layout, standingAreaDTO.getLayoutPosition())).collect(Collectors.toList());
        layout.setStandingAreas(standingAreas);
        layout.setSittingAreas(sittingAreas);
        return layoutService.createLayout(layout);
    }
    @GetMapping("api/layout")
    public List<Layout> getAll(){
        return layoutService.getAll();
    }

    @GetMapping("/api/layout/{id}")
    public Layout getLayoutById(@PathVariable("id") Integer id){
        return layoutService.getLayoutById(id);
    }

    @GetMapping("/api/layout/{id}/sittingAreas")
    public List<SittingArea> getSittingAreasByLocationId(@PathVariable("id") Integer id){
        return layoutService.getSittingAreasByLayoutId(id);
    }

    @GetMapping("/api/layout/{id}/standingAreas")
    public List<StandingArea> getStandingAreasByLocationId(@PathVariable("id") Integer id){
        return layoutService.getStandingAreasByLayoutId(id);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/api/layout/{id}")
    public void deleteLayout(@PathVariable("id") Integer id) {
        layoutService.deleteLayout(id);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/api/layout/{id}")
    public Layout updateLayout(@PathVariable("id") Integer id, @Valid @RequestBody UpdateLayoutDTO updateLayoutDTO) {
        Layout layout = layoutService.getLayoutById(id);
        List<SittingArea> sittingAreas = updateLayoutDTO.getSittingAreas().stream().map(sittingAreaDTO ->
                new SittingArea(sittingAreaDTO.getRowNum(), sittingAreaDTO.getSeatNum(), layout, sittingAreaDTO.getLayoutPosition())).collect(Collectors.toList());
        List<StandingArea> standingAreas = updateLayoutDTO.getStandingAreas().stream().map(standingAreaDTO ->
                new StandingArea(standingAreaDTO.getMaxNumber(), layout, standingAreaDTO.getLayoutPosition())).collect(Collectors.toList());
        layout.setSittingAreas(sittingAreas);
        layout.setStandingAreas(standingAreas);
        return layoutService.updateLayout(id, layout);
    }

}
