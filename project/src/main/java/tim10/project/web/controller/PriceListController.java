package tim10.project.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tim10.project.model.*;
import tim10.project.service.EventService;
import tim10.project.service.LayoutService;
import tim10.project.service.PriceListService;
import tim10.project.service.TicketService;
import tim10.project.web.dto.layout.SittingAreaLayoutDTO;
import tim10.project.web.dto.layout.TicketSittingAreaDTO;
import tim10.project.web.dto.priceList.CreatePriceListDTO;
import tim10.project.web.dto.priceList.UpdatePriceListDTO;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class PriceListController {

    @Autowired
    private PriceListService priceListService;

    @Autowired
    private LayoutService layoutService;

    @Autowired
    private EventService eventService;

    @Autowired
    private TicketService ticketService;

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/api/pricelist")
    public PriceList createPriceList(@Valid @RequestBody CreatePriceListDTO createPriceListDTO) {
        Event event = eventService.getEventById(createPriceListDTO.getEventId());
        PriceList priceList = new PriceList(createPriceListDTO.getDate(), createPriceListDTO.getAreaType(), createPriceListDTO.getAreaId(),
                createPriceListDTO.getPrice(), event, new ArrayList<>());

        return priceListService.createPriceList(priceList);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/api/pricelist/{id}/layout")
    public Layout getLayoutByPriceListId(@PathVariable("id") Integer id){
        return layoutService.getLayoutByPriceListId(id);
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @GetMapping("/api/pricelist/{id}/sittingAreaLayout")
    public SittingAreaLayoutDTO getSittingAreaLayoutByPriceListId(@PathVariable("id") Integer id){
        Layout layout = layoutService.getLayoutByPriceListId(id);
        SittingAreaLayoutDTO sittingAreaLayoutDTO = new SittingAreaLayoutDTO();
        sittingAreaLayoutDTO.setId(layout.getId());
        sittingAreaLayoutDTO.setLayoutName(layout.getLayoutName());
        sittingAreaLayoutDTO.setStagePosition(layout.getStagePosition());
        sittingAreaLayoutDTO.setSittingAreas(layout.getSittingAreas());
        sittingAreaLayoutDTO.setBookedTickets(ticketService.getBookedTicketSittingAreaByPriceListId(id).stream().map(TicketSittingAreaDTO::new).collect(Collectors.toList()));
        return sittingAreaLayoutDTO;
    }

    @GetMapping("/api/pricelist")
    public List<PriceList> getAllPriceLists() {
        return priceListService.getAllPriceLists();
    }

    @GetMapping("/api/pricelist/{id}")
    public PriceList getPriceListById(@PathVariable("id") Integer id) {
        return priceListService.getPriceListById(id);
    }

    @GetMapping("/api/pricelist/{id}/tickets")
    public List<Ticket> getTicketsByPriceListId(@PathVariable("id") Integer id) {
        return priceListService.getTicketsByPriceListId(id);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/api/pricelist/{id}")
    public PriceList updatePriceList(@PathVariable("id") Integer id, @Valid @RequestBody UpdatePriceListDTO updatePriceListDTO) {
        PriceList priceList = priceListService.getPriceListById(id);
        priceList.setDate(updatePriceListDTO.getDate());
        priceList.setPrice(updatePriceListDTO.getPrice());
        return priceListService.updatePriceList(id, priceList);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/api/pricelist/{id}")
    public void deletePriceList(@PathVariable("id") Integer id) {
        priceListService.deletePriceList(id);
    }
}
