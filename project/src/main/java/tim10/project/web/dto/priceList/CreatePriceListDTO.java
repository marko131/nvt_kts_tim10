package tim10.project.web.dto.priceList;

import tim10.project.model.AreaType;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class CreatePriceListDTO {
    @NotNull
    private Date date;

    @NotNull
    private AreaType areaType;

    @NotNull
    private Integer areaId;

    @NotNull
    @Min(0)
    @Max(Long.MAX_VALUE)
    private Double price;

    @NotNull
    private Integer eventId;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public AreaType getAreaType() {
        return areaType;
    }

    public void setAreaType(AreaType areaType) {
        this.areaType = areaType;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }
}
