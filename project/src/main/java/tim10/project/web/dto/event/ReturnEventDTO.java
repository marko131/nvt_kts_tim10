package tim10.project.web.dto.event;

import tim10.project.model.Event;
import tim10.project.model.EventCategory;

public class ReturnEventDTO {

    private Integer id;
    private String name;
    private EventCategory eventCategory;
    private String description;
    private String picture;

    public ReturnEventDTO (String name, EventCategory eventCategory, String description, String picture){
        this.name = name;
        this.eventCategory = eventCategory;
        this.description = description;
        this.picture = picture;
    }
    public ReturnEventDTO(Event event){
        this.id = event.getId();
        this.name = event.getName();
        this.eventCategory = event.getEventCategory();
        this.description = event.getDescription();
        this.picture = event.getPicture();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EventCategory getEventCategory() {
        return eventCategory;
    }

    public void setEventCategory(EventCategory eventCategory) {
        this.eventCategory = eventCategory;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
