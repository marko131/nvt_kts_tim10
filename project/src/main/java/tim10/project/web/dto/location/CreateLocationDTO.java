package tim10.project.web.dto.location;

import javax.validation.constraints.*;
import java.math.BigDecimal;

public class CreateLocationDTO {

    @Size(max = 255, min = 1, message = "Name size should be between 1 and 255 characters")
    @NotNull
    private String name;

    @Size(max = 255, min = 1, message = "Name size should be between 1 and 255 characters")
    @NotNull
    private String address;

    @Digits(integer=2, fraction=6)
    @DecimalMin(value = "-90.000000", inclusive = true)
    @DecimalMax(value = "90.000000", inclusive = true)
    @NotNull
    private BigDecimal latitude;

    @Digits(integer=3, fraction=6)
    @DecimalMin(value = "-180.000000", inclusive = true)
    @DecimalMax(value = "180.000000", inclusive = true)
    @NotNull
    private BigDecimal longitude;

    private String picture;

    public CreateLocationDTO() { }

    public CreateLocationDTO(String name, String address, BigDecimal latitude, BigDecimal longitude, String picture) {
        this.name = name;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.picture = picture;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
