package tim10.project.web.dto.sittingArea;

import java.util.List;

public class ReserveSittingAreaDTO {
    private Integer priceListId;
    private List<SittingAreaTicketDTO> sittingAreaTicketDTOList;

    public ReserveSittingAreaDTO(Integer priceListId, List<SittingAreaTicketDTO> sittingAreaTicketDTOList) {
        this.priceListId = priceListId;
        this.sittingAreaTicketDTOList = sittingAreaTicketDTOList;
    }

    public ReserveSittingAreaDTO() {
    }

    public Integer getPriceListId() {
        return priceListId;
    }

    public void setPriceListId(Integer priceListId) {
        this.priceListId = priceListId;
    }

    public List<SittingAreaTicketDTO> getSittingAreaTicketDTOList() {
        return sittingAreaTicketDTOList;
    }

    public void setSittingAreaTicketDTOList(List<SittingAreaTicketDTO> sittingAreaTicketDTOList) {
        this.sittingAreaTicketDTOList = sittingAreaTicketDTOList;
    }
}
