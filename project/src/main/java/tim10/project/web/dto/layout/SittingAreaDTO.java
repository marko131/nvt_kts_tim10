package tim10.project.web.dto.layout;

import tim10.project.model.Position;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class SittingAreaDTO {
    @NotNull
    @Min(1)
    @Max(Integer.MAX_VALUE)
    private Integer rowNum;
    @Min(1)
    @Max(Integer.MAX_VALUE)
    private Integer seatNum;

    @NotNull
    private Position layoutPosition;

    public Integer getRowNum() {
        return rowNum;
    }

    public void setRowNum(Integer rowNum) {
        this.rowNum = rowNum;
    }

    public Integer getSeatNum() {
        return seatNum;
    }

    public void setSeatNum(Integer seatNum) {
        this.seatNum = seatNum;
    }

    public Position getLayoutPosition() {
        return layoutPosition;
    }

    public void setLayoutPosition(Position layoutPosition) {
        this.layoutPosition = layoutPosition;
    }
}
