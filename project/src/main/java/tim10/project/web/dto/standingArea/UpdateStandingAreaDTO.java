package tim10.project.web.dto.standingArea;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class UpdateStandingAreaDTO {
    @Min(1)
    @Max(Integer.MAX_VALUE)
    private Integer maxNumber;

    public Integer getMaxNumber() {
        return maxNumber;
    }

    public void setMaxNumber(Integer maxNumber) {
        this.maxNumber = maxNumber;
    }
}
