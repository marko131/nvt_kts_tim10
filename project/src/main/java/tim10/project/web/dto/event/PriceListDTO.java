package tim10.project.web.dto.event;

import com.fasterxml.jackson.annotation.JsonFormat;
import tim10.project.model.AreaType;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Date;

public class PriceListDTO {
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
    private Date date;
    private AreaType areaType;
    private Integer areaId;
    @Min(0)
    @Max(Long.MAX_VALUE)
    private Double price;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public AreaType getAreaType() {
        return areaType;
    }

    public void setAreaType(AreaType areaType) {
        this.areaType = areaType;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getPrice() {
        return price;
    }
}
