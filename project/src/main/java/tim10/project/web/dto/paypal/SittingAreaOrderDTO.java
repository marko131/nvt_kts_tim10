package tim10.project.web.dto.paypal;

import tim10.project.web.dto.sittingArea.ReserveSittingAreaDTO;

public class SittingAreaOrderDTO {
    private String orderID;
    private ReserveSittingAreaDTO reserveSittingAreaDTO;

    public SittingAreaOrderDTO(String orderID, ReserveSittingAreaDTO reserveSittingAreaDTO) {
        this.orderID = orderID;
        this.reserveSittingAreaDTO = reserveSittingAreaDTO;
    }

    public SittingAreaOrderDTO() {
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public ReserveSittingAreaDTO getReserveSittingAreaDTO() {
        return reserveSittingAreaDTO;
    }

    public void setReserveSittingAreaDTO(ReserveSittingAreaDTO reserveSittingAreaDTO) {
        this.reserveSittingAreaDTO = reserveSittingAreaDTO;
    }
}
