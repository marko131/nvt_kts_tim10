package tim10.project.web.dto.event;

import com.fasterxml.jackson.annotation.JsonFormat;
import tim10.project.model.EventCategory;

import javax.validation.constraints.Min;
import javax.validation.constraints.NegativeOrZero;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CreateEventDTO {
    @NotNull
    @Size(max = 255, min = 1, message = "Name size should be between 1 and 255 characters")
    private String name;

    @NotNull
    private EventCategory eventCategory;

    @Size(max = 255, min = 1, message = "Description size should be between 1 and 255 characters")
    private String description;
    private String picture;

    @NotNull
    @Min(value = 1)
    private Integer daysReservationIsValid;

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
    private Date reservationsAvailable;

    @Min(value = 1)
    private Integer maxReservationNumber;

    @NotNull
    private List<PriceListDTO> priceLists = new ArrayList<>();

    @NotNull
    private Integer layoutId;

    public List<PriceListDTO> getPriceLists() {
        return priceLists;
    }

    public void setPriceLists(List<PriceListDTO> priceLists) {
        this.priceLists = priceLists;
    }

    public Integer getLayoutId() {
        return layoutId;
    }

    public void setLayoutId(Integer layoutId) {
        this.layoutId = layoutId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EventCategory getEventCategory() {
        return eventCategory;
    }

    public void setEventCategory(EventCategory eventCategory) {
        this.eventCategory = eventCategory;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Date getReservationsAvailable() {
        return reservationsAvailable;
    }

    public void setReservationsAvailable(Date reservationsAvailable) {
        this.reservationsAvailable = reservationsAvailable;
    }

    public Integer getMaxReservationNumber() {
        return maxReservationNumber;
    }

    public void setMaxReservationNumber(Integer maxReservationNumber) {
        this.maxReservationNumber = maxReservationNumber;
    }

    public Integer getDaysReservationIsValid() {
        return daysReservationIsValid;
    }

    public void setDaysReservationIsValid(Integer daysReservationIsValid) {
        this.daysReservationIsValid = daysReservationIsValid;
    }
}
