package tim10.project.web.dto.layout;

import tim10.project.model.Position;
import tim10.project.model.SittingArea;
import tim10.project.model.TicketSittingArea;

import java.util.List;

public class SittingAreaLayoutDTO {
    private Integer id;
    private String layoutName;
    private Position stagePosition;
    private List<SittingArea> sittingAreas;
    private List<TicketSittingAreaDTO> bookedTickets;

    public SittingAreaLayoutDTO() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLayoutName() {
        return layoutName;
    }

    public void setLayoutName(String layoutName) {
        this.layoutName = layoutName;
    }

    public Position getStagePosition() {
        return stagePosition;
    }

    public void setStagePosition(Position stagePosition) {
        this.stagePosition = stagePosition;
    }

    public List<SittingArea> getSittingAreas() {
        return sittingAreas;
    }

    public void setSittingAreas(List<SittingArea> sittingAreas) {
        this.sittingAreas = sittingAreas;
    }

    public List<TicketSittingAreaDTO> getBookedTickets() {
        return bookedTickets;
    }

    public void setBookedTickets(List<TicketSittingAreaDTO> bookedTickets) {
        this.bookedTickets = bookedTickets;
    }
}
