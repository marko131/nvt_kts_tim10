package tim10.project.web.dto.standingArea;

public class ReserveStandingAreaDTO {
    private Integer priceListId;
    private Integer numberOfTickets;

    public ReserveStandingAreaDTO(Integer priceListId, Integer numberOfTickets) {
        this.priceListId = priceListId;
        this.numberOfTickets = numberOfTickets;
    }

    public ReserveStandingAreaDTO() {
    }

    public Integer getPriceListId() {
        return priceListId;
    }

    public void setPriceListId(Integer priceListId) {
        this.priceListId = priceListId;
    }

    public Integer getNumberOfTickets() {
        return numberOfTickets;
    }

    public void setNumberOfTickets(Integer numberOfTickets) {
        this.numberOfTickets = numberOfTickets;
    }
}
