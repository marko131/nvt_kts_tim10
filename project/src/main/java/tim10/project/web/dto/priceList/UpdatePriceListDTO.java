package tim10.project.web.dto.priceList;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class UpdatePriceListDTO {
    @NotNull
    private Date date;

    @NotNull
    @Min(0)
    @Max(Long.MAX_VALUE)
    private Double price;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getPrice() {
        return price;
    }
}
