package tim10.project.web.dto.layout;

import java.util.List;

public class UpdateLayoutDTO {
    List<SittingAreaDTO> sittingAreas;
    List<StandingAreaDTO> standingAreas;

    public List<SittingAreaDTO> getSittingAreas() {
        return sittingAreas;
    }

    public void setSittingAreas(List<SittingAreaDTO> sittingAreas) {
        this.sittingAreas = sittingAreas;
    }

    public List<StandingAreaDTO> getStandingAreas() {
        return standingAreas;
    }

    public void setStandingAreas(List<StandingAreaDTO> standingAreas) {
        this.standingAreas = standingAreas;
    }
}
