package tim10.project.web.dto.layout;

import tim10.project.model.Position;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class StandingAreaDTO {
    @Min(1)
    @Max(Integer.MAX_VALUE)
    private Integer maxNumber;

    @NotNull
    private Position layoutPosition;

    public Integer getMaxNumber() {
        return maxNumber;
    }

    public void setMaxNumber(Integer maxNumber) {
        this.maxNumber = maxNumber;
    }

    public Position getLayoutPosition() {
        return layoutPosition;
    }

    public void setLayoutPosition(Position layoutPosition) {
        this.layoutPosition = layoutPosition;
    }
}
