package tim10.project.web.dto.sittingArea;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class UpdateSittingAreaDTO {
    @Min(1)
    @Max(Integer.MAX_VALUE)
    private Integer rows;
    @Min(1)
    @Max(Integer.MAX_VALUE)
    private Integer colls;

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getColls() {
        return colls;
    }

    public void setColls(Integer colls) {
        this.colls = colls;
    }
}
