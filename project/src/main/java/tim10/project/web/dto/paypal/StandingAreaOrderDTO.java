package tim10.project.web.dto.paypal;

import tim10.project.web.dto.standingArea.ReserveStandingAreaDTO;

public class StandingAreaOrderDTO {
    private String orderID;
    private ReserveStandingAreaDTO reserveStandingAreaDTO;

    public StandingAreaOrderDTO(String orderID, ReserveStandingAreaDTO reserveStandingAreaDTO) {
        this.orderID = orderID;
        this.reserveStandingAreaDTO = reserveStandingAreaDTO;
    }

    public StandingAreaOrderDTO() {
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public ReserveStandingAreaDTO getReserveStandingAreaDTO() {
        return reserveStandingAreaDTO;
    }

    public void setReserveStandingAreaDTO(ReserveStandingAreaDTO reserveStandingAreaDTO) {
        this.reserveStandingAreaDTO = reserveStandingAreaDTO;
    }
}
