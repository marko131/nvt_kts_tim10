package tim10.project.web.dto.priceList;

import tim10.project.model.AreaType;
import tim10.project.model.Position;
import tim10.project.model.PriceList;

import java.util.Date;

public class ReturnPriceListDTO {

    private Integer id;
    private Date date;
    private AreaType areaType;
    private Integer areaId;
    private Double price;
    private Position position;

    public ReturnPriceListDTO(PriceList priceList, Position position) {
        this.id = priceList.getId();
        this.date = priceList.getDate();
        this.areaType = priceList.getAreaType();
        this.areaId = priceList.getAreaId();
        this.price = priceList.getPrice();
        this.position = position;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public AreaType getAreaType() {
        return areaType;
    }

    public void setAreaType(AreaType areaType) {
        this.areaType = areaType;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
}
