package tim10.project.web.dto.ticket;

import java.util.List;

public class BuyReservedTicketDTO {
    private String orderID;
    private List<Integer> reservedTickets;

    public BuyReservedTicketDTO() {
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public List<Integer> getReservedTickets() {
        return reservedTickets;
    }

    public void setReservedTickets(List<Integer> reservedTickets) {
        this.reservedTickets = reservedTickets;
    }
}
