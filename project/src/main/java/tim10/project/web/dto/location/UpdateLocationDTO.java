package tim10.project.web.dto.location;


import javax.validation.constraints.Size;

public class UpdateLocationDTO {
    @Size(max = 255, min = 1, message = "Name size should be between 1 and 255 characters")
    private String name;
    @Size(max = 255, min = 1, message = "Name size should be between 1 and 255 characters")
    private String address;
    private String picture;

    public UpdateLocationDTO(String name, String address, String picture) {
        this.name = name;
        this.address = address;
        this.picture = picture;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
