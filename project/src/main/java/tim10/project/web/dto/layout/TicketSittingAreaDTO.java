package tim10.project.web.dto.layout;

import tim10.project.model.TicketSittingArea;

public class TicketSittingAreaDTO {
    private Integer rowNum;
    private Integer seatNum;

    public TicketSittingAreaDTO(TicketSittingArea ticketSittingArea) {
        this.rowNum = ticketSittingArea.getRowNum();
        this.seatNum = ticketSittingArea.getSeatNum();
    }

    public Integer getRowNum() {
        return rowNum;
    }

    public void setRowNum(Integer rowNum) {
        this.rowNum = rowNum;
    }

    public Integer getSeatNum() {
        return seatNum;
    }

    public void setSeatNum(Integer seatNum) {
        this.seatNum = seatNum;
    }
}
