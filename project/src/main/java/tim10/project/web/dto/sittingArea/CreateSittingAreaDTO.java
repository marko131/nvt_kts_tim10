package tim10.project.web.dto.sittingArea;

import tim10.project.model.Position;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class CreateSittingAreaDTO
{
    @Min(1)
    @Max(Integer.MAX_VALUE)
    private Integer rows;
    @Min(1)
    @Max(Integer.MAX_VALUE)
    private Integer colls;

    @NotNull
    private Integer layoutId;

    @NotNull
    private Position layoutPosition;

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getColls() {
        return colls;
    }

    public void setColls(Integer colls) {
        this.colls = colls;
    }

    public Integer getLayoutId() {
        return layoutId;
    }

    public void setLayoutId(Integer layoutId) {
        this.layoutId = layoutId;
    }

    public Position getLayoutPosition() {
        return layoutPosition;
    }

    public void setLayoutPosition(Position layoutPosition) {
        this.layoutPosition = layoutPosition;
    }
}
