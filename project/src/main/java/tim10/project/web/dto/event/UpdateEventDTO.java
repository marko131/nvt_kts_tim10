package tim10.project.web.dto.event;

import com.fasterxml.jackson.annotation.JsonFormat;
import tim10.project.model.EventCategory;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

public class UpdateEventDTO {
    @NotNull
    @Size(max = 255, min = 1, message = "Name size should be between 1 and 255 characters")
    private String name;

    @NotNull
    private EventCategory eventCategory;

    @Size(max = 255, min = 1, message = "Description size should be between 1 and 255 characters")
    private String description;

    @NotNull
    private String picture;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EventCategory getEventCategory() {
        return eventCategory;
    }

    public void setEventCategory(EventCategory eventCategory) {
        this.eventCategory = eventCategory;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
