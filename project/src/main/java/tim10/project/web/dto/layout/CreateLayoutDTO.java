package tim10.project.web.dto.layout;

import tim10.project.model.Position;
import tim10.project.model.StandingArea;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

public class CreateLayoutDTO {
    @NotNull
    List<SittingAreaDTO> sittingAreas;

    @NotNull
    List<StandingAreaDTO> standingAreas;

    @NotNull
    Integer locationId;

    @NotNull
    Position stagePosition;

    @NotEmpty
    String layoutName;

    public List<SittingAreaDTO> getSittingAreas() {
        return sittingAreas;
    }

    public void setSittingAreas(List<SittingAreaDTO> sittingAreas) {
        this.sittingAreas = sittingAreas;
    }

    public List<StandingAreaDTO> getStandingAreas() {
        return standingAreas;
    }

    public void setStandingAreas(List<StandingAreaDTO> standingAreas) {
        this.standingAreas = standingAreas;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public Position getStagePosition() {
        return stagePosition;
    }

    public void setStagePosition(Position stagePosition) {
        this.stagePosition = stagePosition;
    }

    public String getLayoutName() {
        return layoutName;
    }

    public void setLayoutName(String layoutName) {
        this.layoutName = layoutName;
    }
}
