package tim10.project.web.dto.sittingArea;

public class SittingAreaTicketDTO {
    private Integer rowNum;
    private Integer seatNum;

    public SittingAreaTicketDTO(Integer rowNum, Integer seatNum) {
        this.rowNum = rowNum;
        this.seatNum = seatNum;
    }

    public SittingAreaTicketDTO() {
    }

    public Integer getRowNum() {
        return rowNum;
    }

    public void setRowNum(Integer rowNum) {
        this.rowNum = rowNum;
    }

    public Integer getSeatNum() {
        return seatNum;
    }

    public void setSeatNum(Integer seatNum) {
        this.seatNum = seatNum;
    }
}
