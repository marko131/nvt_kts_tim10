package tim10.project.model;

public enum EventCategory {
    CONCERT, FESTIVAL, THEATER, SPORT, ENTERTAINMENT, OTHER
}
