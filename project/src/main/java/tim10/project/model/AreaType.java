package tim10.project.model;

public enum AreaType {
    SITTING_AREA, STANDING_AREA
}
