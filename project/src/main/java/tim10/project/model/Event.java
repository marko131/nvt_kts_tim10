package tim10.project.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Integer id;

    @Column(unique = true, nullable = false)
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private EventCategory eventCategory;

    private String description;

    private String picture;

    @Column(nullable = false)
    private Integer daysReservationIsValid;

    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    private Date reservationsAvailable;

    @Column(nullable = false)
    private Integer maxReservationNumber;

    @JsonManagedReference("event-priceList")
    @OneToMany(cascade={CascadeType.ALL}, fetch=FetchType.LAZY, mappedBy="event")
    private List<PriceList> priceLists = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "layout", referencedColumnName = "id", nullable = false)
    private Layout layout;

    public Event() { /* empty constructor */ }

    public Event(String name, EventCategory eventCategory, String description, String picture, Integer daysReservationIsValid, Date reservationsAvailable, Integer maxReservationNumber, List<PriceList> priceLists, Layout layout) {
        this.name = name;
        this.eventCategory = eventCategory;
        this.description = description;
        this.picture = picture;
        this.daysReservationIsValid = daysReservationIsValid;
        this.reservationsAvailable = reservationsAvailable;
        this.maxReservationNumber = maxReservationNumber;
        this.priceLists = priceLists;
        this.layout = layout;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EventCategory getEventCategory() {
        return eventCategory;
    }

    public void setEventCategory(EventCategory eventCategory) {
        this.eventCategory = eventCategory;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Integer getDaysReservationIsValid() {
        return daysReservationIsValid;
    }

    public void setDaysReservationIsValid(Integer daysReservationIsValid) {
        this.daysReservationIsValid = daysReservationIsValid;
    }

    public Date getReservationsAvailable() {
        return reservationsAvailable;
    }

    public void setReservationsAvailable(Date reservationsAvailable) {
        this.reservationsAvailable = reservationsAvailable;
    }

    public Integer getMaxReservationNumber() {
        return maxReservationNumber;
    }

    public void setMaxReservationNumber(Integer maxReservationNumber) {
        this.maxReservationNumber = maxReservationNumber;
    }

    public List<PriceList> getPriceLists() {
        return priceLists;
    }

    public void setPriceLists(List<PriceList> priceLists) {
        this.priceLists = priceLists;
    }

    public Layout getLayout() {
        return layout;
    }

    public void setLayout(Layout layout) {
        this.layout = layout;
    }

    @Override
    public String toString() {
        return this.getName();
    }
}
