package tim10.project.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table
public class PriceList {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Integer id;


    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    @NotNull
    private Date date;

    @Column(nullable = false)
    @NotNull
    private AreaType areaType;

    @Column(nullable = false)
    @NotNull
    private Integer areaId;

    @Column(nullable = false)
    @NotNull
    private Double price;

    @JsonBackReference("event-priceList")
    @ManyToOne()
    @JoinColumn(name = "event", referencedColumnName = "id", nullable = false)
    @NotNull
    private Event event;

    @OneToMany(cascade={CascadeType.ALL}, fetch=FetchType.LAZY, mappedBy="priceList")
    @NotNull
    private List<Ticket> tickets = new ArrayList<>();

    public PriceList() { /* empty constructor */ }

    public PriceList(Date date, AreaType areaType, Integer areaId, Double price, Event event, List<Ticket> tickets) {
        this.date = date;
        this.areaType = areaType;
        this.areaId = areaId;
        this.price = price;
        this.event = event;
        this.tickets = tickets;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public AreaType getAreaType() {
        return areaType;
    }

    public void setAreaType(AreaType areaType) {
        this.areaType = areaType;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }
}
