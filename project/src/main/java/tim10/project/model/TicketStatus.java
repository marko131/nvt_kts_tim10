package tim10.project.model;

public enum TicketStatus {
    AVAILABLE, IS_RESERVED, IS_SOLD
}
