package tim10.project.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Integer id;

    @Column(unique = true, nullable = false)
    @Size(max = 255, min = 1, message = "Name size should be between 1 and 255 characters")
    private String name;

    @Column(nullable = false)
    @Size(max = 255, min = 1, message = "Name size should be between 1 and 255 characters")
    private String address;

    @Digits(integer=3, fraction=6)
    @DecimalMin(value = "-90.000000", inclusive = true)
    @DecimalMax(value = "90.000000", inclusive = true)
    private BigDecimal latitude;

    @Digits(integer=3, fraction=6)
    @DecimalMin(value = "-180.000000", inclusive = true)
    @DecimalMax(value = "180.000000", inclusive = true)
    private BigDecimal longitude;

    private String picture;

    @JsonManagedReference("location-layouts")
    @OneToMany(cascade={CascadeType.ALL}, fetch=FetchType.LAZY)
    private List<Layout> layouts = new ArrayList<>();

    public Location() { /* empty constructor */ }

    public Location(String name, String address, BigDecimal latitude, BigDecimal longitude, String picture, List<Layout> layouts) {
        this.name = name;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.picture = picture;
        this.layouts = layouts;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public List<Layout> getLayouts() {
        return layouts;
    }

    public void setLayouts(List<Layout> layouts) {
        this.layouts = layouts;
    }
}
