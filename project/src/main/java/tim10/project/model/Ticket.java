package tim10.project.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private TicketStatus ticketStatus;

    @ManyToOne()
    @JoinColumn(name = "priceList", referencedColumnName = "id", nullable = false)
    private PriceList priceList;

    @Column(nullable = false)
    private String section;

    @ManyToOne
    private User user;

    @Temporal(TemporalType.DATE)
    private Date timestamp;

    public Ticket() { /* empty constructor */ }

    public Ticket(PriceList priceList, String section) {
        this.priceList = priceList;
        this.ticketStatus = TicketStatus.AVAILABLE;
        this.section = section;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TicketStatus getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(TicketStatus ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

    public PriceList getPriceList() {
        return priceList;
    }

    public void setPriceList(PriceList priceList) {
        this.priceList = priceList;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}
