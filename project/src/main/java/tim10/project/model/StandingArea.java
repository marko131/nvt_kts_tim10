package tim10.project.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table
public class StandingArea {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Integer id;

    @Column(nullable = false)
    @NotNull
    @Min(1)
    @Max(Integer.MAX_VALUE)
    private Integer maxNumber;

    @JsonBackReference("layout-standingArea")
    @ManyToOne
    @NotNull
    private Layout layout;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Position layoutPosition;

    public StandingArea() { /* empty constructor */ }

    public StandingArea(Integer maxNumber, Layout layout, Position layoutPosition) {
        this.maxNumber = maxNumber;
        this.layout = layout;
        this.layoutPosition = layoutPosition;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMaxNumber() {
        return maxNumber;
    }

    public void setMaxNumber(Integer maxNumber) {
        this.maxNumber = maxNumber;
    }

    public Layout getLayout() {
        return layout;
    }

    public void setLayout(Layout layout) {
        this.layout = layout;
    }

    public Position getLayoutPosition() {
        return layoutPosition;
    }

    public void setLayoutPosition(Position layoutPosition) {
        this.layoutPosition = layoutPosition;
    }
}
