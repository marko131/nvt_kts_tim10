package tim10.project.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class Layout {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Integer id;

    @Column(nullable = false)
    private String layoutName;

    @JsonManagedReference("layout-standingArea")
    @OneToMany(cascade={CascadeType.ALL}, fetch=FetchType.LAZY)
    private List<StandingArea> standingAreas = new ArrayList<>();

    @JsonManagedReference("layout-sittingArea")
    @OneToMany(cascade={CascadeType.ALL}, fetch=FetchType.LAZY)
    private List<SittingArea> sittingAreas = new ArrayList<>();

    @JsonBackReference("location-layouts")
    @ManyToOne
    private Location location;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Position stagePosition;

    public Layout() { /* empty constructor */ }

    public Layout(List<StandingArea> standingAreas, List<SittingArea> sittingAreas, String name, Location location, Position stagePosition) {
        this.standingAreas = standingAreas;
        this.sittingAreas = sittingAreas;
        this.layoutName = name;
        this.location = location;
        this.stagePosition = stagePosition;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<StandingArea> getStandingAreas() {
        return standingAreas;
    }

    public void setStandingAreas(List<StandingArea> standingAreas) {
        this.standingAreas = standingAreas;
    }

    public List<SittingArea> getSittingAreas() {
        return sittingAreas;
    }

    public void setSittingAreas(List<SittingArea> sittingAreas) {
        this.sittingAreas = sittingAreas;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Position getStagePosition() {
        return stagePosition;
    }

    public void setStagePosition(Position stagePosition) {
        this.stagePosition = stagePosition;
    }

    public String getLayoutName() {
        return layoutName;
    }

    public void setLayoutName(String layoutName) {
        this.layoutName = layoutName;
    }
}
