package tim10.project.model;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class TicketSittingArea extends Ticket {
    @Column(columnDefinition = "integer default 0")
    private Integer rowNum;

    @Column(columnDefinition = "integer default 0")
    private Integer seatNum;

    public TicketSittingArea() { /* empty constructor */ }

    public TicketSittingArea(PriceList priceList, String section, Integer rowNum, Integer seatNum) {
        super(priceList, section);
        this.rowNum = rowNum;
        this.seatNum = seatNum;
    }

    public Integer getRowNum() {
        return rowNum;
    }

    public void setRowNum(Integer rowNum) {
        this.rowNum = rowNum;
    }

    public Integer getSeatNum() {
        return seatNum;
    }

    public void setSeatNum(Integer seatNum) {
        this.seatNum = seatNum;
    }
}
