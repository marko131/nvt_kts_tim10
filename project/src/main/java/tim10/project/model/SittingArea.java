package tim10.project.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table
public class SittingArea {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Integer id;

    @Column(nullable = false)
    @NotNull
    @Min(1)
    @Max(Integer.MAX_VALUE)
    private Integer numRows;

    @Column(nullable = false)
    @NotNull
    @Min(1)
    @Max(Integer.MAX_VALUE)
    private Integer numSeats;

    @JsonBackReference("layout-sittingArea")
    @ManyToOne
    @NotNull
    private Layout layout;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Position layoutPosition;

    public SittingArea() { /* empty constructor */ }
    public SittingArea(Integer numRows, Integer numSeats, Layout layout, Position layoutPosition) {
        this.numRows = numRows;
        this.numSeats = numSeats;
        this.layout = layout;
        this.layoutPosition = layoutPosition;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumRows() {
        return numRows;
    }

    public void setNumRows(Integer numRows) {
        this.numRows = numRows;
    }

    public Integer getNumSeats() {
        return numSeats;
    }

    public void setNumSeats(Integer numSeats) {
        this.numSeats = numSeats;
    }

    public Layout getLayout() {
        return layout;
    }

    public void setLayout(Layout layout) {
        this.layout = layout;
    }

    public Position getLayoutPosition() {
        return layoutPosition;
    }

    public void setLayoutPosition(Position layoutPosition) {
        this.layoutPosition = layoutPosition;
    }
}
