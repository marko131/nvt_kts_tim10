package tim10.project.scheduling_controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import tim10.project.model.PriceList;
import tim10.project.model.Ticket;
import tim10.project.model.TicketStatus;
import tim10.project.service.MailService;
import tim10.project.service.PriceListService;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Controller
public class MailRemindersController {

    @Autowired
    private MailService mailService;

    @Autowired
    private PriceListService priceListService;

    @Scheduled(cron = "${reservation.cron}")
    public void sendReservationExpiresReminder() {
        List<PriceList> priceLists = priceListService.getAllPriceLists();

        for (PriceList priceList: priceLists) {
            if (compareDates(priceList.getDate(), priceList.getEvent().getDaysReservationIsValid())) {
                List<Ticket> tickets = priceListService.getTicketsByPriceListId(priceList.getId());

                for (Ticket ticket : tickets) {
                    if (ticket.getTicketStatus() == TicketStatus.IS_RESERVED) {
                        String content = "Your ticket reservation is expiring in " + priceList.getEvent().getDaysReservationIsValid() + " days!";
                        mailService.sendMail(ticket.getUser(), "Reservation expiring reminder", content);
                    }
                }
            }
        }
    }

    @Scheduled(cron = "${event.cron}")
    public void sendEventReminder() {
        List<PriceList> priceLists = priceListService.getAllPriceLists();

        for (PriceList priceList: priceLists) {
            if (compareDates(priceList.getDate(), 1)) {
                List<Ticket> tickets = priceListService.getTicketsByPriceListId(priceList.getId());

                for (Ticket ticket : tickets) {
                    if (ticket.getTicketStatus() == TicketStatus.IS_SOLD) {
                        String content = "Don't forget your event tommorrow!";
                        mailService.sendMail(ticket.getUser(), "Event reminder", content);
                    }
                }
            }
        }
    }

    private boolean compareDates(Date date, int dayDiff) {
        Calendar c1 = Calendar.getInstance();
        c1.add(Calendar.DAY_OF_YEAR, + dayDiff);

        Calendar c2 = Calendar.getInstance();
        c2.setTime(date);

        if (c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR)
                && c1.get(Calendar.DAY_OF_YEAR) == c2.get(Calendar.DAY_OF_YEAR)) {
            return true;
        }
        return false;
    }
}
