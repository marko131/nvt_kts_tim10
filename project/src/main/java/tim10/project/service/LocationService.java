package tim10.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import tim10.project.model.Layout;
import tim10.project.model.Location;
import tim10.project.repository.LayoutRepository;
import tim10.project.repository.LocationRepository;
import tim10.project.service.exceptions.DatabaseSaveException;
import tim10.project.service.exceptions.NotFoundException;
import tim10.project.service.exceptions.UniqueViolationException;

import java.util.List;

@Service
public class LocationService {

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private LayoutRepository layoutRepository;

    //CREATE
    public Location createLocation(Location location) {
        try {
            return locationRepository.save(location);
        } catch (DataIntegrityViolationException e) {
            if (e.getMessage() != null) {
                if (e.getMessage().contains("UK")) {
                    throw new UniqueViolationException("Location name must be unique!");
                } else {
                    throw new DatabaseSaveException("An error occurred while saving location into database");
                }
            } else {
                throw new DatabaseSaveException("An error occurred while saving location into database");
            }
        }
    }

    //READ
    public Location getLocationById(Integer id) {
        return locationRepository.findById(id).orElseThrow(() -> new NotFoundException(String.format("Location with id: %d does not exist", id)));
    }

    public List<Location> getAllLocations() {
        return locationRepository.findAll();
    }

    public List<Layout> getLayoutsByLocationId(Integer id) {
        locationRepository.findById(id).orElseThrow(() -> new NotFoundException(String.format("Location with id: %d does not exist", id)));
        return layoutRepository.findAllByLocationId(id);
    }

    //UPDATE
    public Location updateLocation(Integer id, Location location) {
        Location locationFromDatabase = locationRepository.findById(id).orElseThrow(() -> new NotFoundException(String.format("Location with id: %d does not exist", id)));

        locationFromDatabase.setAddress(location.getAddress());
        locationFromDatabase.setName(location.getName());
        locationFromDatabase.setPicture(location.getPicture());
        return locationRepository.save(locationFromDatabase);
    }

    //DELETE
    public void deleteLocation(Integer id) {
        locationRepository.findById(id).orElseThrow(() -> new NotFoundException(String.format("Location with id: %d does not exist", id)));
        locationRepository.deleteById(id);
    }
}
