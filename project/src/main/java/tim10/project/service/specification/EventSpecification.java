package tim10.project.service.specification;

import org.springframework.data.jpa.domain.Specification;
import tim10.project.model.*;

import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import java.util.Date;
import java.util.Arrays;
import java.util.List;

public class EventSpecification {
    public static Specification<Event> eventNameContains(String name) {
        return ((root, criteriaQuery, criteriaBuilder) -> {
            String name1 = name == null ? "" : name;
            return criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), "%" + name1.toLowerCase() + "%");
        });
    }

    public static Specification<Event> eventDateAfter(Date date) {
        return ((root, criteriaQuery, criteriaBuilder) -> {
            Join<Event, PriceList> eventPriceList = root.join("priceLists");
            Date date1 = date == null ? new Date(0) : date;
            return criteriaBuilder.greaterThanOrEqualTo(eventPriceList.get("date"), date1);

        });
    }

    public static Specification<Event> eventDateBefore(Date date) {
        return ((root, criteriaQuery, criteriaBuilder) -> {
            Join<Event, PriceList> eventPriceList = root.join("priceLists");
            Date date1 = date == null ? new Date(4102441200000L) : date;
            return criteriaBuilder.lessThanOrEqualTo(eventPriceList.get("date").as(Date.class), date1);
        });
    }

    public static Specification<Event> eventLocation(String location) {
        return ((root, criteriaQuery, criteriaBuilder) -> {
            Join<Event, Layout> eventLayoutJoin = root.join("layout");
            Join<Layout, Location> layoutLocationJoin = eventLayoutJoin.join("location");
            String location1 = location == null ? "" : location;
            return criteriaBuilder.like(criteriaBuilder.lower(layoutLocationJoin.get("name")), "%" + location1.toLowerCase() + "%");
        });
    }

    public static Specification<Event> eventCategories(List<EventCategory> categories) {
        return ((root, criteriaQuery, criteriaBuilder) -> {
            List<EventCategory> eventCategories = categories == null ? Arrays.asList(EventCategory.values()) : categories;
            return root.get("eventCategory").in(eventCategories);});
    }
}
