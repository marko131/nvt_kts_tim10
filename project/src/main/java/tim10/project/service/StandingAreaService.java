package tim10.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tim10.project.model.Layout;
import tim10.project.model.StandingArea;
import tim10.project.repository.LayoutRepository;
import tim10.project.repository.StandingAreaRepository;
import tim10.project.service.exceptions.InvalidUpdateInputException;
import tim10.project.service.exceptions.NotFoundException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Service
public class StandingAreaService {

    @Autowired
    private StandingAreaRepository standingAreaRepository;

    @Autowired
    private LayoutRepository layoutRepository;

    //CREATE
    public StandingArea createStandingArea(@Valid StandingArea standingArea){
        Optional<Layout> layout = layoutRepository.findById(standingArea.getLayout().getId());
        if (layout.isEmpty()) throw new NotFoundException(String.format("Layout with id:%d does not exist", standingArea.getLayout().getId()));
        return standingAreaRepository.save(standingArea);
    }

    //READ
    public StandingArea getStandingAreaById(Integer id){
        StandingArea standingArea = standingAreaRepository.findById(id).orElse(null);
        if (standingArea == null) throw new NotFoundException(String.format("Standing area with id:%d does not exist", id));
        return standingArea;
    }

    public List<StandingArea> getStandingAreasByLayoutId(Integer id){
        if (layoutRepository.findById(id).isEmpty()) throw new NotFoundException(String.format("Layout with id:%d does not exist", id));
        return standingAreaRepository.getAllByLayoutId(id);
    }

    public List<StandingArea> getAllStandingAreas(){
        return standingAreaRepository.findAll();
    }

    //UPDATE
    public StandingArea updateStandingArea(Integer id, @Valid StandingArea standingArea){
        if (standingArea.getMaxNumber() == null) throw new InvalidUpdateInputException(String.format("Invalid update information"));
        StandingArea standingAreaFromDatabase = standingAreaRepository.findById(id).orElse(null);
        if (standingAreaFromDatabase == null) throw new NotFoundException(String.format("Standing area with id:%d does not exist", id));
        standingAreaFromDatabase.setMaxNumber(standingArea.getMaxNumber());
        return standingAreaRepository.save(standingAreaFromDatabase);
    }

    //DELETE
    public void deleteStandingArea(Integer id){
        StandingArea standingArea = standingAreaRepository.findById(id).orElse(null);
        if (standingArea == null) throw new NotFoundException(String.format("Standing area with id:%d does not exist", id));
        standingAreaRepository.deleteById(id);
    }
}
