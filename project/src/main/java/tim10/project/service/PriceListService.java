package tim10.project.service;

import org.hibernate.PropertyValueException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import tim10.project.model.*;
import tim10.project.repository.PriceListRepository;
import tim10.project.repository.SittingAreaRepository;
import tim10.project.repository.StandingAreaRepository;
import tim10.project.repository.TicketRepository;
import tim10.project.service.exceptions.DatabaseSaveException;
import tim10.project.service.exceptions.InvalidAreaException;
import tim10.project.service.exceptions.InvalidDateException;
import tim10.project.service.exceptions.NotFoundException;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;

@Service
@Validated
public class PriceListService {

    @Autowired
    private PriceListRepository priceListRepository;

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private StandingAreaRepository standingAreaRepository;

    @Autowired
    private SittingAreaRepository sittingAreaRepository;

    //CREATE
    public PriceList createPriceList(@Valid PriceList priceList) {
        if (priceList.getDate().before(new Date())) {
            throw new InvalidDateException("Invalid date for PriceList");
        }

        if (priceList.getAreaType() == AreaType.STANDING_AREA) {
            StandingArea area = standingAreaRepository.findById(priceList.getAreaId()).orElseThrow(() -> new NotFoundException(String.format("Standing area with id: %d does not exist", priceList.getAreaId())));

            if (!priceList.getEvent().getLayout().getStandingAreas().contains(area) ) {
                throw new InvalidAreaException(String.format("Standing area with id = %d  can not be applied on chosen event with id = %d", area.getId(), priceList.getEvent().getId()));
            }
        } else {
            SittingArea area = sittingAreaRepository.findById(priceList.getAreaId()).orElseThrow(() -> new NotFoundException(String.format("Sitting area with id: %d does not exist", priceList.getAreaId())));

            if (!priceList.getEvent().getLayout().getSittingAreas().contains(area) ) {
                throw new InvalidAreaException(String.format("Sitting area with id = %d  can not be applied on chosen event with id = %d", area.getId(), priceList.getEvent().getId()));
            }
        }

        return priceListRepository.save(priceList);
    }

    //READ
    public List<PriceList> getAllPriceLists() {
        return priceListRepository.findAll();
    }

    public PriceList getPriceListById(Integer id) {
        return priceListRepository.findById(id).orElseThrow(() -> new NotFoundException(String.format("Price List with id: %d does not exist", id)));
    }

    public List<Ticket> getTicketsByPriceListId(Integer id) {
        priceListRepository.findById(id).orElseThrow(() -> new NotFoundException(String.format("Price List with id: %d does not exist", id)));
        return ticketRepository.findAllByPriceListId(id);
    }

    //UPDATE
    public PriceList updatePriceList(Integer id, @Valid PriceList priceList) {
        PriceList priceListFromDatabase =  priceListRepository.findById(id).orElseThrow(() -> new NotFoundException(String.format("Price List with id: %d does not exist", id)));

        if (priceList.getDate().before(new Date())) {
            throw new InvalidDateException("Invalid date for PriceList");
        }

        if (priceList.getAreaType() == AreaType.STANDING_AREA) {
            StandingArea area = standingAreaRepository.findById(priceList.getAreaId()).orElseThrow(() -> new NotFoundException(String.format("Standing area with id: %d does not exist", priceList.getAreaId())));

            if (!priceList.getEvent().getLayout().getStandingAreas().contains(area) ) {
                throw new InvalidAreaException(String.format("Standing area with id = %d  can not be applied on chosen event with id = %d", area.getId(), priceList.getEvent().getId()));
            }
        } else {
            SittingArea area = sittingAreaRepository.findById(priceList.getAreaId()).orElseThrow(() -> new NotFoundException(String.format("Sitting area with id: %d does not exist", priceList.getAreaId())));

            if (!priceList.getEvent().getLayout().getSittingAreas().contains(area) ) {
                throw new InvalidAreaException(String.format("Sitting area with id = %d  can not be applied on chosen event with id = %d", area.getId(), priceList.getEvent().getId()));
            }
        }

        priceListFromDatabase.setDate(priceList.getDate());
        priceListFromDatabase.setAreaType(priceList.getAreaType());
        priceListFromDatabase.setAreaId(priceList.getAreaId());
        priceListFromDatabase.setPrice(priceList.getPrice());
        return priceListRepository.save(priceListFromDatabase);
    }

    //DELETE
    public void deletePriceList(Integer id) {
        priceListRepository.findById(id).orElseThrow(() -> new NotFoundException(String.format("Price List with id: %d does not exist", id)));
        priceListRepository.deleteById(id);
    }



}
