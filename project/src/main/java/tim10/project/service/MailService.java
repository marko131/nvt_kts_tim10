package tim10.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMailMessage;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import tim10.project.model.User;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Service
public class MailService {
    @Autowired
    private JavaMailSender mailSender;
    @Autowired
    private Environment env;

    @Async
    public void sendMail(User user, String subject, String content) {
        SimpleMailMessage message = new SimpleMailMessage();

        message.setFrom(env.getProperty("spring.mail.username"));
        message.setSubject(subject);
        message.setTo(user.getEmail());
        message.setText(content);

        mailSender.send(message);
    }

    @Async
    public void sendMailAsHTML(User user, String subject, String content) throws MessagingException {
        Properties properties = System.getProperties();
        properties.setProperty("mail.smtp.host", "spring.mail.username");
        properties.put("mail.smtp.auth", "true");
        Session session = Session.getDefaultInstance(properties,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(env.getProperty("spring.mail.username"), env.getProperty("spring.mail.password"));
                    }
                });
        MimeMessage message = new MimeMessage(session);

        message.setSubject(subject);
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(user.getEmail()));
        message.setContent(content, "text/html");

        mailSender.send(message);
    }
}