package tim10.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tim10.project.model.Layout;
import tim10.project.model.SittingArea;
import tim10.project.repository.LayoutRepository;
import tim10.project.repository.SittingAreaRepository;
import tim10.project.service.exceptions.InvalidAreaException;
import tim10.project.service.exceptions.InvalidUpdateInputException;
import tim10.project.service.exceptions.NotFoundException;

import java.util.List;
import java.util.Optional;

@Service
public class SittingAreaService {

    @Autowired
    private SittingAreaRepository sittingAreaRepository;

    @Autowired
    private LayoutRepository layoutRepository;

    //CREATE
    public SittingArea createSittingArea(SittingArea sittingArea){
        Optional<Layout> layout = layoutRepository.findById(sittingArea.getLayout().getId());
        if (layout.isEmpty()) throw new NotFoundException(String.format("Layout with id:%d does not exist", sittingArea.getLayout().getId()));
        return  sittingAreaRepository.save(sittingArea);
    }
    //READ
    public SittingArea getSittingAreaById(Integer id){
        SittingArea sittingArea = sittingAreaRepository.findById(id).orElse(null);
        if (sittingArea == null) throw new NotFoundException(String.format("Sitting area with id:%d does not exist", id));
        return sittingArea;
    }

    public List<SittingArea> getSittingAreasByLayoutId(Integer id){
        if (layoutRepository.findById(id).isEmpty()) throw new NotFoundException(String.format("Layout with id:%d does not exist", id));
        return sittingAreaRepository.getAllByLayoutId(id);
    }

    public List<SittingArea> getAllSittingAreas(){
        return sittingAreaRepository.findAll();
    }
    //UPDATE
    public SittingArea updateSittingArea(Integer id, SittingArea sittingArea){
        SittingArea sittingAreaFromDatabase = sittingAreaRepository.findById(id).orElse(null);
        if (sittingArea.getNumRows() == null) throw new InvalidUpdateInputException("Invalid update information");
        if (sittingAreaFromDatabase == null) throw new NotFoundException(String.format("Sitting area with id:%d does not exist", id));
        sittingAreaFromDatabase.setNumRows(sittingArea.getNumRows());
        sittingAreaFromDatabase.setNumSeats(sittingArea.getNumSeats());
        return sittingAreaRepository.save(sittingAreaFromDatabase);
    }

    //DELETE
    public void deleteSittingArea(Integer id){
        SittingArea sittingArea = sittingAreaRepository.findById(id).orElse(null);
        if (sittingArea == null) throw new NotFoundException(String.format("Sitting area with id:%d does not exist", id));
        sittingAreaRepository.deleteById(id);
    }
}
