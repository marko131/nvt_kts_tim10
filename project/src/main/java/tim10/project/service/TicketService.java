package tim10.project.service;

import org.eclipse.jetty.util.DateCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tim10.project.model.*;
import tim10.project.repository.EventRepository;
import tim10.project.repository.PriceListRepository;
import tim10.project.model.Ticket;
import tim10.project.model.TicketStatus;
import tim10.project.repository.TicketRepository;
import tim10.project.repository.TicketSittingAreaRepository;
import tim10.project.service.exceptions.MaxReservationExceeded;
import tim10.project.service.exceptions.NoTicketsAvailable;
import tim10.project.service.exceptions.NotFoundException;
import tim10.project.service.exceptions.TicketAlreadyBookedException;
import tim10.project.web.dto.sittingArea.ReserveSittingAreaDTO;
import tim10.project.web.dto.sittingArea.SittingAreaTicketDTO;
import tim10.project.service.exceptions.AlreadySoldException;
import tim10.project.service.exceptions.CancelReservationException;
import tim10.project.service.exceptions.NotFoundException;
import tim10.project.service.exceptions.ReserveException;

import javax.mail.MessagingException;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
public class TicketService {

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private TicketSittingAreaRepository ticketSittingAreaRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private PriceListRepository priceListRepository;

    @Autowired
    private MailService mailService;

    //READ
    public List<Ticket> getAllTickets() {
        return ticketRepository.findAll();
    }
    public Ticket getTicketById(Integer id) {
        Ticket ticket = ticketRepository.findById(id).orElse(null);
        if (ticket == null) throw new NotFoundException(String.format("Ticket with id:%d does not exist", id));
        return ticket;
    }

    public Ticket updateTicket(Integer id, Ticket ticket) {
        Ticket ticketFromDatabase = ticketRepository.findById(id).orElse(null);
        if (ticketFromDatabase == null) throw new NotFoundException(String.format("Ticket with id:%d does not exist", id));
        ticketFromDatabase.setTicketStatus(ticket.getTicketStatus());
        ticketFromDatabase.setUser(ticket.getUser());
        ticketFromDatabase.setTimestamp(ticket.getTimestamp());
        return ticketRepository.save(ticketFromDatabase);
    }

    //DELETE
    public void deleteTicket(Integer id) {
        Ticket ticketFromDatabase = ticketRepository.findById(id).orElse(null);
        if (ticketFromDatabase == null) throw new NotFoundException(String.format("Ticket with id:%d does not exist", id));
        ticketRepository.deleteById(id);
    }

    @Transactional
    public void reserveStandingAreaTickets(Integer priceListId, Integer numberOfTickets, User user){
        PriceList priceList = priceListRepository.findById(priceListId).orElse(null);
        if (priceList == null) throw new NotFoundException(String.format("Price list with id: %d", priceListId));
        List<Ticket> tickets = ticketRepository.findAllByTicketStatusAndPriceListId(TicketStatus.AVAILABLE, priceListId);
        if (tickets.size() < numberOfTickets) throw new NoTicketsAvailable("Not enough tickets available for the wanted:"+numberOfTickets);
        List<Ticket> alreadyReserved = ticketRepository.findAllByPriceListIdAndUser(priceListId, user);
        if (numberOfTickets+alreadyReserved.size() > priceList.getEvent().getMaxReservationNumber()) throw new MaxReservationExceeded("Max reservations for this event exceeded. Maximum reservations is:"+priceList.getEvent().getMaxReservationNumber());
        for (int i = 0; i < numberOfTickets; i++) {
            tickets.get(i).setTicketStatus(TicketStatus.IS_RESERVED);
            tickets.get(i).setUser(user);
            tickets.get(i).setTimestamp(new Date());
            ticketRepository.save(tickets.get(i));
        }
    }

    @Transactional
    public void buyStandingAreaTickets(Integer priceListId, Integer numberOfTickets, User user) throws MessagingException {
        PriceList priceList = priceListRepository.findById(priceListId).orElse(null);
        if (priceList == null) throw new NotFoundException(String.format("Price list with id: %d", priceListId));
        List<Ticket> tickets = ticketRepository.findAllByTicketStatusAndPriceListId(TicketStatus.AVAILABLE, priceListId);
        if (tickets.size() < numberOfTickets) throw new NoTicketsAvailable("Not enough tickets available for the wanted:"+numberOfTickets);
        for (int i = 0; i < numberOfTickets; i++) {
            tickets.get(i).setTicketStatus(TicketStatus.IS_SOLD);
            tickets.get(i).setUser(user);
            tickets.get(i).setTimestamp(new Date());
            ticketRepository.save(tickets.get(i));
            mailService.sendMailAsHTML(user, "Successful purchase", String.format("<html><body><img src='https://chart.googleapis.com/chart?cht=qr&chs=500x500&chl=%d'></body></html>", tickets.get(i).getId()));
        }
    }

    @Transactional
    public void reserveSittingAreaTickets(ReserveSittingAreaDTO reserveSittingAreaDTO, User user){
        PriceList priceList = priceListRepository.findById(reserveSittingAreaDTO.getPriceListId()).orElse(null);
        if (priceList == null) throw new NotFoundException(String.format("Price list with id: %d", reserveSittingAreaDTO.getPriceListId()));
        List<Ticket> alreadyReserved = ticketRepository.findAllByPriceListIdAndUser(reserveSittingAreaDTO.getPriceListId(), user);
        if (reserveSittingAreaDTO.getSittingAreaTicketDTOList().size()+alreadyReserved.size() > priceList.getEvent().getMaxReservationNumber()) throw new MaxReservationExceeded("Max reservations for this event exceeded. Maximum reservations is:"+priceList.getEvent().getMaxReservationNumber());
        for (SittingAreaTicketDTO sittingAreaTicketDTO: reserveSittingAreaDTO.getSittingAreaTicketDTOList()) {
            TicketSittingArea ticketSittingArea = ticketSittingAreaRepository.findByPriceListIdAndRowNumAndSeatNum(reserveSittingAreaDTO.getPriceListId(), sittingAreaTicketDTO.getRowNum(), sittingAreaTicketDTO.getSeatNum());
            System.out.println(ticketSittingArea);
            if (ticketSittingArea.getTicketStatus() != TicketStatus.AVAILABLE) throw new TicketAlreadyBookedException(String.format("Ticket on row: %d and col: %d is already booked", ticketSittingArea.getRowNum(), ticketSittingArea.getSeatNum()));
            ticketSittingArea.setTicketStatus(TicketStatus.IS_RESERVED);
            ticketSittingArea.setUser(user);
            ticketSittingArea.setTimestamp(new Date());
            ticketSittingAreaRepository.save(ticketSittingArea);
        }

    }
    @Transactional
    public void buySittingAreaTickets(ReserveSittingAreaDTO reserveSittingAreaDTO, User user) throws MessagingException {
        PriceList priceList = priceListRepository.findById(reserveSittingAreaDTO.getPriceListId()).orElse(null);
        if (priceList == null) throw new NotFoundException(String.format("Price list with id: %d", reserveSittingAreaDTO.getPriceListId()));
        for (SittingAreaTicketDTO sittingAreaTicketDTO: reserveSittingAreaDTO.getSittingAreaTicketDTOList()) {
            TicketSittingArea ticketSittingArea = ticketSittingAreaRepository.findByPriceListIdAndRowNumAndSeatNum(reserveSittingAreaDTO.getPriceListId(), sittingAreaTicketDTO.getRowNum(), sittingAreaTicketDTO.getSeatNum());
            if (ticketSittingArea.getTicketStatus() != TicketStatus.AVAILABLE) throw new TicketAlreadyBookedException(String.format("Ticket on row: %d and col: %d is already booked", ticketSittingArea.getRowNum(), ticketSittingArea.getSeatNum()));
            ticketSittingArea.setTicketStatus(TicketStatus.IS_SOLD);
            ticketSittingArea.setUser(user);
            ticketSittingArea.setTimestamp(new Date());
            ticketSittingAreaRepository.save(ticketSittingArea);
            mailService.sendMailAsHTML(user, "Successful purchase", String.format("<html><body><img src='https://chart.googleapis.com/chart?cht=qr&chs=500x500&chl=%d'></body></html>", ticketSittingArea.getId()));

        }

    }

    @Transactional
    public void buyReservedTickets(List<Integer> ticketIdList, User user){
        List<Ticket> ticketList = ticketRepository.findAllByIdIn(ticketIdList);
        ticketList.forEach(ticket -> {
            if (ticket.getTicketStatus() != TicketStatus.IS_RESERVED) throw new NoTicketsAvailable("Ticket is not reserved");
            ticket.setTicketStatus(TicketStatus.IS_SOLD);
            ticket.setUser(user);
            ticket.setTimestamp(new Date());
            ticketRepository.save(ticket);
            try {
                mailService.sendMailAsHTML(user, "Successful purchase", String.format("<html><body><img src='https://chart.googleapis.com/chart?cht=qr&chs=500x500&chl=%d'></body></html>", ticket.getId()));
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        });
    }

    public List<TicketSittingArea> getBookedTicketSittingAreaByPriceListId(Integer priceListId){
        List<TicketSittingArea> reserved = ticketSittingAreaRepository.findAllByTicketStatusAndPriceListId(TicketStatus.IS_RESERVED, priceListId);
        List<TicketSittingArea> purchased = ticketSittingAreaRepository.findAllByTicketStatusAndPriceListId(TicketStatus.IS_SOLD, priceListId);
        reserved.addAll(purchased);
        return reserved;
    }

    public List<Ticket> getReservedTicketForUser(User user){
        return ticketRepository.findAllByTicketStatusAndUser(TicketStatus.IS_RESERVED, user);
    }

    public List<Ticket> getPurchasedTicketForUser(User user){
        return ticketRepository.findAllByTicketStatusAndUser(TicketStatus.IS_SOLD, user);
    }
}
