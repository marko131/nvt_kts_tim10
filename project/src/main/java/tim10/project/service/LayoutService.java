package tim10.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tim10.project.model.Layout;
import tim10.project.model.PriceList;
import tim10.project.model.SittingArea;
import tim10.project.model.StandingArea;
import tim10.project.repository.LayoutRepository;
import tim10.project.repository.PriceListRepository;
import tim10.project.repository.SittingAreaRepository;
import tim10.project.repository.StandingAreaRepository;
import tim10.project.service.exceptions.NotFoundException;

import java.util.ArrayList;
import java.util.List;

@Service
public class LayoutService {
    @Autowired
    private LayoutRepository layoutRepository;

    @Autowired
    private SittingAreaRepository sittingAreaRepository;

    @Autowired
    private StandingAreaRepository standingAreaRepository;

    @Autowired
    private PriceListRepository priceListRepository;

    //CREATE
    public Layout createLayout(Layout layout){
        return layoutRepository.save(layout);
    }
    //READ
    public List<Layout> getAll(){
        return layoutRepository.findAll();
    }

    public Layout getLayoutById(Integer id){
        Layout layout = layoutRepository.findById(id).orElse(null);
        if (layout == null) throw new NotFoundException(String.format("Layout with id:%d does not exist", id));
        return layout;
    }

    public List<SittingArea> getSittingAreasByLayoutId(Integer id){
        return sittingAreaRepository.getAllByLayoutId(id);
    }
    public List<StandingArea> getStandingAreasByLayoutId(Integer id){
        return standingAreaRepository.getAllByLayoutId(id);
    }

    //UPDATE
    public Layout updateLayout(Integer id, Layout layout) {
        Layout layoutFromDatabase = layoutRepository.findById(id).orElse(null);
        if (layoutFromDatabase == null) throw new NotFoundException(String.format("Layout with id:%d does not exist", id));
        layoutFromDatabase.setSittingAreas(layout.getSittingAreas());
        layoutFromDatabase.setStandingAreas(layout.getStandingAreas());
        return layoutRepository.save(layoutFromDatabase);
    }

    //DELETE
    public void deleteLayout(Integer id){
        Layout layout = layoutRepository.findById(id).orElse(null);
        if (layout == null) throw new NotFoundException(String.format("Layout with id: %d does not exist", id));
        layoutRepository.deleteById(id);
    }

    public Layout getLayoutByPriceListId(Integer priceListId){
        PriceList priceList = priceListRepository.findById(priceListId).orElse(null);
        if (priceList == null) throw new NotFoundException(String.format("Price list with id: %d does not exist", priceListId ));
        return priceList.getEvent().getLayout();
    }

}
