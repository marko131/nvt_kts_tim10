package tim10.project.service.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class AlreadySoldException extends RuntimeException {
    public AlreadySoldException(String message) {
        super(message);
    }
}
