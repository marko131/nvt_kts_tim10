package tim10.project.service.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidAreaForLayout extends RuntimeException{
    public InvalidAreaForLayout(String message){super(message);}
    public InvalidAreaForLayout(){super("Invalid area for layout");}
}
