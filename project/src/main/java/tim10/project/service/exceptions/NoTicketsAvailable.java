package tim10.project.service.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class NoTicketsAvailable extends RuntimeException {
    public NoTicketsAvailable(String message) {super(message);}
}
