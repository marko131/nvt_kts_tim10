package tim10.project.service.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class LayoutUnavailableForDateException extends RuntimeException {
    public LayoutUnavailableForDateException(String message){
        super(message);
    }
}
