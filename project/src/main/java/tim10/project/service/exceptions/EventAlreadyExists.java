package tim10.project.service.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class EventAlreadyExists extends RuntimeException {

    public EventAlreadyExists(String message) {super(message);}

    public EventAlreadyExists(){
        super("Event with that name already exists");
    }
}
