package tim10.project.service.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class DatabaseSaveException extends RuntimeException {
    public DatabaseSaveException(String message) {
        super(message);
    }
}
