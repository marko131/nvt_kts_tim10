package tim10.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tim10.project.model.*;
import tim10.project.repository.*;
import tim10.project.service.exceptions.*;
import tim10.project.service.specification.EventSpecification;

import java.util.*;

@Service
public class EventService {


    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private TicketSittingAreaRepository ticketSittingAreaRepository;

    @Autowired
    private PriceListRepository priceListRepository;

    @Autowired
    private SittingAreaRepository sittingAreaRepository;

    @Autowired
    private StandingAreaRepository standingAreaRepository;

    //CREATE
    @Transactional
    public Event createEvent(Event event) {
        Event eventFromDatabase = eventRepository.findByName(event.getName());
        if (eventFromDatabase != null) throw new EventAlreadyExists();
        Event eventDatabase = eventRepository.save(event);
        List<Ticket> tickets = new ArrayList<>();
        List<TicketSittingArea> ticketsSitting = new ArrayList<>();

        eventDatabase.getPriceLists().forEach(
                priceList -> {
                    if (priceList.getDate().before(new Date())) throw new InvalidDateException("Invalid date");
                    List<PriceList> checkPriceLists = priceListRepository.findAllByDateAndEventLayoutId(priceList.getDate(), priceList.getEvent().getLayout().getId());
                    if (checkPriceLists.size() > eventDatabase.getPriceLists().size())
                        throw new LayoutUnavailableForDateException("Layout is unavailable for selected date");
                    if (priceList.getAreaType() == AreaType.STANDING_AREA) {
                        StandingArea standingArea = standingAreaRepository.findById(priceList.getAreaId()).orElse(null);
                        if (standingArea == null) throw new AreaNotFoundException("Standing area not found");
                        if (!standingArea.getLayout().getId().equals(eventDatabase.getLayout().getId()))
                            throw new InvalidAreaForLayout();
                        for (int i = 0; i < standingArea.getMaxNumber(); i++) {
                            tickets.add(new Ticket(priceList, "Standing area: " + standingArea.getId()));
                        }
                    } else {
                        SittingArea sittingArea = sittingAreaRepository.findById(priceList.getAreaId()).orElse(null);
                        if (sittingArea == null) throw new AreaNotFoundException("Sitting area not found");
                        if (!sittingArea.getLayout().getId().equals(eventDatabase.getLayout().getId()))
                            throw new InvalidAreaForLayout();
                        for (int i = 0; i < sittingArea.getNumRows(); i++) {
                            for (int j = 0; j < sittingArea.getNumSeats(); j++) {
                                ticketsSitting.add(new TicketSittingArea(priceList, "Sitting area: " + sittingArea.getId(), i + 1, j + 1));
                            }
                        }
                    }
                }
        );
        ticketRepository.saveAll(tickets);
        ticketSittingAreaRepository.saveAll(ticketsSitting);
        return eventDatabase;
    }

    //READ
    public List<Event> getAllEvents(Integer page) {
        Pageable pageable = PageRequest.of(page, 10);
        return eventRepository.findAll(pageable).toList();
    }

    public Event getEventById(Integer id) {
        Event eventFromDatabase = eventRepository.findById(id).orElse(null);
        if (eventFromDatabase == null)
            throw new NotFoundException(String.format("Event with id:%d does not exist", id));
        return eventFromDatabase;
    }

    public List<PriceList> getPriceListsByEventId(Integer id) {
        return priceListRepository.findAllByEventId(id);
    }

    public List<Event> search(String name, List<EventCategory> categories, Date begin, Date end, String location, Integer page) {

        Specification<Event> specification = Objects.requireNonNull(Objects.requireNonNull(Objects.requireNonNull(Specification
                .where(EventSpecification.eventNameContains(name))
                .and(EventSpecification.eventCategories(categories)))
                .and(EventSpecification.eventDateAfter(begin)))
                .and(EventSpecification.eventLocation(location)));
        List<Event> events = new ArrayList<>(new HashSet<>(eventRepository.findAll(specification, PageRequest.of(page, 10, Sort.by("name"))).toList()));
        if (end == null) return events;
        ArrayList<Event> eventArrayList = new ArrayList<>();
        for (Event e : events
        ) {
            for (PriceList priceList : e.getPriceLists()
            ) {
                if (priceList.getDate().before(end)) eventArrayList.add(e);
            }
        }
        return eventArrayList;
    }

    //UPDATE
    public Event updateEvent(Integer id, Event event) {
        Optional<Event> eventFromDatabase = eventRepository.findById(id);
        if (eventFromDatabase.isEmpty())
            throw new NotFoundException(String.format("Event with id:%d does not exist", id));
        eventFromDatabase.get().setDescription(event.getDescription());
        eventFromDatabase.get().setEventCategory(event.getEventCategory());
        eventFromDatabase.get().setName(event.getName());
        eventFromDatabase.get().setPicture(event.getPicture());
        return eventRepository.save(eventFromDatabase.get());
    }

    //DELETE
    public void deleteEvent(Integer id) {
        Optional<Event> eventFromDatabase = eventRepository.findById(id);
        if (eventFromDatabase.isEmpty())
            throw new NotFoundException(String.format("Event with id:%d does not exist", id));
        eventRepository.deleteById(id);
    }
}
