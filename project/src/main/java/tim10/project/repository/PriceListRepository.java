package tim10.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tim10.project.model.PriceList;

import java.util.Date;
import java.util.List;

public interface PriceListRepository extends JpaRepository<PriceList, Integer> {
    List<PriceList> findAllByEventId(Integer id);
    List<PriceList> findAllByDateAndEventLayoutId(Date date, Integer layoutId);

}
