package tim10.project.repository;

import org.eclipse.jetty.util.DateCache;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tim10.project.model.Event;
import tim10.project.model.Ticket;
import tim10.project.model.TicketStatus;
import tim10.project.model.User;

import java.util.List;

public interface TicketRepository extends JpaRepository<Ticket, Integer> {
    List<Ticket> findAllByPriceListEventId(Integer id);
    List<Ticket> findAllByPriceListId(Integer id);
    List<Ticket> findAllByTicketStatusAndPriceListId(TicketStatus ticketStatus, Integer id);
    List<Ticket> findAllByPriceListIdAndUser(Integer priceListId, User user);

    List<Ticket> findAllByTicketStatusAndUser(TicketStatus ticketStatus, User user);

    List<Ticket> findAllByIdIn(List<Integer> tickets);

}
