package tim10.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import tim10.project.model.Event;
import tim10.project.model.EventCategory;

import java.util.List;

public interface EventRepository extends JpaRepository<Event, Integer>, JpaSpecificationExecutor<Event> {


    List<Event> findAllByEventCategory(EventCategory category);
    List<Event> findAllByNameContainingIgnoreCase(String name);
    //List<Event> findAllByNameContainingAndEventCategoryIn(String name, List<EventCategory> eventCategory);
    List<Event> findAllByIdInAndNameContainingIgnoreCaseAndEventCategoryIn(List<Integer> eventIds, String name, List<EventCategory> eventCategories);
    Event findByName(String name);
}
