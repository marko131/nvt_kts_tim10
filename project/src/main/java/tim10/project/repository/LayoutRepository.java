package tim10.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tim10.project.model.Layout;

import java.util.List;

public interface LayoutRepository extends JpaRepository<Layout, Integer> {
    List<Layout> findAllByLocationId(Integer id);

}
