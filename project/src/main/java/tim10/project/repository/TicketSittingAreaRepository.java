package tim10.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tim10.project.model.TicketSittingArea;
import tim10.project.model.TicketStatus;

import java.util.List;

public interface TicketSittingAreaRepository extends JpaRepository<TicketSittingArea, Integer> {

    List<TicketSittingArea> findAllByTicketStatusAndPriceListId(TicketStatus ticketStatus, Integer priceListId);
    TicketSittingArea findByPriceListIdAndRowNumAndSeatNum(Integer priceListId, Integer rowNum, Integer seatNum);
}
