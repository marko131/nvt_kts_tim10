package tim10.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tim10.project.model.SittingArea;

import java.util.List;

public interface SittingAreaRepository extends JpaRepository<SittingArea, Integer> {
    public List<SittingArea> getAllByLayoutId(Integer id);
}
