package tim10.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tim10.project.model.StandingArea;

import java.util.List;

public interface StandingAreaRepository extends JpaRepository<StandingArea, Integer> {
    public List<StandingArea> getAllByLayoutId(Integer id);
}
