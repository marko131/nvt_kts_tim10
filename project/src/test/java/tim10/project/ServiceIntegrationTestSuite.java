package tim10.project;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import tim10.project.service.*;
import tim10.project.web.controller.PriceListControllerIntegrationTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({LocationServiceIntegrationTest.class, PriceListServiceIntegrationTest.class,
        EventServiceIntegrationTest.class, LayoutServiceIntegrationTest.class, TicketServiceIntegrationTest.class, StandingAreaServiceIntegrationTest.class, SittingAreaServiceIntegrationTest.class,
        UserServiceIntegrationTest.class})
public class ServiceIntegrationTestSuite {
}
