package tim10.project;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.springframework.test.context.TestPropertySource;
import tim10.project.web.controller.*;

@RunWith(Suite.class)
@Suite.SuiteClasses({EventControllerTest.class, LayoutControllerTest.class, LocationControllerIntegrationTest.class,
        PriceListControllerIntegrationTest.class, TicketControllerTest.class, StandingAreaControllerTest.class, SittingAreaControllerTest.class, UserControllerTest.class})
public class ControllerTestSuite {
}
