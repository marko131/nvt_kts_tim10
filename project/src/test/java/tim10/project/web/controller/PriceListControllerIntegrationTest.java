package tim10.project.web.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import tim10.project.model.*;
import tim10.project.web.dto.priceList.CreatePriceListDTO;
import tim10.project.web.dto.priceList.UpdatePriceListDTO;
import tim10.project.web.dto.user.LoginDTO;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ActiveProfiles("2")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PriceListControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    private SimpleDateFormat sdf;

    private CreatePriceListDTO createPriceListDTO;

    private UpdatePriceListDTO updatePriceListDTO;

    private String login(String email, String password) throws Exception {
        LoginDTO loginDTO = new LoginDTO(email, password);
        String body = objectMapper.writeValueAsString(loginDTO);

        MvcResult result = mvc.perform(MockMvcRequestBuilders
                .post("/api/login")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.ALL)
                .content(body))
                .andReturn();

        return result.getResponse().getContentAsString();
    }

    @Before
    public void setUp() throws ParseException {
        sdf = new SimpleDateFormat("dd/MM/yyyy");

        createPriceListDTO = new CreatePriceListDTO();
        createPriceListDTO.setDate(sdf.parse("19/03/2020"));
        createPriceListDTO.setAreaType(AreaType.STANDING_AREA);
        createPriceListDTO.setAreaId(1);
        createPriceListDTO.setPrice(1600d);
        createPriceListDTO.setEventId(1);

        updatePriceListDTO = new UpdatePriceListDTO();
        updatePriceListDTO.setDate(createPriceListDTO.getDate());
        updatePriceListDTO.setPrice(createPriceListDTO.getPrice());
    }

    @Test
    public void testCreatePriceList_whenUnregisteredUser_thenForbidden() throws Exception {
        String body = objectMapper.writeValueAsString(createPriceListDTO);

        mvc.perform(MockMvcRequestBuilders
                .post("/api/pricelist")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.ALL)
                .content(body))
                .andExpect(status().isForbidden())
                .andExpect(status().reason(containsString("Access Denied")));
    }

    @Test
    public void testCreatePriceList_whenNotAdmin_thenForbidden() throws Exception {
        String accessToken = login("user@gmail.com", "pass123456");

        String body = objectMapper.writeValueAsString(createPriceListDTO);

        mvc.perform(MockMvcRequestBuilders
                .post("/api/pricelist")
                .header("X-Auth-Token", accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.ALL)
                .content(body))
                .andExpect(status().isForbidden())
                .andExpect(status().reason(containsString("Forbidden")));
    }

    @Test
    public void testCreatePriceList_whenValidDTOwithStandingArea_thenOk() throws Exception {
        String accessToken = login("admin@gmail.com", "pass123456");

        String body = objectMapper.writeValueAsString(createPriceListDTO);

        mvc.perform(MockMvcRequestBuilders
                .post("/api/pricelist")
                .header("X-Auth-Token", accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.ALL)
                .content(body))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.areaType").value(createPriceListDTO.getAreaType().toString()))
                .andExpect(jsonPath("$.areaId").value(createPriceListDTO.getAreaId()))
                .andExpect(jsonPath("$.price").value(createPriceListDTO.getPrice()));
    }

    @Test
    public void testCreatePriceList_whenValidDTOwithSittingArea_thenOk() throws Exception {
        String accessToken = login("admin@gmail.com", "pass123456");

        createPriceListDTO.setAreaType(AreaType.SITTING_AREA);
        createPriceListDTO.setAreaId(2);

        String body = objectMapper.writeValueAsString(createPriceListDTO);

        mvc.perform(MockMvcRequestBuilders
                .post("/api/pricelist")
                .header("X-Auth-Token", accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.ALL)
                .content(body))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.areaType").value(createPriceListDTO.getAreaType().toString()))
                .andExpect(jsonPath("$.areaId").value(createPriceListDTO.getAreaId()))
                .andExpect(jsonPath("$.price").value(createPriceListDTO.getPrice()));
    }

    @Test
    public void testCreatePriceList_whenNullFields_thenBadRequest() throws Exception {
        String accessToken = login("admin@gmail.com", "pass123456");

        createPriceListDTO.setAreaType(null);

        String body = objectMapper.writeValueAsString(createPriceListDTO);

        mvc.perform(MockMvcRequestBuilders
                .post("/api/pricelist")
                .header("X-Auth-Token", accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.ALL)
                .content(body))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreatePriceList_whenInvalidDate_thenBadRequest() throws Exception {
        String accessToken = login("admin@gmail.com", "pass123456");

        createPriceListDTO.setDate(sdf.parse("01/01/2020"));

        String body = objectMapper.writeValueAsString(createPriceListDTO);

        mvc.perform(MockMvcRequestBuilders
                .post("/api/pricelist")
                .header("X-Auth-Token", accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.ALL)
                .content(body))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreatePriceList_whenStandingAreaNotFound_thenNotFound() throws Exception {
        String accessToken = login("admin@gmail.com", "pass123456");

        Integer areaId = 444;
        createPriceListDTO.setAreaId(areaId);

        String body = objectMapper.writeValueAsString(createPriceListDTO);

        mvc.perform(MockMvcRequestBuilders
                .post("/api/pricelist")
                .header("X-Auth-Token", accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.ALL)
                .content(body))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testCreatePriceList_whenSittingAreaNotFound_thenNotFound() throws Exception {
        String accessToken = login("admin@gmail.com", "pass123456");

        Integer areaId = 444;
        createPriceListDTO.setAreaId(areaId);
        createPriceListDTO.setAreaType(AreaType.SITTING_AREA);

        String body = objectMapper.writeValueAsString(createPriceListDTO);

        mvc.perform(MockMvcRequestBuilders
                .post("/api/pricelist")
                .header("X-Auth-Token", accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.ALL)
                .content(body))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testCreatePriceList__whenInvalidStandingArea_thenBadRequest() throws Exception {
        String accessToken = login("admin@gmail.com", "pass123456");

        Integer areaId = 2;
        createPriceListDTO.setAreaId(areaId);

        String body = objectMapper.writeValueAsString(createPriceListDTO);

        mvc.perform(MockMvcRequestBuilders
                .post("/api/pricelist")
                .header("X-Auth-Token", accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.ALL)
                .content(body))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreatePriceList__whenInvalidSittingArea_thenBadRequest() throws Exception {
        String accessToken = login("admin@gmail.com", "pass123456");

        Integer areaId = 1;
        createPriceListDTO.setAreaId(areaId);
        createPriceListDTO.setAreaType(AreaType.SITTING_AREA);

        String body = objectMapper.writeValueAsString(createPriceListDTO);

        mvc.perform(MockMvcRequestBuilders
                .post("/api/pricelist")
                .header("X-Auth-Token", accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.ALL)
                .content(body))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Order(1)
    public void testGetAllPriceLists_whenValid() throws Exception {
        MvcResult result = mvc.perform(MockMvcRequestBuilders
                .get("/api/pricelist"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        List<PriceList> priceLists = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
        });

        assertFalse(priceLists.isEmpty());
        assertEquals(3, priceLists.size());
    }

    @Test
    public void testGetPriceListById_whenInvalid_thenNotFound() throws Exception {
        Integer id = 444;

        mvc.perform(MockMvcRequestBuilders
                .get("/api/pricelist/{id}", id))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetPriceListById_whenValid_thenOk() throws Exception {
        Integer id = 2;

        mvc.perform(MockMvcRequestBuilders
                .get("/api/pricelist/{id}", id))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(id))
                .andExpect(jsonPath("$.areaType").value(AreaType.STANDING_AREA.toString()))
                .andExpect(jsonPath("$.areaId").value(1))
                .andExpect(jsonPath("$.price").value(1600d));
    }

    @Test
    public void testGetTicketsByPriceListId_whenInvalid_thenNotFound() throws Exception {
        Integer id = 444;

        mvc.perform(MockMvcRequestBuilders
                .get("/api/pricelist/{id}/tickets", id))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetTicketsByPriceListId_whenValidId_thenOk() throws Exception {
        Integer id = 1;

        MvcResult result = mvc.perform(MockMvcRequestBuilders
                .get("/api/pricelist/{id}/tickets", id))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        List<Ticket> tickets = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
        });

        assertTrue(tickets.isEmpty());
    }

    @Test
    public void testUpdatePriceList_whenUnregisteredUser_thenForbidden() throws Exception {
        Integer id = 1;
        updatePriceListDTO.setPrice(2500d);

        String body = objectMapper.writeValueAsString(updatePriceListDTO);

        mvc.perform(MockMvcRequestBuilders
                .put("/api/location/{id}", id)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.ALL)
                .content(body))
                .andExpect(status().isForbidden())
                .andExpect(status().reason(containsString("Access Denied")));
    }

    @Test
    public void testUpdateLocation_whenNotAdmin_thenForbidden() throws Exception {
        String accessToken = login("user@gmail.com", "pass123456");

        Integer id = 1;
        updatePriceListDTO.setPrice(2500d);

        String body = objectMapper.writeValueAsString(updatePriceListDTO);

        mvc.perform(MockMvcRequestBuilders
                .put("/api/location/{id}", id)
                .header("X-Auth-Token", accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.ALL)
                .content(body))
                .andExpect(status().isForbidden())
                .andExpect(status().reason(containsString("Forbidden")));
    }

    @Test
    public void testUpdateLocation_whenValid_thenOk() throws Exception {
        String accessToken = login("admin@gmail.com", "pass123456");

        Integer id = 1;
        updatePriceListDTO.setPrice(2500d);

        String body = objectMapper.writeValueAsString(updatePriceListDTO);

        mvc.perform(MockMvcRequestBuilders
                .put("/api/pricelist/{id}", id)
                .header("X-Auth-Token", accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.ALL)
                .content(body))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(id))
                .andExpect(jsonPath("$.areaType").value(AreaType.SITTING_AREA.toString()))
                .andExpect(jsonPath("$.areaId").value(2))
                .andExpect(jsonPath("$.price").value(2500d));
    }

    @Test
    public void testUpdatePriceList_whenInvalidDate_thenBadRequest() throws Exception {
        String accessToken = login("admin@gmail.com", "pass123456");

        Integer id = 1;
        updatePriceListDTO.setDate(sdf.parse("01/01/2020"));

        String body = objectMapper.writeValueAsString(updatePriceListDTO);

        mvc.perform(MockMvcRequestBuilders
                .put("/api/pricelist/{id}", id)
                .header("X-Auth-Token", accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.ALL)
                .content(body))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdatePriceList_whenInvalidPrice_thenBadRequest() throws Exception {
        String accessToken = login("admin@gmail.com", "pass123456");

        Integer id = 1;
        updatePriceListDTO.setPrice(-2100d);

        String body = objectMapper.writeValueAsString(updatePriceListDTO);

        mvc.perform(MockMvcRequestBuilders
                .put("/api/pricelist/{id}", id)
                .header("X-Auth-Token", accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.ALL)
                .content(body))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdatePriceList_whenNullFields_thenBadRequest() throws Exception {
        String accessToken = login("admin@gmail.com", "pass123456");

        Integer id = 1;
        updatePriceListDTO.setPrice(null);

        String body = objectMapper.writeValueAsString(updatePriceListDTO);

        mvc.perform(MockMvcRequestBuilders
                .put("/api/pricelist/{id}", id)
                .header("X-Auth-Token", accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.ALL)
                .content(body))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdatePriceList_whenInvalidPriceListId_thenNotFound() throws Exception {
        String accessToken = login("admin@gmail.com", "pass123456");

        Integer id = 444;

        String body = objectMapper.writeValueAsString(updatePriceListDTO);

        mvc.perform(MockMvcRequestBuilders
                .put("/api/pricelist/{id}", id)
                .header("X-Auth-Token", accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.ALL)
                .content(body))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeletePriceList_whenUnregisteredUser_thenForbidden() throws Exception {
        Integer id = 3;

        mvc.perform(MockMvcRequestBuilders
                .delete("/api/pricelist/{id}", id))
                .andExpect(status().isForbidden())
                .andExpect(status().reason(containsString("Access Denied")));
    }

    @Test
    public void testDeletePriceList_whenNotAdmin_thenForbidden() throws Exception {
        String accessToken = login("user@gmail.com", "pass123456");

        Integer id = 3;

        mvc.perform(MockMvcRequestBuilders
                .delete("/api/pricelist/{id}", id)
                .header("X-Auth-Token", accessToken))
                .andExpect(status().isForbidden())
                .andExpect(status().reason(containsString("Forbidden")));
    }

    @Test
    public void testDeletePriceList_whenInvalidId_thenNotFound() throws Exception {
        String accessToken = login("admin@gmail.com", "pass123456");

        Integer id = 444;

        mvc.perform(MockMvcRequestBuilders
                .delete("/api/pricelist/{id}", id)
                .header("X-Auth-Token", accessToken))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeletePriceList_whenValid_thenOk() throws Exception {
        String accessToken = login("admin@gmail.com", "pass123456");

        Integer id = 3;

        MvcResult result = mvc.perform(MockMvcRequestBuilders
                .get("/api/pricelist"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        List<PriceList> priceLists = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {});
        int cnt = priceLists.size();

        mvc.perform(MockMvcRequestBuilders
                .delete("/api/pricelist/{id}", id)
                .header("X-Auth-Token", accessToken))
                .andExpect(status().isOk());

        result = mvc.perform(MockMvcRequestBuilders
                .get("/api/pricelist"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        priceLists = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {});
        int cntAfterDelete = priceLists.size();

        assertEquals(cnt - 1, cntAfterDelete);
    }
}