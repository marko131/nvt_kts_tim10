package tim10.project.web.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import tim10.project.model.Position;
import tim10.project.service.LocationService;
import tim10.project.web.dto.layout.CreateLayoutDTO;
import tim10.project.web.dto.user.LoginDTO;

import java.util.ArrayList;

import static org.hamcrest.core.StringContains.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("1")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class LayoutControllerTest {

    @Autowired
    TestRestTemplate testRestTemplate;

    @Autowired
    private MockMvc mockMvc;

    public String getAccessToken(String email, String password) throws Exception {
        LoginDTO loginDTO = new LoginDTO(email, password);
        loginDTO.setEmail(email);
        loginDTO.setPassword(password);

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/api/login").content(new ObjectMapper().writeValueAsString(loginDTO)).contentType(MediaType.APPLICATION_JSON).accept(MediaType.TEXT_PLAIN)).andReturn();
        return mvcResult.getResponse().getContentAsString();
    }


    @Test
    public void testCreateLayoutAnonymousUser() throws Exception {
        CreateLayoutDTO createLayoutDTO = new CreateLayoutDTO();
        createLayoutDTO.setStandingAreas(new ArrayList<>());
        createLayoutDTO.setSittingAreas(new ArrayList<>());
        createLayoutDTO.setLocationId(1);
        createLayoutDTO.setLayoutName("TestLayout");
        createLayoutDTO.setStagePosition(Position.M);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/layout")
                .content(new ObjectMapper().writeValueAsString(createLayoutDTO))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isForbidden())
                .andExpect(status().reason(containsString("Access Denied")));
    }

    @Test
    public void testCreateLayoutUnauthorized() throws Exception {
        CreateLayoutDTO createLayoutDTO = new CreateLayoutDTO();
        createLayoutDTO.setStandingAreas(new ArrayList<>());
        createLayoutDTO.setSittingAreas(new ArrayList<>());
        createLayoutDTO.setLocationId(1);
        createLayoutDTO.setLayoutName("TestLayout");
        createLayoutDTO.setStagePosition(Position.M);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/layout")
                .header("X-Auth-Token", getAccessToken("user@gmail.com", "password"))
                .content(new ObjectMapper().writeValueAsString(createLayoutDTO))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isForbidden())
                .andExpect(status().reason(containsString("Forbidden")));
    }

    @Test
    public void testCreateLayoutInvalidDTO() throws Exception {
        CreateLayoutDTO createLayoutDTO = new CreateLayoutDTO();

        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/layout")
                .content(new ObjectMapper().writeValueAsString(createLayoutDTO))
                .header("X-Auth-Token", getAccessToken("admin@gmail.com", "password"))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateLayoutLocationNotFound() throws Exception {
        CreateLayoutDTO createLayoutDTO = new CreateLayoutDTO();
        createLayoutDTO.setSittingAreas(new ArrayList<>());
        createLayoutDTO.setStandingAreas(new ArrayList<>());
        createLayoutDTO.setLocationId(2545);
        createLayoutDTO.setLayoutName("TestLayout");
        createLayoutDTO.setStagePosition(Position.M);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/layout")
                .content(new ObjectMapper().writeValueAsString(createLayoutDTO))
                .header("X-Auth-Token", getAccessToken("admin@gmail.com", "password"))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void testCreateLayoutSuccessful() throws Exception {
        CreateLayoutDTO createLayoutDTO = new CreateLayoutDTO();
        createLayoutDTO.setSittingAreas(new ArrayList<>());
        createLayoutDTO.setStandingAreas(new ArrayList<>());
        createLayoutDTO.setLocationId(1);
        createLayoutDTO.setLayoutName("TestLayout");
        createLayoutDTO.setStagePosition(Position.M);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/layout")
                .content(new ObjectMapper().writeValueAsString(createLayoutDTO))
                .header("X-Auth-Token", getAccessToken("admin@gmail.com", "password"))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isOk());
    }


    @Test
    public void testGetLayoutById() throws Exception {
        mockMvc.perform(get("/api/layout/1")).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void testGetLayoutByIdNotFound() throws Exception {
        mockMvc.perform(get("api/layout/1111")).andDo(print()).andExpect(status().isNotFound());
    }

}
