package tim10.project.web.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import tim10.project.model.Position;
import tim10.project.service.SittingAreaService;
import tim10.project.web.dto.sittingArea.CreateSittingAreaDTO;
import tim10.project.web.dto.sittingArea.UpdateSittingAreaDTO;
import tim10.project.web.dto.user.LoginDTO;

import static org.hamcrest.core.StringContains.containsString;
import static org.mockito.ArgumentMatchers.any;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("20")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class SittingAreaControllerTest {

    @Autowired
    TestRestTemplate testRestTemplate;

    @Autowired
    private MockMvc mockMvc;

    public String getAccessToken(String email, String password) throws Exception {
        LoginDTO loginDTO = new LoginDTO(email, password);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/api/login").content(new ObjectMapper().writeValueAsString(loginDTO)).contentType(MediaType.APPLICATION_JSON).accept(MediaType.TEXT_PLAIN)).andReturn();
        return mvcResult.getResponse().getContentAsString();
    }

    @Test
    public void testGetSittingAreaByIdSuccessful() throws Exception {
        mockMvc.perform(get("/api/sittingArea/1")).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void testGetSittingAreaByIdNotFound() throws Exception {
        mockMvc.perform(get("api/sittingArea/2")).andDo(print()).andExpect(status().isNotFound());
    }

    @Test
    public void testGetSittingAreasByLayoutIdSuccessful() throws Exception {
        mockMvc.perform(get("/api/sittingArea/1/layout")).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void testGetSittingAreasByLayoutIdNotFound() throws Exception {
        mockMvc.perform(get("api/sittingArea/2/layout")).andDo(print()).andExpect(status().isNotFound());
    }

    @Test
    public void testCreateSittingAreaAnonymous() throws Exception {
        CreateSittingAreaDTO createSittingAreaDTO = new CreateSittingAreaDTO();
        createSittingAreaDTO.setColls(5);
        createSittingAreaDTO.setRows(5);
        createSittingAreaDTO.setLayoutId(1);
        createSittingAreaDTO.setLayoutPosition(Position.N);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/sittingArea")
                .content(new ObjectMapper().writeValueAsString(createSittingAreaDTO))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isForbidden())
                .andExpect(status().reason(containsString("Access Denied")));
    }

    @Test
    public void testCreateSittingAreaForbidden() throws Exception {
        CreateSittingAreaDTO createSittingAreaDTO = new CreateSittingAreaDTO();
        createSittingAreaDTO.setColls(5);
        createSittingAreaDTO.setRows(5);
        createSittingAreaDTO.setLayoutId(1);
        createSittingAreaDTO.setLayoutPosition(Position.N);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/sittingArea")
                .header("X-Auth-Token", getAccessToken("user@gmail.com", "asd123"))
                .content(new ObjectMapper().writeValueAsString(createSittingAreaDTO))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isForbidden())
                .andExpect(status().reason(containsString("Forbidden")));
    }

    @Test
    public void testCreateSittingAreaSuccessful() throws Exception {
        CreateSittingAreaDTO createSittingAreaDTO = new CreateSittingAreaDTO();
        createSittingAreaDTO.setColls(5);
        createSittingAreaDTO.setRows(5);
        createSittingAreaDTO.setLayoutId(1);
        createSittingAreaDTO.setLayoutPosition(Position.N);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/sittingArea")
                .header("X-Auth-Token", getAccessToken("admin@gmail.com", "asd123"))
                .content(new ObjectMapper().writeValueAsString(createSittingAreaDTO))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void testCreateSittingAreaNullParameters() throws Exception {
        CreateSittingAreaDTO createSittingAreaDTO = new CreateSittingAreaDTO();
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/sittingArea")
                .content(new ObjectMapper().writeValueAsString(createSittingAreaDTO))
                .header("X-Auth-Token", getAccessToken("admin@gmail.com", "asd123"))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateSittingAreaSuccessful() throws Exception {
        UpdateSittingAreaDTO updateSittingAreaDTO = new UpdateSittingAreaDTO();
        updateSittingAreaDTO.setColls(25);
        updateSittingAreaDTO.setRows(20);

        mockMvc.perform(MockMvcRequestBuilders
                .put("/api/sittingArea/1")
                .content(new ObjectMapper().writeValueAsString(updateSittingAreaDTO))
                .header("X-Auth-Token", getAccessToken("admin@gmail.com", "asd123"))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void testUpdateSittingAreaBadRequest() throws Exception {
        UpdateSittingAreaDTO updateSittingAreaDTO = new UpdateSittingAreaDTO();

        mockMvc.perform(MockMvcRequestBuilders
                .put("/api/sittingArea/1")
                .content(new ObjectMapper().writeValueAsString(updateSittingAreaDTO))
                .header("X-Auth-Token", getAccessToken("admin@gmail.com", "asd123"))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }
}
