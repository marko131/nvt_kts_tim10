package tim10.project.web.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import tim10.project.model.Layout;
import tim10.project.model.Location;
import tim10.project.web.dto.location.CreateLocationDTO;
import tim10.project.web.dto.location.UpdateLocationDTO;
import tim10.project.web.dto.user.LoginDTO;

import java.math.BigDecimal;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("2")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class LocationControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    private String login(String email, String password) throws Exception {
        LoginDTO loginDTO = new LoginDTO(email, password);
        String body = objectMapper.writeValueAsString(loginDTO);

        MvcResult result = mvc.perform(MockMvcRequestBuilders
                .post("/api/login")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.ALL)
                .content(body))
                .andReturn();

        return result.getResponse().getContentAsString();
    }

    @Test
    public void testCreateLocation_whenUnregisteredUser_thenForbidden() throws Exception {
        CreateLocationDTO locationDTO = new CreateLocationDTO("Added location", "Test address", BigDecimal.valueOf(45.123456), BigDecimal.valueOf(19.125632), "/picture/path");
        String body = objectMapper.writeValueAsString(locationDTO);

        mvc.perform(MockMvcRequestBuilders
                    .post("/api/location")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.ALL)
                    .content(body))
                    .andExpect(status().isForbidden())
                    .andExpect(status().reason(containsString("Access Denied")));
    }

    @Test
    public void testCreateLocation_whenNotAdmin_thenForbidden() throws Exception {
        String accessToken = login("user@gmail.com", "pass123456");

        CreateLocationDTO locationDTO = new CreateLocationDTO("Added location", "Test address", BigDecimal.valueOf(45.123456), BigDecimal.valueOf(19.125632), "/picture/path");
        String body = objectMapper.writeValueAsString(locationDTO);

        mvc.perform(MockMvcRequestBuilders
                .post("/api/location")
                .header("X-Auth-Token", accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.ALL)
                .content(body))
                .andExpect(status().isForbidden())
                .andExpect(status().reason(containsString("Forbidden")));
    }

    @Test
    public void testCreateLocation_whenInvalidAddress_thenBadRequest() throws Exception {
        String accessToken = login("admin@gmail.com", "pass123456");

        String address = "a";
        address = address.repeat(300);

        CreateLocationDTO locationDTO = new CreateLocationDTO("Added location", address, BigDecimal.valueOf(45.123456), BigDecimal.valueOf(19.125632), "/picture/path");
        String body = objectMapper.writeValueAsString(locationDTO);

        mvc.perform(MockMvcRequestBuilders
                .post("/api/location")
                .header("X-Auth-Token", accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.ALL)
                .content(body))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateLocation_whenInvalidCoordinates_thenBadRequest() throws Exception {
        String accessToken = login("admin@gmail.com", "pass123456");

        CreateLocationDTO locationDTO = new CreateLocationDTO("Added location", "Test address", BigDecimal.valueOf(500), BigDecimal.valueOf(190.125632), "/picture/path");
        String body = objectMapper.writeValueAsString(locationDTO);

        mvc.perform(MockMvcRequestBuilders
                .post("/api/location")
                .header("X-Auth-Token", accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.ALL)
                .content(body))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateLocation_whenDuplicateLocationName_thenConflict() throws Exception {
        String accessToken = login("admin@gmail.com", "pass123456");

        CreateLocationDTO locationDTO = new CreateLocationDTO("Test", "Test address", BigDecimal.valueOf(45.123456), BigDecimal.valueOf(19.125632), "/picture/path");
        String body = objectMapper.writeValueAsString(locationDTO);

        mvc.perform(MockMvcRequestBuilders
                .post("/api/location")
                .header("X-Auth-Token", accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.ALL)
                .content(body))
                .andExpect(status().isConflict());
    }

    @Test
    public void testCreateLocation_whenValidDTO_thenOk() throws Exception {
        String accessToken = login("admin@gmail.com", "pass123456");

        CreateLocationDTO locationDTO = new CreateLocationDTO("Added location", "Test address", BigDecimal.valueOf(45.123456), BigDecimal.valueOf(19.125632), "/picture/path");
        String body = objectMapper.writeValueAsString(locationDTO);

        mvc.perform(MockMvcRequestBuilders
                .post("/api/location")
                .header("X-Auth-Token", accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.ALL)
                .content(body))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(locationDTO.getName()))
                .andExpect(jsonPath("$.address").value(locationDTO.getAddress()))
                .andExpect(jsonPath("$.latitude").value(locationDTO.getLatitude()))
                .andExpect(jsonPath("$.longitude").value(locationDTO.getLongitude()))
                .andExpect(jsonPath("$.picture").value(locationDTO.getPicture()));
    }

    @Test
    public void testCreateLocation_whenInvalidId_thenNotFound() throws Exception {
        String accessToken = login("admin@gmail.com", "pass123456");

        Integer id = 444;
        UpdateLocationDTO locationDTO = new UpdateLocationDTO("Updated location", "Updated address", "updated/picture/path");
        String body = objectMapper.writeValueAsString(locationDTO);

        mvc.perform(MockMvcRequestBuilders
                .put("/api/location/{id}", id)
                .header("X-Auth-Token", accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.ALL)
                .content(body))
                .andExpect(status().isNotFound());
    }

    @Test
    @Order(2)
    public void testGetLocationById_whenValidId_thenOk() throws Exception {
        Integer id = 1;

        mvc.perform(MockMvcRequestBuilders
                    .get("/api/location/{id}", id))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("$.name").value("SPENS"))
                    .andExpect(jsonPath("$.address").value("Sutjeska 2, Novi Sad 21000"))
                    .andExpect(jsonPath("$.latitude").value(45.246937))
                    .andExpect(jsonPath("$.longitude").value(19.845355))
                    .andExpect(jsonPath("$.picture").value("https://rs.n1info.com/Picture/231909/jpeg/SPENS-1-.jpg"));
    }

    @Test
    public void testGetLocationById_whenInvalidId_thenNotFound() throws Exception {
        Integer id = 444;

        mvc.perform(MockMvcRequestBuilders
                .get("/api/location/{id}", id))
                .andExpect(status().isNotFound());
    }

    @Test
    @Order(1)
    public void testGetAllLocations_whenValid_thenOk() throws Exception {
        MvcResult result = mvc.perform(MockMvcRequestBuilders
                                .get("/api/location"))
                                .andExpect(status().isOk())
                                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                                .andReturn();

        List<Location> locations = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
        });

        assertFalse(locations.isEmpty());
        assertEquals(3, locations.size());
    }

    @Test
    public void testGetLayoutsByLocationId_whenInvalidId_thenNotFound() throws Exception {
        Integer id = 444;

        mvc.perform(MockMvcRequestBuilders
                .get("/api/location/{id}/layouts", id))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetLayoutByLocationId_whenValidIdButNoLayouts_thenOk() throws Exception {
        Integer id = 3;

        MvcResult result = mvc.perform(MockMvcRequestBuilders
                                .get("/api/location/{id}/layouts", id))
                                .andExpect(status().isOk())
                                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                                .andReturn();

        List<Layout> resultLayouts = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
        });

        assertTrue(resultLayouts.isEmpty());
    }

    @Test
    public void testGetLayoutByLocationId_whenValidId_thenOk() throws Exception {
        Integer id = 2;

        MvcResult result = mvc.perform(MockMvcRequestBuilders
                                .get("/api/location/{id}/layouts", id))
                                .andExpect(status().isOk())
                                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                                .andReturn();

        List<Layout> resultLayouts = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
        });

        assertEquals(1, resultLayouts.size());
    }

    @Test
    public void testUpdateLocation_whenUnregisteredUser_thenForbidden() throws Exception {
        Integer id = 1;
        UpdateLocationDTO locationDTO = new UpdateLocationDTO("Updated location", "Updated address", "updated/picture/path");
        String body = objectMapper.writeValueAsString(locationDTO);

        mvc.perform(MockMvcRequestBuilders
            .put("/api/location/{id}", id)
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.ALL)
            .content(body))
            .andExpect(status().isForbidden())
            .andExpect(status().reason(containsString("Access Denied")));
    }

    @Test
    public void testUpdateLocation_whenNotAdmin_thenForbidden() throws Exception {
        String accessToken = login("user@gmail.com", "pass123456");

        Integer id = 1;
        UpdateLocationDTO locationDTO = new UpdateLocationDTO("Updated location", "Updated address", "updated/picture/path");
        String body = objectMapper.writeValueAsString(locationDTO);

        mvc.perform(MockMvcRequestBuilders
                .put("/api/location/{id}", id)
                .header("X-Auth-Token", accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.ALL)
                .content(body))
                .andExpect(status().isForbidden())
                .andExpect(status().reason(containsString("Forbidden")));
    }

    @Test
    public void testUpdateLocation_whenInvalidAddress_thenBadRequest() throws Exception {
        String accessToken = login("admin@gmail.com", "pass123456");

        Integer id = 444;
        String address = "a";
        address = address.repeat(300);

        UpdateLocationDTO locationDTO = new UpdateLocationDTO("Updated location", address, "updated/picture/path");
        String body = objectMapper.writeValueAsString(locationDTO);

        mvc.perform(MockMvcRequestBuilders
                .put("/api/location/{id}", id)
                .header("X-Auth-Token", accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.ALL)
                .content(body))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateLocation_whenValid_thenOk() throws Exception {
        String accessToken = login("admin@gmail.com", "pass123456");

        Integer id = 2;
        UpdateLocationDTO locationDTO = new UpdateLocationDTO("Updated location", "Updated address", "updated/picture/path");
        String body = objectMapper.writeValueAsString(locationDTO);

        mvc.perform(MockMvcRequestBuilders
                .put("/api/location/{id}", id)
                .header("X-Auth-Token", accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.ALL)
                .content(body))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(locationDTO.getName()))
                .andExpect(jsonPath("$.address").value(locationDTO.getAddress()))
                .andExpect(jsonPath("$.latitude").value(BigDecimal.valueOf(45.255009)))
                .andExpect(jsonPath("$.longitude").value(BigDecimal.valueOf(19.843016)))
                .andExpect(jsonPath("$.picture").value(locationDTO.getPicture()));
    }

    @Test
    public void testDeleteLocation_whenUnregisteredUser_thenForbidden() throws Exception {
        Integer id = 1;

        mvc.perform(MockMvcRequestBuilders
                .delete("/api/location/{id}", id))
                .andExpect(status().isForbidden())
                .andExpect(status().reason(containsString("Access Denied")));
    }

    @Test
    public void testDeleteLocation_whenNotAdmin_thenForbidden() throws Exception {
        String accessToken = login("user@gmail.com", "pass123456");

        Integer id = 1;

        mvc.perform(MockMvcRequestBuilders
                .delete("/api/location/{id}", id)
                .header("X-Auth-Token", accessToken))
                .andExpect(status().isForbidden())
                .andExpect(status().reason(containsString("Forbidden")));
    }

    @Test
    public void testDeleteLocation_whenInvalidId_thenNotFound() throws Exception {
        String accessToken = login("admin@gmail.com", "pass123456");

        Integer id = 444;

        mvc.perform(MockMvcRequestBuilders
                .delete("/api/location/{id}", id)
                .header("X-Auth-Token", accessToken))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteLocation_whenValid_thenOk() throws Exception {
        String accessToken = login("admin@gmail.com", "pass123456");

        Integer id = 3;

        MvcResult result = mvc.perform(MockMvcRequestBuilders
                .get("/api/location"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        List<Location> locations = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {});
        int cnt = locations.size();

        mvc.perform(MockMvcRequestBuilders
                .delete("/api/location/{id}", id)
                .header("X-Auth-Token", accessToken))
                .andExpect(status().isOk());

        result = mvc.perform(MockMvcRequestBuilders
                .get("/api/location"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        locations = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {});
        int cntAfterDelete = locations.size();

        assertEquals(cnt - 1, cntAfterDelete);
    }
}