package tim10.project.web.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.core.StringContains;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import tim10.project.web.dto.user.ChangePasswordDTO;
import tim10.project.web.dto.user.LoginDTO;
import tim10.project.web.dto.user.RegisterDTO;
import tim10.project.web.dto.user.UserDetailsDTO;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("20")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class UserControllerTest {

    @Autowired
    TestRestTemplate testRestTemplate;

    @Autowired
    private MockMvc mockMvc;

    public String getAccessToken(String email, String password) throws Exception {
        LoginDTO loginDTO = new LoginDTO(email, password);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/api/login").content(new ObjectMapper().writeValueAsString(loginDTO)).contentType(MediaType.APPLICATION_JSON).accept(MediaType.TEXT_PLAIN)).andReturn();
        return mvcResult.getResponse().getContentAsString();
    }

    @Test
    public void testLoginSuccessful() throws Exception {

        LoginDTO loginDTO = new LoginDTO("admin@gmail.com", "asd123");

        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/login")
                .content(new ObjectMapper().writeValueAsString(loginDTO))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void testLoginUserNotFound() throws Exception {

        LoginDTO loginDTO = new LoginDTO("asdfasdfasdf@gmail.com", "asd123");

        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/login")
                .content(new ObjectMapper().writeValueAsString(loginDTO))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testLoginInvalidPassword() throws Exception {

        LoginDTO loginDTO = new LoginDTO("admin@gmail.com", "34543534534543");

        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/login")
                .content(new ObjectMapper().writeValueAsString(loginDTO))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterPasswordsDoNotMatch() throws Exception {
        RegisterDTO registerDTO = new RegisterDTO("asd", "asd", "asd@gmail.com", "asd123", "123asd");

        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/register")
                .content(new ObjectMapper().writeValueAsString(registerDTO))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterSuccessful() throws Exception {
        RegisterDTO registerDTO = new RegisterDTO("asd", "asd", "asd@gmail.com", "asd123", "asd123");

        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/register")
                .content(new ObjectMapper().writeValueAsString(registerDTO))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    public void testViewProfileForbidden() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/api/profile"))
                .andDo(print())
                .andExpect(status().isForbidden())
                .andExpect(status().reason(StringContains.containsString("Access Denied")));
    }

    @Test
    public void testViewProfileSuccessful() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/api/profile")
                .header("X-Auth-Token", getAccessToken("user@gmail.com", "asd123")))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void testChangePasswordPasswordsDoNotMatch() throws Exception {
        ChangePasswordDTO changePasswordDTO = new ChangePasswordDTO("4654651165165", "123asd");

        mockMvc.perform(MockMvcRequestBuilders
                .put("/api/reset")
                .header("X-Auth-Token", getAccessToken("user@gmail.com", "asd123"))
                .content(new ObjectMapper().writeValueAsString(changePasswordDTO))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testChangePasswordSuccessful() throws Exception {
        ChangePasswordDTO changePasswordDTO = new ChangePasswordDTO("123asd", "123asd");

        mockMvc.perform(MockMvcRequestBuilders
                .put("/api/reset")
                .header("X-Auth-Token", getAccessToken("user@gmail.com", "asd123"))
                .content(new ObjectMapper().writeValueAsString(changePasswordDTO))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void testUpdateProfileEmailTaken() throws Exception {
        UserDetailsDTO userDetailsDTO = new UserDetailsDTO("marko", "markovic", "admin@gmail.com");

        mockMvc.perform(MockMvcRequestBuilders
                .put("/api/updateProfile")
                .header("X-Auth-Token", getAccessToken("user@gmail.com", "asd123"))
                .content(new ObjectMapper().writeValueAsString(userDetailsDTO))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isConflict());
    }

    @Test
    public void testUpdateProfileSuccessful() throws Exception {
        UserDetailsDTO userDetailsDTO = new UserDetailsDTO("marko", "markovic", "mm@gmail.com");

        mockMvc.perform(MockMvcRequestBuilders
                .put("/api/updateProfile")
                .header("X-Auth-Token", getAccessToken("user@gmail.com", "asd123"))
                .content(new ObjectMapper().writeValueAsString(userDetailsDTO))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isOk());
    }
}
