package tim10.project.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import tim10.project.web.dto.sittingArea.ReserveSittingAreaDTO;
import tim10.project.web.dto.sittingArea.SittingAreaTicketDTO;
import tim10.project.web.dto.standingArea.ReserveStandingAreaDTO;
import tim10.project.web.dto.user.LoginDTO;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("20")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class TicketControllerTest {

    @Autowired
    TestRestTemplate testRestTemplate;

    @Autowired
    private MockMvc mockMvc;

    public String getAccessToken(String email, String password) throws Exception {
        LoginDTO loginDTO = new LoginDTO(email, password);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/api/login").content(new ObjectMapper().writeValueAsString(loginDTO)).contentType(MediaType.APPLICATION_JSON).accept(MediaType.TEXT_PLAIN)).andReturn();
        return mvcResult.getResponse().getContentAsString();
    }

    @Test
    public void testReserveSittingAreaTicketTaken() throws Exception {
        SittingAreaTicketDTO sittingAreaTicketDTO = new SittingAreaTicketDTO(5, 4);
        SittingAreaTicketDTO sittingAreaTicketDTO2 = new SittingAreaTicketDTO(6, 7);
        List<SittingAreaTicketDTO> listDTO = new ArrayList<>();
        listDTO.add(sittingAreaTicketDTO);
        listDTO.add(sittingAreaTicketDTO2);
        ReserveSittingAreaDTO reserveSittingAreaDTO = new ReserveSittingAreaDTO(2, listDTO);
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/sittingAreaReserve")
                .content(new ObjectMapper().writeValueAsString(reserveSittingAreaDTO))
                .header("X-Auth-Token", getAccessToken("user2@gmail.com", "asd123"))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isConflict());
    }

    @Test
    public void testReserveSittingAreaSuccessful() throws Exception {
        SittingAreaTicketDTO sittingAreaTicketDTO = new SittingAreaTicketDTO(1, 1);
        SittingAreaTicketDTO sittingAreaTicketDTO2 = new SittingAreaTicketDTO(1, 2);
        List<SittingAreaTicketDTO> listDTO = new ArrayList<>();
        listDTO.add(sittingAreaTicketDTO);
        listDTO.add(sittingAreaTicketDTO2);
        ReserveSittingAreaDTO reserveSittingAreaDTO = new ReserveSittingAreaDTO(3, listDTO);
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/sittingAreaReserve")
                .content(new ObjectMapper().writeValueAsString(reserveSittingAreaDTO))
                .header("X-Auth-Token", getAccessToken("user2@gmail.com", "asd123"))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void testReserveStandingAreasNotEnough() throws Exception {
        ReserveStandingAreaDTO reserveStandingAreaDTO = new ReserveStandingAreaDTO(1, 5);
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/standingAreaReserve")
                .content(new ObjectMapper().writeValueAsString(reserveStandingAreaDTO))
                .header("X-Auth-Token", getAccessToken("user2@gmail.com", "asd123"))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isConflict());
    }

    @Test
    public void testReserveStandingAreasSuccessful() throws Exception {
        ReserveStandingAreaDTO reserveStandingAreaDTO = new ReserveStandingAreaDTO(1, 1);
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/standingAreaReserve")
                .content(new ObjectMapper().writeValueAsString(reserveStandingAreaDTO))
                .header("X-Auth-Token", getAccessToken("user2@gmail.com", "asd123"))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void testCancelReservationAlreadySold() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .put("/api/ticket/1/cancel")
                .header("X-Auth-Token", getAccessToken("user@gmail.com", "asd123")))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCancelReservationAlreadyAvailable() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .put("/api/ticket/3/cancel")
                .header("X-Auth-Token", getAccessToken("user@gmail.com", "asd123")))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCancelReservationSuccessful() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .put("/api/ticket/2/cancel")
                .header("X-Auth-Token", getAccessToken("user@gmail.com", "asd123")))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteTicketNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .delete("/api/ticket/1000")
                .header("X-Auth-Token", getAccessToken("admin@gmail.com", "asd123")))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteSuccessful() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .delete("/api/ticket/1")
                .header("X-Auth-Token", getAccessToken("admin@gmail.com", "asd123")))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void testGetAllPurchasedTickets() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/api/ticket/user-tickets")
                .header("X-Auth-Token", getAccessToken("user@gmail.com", "asd123")))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void testGetAllReservedTickets() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/api/ticket/reservations")
                .header("X-Auth-Token", getAccessToken("user@gmail.com", "asd123")))
                .andDo(print())
                .andExpect(status().isOk());
    }

}
