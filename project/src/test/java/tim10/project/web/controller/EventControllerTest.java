package tim10.project.web.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.internal.ErrorMessages;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import tim10.project.model.Event;
import tim10.project.model.EventCategory;
import tim10.project.model.Layout;
import tim10.project.service.EventService;
import tim10.project.service.LayoutService;
import tim10.project.web.dto.event.CreateEventDTO;
import tim10.project.web.dto.event.UpdateEventDTO;
import tim10.project.web.dto.user.LoginDTO;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static junit.framework.TestCase.assertNotNull;
import static org.hamcrest.core.StringContains.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.contains;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("1")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class EventControllerTest {

    @Autowired
    TestRestTemplate testRestTemplate;

    @Autowired
    private MockMvc mockMvc;

    public String getAccessToken(String email, String password) throws Exception {
        LoginDTO loginDTO = new LoginDTO(email, password);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/api/login").content(new ObjectMapper().writeValueAsString(loginDTO)).contentType(MediaType.APPLICATION_JSON).accept(MediaType.TEXT_PLAIN)).andReturn();
        return mvcResult.getResponse().getContentAsString();
    }

    @Test
    public void testGetEventByIdSuccessful() throws Exception {
        mockMvc.perform(get("/api/event/1")).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void testGetEventByIdNotFound() throws Exception {
        mockMvc.perform(get("api/event/2")).andDo(print()).andExpect(status().isNotFound());
    }

    @Test
    public void testCreateEventAnonymousUser() throws Exception {
        CreateEventDTO createEventDTO = new CreateEventDTO();
        createEventDTO.setLayoutId(1);
        createEventDTO.setName("Event");
        createEventDTO.setEventCategory(EventCategory.CONCERT);
        createEventDTO.setDescription("Description");
        createEventDTO.setPicture("img");
        createEventDTO.setDaysReservationIsValid(2);
        createEventDTO.setReservationsAvailable(new Date(1612111111111L));
        createEventDTO.setMaxReservationNumber(1);

        createEventDTO.setPriceLists(new ArrayList<>());

        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/event")
                .content(new ObjectMapper().writeValueAsString(createEventDTO))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isForbidden())
                .andExpect(status().reason(containsString("Access Denied")));
    }

    @Test
    public void testCreateEventForbidden() throws Exception {
        CreateEventDTO createEventDTO = new CreateEventDTO();
        createEventDTO.setLayoutId(1);
        createEventDTO.setName("Event");
        createEventDTO.setEventCategory(EventCategory.CONCERT);
        createEventDTO.setDescription("Description");
        createEventDTO.setPicture("img");
        createEventDTO.setDaysReservationIsValid(2);
        createEventDTO.setReservationsAvailable(new Date(1612111111111L));
        createEventDTO.setMaxReservationNumber(1);

        createEventDTO.setPriceLists(new ArrayList<>());

        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/event")
                .header("X-Auth-Token", getAccessToken("user@gmail.com", "password"))
                .content(new ObjectMapper().writeValueAsString(createEventDTO))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isForbidden())
                .andExpect(status().reason(containsString("Forbidden")));
    }

    @Test
    public void testCreateEventSuccessful() throws Exception {
        CreateEventDTO createEventDTO = new CreateEventDTO();
        createEventDTO.setLayoutId(1);
        createEventDTO.setName("Event11231231");
        createEventDTO.setEventCategory(EventCategory.CONCERT);
        createEventDTO.setDescription("Description");
        createEventDTO.setPicture("img");
        createEventDTO.setDaysReservationIsValid(2);
        createEventDTO.setReservationsAvailable(new Date(1612111111111L));
        createEventDTO.setMaxReservationNumber(1);

        createEventDTO.setPriceLists(new ArrayList<>());

        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/event")
                .content(new ObjectMapper().writeValueAsString(createEventDTO))
                .header("X-Auth-Token", getAccessToken("admin@gmail.com", "password"))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void testCreateEventNullParameters() throws Exception {
        CreateEventDTO createEventDTO = new CreateEventDTO();
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/event")
                .content(new ObjectMapper().writeValueAsString(createEventDTO))
                .header("X-Auth-Token", getAccessToken("admin@gmail.com", "password"))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateEventISuccessful() throws Exception {
        UpdateEventDTO updateEventDTO = new UpdateEventDTO();
        updateEventDTO.setName("EventName");
        updateEventDTO.setEventCategory(EventCategory.CONCERT);
        updateEventDTO.setDescription("Description");
        updateEventDTO.setPicture("img");

        mockMvc.perform(MockMvcRequestBuilders
                .put("/api/event/1")
                .content(new ObjectMapper().writeValueAsString(updateEventDTO))
                .header("X-Auth-Token", getAccessToken("admin@gmail.com", "password"))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void testUpdateEventBadRequest() throws Exception {
        UpdateEventDTO updateEventDTO = new UpdateEventDTO();

        mockMvc.perform(MockMvcRequestBuilders
                .put("/api/event/1")
                .content(new ObjectMapper().writeValueAsString(updateEventDTO))
                .header("X-Auth-Token", getAccessToken("admin@gmail.com", "password"))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }



}
