package tim10.project.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import tim10.project.model.Position;
import tim10.project.web.dto.standingArea.CreateStandingAreaDTO;
import tim10.project.web.dto.standingArea.UpdateStandingAreaDTO;
import tim10.project.web.dto.user.LoginDTO;

import static org.hamcrest.core.StringContains.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("20")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class StandingAreaControllerTest {

    @Autowired
    TestRestTemplate testRestTemplate;

    @Autowired
    private MockMvc mockMvc;

    public String getAccessToken(String email, String password) throws Exception {
        LoginDTO loginDTO = new LoginDTO(email, password);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/api/login").content(new ObjectMapper().writeValueAsString(loginDTO)).contentType(MediaType.APPLICATION_JSON).accept(MediaType.TEXT_PLAIN)).andReturn();
        return mvcResult.getResponse().getContentAsString();
    }

    @Test
    public void testGetStandingAreaByIdSuccessful() throws Exception {
        mockMvc.perform(get("/api/standingArea/1")).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void testGetStandingAreaByIdNotFound() throws Exception {
        mockMvc.perform(get("/api/standingArea/500")).andDo(print()).andExpect(status().isNotFound());
    }

    @Test
    public void testGetStandingAreasByLayoutIdSuccessful() throws Exception {
        mockMvc.perform(get("/api/standingArea/1/layout")).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void testGetStandingAreasByLayoutIdNotFound() throws Exception {
        mockMvc.perform(get("/api/standingArea/200/layout")).andDo(print()).andExpect(status().isNotFound());
    }

    @Test
    public void testCreateStandingAreaAnonymous() throws Exception {
        CreateStandingAreaDTO createStandingAreaDTO = new CreateStandingAreaDTO();
        createStandingAreaDTO.setMaxNumber(5);
        createStandingAreaDTO.setLayoutId(1);
        createStandingAreaDTO.setLayoutPosition(Position.E);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/standingArea")
                .content(new ObjectMapper().writeValueAsString(createStandingAreaDTO))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isForbidden())
                .andExpect(status().reason(containsString("Access Denied")));
    }

    @Test
    public void testCreateStandingAreaForbidden() throws Exception {
        CreateStandingAreaDTO createStandingAreaDTO = new CreateStandingAreaDTO();
        createStandingAreaDTO.setMaxNumber(5);
        createStandingAreaDTO.setLayoutId(1);
        createStandingAreaDTO.setLayoutPosition(Position.E);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/standingArea")
                .header("X-Auth-Token", getAccessToken("user@gmail.com", "asd123"))
                .content(new ObjectMapper().writeValueAsString(createStandingAreaDTO))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isForbidden())
                .andExpect(status().reason(containsString("Forbidden")));
    }

    @Test
    public void testCreateStandingAreaSuccessful() throws Exception {
        CreateStandingAreaDTO createStandingAreaDTO = new CreateStandingAreaDTO();
        createStandingAreaDTO.setMaxNumber(5);
        createStandingAreaDTO.setLayoutId(1);
        createStandingAreaDTO.setLayoutPosition(Position.E);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/standingArea")
                .header("X-Auth-Token", getAccessToken("admin@gmail.com", "asd123"))
                .content(new ObjectMapper().writeValueAsString(createStandingAreaDTO))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void testCreateStandingAreaNullParameters() throws Exception {
        CreateStandingAreaDTO createStandingAreaDTO = new CreateStandingAreaDTO();
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/standingArea")
                .content(new ObjectMapper().writeValueAsString(createStandingAreaDTO))
                .header("X-Auth-Token", getAccessToken("admin@gmail.com", "asd123"))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateStandingAreaSuccessful() throws Exception {
        UpdateStandingAreaDTO updateStandingAreaDTO = new UpdateStandingAreaDTO();
        updateStandingAreaDTO.setMaxNumber(25);

        mockMvc.perform(MockMvcRequestBuilders
                .put("/api/standingArea/1")
                .content(new ObjectMapper().writeValueAsString(updateStandingAreaDTO))
                .header("X-Auth-Token", getAccessToken("admin@gmail.com", "asd123"))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void testUpdateStandingAreaNullParameters() throws Exception {
        UpdateStandingAreaDTO updateStandingAreaDTO = new UpdateStandingAreaDTO();

        mockMvc.perform(MockMvcRequestBuilders
                .put("/api/standingArea/1")
                .content(new ObjectMapper().writeValueAsString(updateStandingAreaDTO))
                .header("X-Auth-Token", getAccessToken("admin@gmail.com", "asd123"))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.ALL))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testDeleteStandingAreaNotFound() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders
                .delete("/api/standingArea/1000")
                .header("X-Auth-Token", getAccessToken("admin@gmail.com", "asd123")))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteStandingAreaSuccessful() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders
                .delete("/api/standingArea/1")
                .header("X-Auth-Token", getAccessToken("admin@gmail.com", "asd123")))
                .andDo(print())
                .andExpect(status().isOk());
    }
}
