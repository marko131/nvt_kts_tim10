package tim10.project;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import tim10.project.repository.LocationRepositoryIntegrationTest;
import tim10.project.service.*;

@RunWith(Suite.class)
@Suite.SuiteClasses({EventServiceTest.class, LayoutServiceTest.class,
        LocationServiceUnitTest.class, PriceListServiceUnitTest.class, StandingAreaServiceUnitTest.class, TicketServiceUnitTest.class, SittingAreaServiceTest.class, UserServiceTest.class})
public class ServiceUnitTestSuite {
}
