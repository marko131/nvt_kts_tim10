package tim10.project.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import tim10.project.model.User;
import tim10.project.service.exceptions.PasswordsDoNotMatchException;
import tim10.project.service.exceptions.UserAlreadyExistsException;

import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("20")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UserServiceIntegrationTest {

    @Autowired
    private UserDetailsServiceImpl userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private User testUser;

    private User updateUser;

    @Before
    public void setUp() {
        testUser = new User();
        testUser.setEmail("admin@gmail.com");
        testUser.setPassword("asd123");
        testUser.setName("nikola");
        testUser.setLastName("nikolic");
        testUser.setAdmin(true);
    }

    @Test(expected = UsernameNotFoundException.class)
    public void testLoadUserByUsernameNotFound() {
        testUser.setEmail("asjodoaksd@gmail.com");
        userService.loadUserByUsername(testUser.getEmail());
    }

    @Test
    public void testLoadUserByUsernameSuccessful() {
        org.springframework.security.core.userdetails.UserDetails result = userService.loadUserByUsername(testUser.getEmail());
        assertEquals(testUser.getEmail(), result.getUsername());
    }

    @Test(expected = UserAlreadyExistsException.class)
    public void testCreateUserAlreadyExists() {
        userService.createUser(testUser);
    }

    @Test
    @Transactional
    public void testCreateUserSuccessful() {
        testUser.setEmail("takonesto@gmail.com");
        User result = userService.createUser(testUser);
        assertEquals(testUser.getEmail(), result.getEmail());
        assertEquals(testUser.getName(), result.getName());
        assertEquals(testUser.getLastName(), result.getLastName());
    }

    @Test(expected = PasswordsDoNotMatchException.class)
    public void testChangePasswordPasswordsDoNotMatch() {
        userService.changePassword(testUser, "asdfgh", "123456");
    }

    @Test
    @Transactional
    public void testChangePasswordSuccessful() {
        User user = userService.findUserByEmail("admin@gmail.com");
        User result = userService.changePassword(user,"Marko123", "Marko123");
        assertEquals(user.getPassword(), result.getPassword());
    }

    @Test(expected = UserAlreadyExistsException.class)
    public void testUpdateProfileEmailTaken() {
        userService.updateProfile(testUser, "user@gmail.com", "tako", "nesto");
    }

    @Test
    @Transactional
    public void testUpdateProfileSuccessful() {
        User user = userService.findUserByEmail("admin@gmail.com");
        User result = userService.updateProfile(user, "asd@gmail.com", "tako", "nesto");
        assertEquals(user.getEmail(), result.getEmail());
        assertEquals(user.getName(), result.getName());
        assertEquals(user.getLastName(), result.getLastName());
    }

}
