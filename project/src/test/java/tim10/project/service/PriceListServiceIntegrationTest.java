package tim10.project.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import tim10.project.model.*;
import tim10.project.repository.EventRepository;
import tim10.project.service.exceptions.InvalidAreaException;
import tim10.project.service.exceptions.InvalidDateException;
import tim10.project.service.exceptions.NotFoundException;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("2")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PriceListServiceIntegrationTest {

    @Autowired
    private PriceListService priceListService;

    @Autowired
    private EventRepository eventRepository;

    private SimpleDateFormat sdf;

    private PriceList testPriceList;

    private PriceList updatedPriceList;

    @Before
    public void setUp() throws ParseException {
        sdf = new SimpleDateFormat("dd/MM/yyyy");

        Event event = eventRepository.getOne(1);

        testPriceList = new PriceList();
        testPriceList.setDate(sdf.parse("19/03/2020"));
        testPriceList.setAreaType(AreaType.STANDING_AREA);
        testPriceList.setAreaId(1);
        testPriceList.setPrice(1600d);
        testPriceList.setEvent(event);
        testPriceList.setTickets(new ArrayList<>());

        updatedPriceList = new PriceList();
        updatedPriceList.setDate(testPriceList.getDate());
        updatedPriceList.setAreaType(testPriceList.getAreaType());
        updatedPriceList.setAreaId(testPriceList.getAreaId());
        updatedPriceList.setPrice(testPriceList.getPrice());
        updatedPriceList.setEvent(testPriceList.getEvent());
        updatedPriceList.setTickets(testPriceList.getTickets());
    }

    @Test
    @Transactional
    public void testCreatePriceList_whenValidWithStandingArea_thenReturnPriceList() {
        PriceList resultPriceList = priceListService.createPriceList(testPriceList);

        assertEquals(testPriceList.getDate(), resultPriceList.getDate());
        assertEquals(testPriceList.getAreaType(), resultPriceList.getAreaType());
        assertEquals(testPriceList.getAreaId(), resultPriceList.getAreaId());
        assertEquals(testPriceList.getPrice(), resultPriceList.getPrice());
        assertEquals(testPriceList.getEvent(), resultPriceList.getEvent());
        assertEquals(testPriceList.getTickets(), resultPriceList.getTickets());
    }

    @Test
    @Transactional
    public void testCreatePriceList_whenValidWithSittingArea_thenReturnPriceList() {
        testPriceList.setAreaType(AreaType.SITTING_AREA);
        testPriceList.setAreaId(2);

        PriceList resultPriceList = priceListService.createPriceList(testPriceList);

        assertEquals(testPriceList.getDate(), resultPriceList.getDate());
        assertEquals(testPriceList.getAreaType(), resultPriceList.getAreaType());
        assertEquals(testPriceList.getAreaId(), resultPriceList.getAreaId());
        assertEquals(testPriceList.getPrice(), resultPriceList.getPrice());
        assertEquals(testPriceList.getEvent(), resultPriceList.getEvent());
        assertEquals(testPriceList.getTickets(), resultPriceList.getTickets());
    }

    @Test(expected = ConstraintViolationException.class)
    public void testCreatePriceList_whenNullFields_thenThrowConstraintViolationException() {
        testPriceList = new PriceList();
        priceListService.createPriceList(testPriceList);
    }

    @Test(expected = InvalidDateException.class)
    public void testCreatePriceList_whenInvalidDate_thenThrowInvalidDateException() throws ParseException {
        testPriceList.setDate(sdf.parse("01/01/2020"));
        priceListService.createPriceList(testPriceList);
    }

    @Test(expected = NotFoundException.class)
    public void testCreatePriceList_whenStandingAreaNotFound_thenThrowNotFoundException() {
        Integer areaId = 444;
        testPriceList.setAreaId(areaId);

        priceListService.createPriceList(testPriceList);
    }

    @Test(expected = NotFoundException.class)
    public void testCreatePriceList_whenSittingAreaNotFound_thenThrowNotFoundException() {
        Integer areaId = 444;
        testPriceList.setAreaId(areaId);
        testPriceList.setAreaType(AreaType.SITTING_AREA);

        priceListService.createPriceList(testPriceList);
    }

    @Test(expected = InvalidAreaException.class)
    @Transactional
    public void testCreatePriceList_whenInvalidStandingArea_thenThrowInvalidAreaException() {
        StandingArea standingArea = new StandingArea();
        standingArea.setId(2);

        testPriceList.setAreaId(standingArea.getId());

        priceListService.createPriceList(testPriceList);
    }

    @Test(expected = InvalidAreaException.class)
    @Transactional
    public void testCreatePriceList_whenInvalidSittingArea_thenThrowInvalidAreaException() {
        SittingArea sittingArea = new SittingArea();
        sittingArea.setId(1);

        testPriceList.setAreaId(sittingArea.getId());
        testPriceList.setAreaType(AreaType.SITTING_AREA);

        priceListService.createPriceList(testPriceList);
    }

    @Test
    @Order(1)
    public void testGetAllPriceLists_whenValid() {
        List<PriceList> resultPriceLists = priceListService.getAllPriceLists();

        assertFalse(resultPriceLists.isEmpty());
        assertEquals(3, resultPriceLists.size());
    }

    @Test(expected = NotFoundException.class)
    public void testGetPriceListById_whenInvalid_thenThrowNotFoundException() {
        Integer id = 444;
        priceListService.getPriceListById(id);
    }

    @Test
    @Transactional
    public void testGetPriceListById_whenValid_thenReturnPriceList() throws ParseException {
        Integer id = 1;

        PriceList resultPriceList =  priceListService.getPriceListById(id);

        assertEquals(id, resultPriceList.getId());
        assertEquals(sdf.parse("19/01/2020"), resultPriceList.getDate());
        assertEquals(AreaType.SITTING_AREA, resultPriceList.getAreaType());
        assertEquals(2, resultPriceList.getAreaId());
        assertEquals(2100d, resultPriceList.getPrice());
        assertEquals(1, resultPriceList.getEvent().getId());
        assertEquals(new ArrayList<>(), resultPriceList.getTickets());
    }

    @Test(expected = NotFoundException.class)
    public void testGetTicketsByPriceListId_whenInvalid_thenThrowNotFoundException() {
        Integer id = 444;
        priceListService.getTicketsByPriceListId(id);
    }

    @Test
    public void testGetTicketsByPriceListId_whenValid_thenReturnTicketList() {
        Integer id = 1;

        List<Ticket> result = priceListService.getTicketsByPriceListId(id);

        assertTrue(result.isEmpty());
    }

    @Test
    @Transactional
    public void testUpdatePriceList_whenValidWithStandingArea_thenReturnPriceList() {
        Integer id = 1;

        updatedPriceList.setId(id);
        updatedPriceList.setPrice(2500d);

        PriceList resultPriceList = priceListService.updatePriceList(id, updatedPriceList);

        assertEquals(updatedPriceList.getId(), resultPriceList.getId());
        assertEquals(updatedPriceList.getDate(), resultPriceList.getDate());
        assertEquals(updatedPriceList.getAreaType(), resultPriceList.getAreaType());
        assertEquals(updatedPriceList.getAreaId(), resultPriceList.getAreaId());
        assertEquals(updatedPriceList.getPrice(), resultPriceList.getPrice());
        assertEquals(updatedPriceList.getEvent(), resultPriceList.getEvent());
        assertEquals(updatedPriceList.getTickets(), resultPriceList.getTickets());
    }

    @Test
    @Transactional
    public void testUpdatePriceList_whenValidWithSittingArea_thenReturnPriceList() {
        Integer id = 1;
        Integer areaId = 2;

        updatedPriceList.setId(id);
        updatedPriceList.setAreaType(AreaType.SITTING_AREA);
        updatedPriceList.setAreaId(areaId);
        updatedPriceList.setPrice(2500d);

        PriceList resultPriceList = priceListService.updatePriceList(id, updatedPriceList);

        assertEquals(testPriceList.getDate(), resultPriceList.getDate());
        assertEquals(AreaType.SITTING_AREA, resultPriceList.getAreaType());
        assertEquals(areaId, resultPriceList.getAreaId());
        assertEquals(2500d, resultPriceList.getPrice());
        assertEquals(testPriceList.getEvent(), resultPriceList.getEvent());
        assertEquals(testPriceList.getTickets(), resultPriceList.getTickets());
    }

    @Test(expected = ConstraintViolationException.class)
    public void testUpdatePriceList_whenNullFields_thenThrowConstraintViolationException() {
        updatedPriceList = new PriceList();
        priceListService.updatePriceList(1, updatedPriceList);
    }

    @Test(expected = InvalidDateException.class)
    public void testUpdatePriceList_whenInvalidDate_thenThrowInvalidDateException() throws ParseException {
        Integer id = 1;

        updatedPriceList.setId(id);
        updatedPriceList.setDate(sdf.parse("01/01/2020"));

        priceListService.updatePriceList(id, updatedPriceList);
    }

    @Test(expected = NotFoundException.class)
    public void testUpdatePriceList_whenStandingAreaNotFound_thenThrowNotFoundException() {
        Integer areaId = 444;
        Integer id = 1;

        updatedPriceList.setId(id);
        updatedPriceList.setAreaType(AreaType.STANDING_AREA);
        updatedPriceList.setAreaId(areaId);

        priceListService.updatePriceList(id, updatedPriceList);
    }

    @Test(expected = NotFoundException.class)
    public void testUpdatePriceList_whenSittingAreaNotFound_thenThrowNotFoundException() {
        Integer areaId = 444;
        Integer id = 1;

        updatedPriceList.setId(id);
        updatedPriceList.setAreaType(AreaType.SITTING_AREA);
        updatedPriceList.setAreaId(areaId);

        priceListService.updatePriceList(id, updatedPriceList);
    }

    @Test(expected = InvalidAreaException.class)
    @Transactional
    public void testUpdatePriceList_whenInvalidStandingArea_thenThrowInvalidAreaException() {
        Integer id = 1;
        Integer areaId = 2;

        updatedPriceList.setId(id);
        updatedPriceList.setAreaType(AreaType.STANDING_AREA);
        updatedPriceList.setAreaId(areaId);

        priceListService.updatePriceList(id, updatedPriceList);
    }

    @Test(expected = InvalidAreaException.class)
    @Transactional
    public void testUpdatePriceList_whenInvalidSittingArea_thenThrowInvalidAreaException() {
        Integer id = 1;
        Integer areaId = 1;

        updatedPriceList.setId(id);
        updatedPriceList.setAreaType(AreaType.SITTING_AREA);
        updatedPriceList.setAreaId(areaId);

        priceListService.updatePriceList(id, updatedPriceList);
    }

    @Test(expected = NotFoundException.class)
    public void testUpdatePriceList_whenInvalidId_thenThrowNotFoundException() {
        Integer id = 444;
        updatedPriceList.setId(id);

        priceListService.updatePriceList(id, updatedPriceList);
    }

    @Test(expected = NotFoundException.class)
    public void testDeletePriceList_whenInvalidId_thenThrowNotFoundException() {
        Integer id = 444;

        priceListService.deletePriceList(id);
    }

    @Test
    public void testDeleteLocation_whenValid_ReturnDecrementedCountLocation() {
        int cnt = priceListService.getAllPriceLists().size();

        Integer id = 3;
        priceListService.deletePriceList(id);

        int cntAfterDelete = priceListService.getAllPriceLists().size();

        assertEquals(cnt - 1, cntAfterDelete);
    }
}