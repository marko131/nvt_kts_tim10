package tim10.project.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import tim10.project.model.*;
import tim10.project.repository.PriceListRepository;
import tim10.project.repository.SittingAreaRepository;
import tim10.project.repository.StandingAreaRepository;
import tim10.project.repository.TicketRepository;
import tim10.project.service.exceptions.InvalidAreaException;
import tim10.project.service.exceptions.InvalidDateException;
import tim10.project.service.exceptions.NotFoundException;

import javax.validation.ConstraintViolationException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ActiveProfiles("2")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PriceListServiceUnitTest {

    @Autowired
    private PriceListService priceListService;

    @MockBean
    private PriceListRepository priceListRepository;

    @MockBean
    private StandingAreaRepository standingAreaRepository;

    @MockBean
    private SittingAreaRepository sittingAreaRepository;

    @MockBean
    private TicketRepository ticketRepository;

    private SimpleDateFormat sdf;

    private PriceList testPriceList;

    private PriceList updatedPriceList;

    @Before
    public void setUp() throws ParseException {
        sdf = new SimpleDateFormat("dd/MM/yyyy");

        Event event = new Event();
        event.setId(1);
        event.setName("Đorđe Balašević");
        event.setEventCategory(EventCategory.CONCERT);
        event.setDescription("Sada već tradicionalni, četvrti po redu novosadski koncert Đorđa Balaševića na " +
                "Bogojavljensku noć biće održan u Velikoj sali SPC Vojvodina (SPENS), u nedelju 19. januara 2020." +
                " godine sa početkom u 20 časova.");
        event.setPicture("picture/path/balasevic.jpg");
        event.setReservationsAvailable(sdf.parse("17/03/2020"));
        event.setDaysReservationIsValid(2);
        event.setMaxReservationNumber(5);
        event.setPriceLists(new ArrayList<>());

        Location location = new Location();
        location.setId(1);
        location.setName("SPENS");
        location.setAddress("Sutjeska 2, Novi Sad 21000");
        location.setLatitude(BigDecimal.valueOf(45.246937));
        location.setLongitude(BigDecimal.valueOf(19.845355));
        location.setPicture("picture/path/spens.jpg");

        StandingArea standingArea = new StandingArea();
        standingArea.setId(1);
        standingArea.setMaxNumber(500);

        SittingArea sittingArea = new SittingArea();
        sittingArea.setId(1);
        sittingArea.setNumRows(10);
        sittingArea.setNumSeats(23);

        Layout layout = new Layout();
        layout.setId(1);
        layout.setLocation(location);
        layout.setStandingAreas(Arrays.asList(standingArea));
        layout.setSittingAreas(Arrays.asList(sittingArea));

        event.setLayout(layout);

        testPriceList = new PriceList();
        testPriceList.setDate(sdf.parse("19/03/2020"));
        testPriceList.setAreaType(AreaType.STANDING_AREA);
        testPriceList.setAreaId(1);
        testPriceList.setPrice(1600d);
        testPriceList.setEvent(event);
        testPriceList.setTickets(new ArrayList<>());

        updatedPriceList = new PriceList();
        updatedPriceList.setId(testPriceList.getId());
        updatedPriceList.setDate(testPriceList.getDate());
        updatedPriceList.setAreaType(testPriceList.getAreaType());
        updatedPriceList.setAreaId(testPriceList.getAreaId());
        updatedPriceList.setPrice(testPriceList.getPrice());
        updatedPriceList.setEvent(testPriceList.getEvent());
        updatedPriceList.setTickets(testPriceList.getTickets());
    }

    @Test
    public void testCreatePriceList_whenValidWithStandingArea_thenReturnPriceList() {
        StandingArea standingArea = testPriceList.getEvent().getLayout().getStandingAreas().get(0);

        when(standingAreaRepository.findById(standingArea.getId())).thenReturn(Optional.of(standingArea));
        when(priceListRepository.save(testPriceList)).thenReturn(testPriceList);

        PriceList resultPriceList = priceListService.createPriceList(testPriceList);

        assertEquals(testPriceList.getDate(), resultPriceList.getDate());
        assertEquals(testPriceList.getAreaType(), resultPriceList.getAreaType());
        assertEquals(testPriceList.getAreaId(), resultPriceList.getAreaId());
        assertEquals(testPriceList.getPrice(), resultPriceList.getPrice());
        assertEquals(testPriceList.getEvent(), resultPriceList.getEvent());
        assertEquals(testPriceList.getTickets(), resultPriceList.getTickets());
    }

    @Test
    public void testCreatePriceList_whenValidWithSittingArea_thenReturnPriceList() {
        testPriceList.setAreaType(AreaType.SITTING_AREA);
        SittingArea sittingArea = testPriceList.getEvent().getLayout().getSittingAreas().get(0);

        when(sittingAreaRepository.findById(sittingArea.getId())).thenReturn(Optional.of(sittingArea));
        when(priceListRepository.save(testPriceList)).thenReturn(testPriceList);

        PriceList resultPriceList = priceListService.createPriceList(testPriceList);

        assertEquals(testPriceList.getDate(), resultPriceList.getDate());
        assertEquals(testPriceList.getAreaType(), resultPriceList.getAreaType());
        assertEquals(testPriceList.getAreaId(), resultPriceList.getAreaId());
        assertEquals(testPriceList.getPrice(), resultPriceList.getPrice());
        assertEquals(testPriceList.getEvent(), resultPriceList.getEvent());
        assertEquals(testPriceList.getTickets(), resultPriceList.getTickets());
    }

    @Test(expected = ConstraintViolationException.class)
    public void testCreatePriceList_whenNullFields_thenThrowConstraintViolationException() {
        testPriceList = new PriceList();
        priceListService.createPriceList(testPriceList);
    }

    @Test(expected = InvalidDateException.class)
    public void testCreatePriceList_whenInvalidDate_thenThrowInvalidDateException() throws ParseException {
        testPriceList.setDate(sdf.parse("01/01/2020"));
        priceListService.createPriceList(testPriceList);
    }

    @Test(expected = NotFoundException.class)
    public void testCreatePriceList_whenStandingAreaNotFound_thenThrowNotFoundException() {
        Integer areaId = 444;
        testPriceList.setAreaId(areaId);

        when(standingAreaRepository.findById(areaId)).thenReturn(Optional.empty());

        priceListService.createPriceList(testPriceList);
    }

    @Test(expected = NotFoundException.class)
    public void testCreatePriceList_whenSittingAreaNotFound_thenThrowNotFoundException() {
        Integer areaId = 444;
        testPriceList.setAreaId(areaId);
        testPriceList.setAreaType(AreaType.SITTING_AREA);

        when(sittingAreaRepository.findById(areaId)).thenReturn(Optional.empty());

        priceListService.createPriceList(testPriceList);
    }

    @Test(expected = InvalidAreaException.class)
    public void testCreatePriceList_whenInvalidStandingArea_thenThrowInvalidAreaException() {
        StandingArea standingArea = new StandingArea();
        standingArea.setId(2);
        standingArea.setMaxNumber(200);

        testPriceList.setAreaId(standingArea.getId());

        when(standingAreaRepository.findById(standingArea.getId())).thenReturn(Optional.of(standingArea));

        priceListService.createPriceList(testPriceList);
    }

    @Test(expected = InvalidAreaException.class)
    public void testCreatePriceList_whenInvalidSittingArea_thenThrowInvalidAreaException() {
        SittingArea sittingArea = new SittingArea();
        sittingArea.setId(2);
        sittingArea.setNumRows(20);
        sittingArea.setNumSeats(20);

        testPriceList.setAreaId(sittingArea.getId());
        testPriceList.setAreaType(AreaType.SITTING_AREA);

        when(sittingAreaRepository.findById(sittingArea.getId())).thenReturn(Optional.of(sittingArea));

        priceListService.createPriceList(testPriceList);
    }

    @Test
    public void testGetAllPriceLists_whenNoPriceLists_thenReturnEmptyList() {
        when(priceListRepository.findAll()).thenReturn(new ArrayList<>());

        List<PriceList> resultPriceLists = priceListService.getAllPriceLists();
        assertTrue(resultPriceLists.isEmpty());
    }

    @Test
    public void testGetAllPriceLists_whenValid() {
        when(priceListRepository.findAll()).thenReturn(Arrays.asList(testPriceList));

        List<PriceList> resultPriceLists = priceListService.getAllPriceLists();
        assertFalse(resultPriceLists.isEmpty());

        assertEquals(testPriceList.getDate(), resultPriceLists.get(0).getDate());
        assertEquals(testPriceList.getAreaType(), resultPriceLists.get(0).getAreaType());
        assertEquals(testPriceList.getAreaId(), resultPriceLists.get(0).getAreaId());
        assertEquals(testPriceList.getPrice(), resultPriceLists.get(0).getPrice());
        assertEquals(testPriceList.getEvent(), resultPriceLists.get(0).getEvent());
        assertEquals(testPriceList.getTickets(), resultPriceLists.get(0).getTickets());
    }

    @Test(expected = NotFoundException.class)
    public void testGetPriceListById_whenInvalid_thenThrowNotFoundException() {
        Integer id = 1;
        when(priceListRepository.findById(id)).thenReturn(Optional.empty());

        priceListService.getPriceListById(id);
    }

    @Test
    public void testGetPriceListById_whenValid_thenReturnPriceList() {
        Integer id = 1;
        testPriceList.setId(id);

        when(priceListRepository.findById(id)).thenReturn(Optional.of(testPriceList));

        PriceList resultPriceList =  priceListService.getPriceListById(id);

        assertEquals(testPriceList.getId(), resultPriceList.getId());
        assertEquals(testPriceList.getDate(), resultPriceList.getDate());
        assertEquals(testPriceList.getAreaType(), resultPriceList.getAreaType());
        assertEquals(testPriceList.getAreaId(), resultPriceList.getAreaId());
        assertEquals(testPriceList.getPrice(), resultPriceList.getPrice());
        assertEquals(testPriceList.getEvent(), resultPriceList.getEvent());
        assertEquals(testPriceList.getTickets(), resultPriceList.getTickets());
    }

    @Test(expected = NotFoundException.class)
    public void testGetTicketsByPriceListId_whenInvalid_thenThrowNotFoundException() {
        Integer id = 1;
        when(priceListRepository.findById(id)).thenReturn(Optional.empty());

        priceListService.getTicketsByPriceListId(id);
    }

    @Test
    public void testGetTicketsByPriceListId_whenValid_thenReturnTicketList() {
        List<Ticket> tickets = Arrays.asList(new Ticket(), new Ticket(), new Ticket());

        Integer id = 1;
        when(priceListRepository.findById(id)).thenReturn(Optional.of(testPriceList));
        when(ticketRepository.findAllByPriceListId(id)).thenReturn(tickets);

        List<Ticket> result = priceListService.getTicketsByPriceListId(id);

        assertEquals(tickets, result);
    }

    @Test
    public void testUpdatePriceList_whenValidWithStandingArea_thenReturnPriceList() {
        Integer id = 1;

        testPriceList.setId(1);
        updatedPriceList.setId(id);
        updatedPriceList.setPrice(2100d);

        StandingArea standingArea = testPriceList.getEvent().getLayout().getStandingAreas().get(0);

        when(priceListRepository.findById(id)).thenReturn(Optional.of(testPriceList));
        when(standingAreaRepository.findById(standingArea.getId())).thenReturn(Optional.of(standingArea));
        when(priceListRepository.save(testPriceList)).thenReturn(testPriceList);

        PriceList resultPriceList = priceListService.updatePriceList(updatedPriceList.getId(), updatedPriceList);

        assertEquals(updatedPriceList.getId(), resultPriceList.getId());
        assertEquals(updatedPriceList.getDate(), resultPriceList.getDate());
        assertEquals(updatedPriceList.getAreaType(), resultPriceList.getAreaType());
        assertEquals(updatedPriceList.getAreaId(), resultPriceList.getAreaId());
        assertEquals(updatedPriceList.getPrice(), resultPriceList.getPrice());
        assertEquals(updatedPriceList.getEvent(), resultPriceList.getEvent());
        assertEquals(updatedPriceList.getTickets(), resultPriceList.getTickets());
    }

    @Test
    public void testUpdatePriceList_whenValidWithSittingArea_thenReturnPriceList() {
        Integer id = 1;
        testPriceList.setId(id);
        testPriceList.setAreaType(AreaType.SITTING_AREA);

        updatedPriceList.setId(id);
        updatedPriceList.setAreaType(testPriceList.getAreaType());
        updatedPriceList.setAreaId(testPriceList.getAreaId());
        updatedPriceList.setPrice(2100d);

        SittingArea sittingArea = testPriceList.getEvent().getLayout().getSittingAreas().get(0);

        when(priceListRepository.findById(id)).thenReturn(Optional.of(testPriceList));
        when(sittingAreaRepository.findById(sittingArea.getId())).thenReturn(Optional.of(sittingArea));
        when(priceListRepository.save(testPriceList)).thenReturn(testPriceList);

        PriceList resultPriceList = priceListService.updatePriceList(id, updatedPriceList);

        assertEquals(testPriceList.getDate(), resultPriceList.getDate());
        assertEquals(testPriceList.getAreaType(), resultPriceList.getAreaType());
        assertEquals(testPriceList.getAreaId(), resultPriceList.getAreaId());
        assertEquals(testPriceList.getPrice(), resultPriceList.getPrice());
        assertEquals(testPriceList.getEvent(), resultPriceList.getEvent());
        assertEquals(testPriceList.getTickets(), resultPriceList.getTickets());
    }

    @Test(expected = ConstraintViolationException.class)
    public void testUpdatePriceList_whenNullFields_thenThrowConstraintViolationException() {
        updatedPriceList = new PriceList();
        priceListService.updatePriceList(1, updatedPriceList);
    }

    @Test(expected = InvalidDateException.class)
    public void testUpdatePriceList_whenInvalidDate_thenThrowInvalidDateException() throws ParseException {
        Integer id = 1;

        updatedPriceList.setId(id);
        updatedPriceList.setDate(sdf.parse("01/01/2020"));

        when(priceListRepository.findById(id)).thenReturn(Optional.of(testPriceList));

        priceListService.updatePriceList(id, updatedPriceList);
    }

    @Test(expected = NotFoundException.class)
    public void testUpdatePriceList_whenStandingAreaNotFound_thenThrowNotFoundException() {
        Integer areaId = 444;
        Integer id = 1;

        updatedPriceList.setId(id);
        updatedPriceList.setAreaType(AreaType.STANDING_AREA);
        updatedPriceList.setAreaId(areaId);

        when(priceListRepository.findById(id)).thenReturn(Optional.of(testPriceList));
        when(standingAreaRepository.findById(areaId)).thenReturn(Optional.empty());

        priceListService.updatePriceList(id, updatedPriceList);
    }

    @Test(expected = NotFoundException.class)
    public void testUpdatePriceList_whenSittingAreaNotFound_thenThrowNotFoundException() {
        Integer areaId = 444;
        Integer id = 1;

        updatedPriceList.setId(id);
        updatedPriceList.setAreaType(AreaType.SITTING_AREA);
        updatedPriceList.setAreaId(areaId);

        when(priceListRepository.findById(id)).thenReturn(Optional.of(testPriceList));
        when(sittingAreaRepository.findById(areaId)).thenReturn(Optional.empty());

        priceListService.updatePriceList(id, updatedPriceList);
    }

    @Test(expected = InvalidAreaException.class)
    public void testUpdatePriceList_whenInvalidStandingArea_thenThrowInvalidAreaException() {
        Integer id = 1;

        StandingArea standingArea = new StandingArea();
        standingArea.setId(2);
        standingArea.setMaxNumber(200);

        updatedPriceList.setId(id);
        updatedPriceList.setAreaType(AreaType.STANDING_AREA);
        updatedPriceList.setAreaId(standingArea.getId());

        when(priceListRepository.findById(id)).thenReturn(Optional.of(testPriceList));
        when(standingAreaRepository.findById(standingArea.getId())).thenReturn(Optional.of(standingArea));

        priceListService.updatePriceList(id, updatedPriceList);
    }

    @Test(expected = InvalidAreaException.class)
    public void testUpdatePriceList_whenInvalidSittingArea_thenThrowInvalidAreaException() {
        Integer id = 1;

        SittingArea sittingArea = new SittingArea();
        sittingArea.setId(2);
        sittingArea.setNumRows(20);
        sittingArea.setNumSeats(20);

        updatedPriceList.setId(id);
        updatedPriceList.setAreaType(AreaType.SITTING_AREA);
        updatedPriceList.setAreaId(sittingArea.getId());

        when(priceListRepository.findById(id)).thenReturn(Optional.of(testPriceList));
        when(sittingAreaRepository.findById(sittingArea.getId())).thenReturn(Optional.of(sittingArea));

        priceListService.updatePriceList(id, updatedPriceList);
    }

    @Test(expected = NotFoundException.class)
    public void testUpdatePriceList_whenInvalidId_thenThrowNotFoundException() {
        Integer id = 444;
        updatedPriceList.setId(id);

        when(priceListRepository.findById(id)).thenReturn(Optional.empty());

        priceListService.updatePriceList(id, updatedPriceList);
    }

    @Test(expected = NotFoundException.class)
    public void testDeletePriceList_whenInvalidId_thenThrowNotFoundException() {
        Integer id = 1;
        when(priceListRepository.findById(id)).thenReturn(Optional.empty());

        priceListService.deletePriceList(id);
    }
}