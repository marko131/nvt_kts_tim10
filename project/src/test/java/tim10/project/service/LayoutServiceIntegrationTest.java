package tim10.project.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import tim10.project.model.*;
import tim10.project.service.LayoutService;
import tim10.project.service.exceptions.NotFoundException;

import java.math.BigDecimal;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;
import static org.testng.AssertJUnit.assertNotNull;

@ActiveProfiles("1")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LayoutServiceIntegrationTest {

    @Autowired
    private LayoutService layoutService;


    @Test
    public void testCreateLayout() {
        Layout layout = new Layout();
        layout.setLayoutName("TestLayout");
        layout.setStagePosition(Position.M);
        Layout created = layoutService.createLayout(layout);
        assertNotNull(created);
    }

    @Test(expected = NotFoundException.class)
    public void testGetLayoutByIdNotFound() {
        layoutService.getLayoutById(55);
    }

    @Test
    public void testGetLayoutByIdSuccessful() {
        Layout layout = layoutService.getLayoutById(1);
        assertNotNull(layout);
        assertNotNull(layout.getLocation());
    }

    @Test
    public void testGetSittingAreasByLayoutId() {
        List<SittingArea> sittingAreas = layoutService.getSittingAreasByLayoutId(1);
        assertNotNull(sittingAreas);
        assertEquals(0, sittingAreas.size());
    }

    @Test
    public void testGetStandingAreasByLayoutId() {
        List<StandingArea> standingAreas = layoutService.getStandingAreasByLayoutId(1);
        assertNotNull(standingAreas);
        assertEquals(1, standingAreas.size());
    }

    /*
    @Test
    public void updateLayout(){
        Layout layout = layoutService.getLayoutById(1);
        Location location = new Location();
        location.setName("Location");
        location.setAddress("Address");
        location.setLatitude(BigDecimal.valueOf(1.000000));
        location.setLongitude(BigDecimal.valueOf(1.000000));
        location.setPicture("imageLink");
        layout.setLocation(location);
        Layout updatedLayout = layoutService.updateLayout(1, layout);
        assertEquals(updatedLayout.getLocation().getName(),"Location");
    }
    */

}
