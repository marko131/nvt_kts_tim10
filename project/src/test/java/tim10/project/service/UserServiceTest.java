package tim10.project.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import tim10.project.model.User;
import tim10.project.repository.UserRepository;
import tim10.project.service.exceptions.PasswordsDoNotMatchException;
import tim10.project.service.exceptions.UserAlreadyExistsException;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;

@ActiveProfiles("20")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserServiceTest {

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private UserRepository userRepository;

    @Test(expected = UserAlreadyExistsException.class)
    public void testCreateUserAlreadyExists(){
        User user = new User();
        user.setEmail("asd@asd.com");
        User newUser = new User();
        newUser.setEmail("asd@asd.com");
        Mockito.when(userRepository.findByEmail(any(String.class))).thenReturn(user);
        userDetailsService.createUser(newUser);
    }

    @Test
    public void testCreateUserSuccess(){
        User newUser = new User();
        newUser.setEmail("asd@asd.com");
        newUser.setPassword("asdasdasd");
        Mockito.when(userRepository.findByEmail(any(String.class))).thenReturn(null);
        userDetailsService.createUser(newUser);
    }

    @Test(expected = PasswordsDoNotMatchException.class)
    public void testChangePasswordDoNotMatchException(){
        User user = new User();
        user.setPassword("asdasd");
        String pass1 = "sssdddaaa";
        String pass2 = "asdasdasdasd";
        userDetailsService.changePassword(user, pass1, pass2);
        //Mockito.when(userRepository.save(any(User.class))).thenReturn(user);
    }

    @Test
    public void testChangePasswordSuccess(){
        User user = new User();
        user.setPassword("asdasd");
        String pass1 = "sssdddaaa";
        String pass2 = "sssdddaaa";
        Mockito.when(userRepository.save(any(User.class))).thenReturn(user);
        User newUser = userDetailsService.changePassword(user, pass1, pass2);
        assertEquals(user.getPassword(), newUser.getPassword());
    }

    @Test(expected = UserAlreadyExistsException.class)
    public void testUpdateProfileFailed(){
        User user = new User();
        user.setEmail("asd@asd.com");
        User updateUser = new User();
        updateUser.setEmail("sss@sss.com");
        Mockito.when(userRepository.findByEmail(any(String.class))).thenReturn(user);
        userDetailsService.updateProfile(updateUser, "asd@asd.com", "sss", "sss");
    }

    @Test
    public void testUpdateProfileSuccess(){
        User updateUser = new User();
        updateUser.setEmail("sss@sss.com");
        updateUser.setName("aaa");
        updateUser.setLastName("aaa");
        Mockito.when(userRepository.findByEmail(any(String.class))).thenReturn(null);
        Mockito.when(userRepository.save(updateUser)).thenReturn(updateUser);
        User test = userDetailsService.updateProfile(updateUser, "aaa@aaa.com", "sss", "sss");
        assertEquals(updateUser.getEmail(), test.getEmail());
        assertEquals(updateUser.getName(), test.getName());
        assertEquals(updateUser.getLastName(), test.getLastName());
    }

    @Test(expected = UsernameNotFoundException.class)
    public void testLoadUserByUsernameNotFoundException(){
        Mockito.when(userRepository.findByEmail(any(String.class))).thenReturn(null);
        userDetailsService.loadUserByUsername("asd");
    }

    @Test
    public void testLoadUserByUsernameSuccess(){
        User user = new User();
        user.setEmail("asd@asd.com");
        user.setPassword("asd");
        user.setAdmin(false);
        Mockito.when(userRepository.findByEmail(any(String.class))).thenReturn(user);
        org.springframework.security.core.userdetails.UserDetails test = userDetailsService.loadUserByUsername("asd@asd.com");
        assertEquals("[ROLE_USER]", test.getAuthorities().toString());
    }
}
