package tim10.project.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import tim10.project.model.Ticket;
import tim10.project.model.TicketStatus;
import tim10.project.repository.TicketRepository;
import tim10.project.service.exceptions.NotFoundException;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TicketServiceUnitTest {

    @Autowired
    private TicketService ticketService;

    @MockBean
    private TicketRepository ticketRepository;

    @Test(expected = NotFoundException.class)
    public void testGetTicketByIdNotFoundException(){
        Mockito.when(ticketRepository.findById(any(Integer.class))).thenReturn(Optional.empty());
        ticketService.getTicketById(1);
    }

    @Test
    public void testGetTicketByIdSuccess(){
        Ticket ticket = new Ticket();
        Mockito.when(ticketRepository.findById(any(Integer.class))).thenReturn(Optional.of(ticket));
        assertNotNull(ticketService.getTicketById(1));
    }

    @Test
    public void testUpdateTicket(){
        Ticket ticket = new Ticket();
        int id = 1;
        ticket.setTicketStatus(TicketStatus.AVAILABLE);
        ticket.setId(id);
        Ticket newTicket = new Ticket();
        newTicket.setTicketStatus(TicketStatus.IS_RESERVED);
        Mockito.when(ticketRepository.findById(any(Integer.class))).thenReturn(Optional.of(ticket));
        Mockito.when(ticketRepository.save(ticket)).thenReturn(ticket);
        Ticket test = ticketService.updateTicket(id, newTicket);
        assertEquals(newTicket.getTicketStatus(), test.getTicketStatus());
    }

    @Test(expected = NotFoundException.class)
    public void testDeleteTicketNotFoundException(){
        Mockito.when(ticketRepository.findById(any(Integer.class))).thenReturn(Optional.empty());
        ticketService.deleteTicket(1);
    }
}
