package tim10.project.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import tim10.project.model.*;
import tim10.project.service.EventService;
import tim10.project.service.LayoutService;
import tim10.project.service.PriceListService;
import tim10.project.service.exceptions.*;
import tim10.project.service.specification.EventSpecification;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ActiveProfiles("1")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Sql(executionPhase= Sql.ExecutionPhase.BEFORE_TEST_METHOD,scripts="classpath:data1.sql")
public class EventServiceIntegrationTest {

    @Autowired
    private EventService eventService;
    @Autowired
    private LayoutService layoutService;

    @Test(expected = NotFoundException.class)
    public void getEventByIdNotFound() {
        eventService.getEventById(111);
    }

    @Test
    public void getEventByIdSuccessful() {
        Event event = eventService.getEventById(1);
        assertNotNull(event);
    }

    @Test(expected = InvalidDateException.class)
    public void testInvalidDateOnCreateEvent() {
        Event event = new Event();
        event.setDaysReservationIsValid(2);
        event.setDescription("Description");
        event.setEventCategory(EventCategory.CONCERT);
        event.setMaxReservationNumber(4);
        event.setName("Event");
        event.setPicture("img");
        event.setReservationsAvailable(new Date(0));
        event.setLayout(layoutService.getLayoutById(1));
        ArrayList<PriceList> priceLists = new ArrayList<>();
        PriceList priceList = new PriceList();
        priceList.setAreaId(1);
        priceList.setAreaType(AreaType.STANDING_AREA);
        priceList.setDate(new Date(0));
        priceLists.add(priceList);
        priceList.setPrice(2000.00);
        priceList.setEvent(event);
        event.setPriceLists(priceLists);
        eventService.createEvent(event);
    }

    @Test(expected = LayoutUnavailableForDateException.class)
    public void testUnavailableLayout() {
        Event event = new Event();
        event.setDaysReservationIsValid(2);
        event.setDescription("Description");
        event.setEventCategory(EventCategory.CONCERT);
        event.setMaxReservationNumber(4);
        event.setName("Event");
        event.setPicture("img");
        event.setReservationsAvailable(new Date(0));
        event.setLayout(layoutService.getLayoutById(1));
        ArrayList<PriceList> priceLists = new ArrayList<>();
        PriceList priceList = new PriceList();
        priceList.setAreaId(1);
        priceList.setAreaType(AreaType.STANDING_AREA);
        priceList.setDate(new Date(1609282800000L));
        priceLists.add(priceList);
        priceList.setPrice(2000.00);
        priceList.setEvent(event);
        event.setPriceLists(priceLists);
        eventService.createEvent(event);

    }

    @Test(expected = EventAlreadyExists.class)
    public void testCreateEventNameAlreadyExists() {
        Event event = new Event();
        event.setDaysReservationIsValid(2);
        event.setDescription("Description");
        event.setEventCategory(EventCategory.CONCERT);
        event.setMaxReservationNumber(4);
        event.setName("Event123");
        event.setPicture("img");
        event.setReservationsAvailable(new Date(0));
        event.setLayout(layoutService.getLayoutById(1));
        ArrayList<PriceList> priceLists = new ArrayList<>();
        PriceList priceList = new PriceList();
        priceList.setAreaId(1);
        priceList.setAreaType(AreaType.STANDING_AREA);
        priceList.setDate(new Date(1609282800000L));
        priceLists.add(priceList);
        priceList.setPrice(2000.00);
        priceList.setEvent(event);
        event.setPriceLists(priceLists);
        eventService.createEvent(event);
    }

    @Test
    public void testCreateEventSuccessful() {
        Event event = new Event();
        event.setDaysReservationIsValid(2);
        event.setDescription("Description");
        event.setEventCategory(EventCategory.CONCERT);
        event.setMaxReservationNumber(4);
        event.setName("Event1234");
        event.setPicture("img");
        event.setReservationsAvailable(new Date(0));
        event.setLayout(layoutService.getLayoutById(1));


        ArrayList<PriceList> priceLists = new ArrayList<>();
        PriceList priceList = new PriceList();
        priceList.setAreaId(1);
        priceList.setAreaType(AreaType.STANDING_AREA);
        priceList.setDate(new Date(1629282800000L));
        priceLists.add(priceList);
        priceList.setPrice(2000.00);
        priceList.setEvent(event);
        event.setPriceLists(priceLists);

        Event created = eventService.createEvent(event);
        assertNotNull(created.getId());
        assertEquals("Event1234", created.getName());
    }

    @Test(expected = AreaNotFoundException.class)
    public void testCreateEventSittingAreaNotFound() {
        Event event = new Event();
        event.setDaysReservationIsValid(2);
        event.setDescription("Description");
        event.setEventCategory(EventCategory.CONCERT);
        event.setMaxReservationNumber(4);
        event.setName("Event1234");
        event.setPicture("img");
        event.setReservationsAvailable(new Date(0));
        event.setLayout(layoutService.getLayoutById(1));

        ArrayList<PriceList> priceLists = new ArrayList<>();
        PriceList priceList = new PriceList();
        priceList.setAreaId(5);
        priceList.setAreaType(AreaType.SITTING_AREA);
        priceList.setDate(new Date(1629282800000L));
        priceLists.add(priceList);
        priceList.setPrice(2000.00);
        priceList.setEvent(event);
        event.setPriceLists(priceLists);

        Event created = eventService.createEvent(event);
        assertNotNull(created.getId());
        assertEquals("Event1234", created.getName());
    }

    @Test(expected = AreaNotFoundException.class)
    public void testCreateEventStandingAreaNotFound() {
        Event event = new Event();
        event.setDaysReservationIsValid(2);
        event.setDescription("Description");
        event.setEventCategory(EventCategory.CONCERT);
        event.setMaxReservationNumber(4);
        event.setName("Event1234");
        event.setPicture("img");
        event.setReservationsAvailable(new Date(0));
        event.setLayout(layoutService.getLayoutById(1));

        ArrayList<PriceList> priceLists = new ArrayList<>();
        PriceList priceList = new PriceList();
        priceList.setAreaId(5);
        priceList.setAreaType(AreaType.STANDING_AREA);
        priceList.setDate(new Date(1629282800000L));
        priceLists.add(priceList);
        priceList.setPrice(2000.00);
        priceList.setEvent(event);
        event.setPriceLists(priceLists);

        Event created = eventService.createEvent(event);
        assertNotNull(created.getId());
        assertEquals("Event1234", created.getName());
    }

    @Test(expected = InvalidAreaForLayout.class)
    public void testCreateEventInvalidAreaForLayout() {
        Event event = new Event();
        event.setDaysReservationIsValid(2);
        event.setDescription("Description");
        event.setEventCategory(EventCategory.CONCERT);
        event.setMaxReservationNumber(4);
        event.setName("Event12346");
        event.setPicture("img");
        event.setReservationsAvailable(new Date(0));
        event.setLayout(layoutService.getLayoutById(1));


        ArrayList<PriceList> priceLists = new ArrayList<>();
        PriceList priceList = new PriceList();
        priceList.setAreaId(2);
        priceList.setAreaType(AreaType.STANDING_AREA);
        priceList.setDate(new Date(1629282800000L));
        priceLists.add(priceList);
        priceList.setPrice(2000.00);
        priceList.setEvent(event);
        event.setPriceLists(priceLists);

        Event created = eventService.createEvent(event);
        assertNotNull(created.getId());
        assertEquals("Event1234", created.getName());
    }

    @Test
    public void testSearchWithNoParameters(){
        assertEquals(1, eventService.search(null, null, null, null, null, 0).size());
    }
    @Test
    public void testSearchNameContains(){
        assertEquals(1, eventService.search("event", null, null, null, null, 0).size());
    }

    @Test
    @Transactional
    public void testSearchDateBefore(){
        List<Event> events = eventService.search("", null, null, new Date(), null, 0);
        assertEquals(0, events.size());
    }

    @Test
    public void testSearchName(){
        assertEquals(0, eventService.search("asd", null, null, null, null, 0).size());
    }

    @Test
    public void testGetPriceListByEventId(){
        List<PriceList> priceLists = eventService.getPriceListsByEventId(1);
        assertEquals(1, priceLists.size());
    }
}
