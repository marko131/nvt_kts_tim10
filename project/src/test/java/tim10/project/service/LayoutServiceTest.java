package tim10.project.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import tim10.project.model.Layout;
import tim10.project.repository.LayoutRepository;
import tim10.project.service.LayoutService;
import tim10.project.service.exceptions.NotFoundException;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.testng.AssertJUnit.assertNotNull;

@ActiveProfiles("1")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LayoutServiceTest {

    @Autowired
    LayoutService layoutService;

    @MockBean
    LayoutRepository layoutRepository;

    @Test(expected = NotFoundException.class)
    public void testGetLayoutByIdNotFoundException(){
        Mockito.when(layoutRepository.findById(any(Integer.class))).thenReturn(Optional.empty());
        layoutService.getLayoutById(1);
    }

    @Test
    public void testGetLayoutById(){
        Layout layout = new Layout();
        Mockito.when(layoutRepository.save(any(Layout.class))).thenReturn(layout);
        Layout created = layoutService.createLayout(layout);
        assertNotNull(created);
    }

    @Test(expected = NotFoundException.class)
    public void testDeleteLayoutNotExist(){
        Mockito.when(layoutRepository.findById(anyInt())).thenReturn(Optional.empty());
        layoutService.deleteLayout(1);
    }



}
