package tim10.project.service;

import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import tim10.project.model.Ticket;
import tim10.project.model.TicketSittingArea;
import tim10.project.model.TicketStatus;
import tim10.project.model.User;
import tim10.project.service.exceptions.MaxReservationExceeded;
import tim10.project.service.exceptions.NoTicketsAvailable;
import tim10.project.service.exceptions.NotFoundException;
import tim10.project.service.exceptions.TicketAlreadyBookedException;
import tim10.project.web.dto.sittingArea.ReserveSittingAreaDTO;
import tim10.project.web.dto.sittingArea.SittingAreaTicketDTO;

import javax.mail.MessagingException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("20")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TicketServiceIntegrationTest {

    @Autowired
    private TicketService ticketService;

    @Autowired
    private UserDetailsServiceImpl userService;

    @Test(expected = NotFoundException.class)
    public void getTicketByIdNotFound() {
        ticketService.getTicketById(5000);
    }


    @Test
    public void getBookedTicketSittingAreaByPriceListId() {
        List<TicketSittingArea> list = ticketService.getBookedTicketSittingAreaByPriceListId(2);
        assertEquals(4, list.size());
    }

    @Test
    public void getReservedTicketForUser() {
        List<Ticket> list = ticketService.getReservedTicketForUser(userService.findUserByEmail("user@gmail.com"));
        assertEquals(6, list.size());
    }

    @Test
    public void getPurchasedTicketForUser() {
        List<Ticket> list = ticketService.getPurchasedTicketForUser(userService.findUserByEmail("user@gmail.com"));
        assertEquals(2, list.size());
    }

    @Test
    public void getTicketByIdSuccessful() {
        Ticket ticket = ticketService.getTicketById(305);
        assertNotNull(ticket);
    }

    @Test(expected = NotFoundException.class)
    public void updateTicketNotFound() {
        Ticket ticket = new Ticket();
        ticketService.updateTicket(5000, ticket);
    }

    @Test
    public void updateTicketSuccessful() {
        Ticket testTicket = ticketService.getTicketById(308);
        Ticket ticket = ticketService.updateTicket(309, testTicket);
        assertNotNull(ticket);
    }

    @Test(expected = NotFoundException.class)
    public void deleteTicketNotFound() {
        ticketService.deleteTicket(5000);
    }

    @Test
    public void deleteTicket() {
        int cnt = ticketService.getAllTickets().size();
        ticketService.deleteTicket(1);
        int cntAfterDelete = ticketService.getAllTickets().size();
        assertEquals(cnt - 1, cntAfterDelete);
    }

    @Test(expected = NotFoundException.class)
    public void reserveStandingAreaTicketsPriceListNotFound() {
        User user = new User();
        user.setEmail("user@gmail.com");
        user.setPassword("asd123");
        ticketService.reserveStandingAreaTickets(10, 1, user);
    }

    @Test(expected = NoTicketsAvailable.class)
    public void reserveStandingAreaTicketsNotEnough() {
        User user = new User();
        user.setEmail("user@gmail.com");
        user.setPassword("asd123");
        ticketService.reserveStandingAreaTickets(1, 50, user);
    }

    @Test(expected = MaxReservationExceeded.class)
    public void reserveStandingAreaTicketsMaxNumberReserved() {
        User user = userService.findUserByEmail("user@gmail.com");
        ticketService.reserveStandingAreaTickets(1, 1, user);
    }

    @Test
    public void reserveStandingAreaTicketsSuccessful() {
        User user = userService.findUserByEmail("user2@gmail.com");
        ticketService.reserveStandingAreaTickets(1, 1, user);
    }

    @Test(expected = NotFoundException.class)
    public void buyStandingAreaTicketsPriceListNotFound() throws MessagingException {
        User user = new User();
        user.setEmail("user@gmail.com");
        user.setPassword("asd123");
        ticketService.buyStandingAreaTickets(10, 1, user);
    }

    @Test(expected = NoTicketsAvailable.class)
    public void buyStandingAreaTicketsNotEnough() throws MessagingException {
        User user = new User();
        user.setEmail("user@gmail.com");
        user.setPassword("asd123");
        ticketService.buyStandingAreaTickets(1, 50, user);
    }

    @Test
    public void buyStandingAreaTicketsSuccessful() throws MessagingException {
        User user = userService.findUserByEmail("user@gmail.com");
        ticketService.buyStandingAreaTickets(1, 1, user);
    }

    @Test(expected = NotFoundException.class)
    public void reserveSittingAreaTicketsPriceListNotFound() {
        User user = new User();
        user.setEmail("user@gmail.com");
        user.setPassword("asd123");
        SittingAreaTicketDTO sittingAreaTicketDTO = new SittingAreaTicketDTO(6, 6);
        SittingAreaTicketDTO sittingAreaTicketDTO2 = new SittingAreaTicketDTO(6, 7);
        List<SittingAreaTicketDTO> listDTO = new ArrayList<>();
        listDTO.add(sittingAreaTicketDTO);
        listDTO.add(sittingAreaTicketDTO2);
        ReserveSittingAreaDTO reserveSittingAreaDTO = new ReserveSittingAreaDTO(20, listDTO);
        ticketService.reserveSittingAreaTickets(reserveSittingAreaDTO, user);
    }

    @Test(expected = MaxReservationExceeded.class)
    public void reserveSittingAreaTicketsMaxNumberReserved() {
        User user = userService.findUserByEmail("user@gmail.com");
        SittingAreaTicketDTO sittingAreaTicketDTO = new SittingAreaTicketDTO(6, 6);
        SittingAreaTicketDTO sittingAreaTicketDTO2 = new SittingAreaTicketDTO(6, 7);
        List<SittingAreaTicketDTO> listDTO = new ArrayList<>();
        listDTO.add(sittingAreaTicketDTO);
        listDTO.add(sittingAreaTicketDTO2);
        ReserveSittingAreaDTO reserveSittingAreaDTO = new ReserveSittingAreaDTO(2, listDTO);
        ticketService.reserveSittingAreaTickets(reserveSittingAreaDTO, user);
    }

    @Test(expected = TicketAlreadyBookedException.class)
    public void reserveSittingAreaTicketsTicketTaken() {
        User user = userService.findUserByEmail("user2@gmail.com");
        SittingAreaTicketDTO sittingAreaTicketDTO = new SittingAreaTicketDTO(5, 4);
        SittingAreaTicketDTO sittingAreaTicketDTO2 = new SittingAreaTicketDTO(6, 7);
        List<SittingAreaTicketDTO> listDTO = new ArrayList<>();
        listDTO.add(sittingAreaTicketDTO);
        listDTO.add(sittingAreaTicketDTO2);
        ReserveSittingAreaDTO reserveSittingAreaDTO = new ReserveSittingAreaDTO(2, listDTO);
        ticketService.reserveSittingAreaTickets(reserveSittingAreaDTO, user);
    }

    @Test
    public void reserveSittingAreaTicketsSuccessful() {
        User user = userService.findUserByEmail("user2@gmail.com");
        SittingAreaTicketDTO sittingAreaTicketDTO = new SittingAreaTicketDTO(1, 1);
        SittingAreaTicketDTO sittingAreaTicketDTO2 = new SittingAreaTicketDTO(1, 2);
        List<SittingAreaTicketDTO> listDTO = new ArrayList<>();
        listDTO.add(sittingAreaTicketDTO);
        listDTO.add(sittingAreaTicketDTO2);
        ReserveSittingAreaDTO reserveSittingAreaDTO = new ReserveSittingAreaDTO(3, listDTO);
        ticketService.reserveSittingAreaTickets(reserveSittingAreaDTO, user);
    }

    @Test(expected = NotFoundException.class)
    public void buySittingAreaTicketsPriceListNotFound() throws MessagingException {
        User user = new User();
        user.setEmail("user@gmail.com");
        user.setPassword("asd123");
        SittingAreaTicketDTO sittingAreaTicketDTO = new SittingAreaTicketDTO(6, 6);
        SittingAreaTicketDTO sittingAreaTicketDTO2 = new SittingAreaTicketDTO(6, 7);
        List<SittingAreaTicketDTO> listDTO = new ArrayList<>();
        listDTO.add(sittingAreaTicketDTO);
        listDTO.add(sittingAreaTicketDTO2);
        ReserveSittingAreaDTO reserveSittingAreaDTO = new ReserveSittingAreaDTO(20, listDTO);
        ticketService.buySittingAreaTickets(reserveSittingAreaDTO, user);
    }

    @Test(expected = TicketAlreadyBookedException.class)
    public void buySittingAreaTicketsTicketTaken() throws MessagingException {
        User user = userService.findUserByEmail("user2@gmail.com");
        SittingAreaTicketDTO sittingAreaTicketDTO = new SittingAreaTicketDTO(5, 4);
        SittingAreaTicketDTO sittingAreaTicketDTO2 = new SittingAreaTicketDTO(6, 7);
        List<SittingAreaTicketDTO> listDTO = new ArrayList<>();
        listDTO.add(sittingAreaTicketDTO);
        listDTO.add(sittingAreaTicketDTO2);
        ReserveSittingAreaDTO reserveSittingAreaDTO = new ReserveSittingAreaDTO(2, listDTO);
        ticketService.buySittingAreaTickets(reserveSittingAreaDTO, user);
    }

    @Test(expected = NoTicketsAvailable.class)
    public void buyReservedTicketsNotReserved() {
        User user = userService.findUserByEmail("user@gmail.com");
        List<Integer> list = new ArrayList<>();
        list.add(10);
        list.add(20);
        ticketService.buyReservedTickets(list, user);
    }

    @Test
    public void buyReservedTicketsSuccessful() {
        User user = userService.findUserByEmail("user@gmail.com");
        List<Integer> list = new ArrayList<>();
        list.add(20);
        list.add(220);
        ticketService.buyReservedTickets(list, user);
    }


}
