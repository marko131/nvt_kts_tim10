package tim10.project.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import tim10.project.model.Layout;
import tim10.project.model.StandingArea;
import tim10.project.repository.LayoutRepository;
import tim10.project.repository.StandingAreaRepository;
import tim10.project.service.exceptions.NotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StandingAreaServiceUnitTest {

    @Autowired
    private StandingAreaService standingAreaService;

    @MockBean
    private StandingAreaRepository standingAreaRepository;

    @MockBean
    private LayoutRepository layoutRepository;

    @Test(expected = NotFoundException.class)
    public void testGetStandingAreaByIdNotFoundException(){
        Mockito.when(standingAreaRepository.findById(any(Integer.class))).thenReturn(Optional.empty());
        standingAreaService.getStandingAreaById(1);
    }

    @Test
    public void testGetStandingAreaByIdSuccess(){
        StandingArea standingArea = new StandingArea();
        Mockito.when(standingAreaRepository.findById(any(Integer.class))).thenReturn(Optional.of(standingArea));
        assertNotNull(standingAreaService.getStandingAreaById(1));
    }

    @Test
    public void testGetStandingAreasByLayoutId(){
        Layout layout = new Layout();
        layout.setId(1);
        List<StandingArea> list = new ArrayList<>();
        StandingArea standingArea1 = new StandingArea();
        StandingArea standingArea2 = new StandingArea();
        list.add(standingArea1);
        list.add(standingArea2);
        layout.setStandingAreas(list);
        Mockito.when(standingAreaRepository.getAllByLayoutId(any(Integer.class))).thenReturn(list);
        Mockito.when(layoutRepository.findById(any(Integer.class))).thenReturn(Optional.of(layout));
        assertEquals(2, standingAreaService.getStandingAreasByLayoutId(1).size());
    }

    @Test
    public void testUpdateStandingArea(){
        StandingArea standingArea = new StandingArea();
        int id = 1;
        standingArea.setMaxNumber(5);
        standingArea.setId(id);
        StandingArea newStandingArea = new StandingArea();
        newStandingArea.setMaxNumber(2);
        Mockito.when(standingAreaRepository.findById(any(Integer.class))).thenReturn(Optional.of(standingArea));
        Mockito.when(standingAreaRepository.save(standingArea)).thenReturn(standingArea);
        StandingArea test = standingAreaService.updateStandingArea(id, newStandingArea);
        assertEquals(newStandingArea.getMaxNumber(), test.getMaxNumber());
    }

    @Test(expected = NotFoundException.class)
    public void testDeleteStandingAreaNotFoundException(){
        Mockito.when(standingAreaRepository.findById(any(Integer.class))).thenReturn(Optional.empty());
        standingAreaService.deleteStandingArea(1);
    }

    @Test
    public void testDeleteStandingSuccessful(){
        StandingArea standingArea = new StandingArea();
        standingArea.setId(1);
        Mockito.when(standingAreaRepository.findById(any(Integer.class))).thenReturn(Optional.of(standingArea));
        standingAreaService.deleteStandingArea(1);
    }

    @Test
    public void testCreateStandingArea(){
        StandingArea standingArea = new StandingArea();
        standingArea.setId(1);
        Layout layout = new Layout();
        layout.setId(1);
        standingArea.setLayout(layout);
        Mockito.when(standingAreaRepository.save(any(StandingArea.class))).thenReturn(standingArea);
        Mockito.when(layoutRepository.findById(any(Integer.class))).thenReturn(Optional.of(layout));
        StandingArea created = standingAreaService.createStandingArea(standingArea);
        assertEquals(standingArea.getId(), created.getId());
    }
}
