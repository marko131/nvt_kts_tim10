package tim10.project.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import tim10.project.model.*;
import tim10.project.repository.LayoutRepository;
import tim10.project.repository.LocationRepository;
import tim10.project.service.exceptions.DatabaseSaveException;
import tim10.project.service.exceptions.NotFoundException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ActiveProfiles("2")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LocationServiceUnitTest {

    @Autowired
    private LocationService locationService;

    @MockBean
    private LocationRepository locationRepository;

    @MockBean
    private LayoutRepository layoutRepository;

    @Test
    public void testCreateLocation_whenValid_thenReturnLocation() {
        Location testLocation = new Location("Test Location", "Test address", BigDecimal.valueOf(19.232), BigDecimal.valueOf(45.432), "picture/path",  new ArrayList<>());

        when(locationRepository.save(testLocation)).thenReturn(testLocation);

        Location resultLocation = locationService.createLocation(testLocation);

        assertEquals(testLocation.getName(), resultLocation.getName());
        assertEquals(testLocation.getAddress(), resultLocation.getAddress());
        assertEquals(testLocation.getLatitude(), resultLocation.getLatitude());
        assertEquals(testLocation.getLongitude(), resultLocation.getLongitude());
        assertEquals(testLocation.getLayouts().size(), resultLocation.getLayouts().size());
    }

    @Test(expected = DatabaseSaveException.class)
    public void testCreateLocation_whenNotUniqueName_thenThrowDatabaseSaveException() {
        Location testLocation = new Location("Test Location", "Test address", BigDecimal.valueOf(19.232), BigDecimal.valueOf(45.432), "picture/path",  new ArrayList<>());
        when(locationRepository.save(testLocation)).thenThrow(DataIntegrityViolationException.class);

        locationService.createLocation(testLocation);
    }

    @Test
    public void testGetLocationById_whenValidLocation_thenReturnLocation() {
        Integer id = 1;
        Location testLocation = new Location("Test Location", "Test address", BigDecimal.valueOf(19.232), BigDecimal.valueOf(45.432), "picture/path",  new ArrayList<>());
        testLocation.setId(id);

        when(locationRepository.findById(id)).thenReturn(Optional.of(testLocation));

        Location resultLocation = locationService.getLocationById(id);

        assertEquals(testLocation.getId(), resultLocation.getId());
        assertEquals(testLocation.getName(), resultLocation.getName());
        assertEquals(testLocation.getAddress(), resultLocation.getAddress());
        assertEquals(testLocation.getLatitude(), resultLocation.getLatitude());
        assertEquals(testLocation.getLongitude(), resultLocation.getLongitude());
        assertEquals(testLocation.getLayouts().size(), resultLocation.getLayouts().size());
    }

    @Test(expected = NotFoundException.class)
    public void testGetLocationById_whenInvalidId_throwNotFoundException() {
        Integer id = 1;
        when(locationRepository.findById(id)).thenReturn(Optional.empty());

        locationService.getLocationById(id);
    }

    @Test
    public void testGetAllLocations_whenNoLocations_returnEmptyList() {
        when(locationRepository.findAll()).thenReturn(new ArrayList<>());

        List<Location> resultLocations = locationService.getAllLocations();
        assertTrue(resultLocations.isEmpty());
    }

    @Test
    public void testGetAllLocations_whenValid() {
        Location location1 = new Location("Test Location 1", "Test address", BigDecimal.valueOf(19.232), BigDecimal.valueOf(45.432), "picture/path1",  new ArrayList<>());
        Location location2 = new Location("Test Location 2", "Test address", BigDecimal.valueOf(19.745), BigDecimal.valueOf(45.221), "picture/path2",  new ArrayList<>());
        List<Location> testLocations = Arrays.asList(location1, location2);

        when(locationRepository.findAll()).thenReturn(testLocations);

        List<Location> resultLocations = locationService.getAllLocations();

        assertFalse(resultLocations.isEmpty());
        assertEquals(2, resultLocations.size());

        assertEquals(location1.getName(), resultLocations.get(0).getName());
        assertEquals(location1.getAddress(), resultLocations.get(0).getAddress());
        assertEquals(location1.getLatitude(), resultLocations.get(0).getLatitude());
        assertEquals(location1.getLongitude(), resultLocations.get(0).getLongitude());
        assertEquals(location1.getLayouts().size(), resultLocations.get(0).getLayouts().size());

        assertEquals(location2.getName(), resultLocations.get(1).getName());
        assertEquals(location2.getAddress(), resultLocations.get(1).getAddress());
        assertEquals(location2.getLatitude(), resultLocations.get(1).getLatitude());
        assertEquals(location2.getLongitude(), resultLocations.get(1).getLongitude());
        assertEquals(location2.getLayouts().size(), resultLocations.get(1).getLayouts().size());
    }

    @Test(expected = NotFoundException.class)
    public void testGetLayoutsByLocationId_whenInvalidId_throwNotFoundException() {
        Integer id = 1;
        when(locationRepository.findById(id)).thenReturn(Optional.empty());

        locationService.getLayoutsByLocationId(id);
    }

    @Test
    public void testGetLayoutByLocationId_whenNoLayouts_returnEmptyList() {
        Integer id = 1;
        Location testLocation = new Location("Test Location", "Test address", BigDecimal.valueOf(19.232), BigDecimal.valueOf(45.432), "picture/path",  new ArrayList<>());
        testLocation.setId(id);

        when(locationRepository.findById(id)).thenReturn(Optional.of(testLocation));
        when(layoutRepository.findAllByLocationId(id)).thenReturn(new ArrayList<>());

        List<Layout> resultLayouts =  locationService.getLayoutsByLocationId(id);
        assertTrue(resultLayouts.isEmpty());
    }

    @Test
    public void testGetLayoutByLocationId_whenValidId_thenReturnLayoutsList() {
        Integer id = 1;
        Location testLocation = new Location("Test Location", "Test address", BigDecimal.valueOf(19.232), BigDecimal.valueOf(45.432), "picture/path",  new ArrayList<>());
        testLocation.setId(id);

        Layout layout1 = new Layout(new ArrayList<>(), new ArrayList<>(), "Layout1", testLocation, Position.M);
        Layout layout2 = new Layout(new ArrayList<>(), new ArrayList<>(), "Layout2", testLocation, Position.M);
        Layout layout3 = new Layout(new ArrayList<>(), new ArrayList<>(), "Layout2", testLocation, Position.M);

        SittingArea area1 = new SittingArea(10, 5, layout1, Position.E);
        SittingArea area2 = new SittingArea(13, 15, layout1, Position.W);
        layout1.getSittingAreas().add(area1);
        layout1.getSittingAreas().add(area2);

        StandingArea area3 = new StandingArea(1500, layout2, Position.N);
        layout2.getStandingAreas().add(area3);

        List<Layout> testLayouts = Arrays.asList(layout1, layout2, layout3);

        when(locationRepository.findById(id)).thenReturn(Optional.of(testLocation));
        when(layoutRepository.findAllByLocationId(id)).thenReturn(testLayouts);

        List<Layout> resultLayouts =  locationService.getLayoutsByLocationId(id);

        assertFalse(resultLayouts.isEmpty());
        assertEquals(3, resultLayouts.size());

        // provera velicina area lista
        assertEquals(layout1.getStandingAreas().size(), resultLayouts.get(0).getStandingAreas().size());
        assertEquals(layout1.getSittingAreas().size(), resultLayouts.get(0).getSittingAreas().size());

        assertEquals(layout2.getStandingAreas().size(), resultLayouts.get(1).getStandingAreas().size());
        assertEquals(layout2.getSittingAreas().size(), resultLayouts.get(1).getSittingAreas().size());

        assertEquals(layout3.getStandingAreas().size(), resultLayouts.get(2).getStandingAreas().size());
        assertEquals(layout3.getSittingAreas().size(), resultLayouts.get(2).getSittingAreas().size());

        // poredjenje vrednosti u area poljima
        assertEquals(layout1.getSittingAreas().get(0).getNumRows(), resultLayouts.get(0).getSittingAreas().get(0).getNumRows());
        assertEquals(layout1.getSittingAreas().get(0).getNumSeats(), resultLayouts.get(0).getSittingAreas().get(0).getNumSeats());
        assertEquals(layout1, resultLayouts.get(0).getSittingAreas().get(0).getLayout());

        assertEquals(layout2.getStandingAreas().get(0).getMaxNumber(), resultLayouts.get(1).getStandingAreas().get(0).getMaxNumber());
        assertEquals(layout2, resultLayouts.get(1).getStandingAreas().get(0).getLayout());
    }

    @Test(expected = NotFoundException.class)
    public void testUpdateLocation_whnInvalidId_throwNotFoundException() {
        Integer id = 1;
        Location testLocation = new Location("Test Location", "Test address", BigDecimal.valueOf(19.232), BigDecimal.valueOf(45.432), "picture/path",  new ArrayList<>());
        testLocation.setId(id);

        when(locationRepository.findById(id)).thenReturn(Optional.empty());

        locationService.updateLocation(id, testLocation);
    }

    @Test
    public void testUpdateLocation_whenValidLocation_thenReturnUpdatedLocation() {
        Integer id = 1;
        Location testLocation = new Location("Test Location", "Test address", BigDecimal.valueOf(19.232), BigDecimal.valueOf(45.432), "picture/path",  new ArrayList<>());
        testLocation.setId(id);

        Location updatedLocation = new Location();
        updatedLocation.setName("Updated test Location");
        updatedLocation.setAddress("Updated test address");
        updatedLocation.setPicture("updated/picture/path");

        when(locationRepository.findById(id)).thenReturn(Optional.of(testLocation));
        when(locationRepository.save(testLocation)).thenReturn(testLocation);

        Location resultLocation = locationService.updateLocation(id, updatedLocation);

        assertEquals(updatedLocation.getName(), resultLocation.getName());
        assertEquals(updatedLocation.getAddress(), resultLocation.getAddress());
        assertEquals(updatedLocation.getPicture(), resultLocation.getPicture());
    }

    @Test(expected = NotFoundException.class)
    public void testDeleteLocation_whenInvalidId_thenThrowNotFoundException() {
        Integer id = 1;
        when(locationRepository.findById(id)).thenReturn(Optional.empty());

        locationService.deleteLocation(id);
    }
}