package tim10.project.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.testng.AssertJUnit;
import tim10.project.model.Layout;
import tim10.project.model.SittingArea;
import tim10.project.repository.LayoutRepository;
import tim10.project.repository.SittingAreaRepository;
import tim10.project.service.exceptions.NotFoundException;

import javax.validation.constraints.Max;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.AssertJUnit.assertNotNull;

@ActiveProfiles("20")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SittingAreaServiceTest {

    @Autowired
    private SittingAreaService sittingAreaService;

    @MockBean
    private SittingAreaRepository sittingAreaRepository;

    @MockBean
    private LayoutRepository layoutRepository;

    @Test(expected = NotFoundException.class)
    public void testGetSittingAreaByIdNotFoundException(){
        Mockito.when(sittingAreaRepository.findById(any(Integer.class))).thenReturn(Optional.empty());
        sittingAreaService.getSittingAreaById(1);
    }

    @Test
    public void testGetSittingAreaByIdSuccess(){
        SittingArea sittingArea = new SittingArea();
        Mockito.when(sittingAreaRepository.findById(any(Integer.class))).thenReturn(Optional.of(sittingArea));
        assertNotNull(sittingAreaService.getSittingAreaById(1));
    }

    @Test
    public void testGetSittingAreasByLayoutId(){
        Layout layout = new Layout();
        layout.setId(1);
        List<SittingArea> list = new ArrayList<>();
        SittingArea sittingArea1 = new SittingArea();
        SittingArea sittingArea2 = new SittingArea();
        list.add(sittingArea1);
        list.add(sittingArea2);
        layout.setSittingAreas(list);
        Mockito.when(sittingAreaRepository.getAllByLayoutId(any(Integer.class))).thenReturn(list);
        Mockito.when(layoutRepository.findById(any(Integer.class))).thenReturn(Optional.of(layout));
        assertEquals(2, sittingAreaService.getSittingAreasByLayoutId(1).size());
    }

    @Test
    public void testUpdateSittingArea(){
        SittingArea sittingArea = new SittingArea();
        int id = 1;
        sittingArea.setNumRows(5);
        sittingArea.setNumSeats(5);
        sittingArea.setId(id);
        SittingArea newSittingArea = new SittingArea();
        newSittingArea.setNumSeats(2);
        newSittingArea.setNumRows(2);
        Mockito.when(sittingAreaRepository.findById(any(Integer.class))).thenReturn(Optional.of(sittingArea));
        Mockito.when(sittingAreaRepository.save(sittingArea)).thenReturn(sittingArea);
        SittingArea test = sittingAreaService.updateSittingArea(id, newSittingArea);
        assertEquals(newSittingArea.getNumRows(), test.getNumRows());
        assertEquals(newSittingArea.getNumSeats(), test.getNumSeats());
    }

    @Test(expected = NotFoundException.class)
    public void testDeleteSittingAreaNotFoundException(){
        Mockito.when(sittingAreaRepository.findById(any(Integer.class))).thenReturn(Optional.empty());
        sittingAreaService.deleteSittingArea(1);
    }

    @Test
    public void testCreateSittingArea(){
        SittingArea sittingArea = new SittingArea();
        sittingArea.setId(1);
        Layout layout = new Layout();
        layout.setId(1);
        sittingArea.setLayout(layout);
        when(sittingAreaRepository.save(any(SittingArea.class))).thenReturn(sittingArea);
        Mockito.when(layoutRepository.findById(any(Integer.class))).thenReturn(Optional.of(layout));
        SittingArea created = sittingAreaService.createSittingArea(sittingArea);
        assertEquals(sittingArea.getId(), created.getId());
    }
}
