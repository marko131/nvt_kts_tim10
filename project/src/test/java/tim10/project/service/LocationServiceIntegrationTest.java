package tim10.project.service;

import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import tim10.project.model.Layout;
import tim10.project.model.Location;
import tim10.project.service.exceptions.NotFoundException;
import tim10.project.service.exceptions.UniqueViolationException;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("2")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class LocationServiceIntegrationTest {

    @Autowired
    private LocationService locationService;

    @Test
    public void testCreateLocation_whenValid_thenReturnLocation() {
        Location testLocation = new Location("Test Location", "Test address", BigDecimal.valueOf(19.232), BigDecimal.valueOf(45.432), "picture/path",  new ArrayList<>());

        Location resultLocation = locationService.createLocation(testLocation);

        assertEquals(testLocation.getName(), resultLocation.getName());
        assertEquals(testLocation.getAddress(), resultLocation.getAddress());
        assertEquals(testLocation.getLatitude(), resultLocation.getLatitude());
        assertEquals(testLocation.getLongitude(), resultLocation.getLongitude());
        assertEquals(testLocation.getLayouts().size(), resultLocation.getLayouts().size());
    }

    @Test(expected = UniqueViolationException.class)
    public void testCreateLocation_whenNotUniqueName_thenThrowUniqueViolationException() {
        Location testLocation = new Location("Test", "Test address", BigDecimal.valueOf(19.232), BigDecimal.valueOf(45.432), "picture/path",  new ArrayList<>());
        locationService.createLocation(testLocation);
    }

    @Test
    @Transactional
    public void testGetLocationById_whenValidLocation_thenReturnLocation() {
        Integer id = 2;
        Location resultLocation = locationService.getLocationById(id);

        assertEquals(2, resultLocation.getId());
        assertEquals("Srpsko narodno pozoriste", resultLocation.getName());
        assertEquals("Pozorišni trg 1, Novi Sad 21000", resultLocation.getAddress());
        assertEquals(BigDecimal.valueOf(19.843016), resultLocation.getLongitude());
        assertEquals(BigDecimal.valueOf(45.255009), resultLocation.getLatitude());
        assertEquals(1, resultLocation.getLayouts().size());
    }

    @Test(expected = NotFoundException.class)
    public void testGetLocationById_whenInvalidId_throwNotFoundException() {
        Integer id = 444;
        locationService.getLocationById(id);
    }

    @Test
    @Order(1)
    public void testGetAllLocations_whenValid_thenReturnLocationsList() {
        List<Location> resultLocations = locationService.getAllLocations();

        assertFalse(resultLocations.isEmpty());
        assertEquals(3, resultLocations.size());
    }

    @Test(expected = NotFoundException.class)
    public void testGetLayoutsByLocationId_whenInvalidId_throwNotFoundException() {
        Integer id = 44;
        locationService.getLayoutsByLocationId(id);
    }

    @Test
    @Transactional
    public void testGetLayoutByLocationId_whenValidId_thenReturnLayoutsList() {
        Integer id = 2;

        List<Layout> resultLayouts =  locationService.getLayoutsByLocationId(id);

        assertFalse(resultLayouts.isEmpty());
        assertEquals(1, resultLayouts.size());

        // provera velicina area lista
        assertEquals(1, resultLayouts.get(0).getStandingAreas().size());
        assertEquals(1, resultLayouts.get(0).getSittingAreas().size());

        // poredjenje vrednosti u area poljima
        assertEquals(15, resultLayouts.get(0).getSittingAreas().get(0).getNumRows());
        assertEquals(20, resultLayouts.get(0).getSittingAreas().get(0).getNumSeats());
    }

    @Test(expected = NotFoundException.class)
    public void testUpdateLocation_whnInvalidId_throwNotFoundException() {
        Integer id = 444;
        Location testLocation = new Location("Test Location", "Test address", BigDecimal.valueOf(19.232), BigDecimal.valueOf(45.432), "picture/path",  new ArrayList<>());
        testLocation.setId(id);

        locationService.updateLocation(id, testLocation);
    }

    @Test
    public void testUpdateLocation_whenValidLocation_thenReturnUpdatedLocation() {
        Integer id = 1;
        Location testLocation = new Location("Test Location", "Test address", BigDecimal.valueOf(19.232), BigDecimal.valueOf(45.432), "picture/path",  new ArrayList<>());
        testLocation.setId(id);

        Location updatedLocation = new Location();
        updatedLocation.setName("Updated test Location");
        updatedLocation.setAddress("Updated test address");
        updatedLocation.setPicture("updated/picture/path");

        Location resultLocation = locationService.updateLocation(id, updatedLocation);

        assertEquals(updatedLocation.getName(), resultLocation.getName());
        assertEquals(updatedLocation.getAddress(), resultLocation.getAddress());
        assertEquals(updatedLocation.getPicture(), resultLocation.getPicture());
    }

    @Test(expected = NotFoundException.class)
    public void testDeleteLocation_whenInvalidId_thenThrowNotFoundException() {
        Integer id = 444;
        locationService.deleteLocation(id);
    }

    @Test
    public void testDeleteLocation_whenValid_ReturnDecrementedCountLocation() {
        int cnt = locationService.getAllLocations().size();

        Integer id = 3;
        locationService.deleteLocation(id);

        int cntAfterDelete = locationService.getAllLocations().size();

        assertEquals(cnt - 1, cntAfterDelete);
    }
}