package tim10.project.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import tim10.project.model.Layout;
import tim10.project.model.Position;
import tim10.project.model.SittingArea;
import tim10.project.repository.LayoutRepository;
import tim10.project.service.exceptions.NotFoundException;

import javax.transaction.Transactional;

import static org.junit.Assert.assertEquals;

@ActiveProfiles("20")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class SittingAreaServiceIntegrationTest {

    @Autowired
    private SittingAreaService sittingAreaService;

    @Autowired
    private LayoutRepository layoutRepository;

    private SittingArea testSittingArea;

    private SittingArea updatedSittingArea;

    @Before
    public void setUp() {

        Layout layout = layoutRepository.getOne(1);

        testSittingArea = new SittingArea();
        testSittingArea.setNumRows(10);
        testSittingArea.setNumSeats(10);
        testSittingArea.setLayout(layout);
        testSittingArea.setLayoutPosition(Position.E);

        updatedSittingArea = new SittingArea();
        updatedSittingArea.setNumSeats(testSittingArea.getNumSeats());
        updatedSittingArea.setNumRows(testSittingArea.getNumRows());
        updatedSittingArea.setLayout(testSittingArea.getLayout());
        updatedSittingArea.setLayoutPosition(testSittingArea.getLayoutPosition());
    }

    @Test
    @Transactional
    public void testCreateSittingAreaSuccessful() {
        SittingArea resultStandingArea = sittingAreaService.createSittingArea(testSittingArea);

        assertEquals(testSittingArea.getNumRows(), resultStandingArea.getNumRows());
        assertEquals(testSittingArea.getNumSeats(), resultStandingArea.getNumSeats());
        assertEquals(testSittingArea.getLayout(), resultStandingArea.getLayout());
    }

    @Test(expected = NotFoundException.class)
    public void testCreateSittingAreaLayoutNotFound() {
        Layout layout = new Layout();
        layout.setId(500);
        testSittingArea.setLayout(layout);

        sittingAreaService.createSittingArea(testSittingArea);
    }

    @Test
    public void testGetSittingAreaByIdSuccessful() {
        Integer id = 1;
        sittingAreaService.getSittingAreaById(1);
    }

    @Test(expected = NotFoundException.class)
    public void testGetSittingAreaByIdNotFound() {
        Integer id = 500;
        sittingAreaService.getSittingAreaById(id);
    }

    @Test
    public void testGetSittingAreasByLayoutIdSuccessful() {
        Integer id = 1;
        sittingAreaService.getSittingAreasByLayoutId(id);
    }

    @Test(expected = NotFoundException.class)
    public void testGetSittingAreasByLayoutIdNotFound() {
        Integer id = 500;
        sittingAreaService.getSittingAreasByLayoutId(id);
    }

    @Test
    @Transactional
    public void testUpdateSittingAreaSuccessful() {
        Integer id = 1;
        updatedSittingArea.setNumRows(13);
        updatedSittingArea.setNumSeats(13);

        SittingArea result = sittingAreaService.updateSittingArea(id, updatedSittingArea);
        assertEquals(updatedSittingArea.getNumSeats(), result.getNumSeats());
        assertEquals(updatedSittingArea.getNumRows(), result.getNumRows());
    }

    @Test(expected = NotFoundException.class)
    public void testUpdateSittingAreaNotFound() {
        Integer id = 500;
        updatedSittingArea.setNumRows(13);
        updatedSittingArea.setNumSeats(13);

        sittingAreaService.updateSittingArea(id, updatedSittingArea);
    }

    @Test(expected = NotFoundException.class)
    public void testDeleteSittingAreaNotFound() {
        Integer id = 500;
        sittingAreaService.deleteSittingArea(id);
    }

    @Test
    public void testDeleteSittingAreaSuccessful() {
        int cnt = sittingAreaService.getAllSittingAreas().size();
        Integer id = 1;
        sittingAreaService.deleteSittingArea(id);
        int cntAfterDelete = sittingAreaService.getAllSittingAreas().size();
        assertEquals(cnt - 1, cntAfterDelete);
    }
}
