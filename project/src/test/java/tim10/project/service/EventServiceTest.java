package tim10.project.service;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import tim10.project.model.*;
import tim10.project.repository.*;
import tim10.project.service.exceptions.*;
import tim10.project.service.specification.EventSpecification;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ActiveProfiles("1")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EventServiceTest {

    @Autowired
    EventService eventService;

    @MockBean
    EventRepository eventRepository;

    @MockBean
    PriceListRepository priceListRepository;

    @MockBean
    TicketRepository ticketRepository;

    @MockBean
    TicketSittingAreaRepository ticketSittingAreaRepository;

    @MockBean
    StandingAreaRepository standingAreaRepository;

    @MockBean
    SittingAreaRepository sittingAreaRepository;

    @Test(expected = InvalidDateException.class)
    public void testInvalidDateOnCreateEvent(){
        Event event = new Event();
        event.setName("Event");
        ArrayList<PriceList> priceLists = new ArrayList<>();
        PriceList priceList = new PriceList();
        priceList.setDate(new Date(0));
        priceLists.add(priceList);
        event.setPriceLists(priceLists);
        when(eventRepository.save(any(Event.class))).thenReturn(event);
        eventService.createEvent(new Event());
    }

    @Test(expected = LayoutUnavailableForDateException.class)
    public void testUnavailableLayout(){
        Event event = new Event();
        event.setName("Event");
        when(eventRepository.save(event)).thenReturn(event);

        List<PriceList> priceLists = new ArrayList<>();
        PriceList priceList = new PriceList();
        priceList.setDate(new Date(Long.parseLong("9999999999999")));
        priceLists.add(priceList);
        event.setPriceLists(priceLists);

        List<PriceList> checkPriceLists = new ArrayList<>();
        checkPriceLists.add(priceList);
        checkPriceLists.add(priceList);
        Layout layout = new Layout();
        layout.setId(1);
        event.setLayout(layout);
        priceList.setEvent(event);

        when(priceListRepository.findAllByDateAndEventLayoutId(priceList.getDate(), priceList.getEvent().getLayout().getId())).thenReturn(checkPriceLists);
        Event created = eventService.createEvent(event);
        assertEquals(created.getName(), "Event");
    }

    @Test(expected = NotFoundException.class)
    public void testNotFoundEventException(){
        Event event = new Event();
        event.setName("ev1");
        event.setDescription("dsc");
        event.setEventCategory(EventCategory.FESTIVAL);
        event.setPicture("img123");

        Mockito.when(eventRepository.findById(any(Integer.class))).thenReturn(Optional.empty());
        eventService.updateEvent(1, event);
    }

    @Test
    public void testCreateEvent(){
        Event event = new Event();
        event.setName("Event");
        when(eventRepository.save(any(Event.class))).thenReturn(event);

        Event created = eventService.createEvent(event);
        assertEquals(event.getName(), created.getName());
    }

    @Test
    public void testSearchWithNoParameters(){
        Specification<Event> specification = Specification
                .where(EventSpecification.eventNameContains(null))
                .and(EventSpecification.eventCategories(null))
                .and(EventSpecification.eventDateAfter(null))
                .and(EventSpecification.eventLocation(null));
        List<Event> events = new ArrayList<>();

        Location location = new Location();
        location.setName("Location");
        Layout layout = new Layout();
        layout.setLocation(location);


        Event event = new Event();
        event.setName("Event");
        ArrayList<PriceList> priceLists = new ArrayList<>();
        PriceList priceList = new PriceList();
        priceList.setDate(new Date(0));
        priceLists.add(priceList);
        event.setPriceLists(priceLists);
        event.setLayout(layout);

        Event event2 = new Event();
        event2.setName("Event");
        priceList.setDate(new Date(0));
        priceLists.add(priceList);
        event2.setPriceLists(priceLists);
        event2.setLayout(layout);

        events.add(event);
        events.add(event2);
        Mockito.when(eventRepository.findAll(any(Specification.class),  any(Pageable.class))).thenReturn(new PageImpl<>(events));
        assertEquals(2, eventService.search(null, null, null, null, null, 0).size());
    }

    @Test
    public void testUpdateEvent(){
        Event eventToUpdate = new Event();

        Event newEvent = new Event();
        newEvent.setName("Event");
        newEvent.setDescription("Description");
        newEvent.setEventCategory(EventCategory.CONCERT);
        newEvent.setPicture("img");

        Mockito.when(eventRepository.findById(any(Integer.class))).thenReturn(java.util.Optional.of(eventToUpdate));
        Mockito.when(eventRepository.save(eventToUpdate)).thenReturn(eventToUpdate);

        Event updatedEvent = eventService.updateEvent(1, newEvent);
        assertTrue(
                newEvent.getName().equals(updatedEvent.getName())
                        && newEvent.getDescription().equals(updatedEvent.getDescription())
                        && newEvent.getEventCategory().equals(updatedEvent.getEventCategory())
                        && newEvent.getPicture().equals(updatedEvent.getPicture()));


    }

    @Test(expected = NotFoundException.class)
    public void testGetEventThatNotExist(){
        Mockito.when(eventRepository.findById(any(Integer.class))).thenReturn(Optional.empty());
        eventService.getEventById(1);
    }

    @Test(expected = NotFoundException.class)
    public void testDeleteEventThatNotExist(){
        Mockito.when(eventRepository.findById(any(Integer.class))).thenReturn(Optional.empty());
        eventService.deleteEvent(1);
    }

    @Test(expected = AreaNotFoundException.class)
    public void testCreateEventStandingAreaNotFound(){
        Layout layout = new Layout();
        layout.setId(1);

        Event event = new Event();
        event.setDaysReservationIsValid(2);
        event.setDescription("Description");
        event.setEventCategory(EventCategory.CONCERT);
        event.setMaxReservationNumber(4);
        event.setName("Event1234");
        event.setPicture("img");
        event.setReservationsAvailable(new Date(0));
        event.setLayout(layout);

        ArrayList<PriceList> priceLists1 = new ArrayList<>();
        ArrayList<PriceList> priceLists = new ArrayList<>();
        PriceList priceList = new PriceList();
        priceList.setAreaId(5);
        priceList.setAreaType(AreaType.STANDING_AREA);
        priceList.setDate(new Date(1629282800000L));
        priceLists.add(priceList);
        priceList.setPrice(2000.00);
        priceList.setEvent(event);
        event.setPriceLists(priceLists);

        priceLists1.add(priceList);
        priceLists1.add(priceList);
        priceLists1.add(priceList);

        Mockito.when(eventRepository.findByName("EventName")).thenReturn(new Event());
        Mockito.when(eventRepository.save(Mockito.any(Event.class))).thenReturn(event);
        Mockito.when(priceListRepository.findAllByDateAndEventLayoutId(priceList.getDate(), priceList.getEvent().getLayout().getId())).thenReturn(priceLists);
        Mockito.when(standingAreaRepository.findById(any(Integer.class))).thenReturn(Optional.empty());

        eventService.createEvent(event);
    }

    @Test(expected = AreaNotFoundException.class)
    public void testCreateEventSittingAreaNotFound(){
        Layout layout = new Layout();
        layout.setId(1);

        Event event = new Event();
        event.setDaysReservationIsValid(2);
        event.setDescription("Description");
        event.setEventCategory(EventCategory.CONCERT);
        event.setMaxReservationNumber(4);
        event.setName("Event1234");
        event.setPicture("img");
        event.setReservationsAvailable(new Date(0));
        event.setLayout(layout);

        ArrayList<PriceList> priceLists1 = new ArrayList<>();
        ArrayList<PriceList> priceLists = new ArrayList<>();
        PriceList priceList = new PriceList();
        priceList.setAreaId(5);
        priceList.setAreaType(AreaType.SITTING_AREA);
        priceList.setDate(new Date(1629282800000L));
        priceLists.add(priceList);
        priceList.setPrice(2000.00);
        priceList.setEvent(event);
        event.setPriceLists(priceLists);

        priceLists1.add(priceList);
        priceLists1.add(priceList);
        priceLists1.add(priceList);

        Mockito.when(eventRepository.findByName("EventName")).thenReturn(new Event());
        Mockito.when(eventRepository.save(Mockito.any(Event.class))).thenReturn(event);
        Mockito.when(priceListRepository.findAllByDateAndEventLayoutId(priceList.getDate(), priceList.getEvent().getLayout().getId())).thenReturn(priceLists);
        Mockito.when(sittingAreaRepository.findById(any(Integer.class))).thenReturn(Optional.empty());

        eventService.createEvent(event);
    }

    @Test(expected = InvalidAreaForLayout.class)
    public void testCreateEventInvalidStandingAreaForLayout(){
        Layout layout = new Layout();
        layout.setId(1);

        Layout layout1 = new Layout();
        layout1.setId(2);

        Event event = new Event();
        event.setDaysReservationIsValid(2);
        event.setDescription("Description");
        event.setEventCategory(EventCategory.CONCERT);
        event.setMaxReservationNumber(4);
        event.setName("Event1234");
        event.setPicture("img");
        event.setReservationsAvailable(new Date(0));
        event.setLayout(layout);

        ArrayList<PriceList> priceLists1 = new ArrayList<>();
        ArrayList<PriceList> priceLists = new ArrayList<>();
        PriceList priceList = new PriceList();
        priceList.setAreaId(5);
        priceList.setAreaType(AreaType.STANDING_AREA);
        priceList.setDate(new Date(1629282800000L));
        priceLists.add(priceList);
        priceList.setPrice(2000.00);
        priceList.setEvent(event);
        event.setPriceLists(priceLists);

        priceLists1.add(priceList);
        priceLists1.add(priceList);
        priceLists1.add(priceList);

        StandingArea standingArea = new StandingArea();
        standingArea.setLayout(layout1);


        Mockito.when(eventRepository.findByName("EventName")).thenReturn(new Event());
        Mockito.when(eventRepository.save(Mockito.any(Event.class))).thenReturn(event);
        Mockito.when(priceListRepository.findAllByDateAndEventLayoutId(priceList.getDate(), priceList.getEvent().getLayout().getId())).thenReturn(priceLists);
        Mockito.when(standingAreaRepository.findById(any(Integer.class))).thenReturn(Optional.of(standingArea));

        eventService.createEvent(event);
    }

    @Test(expected = InvalidAreaForLayout.class)
    public void testCreateEventInvalidSittingAreaForLayout(){
        Layout layout = new Layout();
        layout.setId(1);

        Layout layout1 = new Layout();
        layout1.setId(2);

        Event event = new Event();
        event.setDaysReservationIsValid(2);
        event.setDescription("Description");
        event.setEventCategory(EventCategory.CONCERT);
        event.setMaxReservationNumber(4);
        event.setName("Event1234");
        event.setPicture("img");
        event.setReservationsAvailable(new Date(0));
        event.setLayout(layout);

        ArrayList<PriceList> priceLists1 = new ArrayList<>();
        ArrayList<PriceList> priceLists = new ArrayList<>();
        PriceList priceList = new PriceList();
        priceList.setAreaId(5);
        priceList.setAreaType(AreaType.SITTING_AREA);
        priceList.setDate(new Date(1629282800000L));
        priceLists.add(priceList);
        priceList.setPrice(2000.00);
        priceList.setEvent(event);
        event.setPriceLists(priceLists);

        priceLists1.add(priceList);
        priceLists1.add(priceList);
        priceLists1.add(priceList);

        SittingArea sittingArea = new SittingArea();
        sittingArea.setLayout(layout1);


        Mockito.when(eventRepository.findByName("EventName")).thenReturn(new Event());
        Mockito.when(eventRepository.save(Mockito.any(Event.class))).thenReturn(event);
        Mockito.when(priceListRepository.findAllByDateAndEventLayoutId(priceList.getDate(), priceList.getEvent().getLayout().getId())).thenReturn(priceLists);
        Mockito.when(sittingAreaRepository.findById(any(Integer.class))).thenReturn(Optional.of(sittingArea));

        eventService.createEvent(event);
    }

}
