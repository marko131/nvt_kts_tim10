package tim10.project.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import tim10.project.model.Layout;
import tim10.project.model.Position;
import tim10.project.model.StandingArea;
import tim10.project.repository.LayoutRepository;
import tim10.project.service.exceptions.NotFoundException;

import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ActiveProfiles("20")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class StandingAreaServiceIntegrationTest {

    @Autowired
    private StandingAreaService standingAreaService;

    @Autowired
    private LayoutRepository layoutRepository;

    private StandingArea testStandingArea;

    private StandingArea updatedStandingArea;

    @Before
    public void setUp() {

        Layout layout = layoutRepository.getOne(1);


        testStandingArea = new StandingArea();
        testStandingArea.setMaxNumber(10);
        testStandingArea.setLayout(layout);
        testStandingArea.setLayoutPosition(Position.E);

        updatedStandingArea = new StandingArea();
        updatedStandingArea.setMaxNumber(testStandingArea.getMaxNumber());
        updatedStandingArea.setLayout(testStandingArea.getLayout());
        updatedStandingArea.setLayoutPosition(testStandingArea.getLayoutPosition());
    }

    @Test
    @Transactional
    public void createStandingAreaSuccessful() {
        StandingArea resultStandingArea = standingAreaService.createStandingArea(testStandingArea);

        assertEquals(testStandingArea.getMaxNumber(), resultStandingArea.getMaxNumber());
        assertEquals(testStandingArea.getLayout(), resultStandingArea.getLayout());
    }

    @Test(expected = NotFoundException.class)
    public void createStandingAreaLayoutNotFound() {
        Layout layout = new Layout();
        layout.setId(500);
        testStandingArea.setLayout(layout);

        standingAreaService.createStandingArea(testStandingArea);
    }

    @Test
    public void getStandingAreaByIdSuccessful() {
        Integer id = 1;
        standingAreaService.getStandingAreaById(1);
    }

    @Test(expected = NotFoundException.class)
    public void getStandingAreaByIdNotFound() {
        Integer id = 500;
        standingAreaService.getStandingAreaById(id);
    }

    @Test
    public void getStandingAreasByLayoutIdSuccessful() {
        Integer id = 1;
        standingAreaService.getStandingAreasByLayoutId(id);
    }

    @Test(expected = NotFoundException.class)
    public void getStandingAreasByLayoutIdNotFound() {
        Integer id = 500;
        standingAreaService.getStandingAreasByLayoutId(id);
    }

    @Test
    @Transactional
    public void updateStandingAreaSuccessful() {
        Integer id = 1;
        updatedStandingArea.setMaxNumber(13);

        StandingArea result = standingAreaService.updateStandingArea(id, updatedStandingArea);
        assertEquals(updatedStandingArea.getMaxNumber(), result.getMaxNumber());
    }

    @Test(expected = NotFoundException.class)
    public void updateStandingAreaNotFound() {
        Integer id = 500;
        updatedStandingArea.setMaxNumber(13);

        standingAreaService.updateStandingArea(id, updatedStandingArea);
    }

    @Test(expected = NotFoundException.class)
    public void testDeleteStandingAreaNotFound() {
        Integer id = 500;
        standingAreaService.deleteStandingArea(id);
    }

    @Test
    @Transactional
    public void testDeleteStandingAreaSuccessful() {
        int cnt = standingAreaService.getAllStandingAreas().size();
        Integer id = 1;
        standingAreaService.deleteStandingArea(id);
        int cntAfterDelete = standingAreaService.getAllStandingAreas().size();
        assertEquals(cnt - 1, cntAfterDelete);
    }
}
