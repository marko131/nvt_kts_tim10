package tim10.project;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import tim10.project.repository.*;
import tim10.project.web.controller.PriceListControllerIntegrationTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({LocationRepositoryIntegrationTest.class, PriceListRepositoryIntegrationTest.class, TicketRepositoryIntegrationTest.class, StandingAreaRepositoryIntegrationTest.class,
                        SittingAreaRepositoryIntegrationTest.class, UserRepositoryIntegrationTest.class})
public class RepositoryIntegrationTestSuite {
}
