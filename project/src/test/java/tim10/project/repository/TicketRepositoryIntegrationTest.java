package tim10.project.repository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import tim10.project.model.Ticket;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("20")
@RunWith(SpringRunner.class)
@DataJpaTest
public class TicketRepositoryIntegrationTest {

    @Autowired
    private TicketRepository ticketRepository;

    @Test
    public void testFindAllByPriceListIdPriceListNotFound() {
        List<Ticket> tickets = ticketRepository.findAllByPriceListId(200);
        assertTrue(tickets.isEmpty());
    }

    @Test
    public void testFindAllByPriceListIdSuccessful() {
        List<Ticket> tickets = ticketRepository.findAllByPriceListId(1);
        assertFalse(tickets.isEmpty());
    }

    @Test
    public void testFindAllByPriceListEventIdEventNotFound() {
        List<Ticket> tickets = ticketRepository.findAllByPriceListEventId(200);
        assertTrue(tickets.isEmpty());
    }

    @Test
    public void testFindAllByPriceListEventIdSuccessful() {
        List<Ticket> tickets = ticketRepository.findAllByPriceListEventId(1);
        assertFalse(tickets.isEmpty());
    }
}
