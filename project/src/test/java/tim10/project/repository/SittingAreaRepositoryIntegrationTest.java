package tim10.project.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import tim10.project.model.SittingArea;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("21")
@RunWith(SpringRunner.class)
@DataJpaTest
public class SittingAreaRepositoryIntegrationTest {

    @Autowired
    private SittingAreaRepository sittingAreaRepository;

    @Test
    public void testGetAllByLayoutIdLayoutNotFound() {
        List<SittingArea> sittingAreas = sittingAreaRepository.getAllByLayoutId(200);
        assertEquals(0, sittingAreas.size());
    }

    @Test
    public void testGetAllByLayoutIdSuccessful() {
        List<SittingArea> standingAreas = sittingAreaRepository.getAllByLayoutId(1);
        assertFalse(standingAreas.isEmpty());
    }
}
