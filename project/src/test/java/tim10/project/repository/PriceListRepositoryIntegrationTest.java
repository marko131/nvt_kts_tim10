package tim10.project.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import tim10.project.model.AreaType;
import tim10.project.model.PriceList;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("2")
@RunWith(SpringRunner.class)
@DataJpaTest
public class PriceListRepositoryIntegrationTest {

    @Autowired
    private PriceListRepository priceListRepository;

    private SimpleDateFormat sdf;

    @Before
    public void SetUp() {
        sdf = new SimpleDateFormat("yyyy-MM-dd");
    }

    @Test
    public void testFindAllByEventId_whenEventNotFound_thenReturnEmptyList() {
        Integer id = 444;
        List<PriceList> foundPriceLists = priceListRepository.findAllByEventId(id);

        assertTrue(foundPriceLists.isEmpty());
    }

    @Test
    public void testFindAllByEventId_whenValidEventId_thenReturnPriceLists() throws ParseException {
        Integer id = 1;
        List<PriceList> foundPriceLists = priceListRepository.findAllByEventId(id);

        assertFalse(foundPriceLists.isEmpty());
        assertEquals(3, foundPriceLists.size());

        assertEquals(1, foundPriceLists.get(0).getId());
        assertEquals(2, foundPriceLists.get(0).getAreaId());
        assertEquals(AreaType.SITTING_AREA, foundPriceLists.get(0).getAreaType());
        assertEquals(sdf.parse("2020-01-19"), foundPriceLists.get(0).getDate());
        assertEquals(2100d, foundPriceLists.get(0).getPrice());
        assertEquals(1, foundPriceLists.get(0).getEvent().getId());

        assertEquals(2, foundPriceLists.get(1).getId());
        assertEquals(1, foundPriceLists.get(1).getAreaId());
        assertEquals(AreaType.STANDING_AREA, foundPriceLists.get(1).getAreaType());
        assertEquals(sdf.parse("2020-01-19"), foundPriceLists.get(1).getDate());
        assertEquals(1600d, foundPriceLists.get(1).getPrice());
        assertEquals(1, foundPriceLists.get(1).getEvent().getId());

        assertEquals(3, foundPriceLists.get(2).getId());
        assertEquals(1, foundPriceLists.get(2).getAreaId());
        assertEquals(AreaType.STANDING_AREA, foundPriceLists.get(2).getAreaType());
        assertEquals(sdf.parse("2020-01-20"), foundPriceLists.get(2).getDate());
        assertEquals(1600d, foundPriceLists.get(2).getPrice());
        assertEquals(1, foundPriceLists.get(2).getEvent().getId());
    }

    @Test
    public void testFindAllByDateAndEventLayoutId_whenLayoutNotFound_thenReturnEmptyList() throws ParseException {
        Date date = sdf.parse("2020-01-19");
        Integer layoutId = 444;

        List<PriceList> foundPriceLists = priceListRepository.findAllByDateAndEventLayoutId(date, layoutId);

        assertTrue(foundPriceLists.isEmpty());
    }

    @Test
    public void testFindAllByDateAndEventLayoutId_whenValidEventId_thenReturnPriceLists() throws ParseException {
        Date date = sdf.parse("2020-01-19");
        Integer layoutId = 2;

        List<PriceList> foundPriceLists = priceListRepository.findAllByDateAndEventLayoutId(date, layoutId);

        assertFalse(foundPriceLists.isEmpty());
        assertEquals(2, foundPriceLists.size());

        assertEquals(1, foundPriceLists.get(0).getId());
        assertEquals(2, foundPriceLists.get(0).getAreaId());
        assertEquals(AreaType.SITTING_AREA, foundPriceLists.get(0).getAreaType());
        assertEquals(sdf.parse("2020-01-19"), foundPriceLists.get(0).getDate());
        assertEquals(2100d, foundPriceLists.get(0).getPrice());
        assertEquals(1, foundPriceLists.get(0).getEvent().getId());

        assertEquals(2, foundPriceLists.get(1).getId());
        assertEquals(1, foundPriceLists.get(1).getAreaId());
        assertEquals(AreaType.STANDING_AREA, foundPriceLists.get(1).getAreaType());
        assertEquals(sdf.parse("2020-01-19"), foundPriceLists.get(1).getDate());
        assertEquals(1600d, foundPriceLists.get(1).getPrice());
        assertEquals(1, foundPriceLists.get(1).getEvent().getId());
    }
}