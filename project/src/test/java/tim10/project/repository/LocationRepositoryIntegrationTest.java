package tim10.project.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import tim10.project.model.Location;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("2")
@RunWith(SpringRunner.class)
@DataJpaTest
public class LocationRepositoryIntegrationTest {

    @Autowired
    private LocationRepository locationRepository;

    @Test
    public void testFindByNameContaining_whenInvalidName_thenReturnEmptyList() {
        List<Location> foundLocations = locationRepository.findByNameContaining("test");
        assertTrue(foundLocations.isEmpty());
    }

    @Test
    public void testFindByNameContaining_whenWholeName_thenReturnLocationsList() {
        List<Location> foundLocations = locationRepository.findByNameContaining("SPENS");

        assertFalse(foundLocations.isEmpty());
        assertEquals(1, foundLocations.size());

        assertEquals("SPENS", foundLocations.get(0).getName());
        assertEquals("Sutjeska 2, Novi Sad 21000", foundLocations.get(0).getAddress());
        assertEquals(BigDecimal.valueOf(19.845355), foundLocations.get(0).getLongitude());
        assertEquals(BigDecimal.valueOf(45.246937), foundLocations.get(0).getLatitude());
    }

    @Test
    public void testFindByNameContaining_whenPartOfName_thenReturnLocationsList() {
        List<Location> foundLocations = locationRepository.findByNameContaining("SP");

        assertFalse(foundLocations.isEmpty());
        assertEquals(1, foundLocations.size());

        assertEquals("SPENS", foundLocations.get(0).getName());
        assertEquals("Sutjeska 2, Novi Sad 21000", foundLocations.get(0).getAddress());
        assertEquals(BigDecimal.valueOf(19.845355), foundLocations.get(0).getLongitude());
        assertEquals(BigDecimal.valueOf(45.246937), foundLocations.get(0).getLatitude());
    }
}