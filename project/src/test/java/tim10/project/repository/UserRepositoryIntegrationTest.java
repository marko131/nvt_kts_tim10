package tim10.project.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import tim10.project.model.User;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("21")
@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryIntegrationTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testFindByEmailNotFound() {
        User user = userRepository.findByEmail("asdasdijoasdjoias@gmail.com");
        assertNull(user);
    }

    @Test
    public void testFindByEmailSuccessful() {
        User user = userRepository.findByEmail("admin@gmail.com");
        System.out.println(user.getEmail() +  user.getLastName() + user.getName() + user.getPassword());
        assertEquals("nikola", user.getName());
        assertEquals("nikolic", user.getLastName());
        assertTrue(user.isAdmin());
    }
}
