package tim10.project.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import tim10.project.model.StandingArea;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("20")
@RunWith(SpringRunner.class)
@DataJpaTest
public class StandingAreaRepositoryIntegrationTest {

    @Autowired
    private StandingAreaRepository standingAreaRepository;

    @Test
    public void testGetAllByLayoutIdLayoutNotFound() {
        List<StandingArea> standingAreas = standingAreaRepository.getAllByLayoutId(200);
        assertEquals(0, standingAreas.size());
    }

    @Test
    public void testGetAllByLayoutIdSuccessful() {
        List<StandingArea> standingAreas = standingAreaRepository.getAllByLayoutId(1);
        assertFalse(standingAreas.isEmpty());
    }
}
