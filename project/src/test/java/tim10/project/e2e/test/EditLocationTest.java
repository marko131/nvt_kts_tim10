package tim10.project.e2e.test;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import tim10.project.e2e.pages.*;

import static org.junit.Assert.assertEquals;

@ActiveProfiles("2")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.DEFINED_PORT)
public class EditLocationTest {

    private WebDriver browser;

    HomePage homePage;
    LoginPage loginPage;
    LocationsPage locationsPage;
    EditLocationPage editLocationPage;

    @Before
    public void setUpSelenium() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        browser = new ChromeDriver();
        browser.manage().window().maximize();
        browser.navigate().to("http://localhost:4200");

        homePage = PageFactory.initElements(browser, HomePage.class);
        loginPage = PageFactory.initElements(browser, LoginPage.class);
        locationsPage = PageFactory.initElements(browser, LocationsPage.class);
        editLocationPage = PageFactory.initElements(browser, EditLocationPage.class);
    }

    @Test
    public void testEditLocation() {
        // Home page
        assertEquals("http://localhost:4200/event", browser.getCurrentUrl());
        homePage.ensureWebElementIsDisplayed(homePage.getNavLink2());
        homePage.getNavLink2().click();

        // Login
        assertEquals("http://localhost:4200/login", browser.getCurrentUrl());
        loginPage.ensureWebElementIsDisplayed(loginPage.getLoginButton());
        loginPage.setEmailInput("admin@gmail.com");
        loginPage.setPasswordInput("pass123456");
        loginPage.getLoginButton().click();

        new WebDriverWait(browser, 5).until(ExpectedConditions.urlToBe("http://localhost:4200/event"));
        assertEquals("http://localhost:4200/event", browser.getCurrentUrl());

        // To AddLocation page
        homePage.ensureWebElementIsDisplayed(homePage.getNavLink3());
        homePage.getNavLink3().click();
        new WebDriverWait(browser, 5).until(ExpectedConditions.urlToBe("http://localhost:4200/locations"));
        assertEquals("http://localhost:4200/locations", browser.getCurrentUrl());

        // To EditLocation page
        locationsPage.ensureWebElementIsDispalyed(locationsPage.getEditLocationButton());
        locationsPage.getEditLocationButton().click();
        new WebDriverWait(browser, 5).until(ExpectedConditions.urlToBe("http://localhost:4200/edit-location/1"));
        assertEquals("http://localhost:4200/edit-location/1", browser.getCurrentUrl());

        // Edit location nam
        editLocationPage.setNameInput("SPENS - Edited");
        editLocationPage.ensureWebElementIsDisplayed(editLocationPage.getEditLocationButton());
        editLocationPage.getEditLocationButton().click();

        new WebDriverWait(browser, 5).until(ExpectedConditions.urlToBe("http://localhost:4200/locations"));
        assertEquals("http://localhost:4200/locations", browser.getCurrentUrl());
    }
}
