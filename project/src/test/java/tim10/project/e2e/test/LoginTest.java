package tim10.project.e2e.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import tim10.project.e2e.pages.LoginPage;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

@ActiveProfiles("1")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.DEFINED_PORT)
public class LoginTest {
    private WebDriver browser;
    private LoginPage loginPage;

    @Before
    public void setupSelenium() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        browser = new ChromeDriver();
        browser.manage().window().maximize();
        browser.navigate().to("http://localhost:4200/login");
        loginPage = PageFactory.initElements(browser, LoginPage.class);
    }

    @Test
    public void testLogin(){
        WebDriverWait wait = new WebDriverWait(browser,2);
        //Test empty inputs
        loginPage.getLoginButton().click();
        wait.until(ExpectedConditions.visibilityOf(loginPage.getEmailError()));
        assertEquals("Email is required", loginPage.getEmailError().getText());
        assertEquals("Password is required", loginPage.getPasswordError().getText());

        //Test invalid email
        loginPage.setEmailInput("123");
        assertEquals("Email is not valid", loginPage.getEmailError().getText());

        //Test successful login
        login("admin@gmail.com", "password");
        assertEquals("http://localhost:4200/event", browser.getCurrentUrl());
    }


    private void login(String email, String password) {
        loginPage.setEmailInput(email);
        loginPage.setPasswordInput(password);
        loginPage.getLoginButton().click();
        new WebDriverWait(browser, 5).until(ExpectedConditions.urlToBe("http://localhost:4200/event"));
    }

    @After
    public void closeSelenium() {
        browser.quit();
    }
}
