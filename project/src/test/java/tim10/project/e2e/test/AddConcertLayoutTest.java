package tim10.project.e2e.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import tim10.project.e2e.pages.AddLayoutPage;
import tim10.project.e2e.pages.HomePage;
import tim10.project.e2e.pages.LocationsPage;
import tim10.project.e2e.pages.LoginPage;

import static org.junit.Assert.assertEquals;

@ActiveProfiles("2")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.DEFINED_PORT)
public class AddConcertLayoutTest {
    private WebDriver browser;

    HomePage homePage;
    LoginPage loginPage;
    LocationsPage locationsPage;
    AddLayoutPage addLayoutPage;

    @Before
    public void setUpSelenium() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        browser = new ChromeDriver();
        browser.manage().window().maximize();
        browser.navigate().to("http://localhost:4200");

        homePage = PageFactory.initElements(browser, HomePage.class);
        loginPage = PageFactory.initElements(browser, LoginPage.class);
        locationsPage = PageFactory.initElements(browser, LocationsPage.class);
        addLayoutPage = PageFactory.initElements(browser, AddLayoutPage.class);
    }

    @Test
    public void testAddLayoutConcert() {
        // Home page
        assertEquals("http://localhost:4200/event", browser.getCurrentUrl());
        homePage.ensureWebElementIsDisplayed(homePage.getNavLink2());
        homePage.getNavLink2().click();

        // Login
        assertEquals("http://localhost:4200/login", browser.getCurrentUrl());
        loginPage.setEmailInput("admin@gmail.com");
        loginPage.setPasswordInput("pass123456");
        loginPage.ensureWebElementIsDisplayed(loginPage.getLoginButton());
        loginPage.getLoginButton().click();

        new WebDriverWait(browser, 5).until(ExpectedConditions.urlToBe("http://localhost:4200/event"));
        assertEquals("http://localhost:4200/event", browser.getCurrentUrl());

        // To Locations page
        homePage.ensureWebElementIsDisplayed(homePage.getNavLink3());
        homePage.getNavLink3().click();
        new WebDriverWait(browser, 5).until(ExpectedConditions.urlToBe("http://localhost:4200/locations"));
        assertEquals("http://localhost:4200/locations", browser.getCurrentUrl());

        // To AddLayout page
        locationsPage.ensureWebElementIsDispalyed(locationsPage.getAddLayoutButton());
        locationsPage.getAddLayoutButton().click();
        new WebDriverWait(browser, 5).until(ExpectedConditions.urlToBe("http://localhost:4200/location/1/add-layout"));
        assertEquals("http://localhost:4200/location/1/add-layout", browser.getCurrentUrl());

        // First step
        addLayoutPage.setLayoutNameInput("Concert test layout");
        addLayoutPage.setStagePositionSelect("North");

        addLayoutPage.ensureWebElementIsDisplayed(addLayoutPage.getFirstNextButton());
        addLayoutPage.getFirstNextButton().click();

        //Second step
        addLayoutPage.ensureWebElementIsDisplayed(addLayoutPage.getRowNumberInput());
        addLayoutPage.ensureWebElementIsDisplayed(addLayoutPage.getSeatsNumberInput());
        addLayoutPage.ensureWebElementIsDisplayed(addLayoutPage.getSeatingLayoutPositionSelect());

        addLayoutPage.setRowNumberInput("5.65");
        addLayoutPage.setSeatsNumberInput("5.2");
        addLayoutPage.setSeatingLayoutPositionSelect("West");

        assertEquals("Row number must be whole number.", addLayoutPage.getRowNumberInputError().getText());
        assertEquals("Seats number must be whole number.", addLayoutPage.getSeatsNumberInputError().getText());

        addLayoutPage.setRowNumberInput("50");
        addLayoutPage.setSeatsNumberInput("57");

        assertEquals("Row number must be between 1 and 7.", addLayoutPage.getRowNumberInputError().getText());
        assertEquals("Seats number must be between 1 and 7.", addLayoutPage.getSeatsNumberInputError().getText());

        addLayoutPage.setRowNumberInput("7");
        addLayoutPage.setSeatsNumberInput("7");
        addLayoutPage.setSeatingLayoutPositionSelect("West");
        addLayoutPage.ensureWebElementIsDisplayed(addLayoutPage.getAddSeatingLayoutButton());
        addLayoutPage.getAddSeatingLayoutButton().click();

        addLayoutPage.setRowNumberInput("7");
        addLayoutPage.setSeatsNumberInput("7");
        addLayoutPage.setSeatingLayoutPositionSelect("East");
        addLayoutPage.ensureWebElementIsDisplayed(addLayoutPage.getAddSeatingLayoutButton());
        addLayoutPage.getAddSeatingLayoutButton().click();

        addLayoutPage.ensureWebElementIsDisplayed(addLayoutPage.getSecondNextButton());
        addLayoutPage.getSecondNextButton().click();

        // Third step
        addLayoutPage.ensureWebElementIsDisplayed(addLayoutPage.getMaxNumberInput());
        addLayoutPage.ensureWebElementIsDisplayed(addLayoutPage.getStandingLayoutPositionSelect());

        addLayoutPage.setMaxNumberInput("5.65");
        addLayoutPage.setStandingLayoutPositionSelect("Middle");
        assertEquals("Max number must be whole number.", addLayoutPage.getMaxNumberInputError().getText());

        addLayoutPage.setMaxNumberInput("30");
        addLayoutPage.ensureWebElementIsDisplayed(addLayoutPage.getAddStandingLayoutButton());
        addLayoutPage.getAddStandingLayoutButton().click();

        // To Finish
        addLayoutPage.ensureWebElementIsDisplayed(addLayoutPage.getFinishButton());
        addLayoutPage.getFinishButton().click();

        // Submit
        addLayoutPage.ensureWebElementIsDisplayed(addLayoutPage.getSubmitButton());
        //addLayoutPage.getSubmitButton().click();

        // To Layout display page
        //new WebDriverWait(browser, 25).until(ExpectedConditions.urlToBe("http://localhost:4200/location/1/layout"));
        //assertEquals("http://localhost:4200/location/1/layout", browser.getCurrentUrl());
    }

    @After
    public void closeSelenium() {
        browser.quit();
    }
}
