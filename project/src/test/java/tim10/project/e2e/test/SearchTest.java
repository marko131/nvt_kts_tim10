package tim10.project.e2e.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import tim10.project.e2e.pages.AddLocationPage;
import tim10.project.e2e.pages.HomePage;
import tim10.project.e2e.pages.LoginPage;
import tim10.project.model.EventCategory;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@ActiveProfiles("20")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.DEFINED_PORT)
public class SearchTest {

    private WebDriver browser;

    HomePage homePage;

    @Before
    public void setUpSelenium() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        browser = new ChromeDriver();
        browser.manage().window().maximize();
        browser.navigate().to("http://localhost:4200");

        homePage = PageFactory.initElements(browser, HomePage.class);
    }

    @Test
    public void testSearch() {
        // Home page
        assertEquals("http://localhost:4200/event", browser.getCurrentUrl());

        //Basic search
        homePage.ensureWebElementIsDisplayed(homePage.getSearchInput());
        homePage.getSearchInput().click();
        String searchInput = "e";
        homePage.setSearchInput(searchInput);

        homePage.ensureWebElementIsDisplayed(homePage.getSearchButton());
        homePage.getSearchButton().click();
        assertTrue(homePage.getAllEventNames().stream().allMatch(name  -> name.toLowerCase().contains(searchInput.toLowerCase())));

        // Advanced search
        homePage.ensureWebElementIsDisplayed(homePage.getAdvancedSearchPanel());
        homePage.getAdvancedSearchPanel().click();

        homePage.ensureWebElementIsDisplayed(homePage.getLocationInput());
        homePage.getLocationInput().click();
        homePage.setLocationInput("123");

        homePage.ensureWebElementIsDisplayed(homePage.getCategoryChoicePlaceholder());
        homePage.getCategoryChoicePlaceholder().click();
        homePage.ensureWebElementIsDisplayed(homePage.getCategoryTheater());
        homePage.getCategoryTheater().click();
        homePage.ensureWebElementIsDisplayed(homePage.getCategorySport());
        homePage.getCategorySport().click();
        homePage.clickBackground();

        // Invalid dates
        homePage.ensureWebElementIsDisplayed(homePage.getDateBegin());
        homePage.getDateBegin().click();
        homePage.setDateBegin("asd");
        assertEquals("Invalid date", homePage.getDateBeginError().getText());

        homePage.ensureWebElementIsDisplayed(homePage.getDateEnd());
        homePage.getDateEnd().click();
        homePage.setDateEnd("asd");
        assertEquals("Invalid date", homePage.getDateEndError().getText());

        //Valid dates
        homePage.getDateBegin().click();
        homePage.setDateBegin("10/10/2020");
        homePage.getDateEnd().click();
        homePage.setDateEnd("10/20/2020");

        homePage.getSearchButton().click();
        assertTrue(homePage.getAllEventNames().stream().allMatch(name  -> name.toLowerCase().contains(searchInput.toLowerCase())));
        assertTrue(homePage.getAllCategories().stream().allMatch(homePage.getCategoryChoice().getText()::contains));

    }

    @After
    public void closeSelenium() {
        browser.quit();
    }
}
