package tim10.project.e2e.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import tim10.project.e2e.pages.LoginPage;
import tim10.project.e2e.pages.ProfilePage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@ActiveProfiles("1")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class ProfileTest {
    private WebDriver browser;
    private LoginPage loginPage;
    private ProfilePage profilePage;

    @Before
    public void setupSelenium() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        browser = new ChromeDriver();
        browser.manage().window().maximize();
        browser.navigate().to("http://localhost:4200/login");

        loginPage = PageFactory.initElements(browser, LoginPage.class);
        profilePage = PageFactory.initElements(browser, ProfilePage.class);

        login("admin@gmail.com", "password");
        Thread.sleep(2000);
        loginPage.getAdminProfileButton().click();
        Thread.sleep(2000);
    }
    @Test
    public void testSwitchTabs() throws InterruptedException {
        //Test profile Fetch Data
        assertEquals("admin@gmail.com", profilePage.getEmailInput().getAttribute("value"));
        assertEquals("admin", profilePage.getFirstNameInput().getAttribute("value"));
        assertEquals("admin", profilePage.getFirstNameInput().getAttribute("value"));
        profilePage.getChangePasswordTabButton().click();
        Thread.sleep(1000);

        //TestShortPassword
        profilePage.setPassword1Input("1");
        profilePage.setPassword1Input("2");
        Thread.sleep(500);
        assertEquals("Password must be at least 6 characters long", profilePage.getPassword1Error().getText());
        assertEquals("Passwords do not match", profilePage.getPassword2Error().getText());

        //Test update profile empty inputs
        profilePage.getInfoTabButton().click();
        Thread.sleep(5000);
        int emailLength = profilePage.getEmailInput().getAttribute("value").length();
        int firstNameLength = profilePage.getFirstNameInput().getAttribute("value").length();
        int lastNameLength = profilePage.getLastNameInput().getAttribute("value").length();
        for (int i = 0; i < emailLength; i++)
            profilePage.getEmailInput().sendKeys(Keys.BACK_SPACE);
        for (int i = 0; i < firstNameLength; i++)
            profilePage.getFirstNameInput().sendKeys(Keys.BACK_SPACE);
        for (int i = 0; i < lastNameLength; i++)
            profilePage.getLastNameInput().sendKeys(Keys.BACK_SPACE);;

        assertEquals("Email is required", profilePage.getEmailError().getText());
        assertEquals("First name is required", profilePage.getFirstNameError().getText());


        //Test email is not valid
        profilePage.setEmailInput("email");
        assertEquals("Last name is required", profilePage.getLastNameError().getText());
        profilePage.setFirstNameInput("1");

        //Test successful update
        Thread.sleep(1000);
        profilePage.setEmailInput("admin2@gmail.com");
        profilePage.setFirstNameInput("firstName");
        profilePage.setLastNameInput("lastName");
        profilePage.getSaveButton().click();
        Thread.sleep(2000);
        login("admin2@gmail.com", "password");
        Thread.sleep(2000);
        assertEquals("Admin: admin2@gmail.com", loginPage.getAdminProfileButton().getText());

        loginPage.getAdminProfileButton().click();

        Thread.sleep(500);
        profilePage.getLogoutButton().click();
        Thread.sleep(1000);

        login("user@gmail.com", "password");

        loginPage.getUserProfileButton().click();
        Thread.sleep(1000);
        profilePage.getMyReservationsTabButton().click();
        Thread.sleep(3000);

        browser.findElement(By.xpath("/html/body/app-root/app-profile/mat-card/mat-tab-group/div/mat-tab-body[3]/div/reservations/table/tbody/tr[1]/td[9]/button")).click();
        Thread.sleep(1000);

        profilePage.getMyTicketsTabButton().click();
        Thread.sleep(3000);
        browser.findElement(By.xpath("/html/body/app-root/app-profile/mat-card/mat-tab-group/div/mat-tab-body[4]/div/tickets/table/tbody/tr/td[8]/button")).click();
        
    }



    private void login(String email, String password) throws InterruptedException {
        loginPage.setEmailInput(email);
        loginPage.setPasswordInput(password);
        loginPage.getLoginButton().click();
        Thread.sleep(5000);
    }

}
