package tim10.project.e2e.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import tim10.project.e2e.pages.AddLocationPage;
import tim10.project.e2e.pages.HomePage;
import tim10.project.e2e.pages.LoginPage;

import static org.junit.Assert.assertEquals;

@ActiveProfiles("2")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.DEFINED_PORT)
public class AddLocationTest {

    private WebDriver browser;

    HomePage homePage;
    LoginPage loginPage;
    AddLocationPage addLocationPage;

    @Before
    public void setUpSelenium() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        browser = new ChromeDriver();
        browser.manage().window().maximize();
        browser.navigate().to("http://localhost:4200");

        homePage = PageFactory.initElements(browser, HomePage.class);
        loginPage = PageFactory.initElements(browser, LoginPage.class);
        addLocationPage = PageFactory.initElements(browser, AddLocationPage.class);
    }

    @Test
    public void testAddLocation() {
        // Home page
        assertEquals("http://localhost:4200/event", browser.getCurrentUrl());
        homePage.ensureWebElementIsDisplayed(homePage.getNavLink2());
        homePage.getNavLink2().click();

        // Login
        assertEquals("http://localhost:4200/login", browser.getCurrentUrl());
        loginPage.ensureWebElementIsDisplayed(loginPage.getLoginButton());
        loginPage.setEmailInput("admin@gmail.com");
        loginPage.setPasswordInput("pass123456");
        loginPage.getLoginButton().click();

        new WebDriverWait(browser, 5).until(ExpectedConditions.urlToBe("http://localhost:4200/event"));
        assertEquals("http://localhost:4200/event", browser.getCurrentUrl());

        // To AddLocation page
        homePage.ensureWebElementIsDisplayed(homePage.getNavLink4());
        homePage.getNavLink4().click();
        new WebDriverWait(browser, 5).until(ExpectedConditions.urlToBe("http://localhost:4200/add-location"));
        assertEquals("http://localhost:4200/add-location", browser.getCurrentUrl());

        // All empty inputs
        addLocationPage.setNameInput("");
        addLocationPage.setAddressInput("");
        addLocationPage.setLatitudeInput("");
        addLocationPage.setLongitudeInput("");
        addLocationPage.setPictureInput("");
        addLocationPage.setNameInput("");

        assertEquals("Name is required", addLocationPage.getNameInputError().getText());
        assertEquals("Address is required", addLocationPage.getAddressInputError().getText());
        assertEquals("Latitude is required", addLocationPage.getLatitudeInputError().getText());
        assertEquals("Longitude is required", addLocationPage.getLongitudeInputError().getText());
        assertEquals("Image is required", addLocationPage.getPictureInputError().getText());

        // Name input longer than 255 chars
        addLocationPage.setNameInput("a".repeat(300));
        assertEquals("Maximum number of characters allowed is 255", addLocationPage.getNameInputError().getText());

        // Address input longer than 255 chars
        addLocationPage.setAddressInput("a".repeat(300));
        assertEquals("Maximum number of characters allowed is 255", addLocationPage.getNameInputError().getText());

        // Latitude < -90.000000
        addLocationPage.setLatitudeInput("-15635");
        assertEquals("Minimum value is -90.000000", addLocationPage.getLatitudeInputError().getText());

        // Latitude > 90.000000
        addLocationPage.setLatitudeInput("15635");
        assertEquals("Maximum value is 90.000000", addLocationPage.getLatitudeInputError().getText());

        // Longitude < -90.000000
        addLocationPage.setLongitudeInput("-15635");
        assertEquals("Minimum value is -180.000000", addLocationPage.getLongitudeInputError().getText());

        // Longitude > 90.000000
        addLocationPage.setLongitudeInput("15635");
        assertEquals("Maximum value is 180.000000", addLocationPage.getLongitudeInputError().getText());

        addLocationPage.setLatitudeInput("46.1002355435");
        assertEquals("The value can have a maximum of 6 decimal places", addLocationPage.getLatitudeInputError().getText());

        addLocationPage.setLongitudeInput("19.6661005435");
        assertEquals("The value can have a maximum of 6 decimal places", addLocationPage.getLongitudeInputError().getText());

        // Valid input
        addLocationPage.setNameInput("Narodno pozoriste Subotica");
        addLocationPage.setAddressInput("Trg slobode, Subotica 2400");
        addLocationPage.setLatitudeInput("46.100235");
        addLocationPage.setLongitudeInput("19.666100");
        addLocationPage.setPictureInput("https://mojakartica.rs/wp-content/uploads/2019/12/pozorište.jpg");

        addLocationPage.ensureWebElementIsDisplayed(addLocationPage.getAddLocationButton());
        addLocationPage.getAddLocationButton().click();

        new WebDriverWait(browser, 5).until(ExpectedConditions.urlToBe("http://localhost:4200/locations"));
        assertEquals("http://localhost:4200/locations", browser.getCurrentUrl());
    }

    @After
    public void closeSelenium() {
        browser.quit();
    }
}
