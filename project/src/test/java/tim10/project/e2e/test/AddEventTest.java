package tim10.project.e2e.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import tim10.project.e2e.pages.AddEventPage;
import tim10.project.e2e.pages.AddLocationPage;
import tim10.project.e2e.pages.HomePage;
import tim10.project.e2e.pages.LoginPage;

import static org.junit.Assert.assertEquals;

@ActiveProfiles("20")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.DEFINED_PORT)
public class AddEventTest {

    private WebDriver browser;

    HomePage homePage;
    LoginPage loginPage;
    AddEventPage addEventPage;

    @Before
    public void setUpSelenium() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        browser = new ChromeDriver();
        browser.manage().window().maximize();
        browser.navigate().to("http://localhost:4200");

        homePage = PageFactory.initElements(browser, HomePage.class);
        loginPage = PageFactory.initElements(browser, LoginPage.class);
        addEventPage = PageFactory.initElements(browser, AddEventPage.class);
    }

    @Test
    public void testAddEvent() {
        // Home page
        assertEquals("http://localhost:4200/event", browser.getCurrentUrl());
        homePage.ensureWebElementIsDisplayed(homePage.getNavLink2());
        homePage.getNavLink2().click();

        // Login
        assertEquals("http://localhost:4200/login", browser.getCurrentUrl());
        loginPage.ensureWebElementIsDisplayed(loginPage.getLoginButton());
        loginPage.setEmailInput("admin@gmail.com");
        loginPage.setPasswordInput("asd123");
        loginPage.getLoginButton().click();

        new WebDriverWait(browser, 5).until(ExpectedConditions.urlToBe("http://localhost:4200/event"));
        assertEquals("http://localhost:4200/event", browser.getCurrentUrl());

        // To AddEvent page
        homePage.ensureWebElementIsDisplayed(homePage.getNavLink2());
        homePage.getNavLink2().click();
        new WebDriverWait(browser, 5).until(ExpectedConditions.urlToBe("http://localhost:4200/add-event"));
        assertEquals("http://localhost:4200/add-event", browser.getCurrentUrl());

        //Leaving fields empty
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getName());
        addEventPage.getName().click();
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getImage());
        addEventPage.getImage().click();
        assertEquals("Name required", addEventPage.getNameError().getText());

        addEventPage.ensureWebElementIsDisplayed(addEventPage.getCategory());
        addEventPage.getCategory().click();
        addEventPage.clickBackground();
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getImage());
        addEventPage.getImage().click();
        assertEquals("Category required", addEventPage.getCategoryError().getText());

        assertEquals("Image link required", addEventPage.getImageError().getText());

        addEventPage.ensureWebElementIsDisplayed(addEventPage.getDescription());
        addEventPage.getDescription().click();
        addEventPage.getName().click();
        assertEquals("Description required", addEventPage.getDescriptionError().getText());

        addEventPage.ensureWebElementIsDisplayed(addEventPage.getDaysReservationIsValid());
        addEventPage.getDaysReservationIsValid().click();
        addEventPage.getName().click();
        assertEquals("Field required", addEventPage.getDaysReservationIsValidError().getText());

        addEventPage.getDaysReservationIsValid().click();
        addEventPage.setDaysReservationIsValid("-5");
        assertEquals("Invalid number", addEventPage.getDaysReservationIsValidError().getText());

        addEventPage.ensureWebElementIsDisplayed(addEventPage.getLocation());
        addEventPage.getLocation().click();
        addEventPage.clickBackground();
        assertEquals("Location required", addEventPage.getLocationError().getText());

        addEventPage.ensureWebElementIsDisplayed(addEventPage.getLayout());
        addEventPage.getLayout().click();
        addEventPage.clickBackground();
        assertEquals("Layout required", addEventPage.getLayoutError().getText());

        addEventPage.ensureWebElementIsDisplayed(addEventPage.getAvailableUntil());
        addEventPage.getAvailableUntil().click();
        addEventPage.setAvailableUntil("asd");
        addEventPage.getName().click();
        assertEquals("Invalid date", addEventPage.getAvailableUntilError().getText());

        addEventPage.getMaxReservations().click();
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getName());
        addEventPage.getName().click();
        assertEquals("Field required", addEventPage.getMaxReservationsError().getText());

        addEventPage.getMaxReservations().click();
        addEventPage.setMaxReservations("-5");
        assertEquals("Invalid number", addEventPage.getMaxReservationsError().getText());

        //Valid info
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getName());
        addEventPage.setName("Event456");
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getCategory());
        addEventPage.getCategory().click();
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getCategorySport());
        addEventPage.getCategorySport().click();
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getImage());
        addEventPage.setImage("https://www.spens.rs/wp-content/uploads/2015/10/velika_dvorana_Spens11024x768.jpg");
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getDescription());
        addEventPage.setDescription("Some sporting event");
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getDaysReservationIsValid());
        addEventPage.setDaysReservationIsValid("2");
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getLocation());
        addEventPage.getLocation().click();
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getLocationChoice());
        addEventPage.getLocationChoice().click();
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getLayout());
        addEventPage.getLayout().click();
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getLayoutChoice());
        addEventPage.getLayoutChoice().click();
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getAvailableUntil());
        addEventPage.setAvailableUntil("11/11/2020");
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getMaxReservations());
        addEventPage.setMaxReservations("5");

        //Second page
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getEventInfoNext());
        addEventPage.getEventInfoNext().click();
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getAddPriceList());
        addEventPage.getAddPriceList().click();

        //Leaving fields empty
        assertEquals("Invalid date", addEventPage.getStandingAreaDateError().getText());
        assertEquals("Field required", addEventPage.getStandingAreaTypeError().getText());
        assertEquals("Field required", addEventPage.getStandingAreaPositionError().getText());
        assertEquals("Invalid date", addEventPage.getSittingAreaDateError().getText());
        assertEquals("Field required", addEventPage.getSittingAreaTypeError().getText());
        assertEquals("Field required", addEventPage.getSittingAreaPositionError().getText());

        //Valid info
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getStandingAreaDate());
        addEventPage.setStandingAreaDate("11/20/2020");
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getStandingAreaType());
        addEventPage.getStandingAreaType().click();
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getStandingAreaTypeChoice());
        addEventPage.getStandingAreaTypeChoice().click();
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getStandingAreaPosition());
        addEventPage.getStandingAreaPosition().click();
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getStandingAreaPositionChoice());
        addEventPage.getStandingAreaPositionChoice().click();
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getStandingAreaPrice());
        addEventPage.setStandingAreaPrice("3");

        addEventPage.ensureWebElementIsDisplayed(addEventPage.getSittingAreaDate());
        addEventPage.setSittingAreaDate("11/20/2020");
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getSittingAreaType());
        addEventPage.getSittingAreaType().click();
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getSittingAreaTypeChoice());
        addEventPage.getSittingAreaTypeChoice().click();
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getSittingAreaPosition());
        addEventPage.getSittingAreaPosition().click();
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getSittingAreaPositionChoice());
        addEventPage.getSittingAreaPositionChoice().click();
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getSittingAreaPrice());
        addEventPage.setSittingAreaPrice("13");

        addEventPage.ensureWebElementIsDisplayed(addEventPage.getAddPriceList());
        addEventPage.getAddPriceList().click();
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getDeleteThirdArea());
        addEventPage.getDeleteThirdArea().click();
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getScheduleNext());
        addEventPage.getScheduleNext().click();

        //Third page
        addEventPage.ensureWebElementIsDisplayed(addEventPage.getCreateEvent());
        addEventPage.getCreateEvent().click();

        addEventPage.ensureWebElementIsDisplayed(addEventPage.getSnackBar());
        assertEquals("Event successfully created", addEventPage.getSnackBar().getText());

    }

    @After
    public void closeSelenium() {
        browser.quit();
    }
}
