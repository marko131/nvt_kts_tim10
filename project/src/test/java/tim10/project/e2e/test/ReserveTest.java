package tim10.project.e2e.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import tim10.project.e2e.pages.AddEventPage;
import tim10.project.e2e.pages.HomePage;
import tim10.project.e2e.pages.LoginPage;
import tim10.project.e2e.pages.ReservePage;

import static org.junit.Assert.assertEquals;

@ActiveProfiles("21")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.DEFINED_PORT)
public class ReserveTest {

    private WebDriver browser;

    HomePage homePage;
    LoginPage loginPage;
    ReservePage reservePage;

    @Before
    public void setUpSelenium() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        browser = new ChromeDriver();
        browser.manage().window().maximize();
        browser.navigate().to("http://localhost:4200");

        homePage = PageFactory.initElements(browser, HomePage.class);
        loginPage = PageFactory.initElements(browser, LoginPage.class);
        reservePage = PageFactory.initElements(browser, ReservePage.class);
    }

    @Test
    public void testReserveStanding(){
        // Home page
        assertEquals("http://localhost:4200/event", browser.getCurrentUrl());
        homePage.ensureWebElementIsDisplayed(homePage.getNavLink2());
        homePage.getNavLink2().click();

        // Login
        assertEquals("http://localhost:4200/login", browser.getCurrentUrl());
        loginPage.ensureWebElementIsDisplayed(loginPage.getLoginButton());
        loginPage.setEmailInput("user@gmail.com");
        loginPage.setPasswordInput("asd123");
        loginPage.getLoginButton().click();

        new WebDriverWait(browser, 5).until(ExpectedConditions.urlToBe("http://localhost:4200/event"));
        assertEquals("http://localhost:4200/event", browser.getCurrentUrl());

        // Searching and selecting event
        homePage.ensureWebElementIsDisplayed(homePage.getSearchButton());
        homePage.getSearchButton().click();
        homePage.ensureWebElementIsDisplayed(homePage.getDetailsButton());
        homePage.getDetailsButton().click();

        new WebDriverWait(browser, 20).until(ExpectedConditions.urlToBe("http://localhost:4200/event/1"));
        assertEquals("http://localhost:4200/event/1", browser.getCurrentUrl());

        // Failed reserving standing area
        reservePage.ensureWebElementIsDisplayed(reservePage.getSelectStandingArea());
        reservePage.getSelectStandingArea().click();

        reservePage.ensureWebElementIsDisplayed(reservePage.getReserveButtonStanding());
        reservePage.getReserveButtonStanding().click();
        reservePage.ensureWebElementIsDisplayed(reservePage.getNumberOfSeatsError());
        assertEquals("Field required", reservePage.getNumberOfSeatsError().getText());
        reservePage.ensureWebElementIsDisplayed(reservePage.getNumberOfSeats());
        reservePage.setNumberOfSeats("-5");
        reservePage.getReserveButtonStanding().click();
        assertEquals("Invalid number", reservePage.getNumberOfSeatsError().getText());

        reservePage.ensureWebElementIsDisplayed(reservePage.getNumberOfSeats());
        reservePage.setNumberOfSeats("4");
        reservePage.ensureWebElementIsDisplayed(reservePage.getReserveButtonStanding());
        reservePage.getReserveButtonStanding().click();
        reservePage.ensureWebElementIsDisplayed(reservePage.getSnackBar());
        assertEquals("Max reservations for this event exceeded. Maximum reservations is:3", reservePage.getSnackBar().getText());

        // Valid reservation
        reservePage.ensureWebElementIsDisplayed(reservePage.getSelectStandingArea());
        reservePage.getSelectStandingArea().click();
        reservePage.ensureWebElementIsDisplayed(reservePage.getNumberOfSeats());
        reservePage.setNumberOfSeats("1");
        reservePage.ensureWebElementIsDisplayed(reservePage.getReserveButtonStanding());
        reservePage.getReserveButtonStanding().click();
        reservePage.ensureWebElementIsDisplayed(reservePage.getSnackBar());
        assertEquals("Successful reservation", reservePage.getSnackBar().getText());
    }

    @Test
    public void testReserveSitting() {
        // Home page
        assertEquals("http://localhost:4200/event", browser.getCurrentUrl());
        homePage.ensureWebElementIsDisplayed(homePage.getNavLink2());
        homePage.getNavLink2().click();

        // Login
        assertEquals("http://localhost:4200/login", browser.getCurrentUrl());
        loginPage.ensureWebElementIsDisplayed(loginPage.getLoginButton());
        loginPage.setEmailInput("user@gmail.com");
        loginPage.setPasswordInput("asd123");
        loginPage.getLoginButton().click();

        new WebDriverWait(browser, 5).until(ExpectedConditions.urlToBe("http://localhost:4200/event"));
        assertEquals("http://localhost:4200/event", browser.getCurrentUrl());

        // Searching and selecting event
        homePage.ensureWebElementIsDisplayed(homePage.getSearchButton());
        homePage.getSearchButton().click();
        homePage.ensureWebElementIsDisplayed(homePage.getDetailsButton());
        homePage.getDetailsButton().click();

        new WebDriverWait(browser, 20).until(ExpectedConditions.urlToBe("http://localhost:4200/event/1"));
        assertEquals("http://localhost:4200/event/1", browser.getCurrentUrl());

        // Failed reserving sitting area
        reservePage.ensureWebElementIsDisplayed(reservePage.getSelectSittingArea());
        reservePage.getSelectSittingArea().click();

        reservePage.ensureWebElementIsDisplayed(reservePage.getReserveButtonSitting());
        reservePage.getReserveButtonSitting().click();
        reservePage.ensureWebElementIsDisplayed(reservePage.getSeatsError());
        assertEquals("No seats selected", reservePage.getSeatsError().getText());

        // Valid reservation
        reservePage.ensureWebElementIsDisplayed(reservePage.getSeat1A());
        reservePage.getSeat1A().click();
        reservePage.ensureWebElementIsDisplayed(reservePage.getSeat1B());
        reservePage.getSeat1B().click();
        reservePage.ensureWebElementIsDisplayed(reservePage.getReserveButtonSitting());
        reservePage.getReserveButtonSitting().click();
        reservePage.ensureWebElementIsDisplayed(reservePage.getSnackBar());
        assertEquals("Successful reservation", reservePage.getSnackBar().getText());
    }

    @After
    public void closeSelenium() {
        browser.quit();
    }
}
