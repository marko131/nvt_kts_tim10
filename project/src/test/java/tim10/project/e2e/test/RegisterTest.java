package tim10.project.e2e.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import tim10.project.e2e.pages.AddLocationPage;
import tim10.project.e2e.pages.HomePage;
import tim10.project.e2e.pages.LoginPage;
import tim10.project.e2e.pages.RegisterPage;

import static org.junit.Assert.assertEquals;

@ActiveProfiles("2")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.DEFINED_PORT)
public class RegisterTest {

    private WebDriver browser;

    HomePage homePage;
    RegisterPage registerPage;

    @Before
    public void setUpSelenium() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        browser = new ChromeDriver();
        browser.manage().window().maximize();
        browser.navigate().to("http://localhost:4200");

        homePage = PageFactory.initElements(browser, HomePage.class);
        registerPage = PageFactory.initElements(browser, RegisterPage.class);
    }

    @Test
    public void testRegisterUser() {
        // Home page
        assertEquals("http://localhost:4200/event", browser.getCurrentUrl());

        // To Register page
        homePage.ensureWebElementIsDisplayed(homePage.getNavLink3());
        homePage.getNavLink3().click();
        new WebDriverWait(browser, 5).until(ExpectedConditions.urlToBe("http://localhost:4200/register"));
        assertEquals("http://localhost:4200/register", browser.getCurrentUrl());

        // All empty inputs
        registerPage.setFirstNameInput("");
        registerPage.setLastNameInput("");
        registerPage.setEmailInput("");
        registerPage.setPasswordInput("");
        registerPage.setConfirmPasswordInput("");

        assertEquals("First name is required", registerPage.getGetFirstNameInputError().getText());
        assertEquals("Last name is required", registerPage.getLastNameInputError().getText());
        assertEquals("Email is required", registerPage.getEmailInputError().getText());
        assertEquals("Password is required", registerPage.getPasswordInputError().getText());

        // Invalid email address
        registerPage.setEmailInput("test");
        assertEquals("Email is not valid", registerPage.getEmailInputError().getText());

        // Short password
        registerPage.setPasswordInput("pass");
        assertEquals("Password must be at least 6 characters long", registerPage.getPasswordInputError().getText());

        // Valid inputs but passwords do not match
        registerPage.setFirstNameInput("Test");
        registerPage.setLastNameInput("Test");
        registerPage.setEmailInput("test@gmail.com");
        registerPage.setPasswordInput("testpass");
        registerPage.setConfirmPasswordInput("testpas");

        assertEquals("Passwords do not match", registerPage.getConfirmPasswordInputError().getText());

        // Valid inputs
        registerPage.setConfirmPasswordInput("testpass");

        registerPage.ensureWebElementIsDisplayed(registerPage.getRegisterButton());
        registerPage.getRegisterButton().click();

        new WebDriverWait(browser, 5).until(ExpectedConditions.urlToBe("http://localhost:4200/login"));
        assertEquals("http://localhost:4200/login", browser.getCurrentUrl());
    }

    @After
    public void closeSelenium() {
        browser.quit();
    }
}
