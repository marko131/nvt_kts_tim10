package tim10.project.e2e.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class HomePage {

    private WebDriver driver;

    @FindBy(xpath = "/html/body/app-root/app-navbar/mat-toolbar/mat-toolbar-row/button[1]")
    private WebElement navLink1;

    @FindBy(xpath = "/html/body/app-root/app-navbar/mat-toolbar/mat-toolbar-row/button[2]")
    private WebElement navLink2;

    @FindBy(xpath = "/html/body/app-root/app-navbar/mat-toolbar/mat-toolbar-row/button[3]")
    private WebElement navLink3;

    @FindBy(xpath = "/html/body/app-root/app-navbar/mat-toolbar/mat-toolbar-row/button[4]")
    private WebElement navLink4;

    @FindBy(xpath = "/html/body/app-root/app-navbar/mat-toolbar/mat-toolbar-row/button[5]")
    private WebElement navLink5;

    @FindBy(xpath = "/html/body/app-root/app-event/form/div[1]/mat-form-field/div/div[1]/div/input")
    private WebElement searchInput;

    @FindBy(xpath = "/html/body/app-root/app-event/form/div[1]/button/span/mat-icon")
    private WebElement searchButton;

    @FindBy(xpath = "/html/body/app-root/app-event/form/div[2]/mat-expansion-panel/mat-expansion-panel-header/span[1]/mat-panel-title")
    private WebElement advancedSearchPanel;

    @FindBy(xpath = "/html/body/app-root/app-event/form/div[2]/mat-expansion-panel/div/div/mat-form-field[1]/div/div[1]/div/input")
    private WebElement locationInput;

    @FindBy(xpath = "/html/body/app-root/app-event/form/div[2]/mat-expansion-panel/div/div/mat-form-field[2]/div/div[1]/div/mat-select/div/div[1]/span")
    private WebElement categoryChoicePlaceholder;

    @FindBy(xpath = "/html/body/div[2]/div[2]/div/div/div/mat-option[3]/span")
    private WebElement categoryTheater;

    @FindBy(xpath = "/html/body/div[2]/div[2]/div/div/div/mat-option[4]/span")
    private WebElement categorySport;

    @FindBy(xpath = "/html/body/app-root/app-event/form/div[2]/mat-expansion-panel/div/div/mat-form-field[2]/div/div[1]/div/mat-select/div/div[1]/span/span")
    private WebElement categoryChoice;

    @FindBy(xpath = "/html/body/app-root/app-event/form/div[2]/mat-expansion-panel/div/div/mat-form-field[3]/div/div[1]/div[1]/input")
    private WebElement dateBegin;

    @FindBy(xpath = "/html/body/app-root/app-event/form/div[2]/mat-expansion-panel/div/div/mat-form-field[4]/div/div[1]/div[1]/input")
    private WebElement dateEnd;

    @FindBy(xpath = "/html/body/app-root/app-event/form/div[2]/mat-expansion-panel/div/div/mat-form-field[3]/div/div[3]/div/mat-error")
    private WebElement dateBeginError;

    @FindBy(xpath = "/html/body/app-root/app-event/form/div[2]/mat-expansion-panel/div/div/mat-form-field[4]/div/div[3]/div/mat-error")
    private WebElement dateEndError;

    @FindBy(xpath = "/html/body/app-root/app-event/mat-grid-list/div/mat-grid-tile/figure/mat-card/mat-card-actions/a[1]")
    private WebElement detailsButton;


    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    public void ensureWebElementIsDisplayed(WebElement element) {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(element));
    }

    public WebElement getDetailsButton() {
        return detailsButton;
    }

    public void setDetailsButton(WebElement detailsButton) {
        this.detailsButton = detailsButton;
    }

    public WebElement getNavLink1() {
        return navLink1;
    }

    public void setNavLink1(WebElement navLink1) {
        this.navLink1 = navLink1;
    }

    public WebElement getNavLink2() {
        return navLink2;
    }

    public void setNavLink2(WebElement navLink2) {
        this.navLink2 = navLink2;
    }

    public WebElement getNavLink3() {
        return navLink3;
    }

    public void setNavLink3(WebElement navLink3) {
        this.navLink3 = navLink3;
    }

    public WebElement getNavLink4() {
        return navLink4;
    }

    public void setNavLink4(WebElement navLink4) {
        this.navLink4 = navLink4;
    }

    public WebElement getNavLink5() {
        return navLink5;
    }

    public void setNavLink5(WebElement navLink5) {
        this.navLink5 = navLink5;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getSearchInput() {
        return searchInput;
    }

    public void setSearchInput(String searchInput) {
        this.searchInput.clear();
        this.searchInput.sendKeys(searchInput);
    }

    public WebElement getSearchButton() {
        return searchButton;
    }

    public void setSearchButton(WebElement searchButton) {
        this.searchButton = searchButton;
    }

    public WebElement getAdvancedSearchPanel() {
        return advancedSearchPanel;
    }

    public void setAdvancedSearchPanel(WebElement advancedSearchPanel) {
        this.advancedSearchPanel = advancedSearchPanel;
    }

    public WebElement getLocationInput() {
        return locationInput;
    }

    public void setLocationInput(String locationInput) {
        this.locationInput.clear();
        this.locationInput.sendKeys(locationInput);
    }

    public WebElement getCategoryChoice() {
        return categoryChoice;
    }

    public void setCategoryChoice(String categoryChoice) {
        this.categoryChoice.clear();
        this.categoryChoice.sendKeys(categoryChoice);
    }

    public WebElement getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(String dateBegin) {
        this.dateBegin.clear();
        this.dateBegin.sendKeys(dateBegin);
    }

    public WebElement getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd.clear();
        this.dateEnd.sendKeys(dateEnd);
    }

    public WebElement getDateBeginError() {
        return dateBeginError;
    }

    public void setDateBeginError(WebElement dateBeginError) {
        this.dateBeginError = dateBeginError;
    }

    public WebElement getDateEndError() {
        return dateEndError;
    }

    public void setDateEndError(WebElement dateEndError) {
        this.dateEndError = dateEndError;
    }

    public WebElement getCategoryChoicePlaceholder() {
        return categoryChoicePlaceholder;
    }

    public void setCategoryChoicePlaceholder(WebElement categoryChoicePlaceholder) {
        this.categoryChoicePlaceholder = categoryChoicePlaceholder;
    }

    public WebElement getCategoryTheater() {
        return categoryTheater;
    }

    public void setCategoryTheater(WebElement categoryTheater) {
        this.categoryTheater = categoryTheater;
    }

    public WebElement getCategorySport() {
        return categorySport;
    }

    public void setCategorySport(WebElement categorySport) {
        this.categorySport = categorySport;
    }

    public List<String> getAllEventNames() {
        List<WebElement> events = driver.findElements(By.className("mat-card-title"));
        List<String> names = new ArrayList<>();
        for (WebElement event: events) {
            names.add(event.getText());
        }
        return names;
    }

    public List<String> getAllCategories() {
        List<WebElement> events = driver.findElements(By.className("mat-card-subtitle"));
        List<String> categories = new ArrayList<>();
        for (WebElement event: events) {
            categories.add(event.getText());
        }
        return categories;
    }

    public void clickBackground() {
        WebElement background = driver.findElement(By.className("cdk-overlay-container"));
        background.click();
    }
}
