package tim10.project.e2e.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProfilePage {
    private WebDriver driver;

    @FindBy(xpath = "/html/body/app-root/app-profile/mat-card/mat-tab-group/mat-tab-header/div[2]/div/div/div[1]")
    private WebElement infoTabButton;

    @FindBy(xpath = "/html/body/app-root/app-profile/mat-card/mat-tab-group/mat-tab-header/div[2]/div/div/div[2]")
    private WebElement changePasswordTabButton;

    @FindBy(xpath = "/html/body/app-root/app-profile/mat-card/mat-tab-group/mat-tab-header/div[2]/div/div/div[3]")
    private WebElement myReservationsTabButton;

    @FindBy(xpath = "/html/body/app-root/app-profile/mat-card/mat-tab-group/mat-tab-header/div[2]/div/div/div[4]")
    private WebElement myTicketsTabButton;

    @FindBy(xpath = "/html/body/app-root/app-profile/mat-card/mat-tab-group/div/mat-tab-body[1]/div/form/div[3]/button")
    private WebElement saveButton;

    @FindBy(name = "email")
    private WebElement emailInput;

    @FindBy(css = "#mat-input-7")
    private WebElement firstNameInput;

    @FindBy(xpath = "/html/body/app-root/app-profile/mat-card/mat-tab-group/div/mat-tab-body[1]/div/form/div[2]/mat-form-field[2]/div/div[1]/div/input")
    private WebElement lastNameInput;

    @FindBy(id = "mat-error-12")
    private WebElement emailError;

    @FindBy(id = "mat-error-13")
    private WebElement firstNameError;

    @FindBy(id = "mat-error-14")
    private WebElement lastNameError;

    @FindBy(xpath = "/html/body/app-root/app-profile/mat-card/mat-tab-group/div/mat-tab-body[2]/div/form/div[1]/mat-form-field/div/div[1]/div/input")
    private WebElement password1Input;

    @FindBy(xpath = "/html/body/app-root/app-profile/mat-card/mat-tab-group/div/mat-tab-body[2]/div/form/div[2]/mat-form-field/div/div[1]/div/input")
    private WebElement password2Input;

    @FindBy(xpath = "/html/body/app-root/app-profile/mat-card/mat-tab-group/div/mat-tab-body[2]/div/form/div[1]/mat-form-field/div/div[3]/div/mat-error")
    private WebElement password1Error;

    @FindBy(xpath = "/html/body/app-root/app-profile/mat-card/mat-tab-group/div/mat-tab-body[2]/div/form/div[2]/mat-form-field/div/div[3]/div/mat-error")
    private WebElement password2Error;

    @FindBy(xpath = "/html/body/app-root/app-profile/form[2]/mat-card/button")
    private WebElement changePassword;

    @FindBy(xpath = "/html/body/app-root/app-profile/div/button")
    private WebElement logoutButton;

    public ProfilePage(WebDriver driver){
        this.driver = driver;
    }

    public WebElement getLogoutButton() {
        return logoutButton;
    }

    public WebElement getSaveButton() {
        return saveButton;
    }

    public WebElement getEmailInput() {
        return emailInput;
    }

    public void setEmailInput(String email) {
        this.emailInput.clear();
        this.emailInput.sendKeys(email);
    }

    public WebElement getFirstNameInput() {
        return firstNameInput;
    }

    public void setFirstNameInput(String firstName) {
        this.firstNameInput.clear();
        this.firstNameInput.sendKeys(firstName);
    }

    public WebElement getLastNameInput() {
        return lastNameInput;
    }

    public void setLastNameInput(String lastName) {
        this.lastNameInput.clear();
        this.lastNameInput.sendKeys(lastName);
    }

    public WebElement getEmailError() {
        return emailError;
    }

    public WebElement getFirstNameError() {
        return firstNameError;
    }

    public WebElement getLastNameError() {
        return lastNameError;
    }

    public WebElement getPassword1Error() {
        return password1Error;
    }

    public WebElement getPassword2Error() {
        return password2Error;
    }

    public WebElement getPassword1Input() {
        return password1Input;
    }

    public void setPassword1Input(String password1) {
        this.password1Input.clear();
        this.password1Input.sendKeys(password1);
    }

    public WebElement getPassword2Input() {
        return password2Input;
    }

    public void setPassword2Input(String password2) {
        this.password2Input.clear();
        this.password2Input.sendKeys(password2);
    }

    public WebElement getChangePassword() {
        return changePassword;
    }

    public WebElement getInfoTabButton() {
        return infoTabButton;
    }

    public WebElement getChangePasswordTabButton() {
        return changePasswordTabButton;
    }

    public WebElement getMyReservationsTabButton() {
        return myReservationsTabButton;
    }

    public WebElement getMyTicketsTabButton() {
        return myTicketsTabButton;
    }
}
