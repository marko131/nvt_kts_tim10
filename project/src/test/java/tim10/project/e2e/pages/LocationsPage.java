package tim10.project.e2e.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class LocationsPage {
    private WebDriver driver;

    @FindBy(css = "mat-card[class='mat-grid-tile']")
    List<WebElement> locationsListMatCards;

    @FindBy(xpath = "/html/body/app-root/app-location/mat-grid-list/div/mat-grid-tile/figure/mat-card[1]/mat-card-actions/a[1]")
    WebElement editLocationButton;

    @FindBy(xpath = "/html/body/app-root/app-location/mat-grid-list/div/mat-grid-tile/figure/mat-card[1]/mat-card-actions/a[2]")
    WebElement locationLayoutsButton;

    @FindBy(xpath = "/html/body/app-root/app-location/mat-grid-list/div/mat-grid-tile/figure/mat-card[1]/mat-card-actions/a[3]")
    WebElement addLayoutButton;

    public LocationsPage(WebDriver driver) {
        this.driver = driver;
    }

    public void ensureWebElementIsDispalyed(WebElement element) {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(element));
    }

    public List<WebElement> getLocationsListMatCards() {
        return locationsListMatCards;
    }

    public void setLocationsListMatCards(List<WebElement> locationsListMatCards) {
        this.locationsListMatCards = locationsListMatCards;
    }

    public WebElement getEditLocationButton() {
        return editLocationButton;
    }

    public void setEditLocationButton(WebElement editLocationButton) {
        this.editLocationButton = editLocationButton;
    }

    public WebElement getLocationLayoutsButton() {
        return locationLayoutsButton;
    }

    public void setLocationLayoutsButton(WebElement locationLayoutsButton) {
        this.locationLayoutsButton = locationLayoutsButton;
    }

    public WebElement getAddLayoutButton() {
        return addLayoutButton;
    }

    public void setAddLayoutButton(WebElement addLayoutButton) {
        this.addLayoutButton = addLayoutButton;
    }
}
