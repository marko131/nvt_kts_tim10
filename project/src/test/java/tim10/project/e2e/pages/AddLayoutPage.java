package tim10.project.e2e.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AddLayoutPage {

    private WebDriver driver;

    @FindBy(name="layoutName")
    WebElement layoutNameInput;

    @FindBy(xpath ="/html/body/app-root/app-add-layout/div/mat-card/div[2]/mat-horizontal-stepper/div[2]/div[1]/form/mat-form-field[1]/div/div[3]/div/mat-error")
    WebElement layoutNameInputError;

    @FindBy(name="stagePosition")
    WebElement stagePositionSelect;

    @FindBy(xpath ="/html/body/app-root/app-add-layout/div/mat-card/div[2]/mat-horizontal-stepper/div[2]/div[1]/form/mat-form-field[2]/div/div[3]/div/mat-error")
    WebElement stagePositionSelectError;

    @FindBy(xpath="/html/body/app-root/app-add-layout/div/mat-card/div[2]/mat-horizontal-stepper/div[2]/div[1]/form/div/button")
    WebElement firstNextButton;

    @FindBy(name="rowNum")
    WebElement rowNumberInput;

    @FindBy(xpath="/html/body/app-root/app-add-layout/div/mat-card/div[2]/mat-horizontal-stepper/div[2]/div[2]/form/mat-form-field[1]/div/div[3]/div/mat-error")
    WebElement rowNumberInputError;

    @FindBy(name="seatsNum")
    WebElement seatsNumberInput;

    @FindBy(xpath="/html/body/app-root/app-add-layout/div/mat-card/div[2]/mat-horizontal-stepper/div[2]/div[2]/form/mat-form-field[2]/div/div[3]/div/mat-error")
    WebElement seatsNumberInputError;

    @FindBy(name="layoutPositionSittingArea")
    WebElement seatingLayoutPositionSelect;

    @FindBy(xpath="/html/body/app-root/app-add-layout/div/mat-card/div[2]/mat-horizontal-stepper/div[2]/div[2]/form/mat-form-field[3]/div/div[3]/div/mat-error")
    WebElement seatingLayoutPositionSelectError;

    @FindBy(xpath = "/html/body/app-root/app-add-layout/div/mat-card/div[2]/mat-horizontal-stepper/div[2]/div[2]/form/div[1]/button")
    WebElement addSeatingLayoutButton;

    @FindBy(xpath = "/html/body/app-root/app-add-layout/div/mat-card/div[2]/mat-horizontal-stepper/div[2]/div[2]/form/div[2]/button")
    WebElement secondNextButton;

    @FindBy(name="maxNumber")
    WebElement maxNumberInput;

    @FindBy(xpath = "/html/body/app-root/app-add-layout/div/mat-card/div[2]/mat-horizontal-stepper/div[2]/div[3]/form/mat-form-field[1]/div/div[3]/div/mat-error[1]")
    WebElement maxNumberInputError;

    @FindBy(name="layoutPositionStandingArea")
    WebElement standingLayoutPositionSelect;

    @FindBy(xpath = "/html/body/app-root/app-add-layout/div/mat-card/div[2]/mat-horizontal-stepper/div[2]/div[3]/form/mat-form-field[2]/div/div[3]/div/mat-error")
    WebElement standingLayoutPositionSelectError;

    @FindBy(xpath = "/html/body/app-root/app-add-layout/div/mat-card/div[2]/mat-horizontal-stepper/div[2]/div[3]/form/div[1]/button")
    WebElement addStandingLayoutButton;

    @FindBy(xpath = "/html/body/app-root/app-add-layout/div/mat-card/div[2]/mat-horizontal-stepper/div[2]/div[3]/form/div[2]/button[2]")
    WebElement finishButton;

    @FindBy(xpath = "/html/body/app-root/app-add-layout/div/mat-card/div[2]/mat-horizontal-stepper/div[2]/div[4]/div/button[3]")
    WebElement submitButton;

    public AddLayoutPage(WebDriver driver) {
        this.driver = driver;
    }

    public void ensureWebElementIsDisplayed(WebElement element) {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(element));
    }

    public WebElement getLayoutNameInputError() {
        return layoutNameInputError;
    }

    public void setLayoutNameInputError(WebElement layoutNameInputError) {
        this.layoutNameInputError = layoutNameInputError;
    }

    public WebElement getStagePositionSelectError() {
        return stagePositionSelectError;
    }

    public void setStagePositionSelectError(WebElement stagePositionSelectError) {
        this.stagePositionSelectError = stagePositionSelectError;
    }

    public WebElement getFirstNextButton() {
        return firstNextButton;
    }

    public void setFirstNextButton(WebElement firstNextButton) {
        this.firstNextButton = firstNextButton;
    }

    public WebElement getRowNumberInputError() {
        return rowNumberInputError;
    }

    public void setRowNumberInputError(WebElement rowNumberInputError) {
        this.rowNumberInputError = rowNumberInputError;
    }

    public WebElement getSeatsNumberInputError() {
        return seatsNumberInputError;
    }

    public void setSeatsNumberInputError(WebElement seatsNumberInputError) {
        this.seatsNumberInputError = seatsNumberInputError;
    }

    public WebElement getSeatingLayoutPositionSelectError() {
        return seatingLayoutPositionSelectError;
    }

    public void setSeatingLayoutPositionSelectError(WebElement seatingLayoutPositionSelectError) {
        this.seatingLayoutPositionSelectError = seatingLayoutPositionSelectError;
    }

    public WebElement getAddSeatingLayoutButton() {
        return addSeatingLayoutButton;
    }

    public void setAddSeatingLayoutButton(WebElement addSeatingLayoutButton) {
        this.addSeatingLayoutButton = addSeatingLayoutButton;
    }

    public WebElement getSecondNextButton() {
        return secondNextButton;
    }

    public void setSecondNextButton(WebElement secondNextButton) {
        this.secondNextButton = secondNextButton;
    }

    public WebElement getMaxNumberInputError() {
        return maxNumberInputError;
    }

    public void setMaxNumberInputError(WebElement maxNumberInputError) {
        this.maxNumberInputError = maxNumberInputError;
    }

    public WebElement getStandingLayoutPositionSelectError() {
        return standingLayoutPositionSelectError;
    }

    public void setStandingLayoutPositionSelectError(WebElement standingLayoutPositionSelectError) {
        this.standingLayoutPositionSelectError = standingLayoutPositionSelectError;
    }

    public WebElement getAddStandingLayoutButton() {
        return addStandingLayoutButton;
    }

    public void setAddStandingLayoutButton(WebElement addStandingLayoutButton) {
        this.addStandingLayoutButton = addStandingLayoutButton;
    }

    public WebElement getFinishButton() {
        return finishButton;
    }

    public void setFinishButton(WebElement finishButton) {
        this.finishButton = finishButton;
    }

    public WebElement getSubmitButton() {
        return submitButton;
    }

    public void setSubmitButton(WebElement submitButton) {
        this.submitButton = submitButton;
    }

    public WebElement getLayoutNameInput() {
        return layoutNameInput;
    }

    public void setLayoutNameInput(String value) {
        WebElement element = getLayoutNameInput();
        element.clear();
        element.sendKeys(value);
    }

    public WebElement getRowNumberInput() {
        return rowNumberInput;
    }

    public void setRowNumberInput(String value) {
        WebElement element = getRowNumberInput();
        element.clear();
        element.sendKeys(value);
    }

    public WebElement getSeatsNumberInput() {
        return seatsNumberInput;
    }

    public void setSeatsNumberInput(String value) {
        WebElement element = getSeatsNumberInput();
        element.clear();
        element.sendKeys(value);
    }

    public WebElement getMaxNumberInput() {
        return maxNumberInput;
    }

    public void setMaxNumberInput(String value) {
        WebElement element = getMaxNumberInput();
        element.clear();
        element.sendKeys(value);
    }

    public WebElement getStagePositionSelect() {
        return stagePositionSelect;
    }

    public void setStagePositionSelect(String value) {
        WebElement select = getStagePositionSelect();
        ensureWebElementIsDisplayed(select);
        select.click();
        WebElement option = driver.findElement(By.xpath("/html/body/div[2]/div[2]/div/div/div/mat-option/span[contains(text(),'" + value + "')]"));
        ensureWebElementIsDisplayed(option);
        option.click();
    }

    public WebElement getSeatingLayoutPositionSelect() {
        return seatingLayoutPositionSelect;
    }

    public void setSeatingLayoutPositionSelect(String value) {
        WebElement select = getSeatingLayoutPositionSelect();
        ensureWebElementIsDisplayed(select);
        select.click();
        WebElement option = driver.findElement(By.xpath("/html/body/div[2]/div[2]/div/div/div/mat-option/span[contains(text(),'" + value + "')]"));
        ensureWebElementIsDisplayed(option);
        option.click();
    }

    public WebElement getStandingLayoutPositionSelect() {
        return standingLayoutPositionSelect;
    }

    public void setStandingLayoutPositionSelect(String value) {
        WebElement select = getStandingLayoutPositionSelect();
        ensureWebElementIsDisplayed(select);
        select.click();
        WebElement option = driver.findElement(By.xpath("/html/body/div[2]/div[2]/div/div/div/mat-option/span[contains(text(),'" + value + "')]"));
        ensureWebElementIsDisplayed(option);
        option.click();
    }
}
