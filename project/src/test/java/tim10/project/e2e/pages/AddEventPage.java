package tim10.project.e2e.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AddEventPage {

    private WebDriver driver;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[1]/form/div[1]/mat-form-field[1]/div/div[1]/div/input")
    private WebElement name;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[1]/form/div[1]/mat-form-field[1]/div/div[3]/div/mat-error")
    private WebElement nameError;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[1]/form/div[1]/mat-form-field[2]/div/div[1]/div/mat-select")
    private WebElement category;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[1]/form/div[1]/mat-form-field[2]/div/div[3]/div/mat-error")
    private WebElement categoryError;

    @FindBy(xpath = "/html/body/div[2]/div[2]/div/div/div/mat-option[4]/span")
    private WebElement categorySport;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[1]/form/div[1]/mat-form-field[3]/div/div[1]/div/input")
    private WebElement image;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[1]/form/div[1]/mat-form-field[3]/div/div[3]/div/mat-error")
    private WebElement imageError;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[1]/form/mat-form-field/div/div[1]/div[3]/textarea")
    private WebElement description;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[1]/form/mat-form-field/div/div[2]/div/mat-error")
    private WebElement descriptionError;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[1]/form/div[2]/mat-form-field[1]/div/div[1]/div/input")
    private WebElement daysReservationIsValid;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[1]/form/div[2]/mat-form-field[1]/div/div[3]/div/mat-error")
    private WebElement daysReservationIsValidError;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[1]/form/div[2]/mat-form-field[2]/div/div[1]/div/mat-select")
    private WebElement location;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[1]/form/div[2]/mat-form-field[2]/div/div[3]/div/mat-error")
    private WebElement locationError;

    @FindBy(xpath = "/html/body/div[2]/div[2]/div/div/div/mat-option/span")
    private WebElement locationChoice;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[1]/form/div[2]/mat-form-field[3]/div/div[1]/div/mat-select")
    private WebElement layout;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[1]/form/div[2]/mat-form-field[3]/div/div[3]/div/mat-error")
    private WebElement layoutError;

    @FindBy(xpath = "/html/body/div[2]/div[2]/div/div/div/mat-option/span")
    private WebElement layoutChoice;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[1]/form/div[2]/mat-form-field[4]/div/div[1]/div[1]/input")
    private WebElement availableUntil;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[1]/form/div[2]/mat-form-field[4]/div/div[3]/div/mat-error")
    private WebElement availableUntilError;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[1]/form/div[2]/mat-form-field[5]/div/div[1]/div/input")
    private WebElement maxReservations;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[1]/form/div[2]/mat-form-field[5]/div/div[3]/div/mat-error")
    private WebElement maxReservationsError;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[2]/form/div[2]/div/div/mat-form-field[1]/div/div[1]/div[1]/input")
    private WebElement standingAreaDate;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[2]/form/div[2]/div/div/mat-form-field[1]/div/div[3]/div/mat-error")
    private WebElement standingAreaDateError;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[2]/form/div[2]/div/div/mat-form-field[2]/div/div[1]/div/mat-select")
    private WebElement standingAreaType;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[2]/form/div[2]/div/div/mat-form-field[2]/div/div[3]/div/mat-error")
    private WebElement standingAreaTypeError;

    @FindBy(xpath = "/html/body/div[2]/div[2]/div/div/div/mat-option[2]/span")
    private WebElement standingAreaTypeChoice;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[2]/form/div[2]/div/div/mat-form-field[3]/div/div[1]/div/mat-select")
    private WebElement standingAreaPosition;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[2]/form/div[2]/div/div/mat-form-field[3]/div/div[3]/div/mat-error")
    private WebElement standingAreaPositionError;

    @FindBy(xpath = "/html/body/div[2]/div[2]/div/div/div/mat-option/span")
    private WebElement standingAreaPositionChoice;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[2]/form/div[2]/div/div/mat-form-field[4]/div/div[1]/div/input")
    private WebElement standingAreaPrice;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[2]/form/div[2]/div/div/mat-form-field[3]/div/div[3]/div/mat-error")
    private WebElement standingAreaPriceError;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[2]/form/div[3]/div/div/mat-form-field[1]/div/div[1]/div[1]/input")
    private WebElement sittingAreaDate;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[2]/form/div[3]/div/div/mat-form-field[1]/div/div[3]/div/mat-error")
    private WebElement sittingAreaDateError;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[2]/form/div[3]/div/div/mat-form-field[2]/div/div[1]/div/mat-select")
    private WebElement sittingAreaType;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[2]/form/div[3]/div/div/mat-form-field[2]/div/div[3]/div/mat-error")
    private WebElement sittingAreaTypeError;

    @FindBy(xpath = "/html/body/div[2]/div[2]/div/div/div/mat-option[1]/span")
    private WebElement sittingAreaTypeChoice;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[2]/form/div[3]/div/div/mat-form-field[3]/div/div[1]/div/mat-select")
    private WebElement sittingAreaPosition;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[2]/form/div[3]/div/div/mat-form-field[3]/div/div[3]/div/mat-error")
    private WebElement sittingAreaPositionError;

    @FindBy(xpath = "/html/body/div[2]/div[2]/div/div/div/mat-option/span")
    private WebElement sittingAreaPositionChoice;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[2]/form/div[3]/div/div/mat-form-field[4]/div/div[1]/div/input")
    private WebElement sittingAreaPrice;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[2]/form/div[3]/div/div/mat-form-field[4]/div/div[3]/div/mat-error")
    private WebElement sittingAreaPriceError;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[1]/mat-step-header[1]")
    private WebElement eventInfoTab;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[1]/mat-step-header[2]")
    private WebElement scheduleTab;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[1]/mat-step-header[3]")
    private WebElement doneTab;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[1]/form/div[3]/button")
    private WebElement eventInfoNext;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[2]/form/div[4]/button[2]")
    private WebElement scheduleNext;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[2]/form/div[4]/button[1]")
    private WebElement scheduleBack;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[2]/form/div[1]/button")
    private WebElement addPriceList;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[2]/form/div[4]/div/div/a/span/mat-icon")
    private WebElement deleteThirdArea;

    @FindBy(xpath = "/html/body/app-root/app-create-event/div/mat-card/mat-horizontal-stepper/div[2]/div[3]/div/button[3]")
    private WebElement createEvent;

    @FindBy(xpath = "/html/body/div[2]/div/div/snack-bar-container/simple-snack-bar")
    private WebElement snackBar;

    public void ensureWebElementIsDisplayed(WebElement element) {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(element));
    }

    public AddEventPage(WebDriver driver) {
        this.driver = driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getName() {
        return name;
    }

    public void setName(String name) {
        this.name.clear();
        this.name.sendKeys(name);
    }

    public WebElement getNameError() {
        return nameError;
    }

    public void setNameError(WebElement nameError) {
        this.nameError = nameError;
    }

    public WebElement getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category.clear();
        this.category.sendKeys(category);
    }

    public WebElement getCategoryError() {
        return categoryError;
    }

    public void setCategoryError(WebElement categoryError) {
        this.categoryError = categoryError;
    }

    public WebElement getCategorySport() {
        return categorySport;
    }

    public void setCategorySport(WebElement categorySport) {
        this.categorySport = categorySport;
    }

    public WebElement getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image.clear();
        this.image.sendKeys(image);
    }

    public WebElement getImageError() {
        return imageError;
    }

    public void setImageError(WebElement imageError) {
        this.imageError = imageError;
    }

    public WebElement getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description.clear();
        this.description.sendKeys(description);
    }

    public WebElement getDescriptionError() {
        return descriptionError;
    }

    public void setDescriptionError(WebElement descriptionError) {
        this.descriptionError = descriptionError;
    }

    public WebElement getDaysReservationIsValid() {
        return daysReservationIsValid;
    }

    public void setDaysReservationIsValid(String daysReservationIsValid) {
        this.daysReservationIsValid.clear();
        this.daysReservationIsValid.sendKeys(daysReservationIsValid);
    }

    public WebElement getDaysReservationIsValidError() {
        return daysReservationIsValidError;
    }

    public void setDaysReservationIsValidError(WebElement daysReservationIsValidError) {
        this.daysReservationIsValidError = daysReservationIsValidError;
    }

    public WebElement getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location.clear();
        this.location.sendKeys(location);
    }

    public WebElement getLocationError() {
        return locationError;
    }

    public void setLocationError(WebElement locationError) {
        this.locationError = locationError;
    }

    public WebElement getLocationChoice() {
        return locationChoice;
    }

    public void setLocationChoice(WebElement locationChoice) {
        this.locationChoice = locationChoice;
    }

    public WebElement getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout.clear();
        this.layout.sendKeys(layout);
    }

    public WebElement getLayoutError() {
        return layoutError;
    }

    public void setLayoutError(WebElement layoutError) {
        this.layoutError = layoutError;
    }

    public WebElement getLayoutChoice() {
        return layoutChoice;
    }

    public void setLayoutChoice(WebElement layoutChoice) {
        this.layoutChoice = layoutChoice;
    }

    public WebElement getAvailableUntil() {
        return availableUntil;
    }

    public void setAvailableUntil(String availableUntil) {
        this.availableUntil.clear();
        this.availableUntil.sendKeys(availableUntil);
    }

    public WebElement getAvailableUntilError() {
        return availableUntilError;
    }

    public void setAvailableUntilError(WebElement availableUntilError) {
        this.availableUntilError = availableUntilError;
    }

    public WebElement getMaxReservations() {
        return maxReservations;
    }

    public void setMaxReservations(String maxReservations) {
        this.maxReservations.clear();
        this.maxReservations.sendKeys(maxReservations);
    }

    public WebElement getMaxReservationsError() {
        return maxReservationsError;
    }

    public void setMaxReservationsError(WebElement maxReservationsError) {
        this.maxReservationsError = maxReservationsError;
    }

    public WebElement getStandingAreaDate() {
        return standingAreaDate;
    }

    public void setStandingAreaDate(String standingAreaDate) {
        this.standingAreaDate.clear();
        this.standingAreaDate.sendKeys(standingAreaDate);
    }

    public WebElement getStandingAreaDateError() {
        return standingAreaDateError;
    }

    public void setStandingAreaDateError(WebElement standingAreaDateError) {
        this.standingAreaDateError = standingAreaDateError;
    }

    public WebElement getStandingAreaType() {
        return standingAreaType;
    }

    public void setStandingAreaType(String standingAreaType) {
        this.standingAreaType.clear();
        this.standingAreaType.sendKeys(standingAreaType);
    }

    public WebElement getStandingAreaTypeError() {
        return standingAreaTypeError;
    }

    public void setStandingAreaTypeError(WebElement standingAreaTypeError) {
        this.standingAreaTypeError = standingAreaTypeError;
    }

    public WebElement getStandingAreaTypeChoice() {
        return standingAreaTypeChoice;
    }

    public void setStandingAreaTypeChoice(WebElement standingAreaTypeChoice) {
        this.standingAreaTypeChoice = standingAreaTypeChoice;
    }

    public WebElement getStandingAreaPosition() {
        return standingAreaPosition;
    }

    public void setStandingAreaPosition(String standingAreaPosition) {
        this.standingAreaPosition.clear();
        this.standingAreaPosition.sendKeys(standingAreaPosition);
    }

    public WebElement getStandingAreaPositionError() {
        return standingAreaPositionError;
    }

    public void setStandingAreaPositionError(WebElement standingAreaPositionError) {
        this.standingAreaPositionError = standingAreaPositionError;
    }

    public WebElement getStandingAreaPositionChoice() {
        return standingAreaPositionChoice;
    }

    public void setStandingAreaPositionChoice(WebElement standingAreaPositionChoice) {
        this.standingAreaPositionChoice = standingAreaPositionChoice;
    }

    public WebElement getStandingAreaPrice() {
        return standingAreaPrice;
    }

    public void setStandingAreaPrice(String standingAreaPrice) {
        this.standingAreaPrice.clear();
        this.standingAreaPrice.sendKeys(standingAreaPrice);
    }

    public WebElement getStandingAreaPriceError() {
        return standingAreaPriceError;
    }

    public void setStandingAreaPriceError(WebElement standingAreaPriceError) {
        this.standingAreaPriceError = standingAreaPriceError;
    }

    public WebElement getSittingAreaDate() {
        return sittingAreaDate;
    }

    public void setSittingAreaDate(String sittingAreaDate) {
        this.sittingAreaDate.clear();
        this.sittingAreaDate.sendKeys(sittingAreaDate);
    }

    public WebElement getSittingAreaDateError() {
        return sittingAreaDateError;
    }

    public void setSittingAreaDateError(WebElement sittingAreaDateError) {
        this.sittingAreaDateError = sittingAreaDateError;
    }

    public WebElement getSittingAreaType() {
        return sittingAreaType;
    }

    public void setSittingAreaType(String sittingAreaType) {
        this.sittingAreaType.clear();
        this.sittingAreaType.sendKeys(sittingAreaType);
    }

    public WebElement getSittingAreaTypeError() {
        return sittingAreaTypeError;
    }

    public void setSittingAreaTypeError(WebElement sittingAreaTypeError) {
        this.sittingAreaTypeError = sittingAreaTypeError;
    }

    public WebElement getSittingAreaTypeChoice() {
        return sittingAreaTypeChoice;
    }

    public void setSittingAreaTypeChoice(WebElement sittingAreaTypeChoice) {
        this.sittingAreaTypeChoice = sittingAreaTypeChoice;
    }

    public WebElement getSittingAreaPosition() {
        return sittingAreaPosition;
    }

    public void setSittingAreaPosition(String sittingAreaPosition) {
        this.sittingAreaPosition.clear();
        this.sittingAreaPosition.sendKeys(sittingAreaPosition);
    }

    public WebElement getSittingAreaPositionError() {
        return sittingAreaPositionError;
    }

    public void setSittingAreaPositionError(WebElement sittingAreaPositionError) {
        this.sittingAreaPositionError = sittingAreaPositionError;
    }

    public WebElement getSittingAreaPositionChoice() {
        return sittingAreaPositionChoice;
    }

    public void setSittingAreaPositionChoice(WebElement sittingAreaPositionChoice) {
        this.sittingAreaPositionChoice = sittingAreaPositionChoice;
    }

    public WebElement getSittingAreaPrice() {
        return sittingAreaPrice;
    }

    public void setSittingAreaPrice(String sittingAreaPrice) {
        this.sittingAreaPrice.clear();
        this.sittingAreaPrice.sendKeys(sittingAreaPrice);
    }

    public WebElement getSittingAreaPriceError() {
        return sittingAreaPriceError;
    }

    public void setSittingAreaPriceError(WebElement sittingAreaPriceError) {
        this.sittingAreaPriceError = sittingAreaPriceError;
    }

    public WebElement getEventInfoTab() {
        return eventInfoTab;
    }

    public void setEventInfoTab(WebElement eventInfoTab) {
        this.eventInfoTab = eventInfoTab;
    }

    public WebElement getScheduleTab() {
        return scheduleTab;
    }

    public void setScheduleTab(WebElement scheduleTab) {
        this.scheduleTab = scheduleTab;
    }

    public WebElement getDoneTab() {
        return doneTab;
    }

    public void setDoneTab(WebElement doneTab) {
        this.doneTab = doneTab;
    }

    public WebElement getEventInfoNext() {
        return eventInfoNext;
    }

    public void setEventInfoNext(WebElement eventInfoNext) {
        this.eventInfoNext = eventInfoNext;
    }

    public WebElement getScheduleNext() {
        return scheduleNext;
    }

    public void setScheduleNext(WebElement scheduleNext) {
        this.scheduleNext = scheduleNext;
    }

    public WebElement getScheduleBack() {
        return scheduleBack;
    }

    public void setScheduleBack(WebElement scheduleBack) {
        this.scheduleBack = scheduleBack;
    }

    public WebElement getAddPriceList() {
        return addPriceList;
    }

    public void setAddPriceList(WebElement addPriceList) {
        this.addPriceList = addPriceList;
    }

    public WebElement getDeleteThirdArea() {
        return deleteThirdArea;
    }

    public void setDeleteThirdArea(WebElement deleteThirdArea) {
        this.deleteThirdArea = deleteThirdArea;
    }

    public WebElement getCreateEvent() {
        return createEvent;
    }

    public void setCreateEvent(WebElement createEvent) {
        this.createEvent = createEvent;
    }

    public WebElement getSnackBar() {
        return snackBar;
    }

    public void setSnackBar(WebElement snackBar) {
        this.snackBar = snackBar;
    }

    public void clickBackground() {
        Actions action = new Actions(driver);
        action.sendKeys(Keys.ESCAPE).perform();
    }
}
