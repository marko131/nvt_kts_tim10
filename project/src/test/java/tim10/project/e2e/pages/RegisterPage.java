package tim10.project.e2e.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RegisterPage {

    private WebDriver driver;

    @FindBy(name="firstName")
    WebElement firstNameInput;

    @FindBy(xpath="/html/body/app-root/app-register/mat-card/form/mat-form-field[1]/div/div[3]/div/mat-error")
    WebElement getFirstNameInputError;

    @FindBy(name="lastName")
    WebElement lastNameInput;

    @FindBy(xpath="/html/body/app-root/app-register/mat-card/form/mat-form-field[2]/div/div[3]/div/mat-error")
    WebElement lastNameInputError;

    @FindBy(name="email")
    WebElement emailInput;

    @FindBy(xpath="/html/body/app-root/app-register/mat-card/form/mat-form-field[3]/div/div[3]/div/mat-error")
    WebElement emailInputError;

    @FindBy(name="password")
    WebElement passwordInput;

    @FindBy(xpath="/html/body/app-root/app-register/mat-card/form/mat-form-field[4]/div/div[3]/div/mat-error")
    WebElement passwordInputError;

    @FindBy(name="confirmPassword")
    WebElement confirmPasswordInput;

    @FindBy(xpath="/html/body/app-root/app-register/mat-card/form/mat-form-field[5]/div/div[3]/div/mat-error")
    WebElement confirmPasswordInputError;

    @FindBy(xpath="/html/body/app-root/app-register/mat-card/form/button")
    WebElement registerButton;

    public RegisterPage(WebDriver driver) {
        this.driver = driver;
    }

    public void ensureWebElementIsDisplayed(WebElement element) {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(element));
    }

    public WebElement getFirstNameInput() {
        return firstNameInput;
    }

    public void setFirstNameInput(String value) {
        WebElement element = getFirstNameInput();
        element.clear();
        element.sendKeys(value);
    }

    public WebElement getLastNameInput() {
        return lastNameInput;
    }

    public void setLastNameInput(String value) {
        WebElement element = getLastNameInput();
        element.clear();
        element.sendKeys(value);
    }

    public WebElement getEmailInput() {
        return emailInput;
    }

    public void setEmailInput(String value) {
        WebElement element = getEmailInput();
        element.clear();
        element.sendKeys(value);
    }

    public WebElement getPasswordInput() {
        return passwordInput;
    }

    public void setPasswordInput(String value) {
        WebElement element = getPasswordInput();
        element.clear();
        element.sendKeys(value);
    }

    public WebElement getGetFirstNameInputError() {
        return getFirstNameInputError;
    }

    public void setGetFirstNameInputError(WebElement getFirstNameInputError) {
        this.getFirstNameInputError = getFirstNameInputError;
    }

    public WebElement getLastNameInputError() {
        return lastNameInputError;
    }

    public void setLastNameInputError(WebElement lastNameInputError) {
        this.lastNameInputError = lastNameInputError;
    }

    public WebElement getEmailInputError() {
        return emailInputError;
    }

    public void setEmailInputError(WebElement emailInputError) {
        this.emailInputError = emailInputError;
    }

    public WebElement getPasswordInputError() {
        return passwordInputError;
    }

    public void setPasswordInputError(WebElement passwordInputError) {
        this.passwordInputError = passwordInputError;
    }

    public WebElement getConfirmPasswordInput() {
        return confirmPasswordInput;
    }

    public void setConfirmPasswordInput(String value) {
        WebElement element = getConfirmPasswordInput();
        element.clear();
        element.sendKeys(value);
    }

    public WebElement getConfirmPasswordInputError() {
        return confirmPasswordInputError;
    }

    public void setConfirmPasswordInputError(WebElement confirmPasswordInputError) {
        this.confirmPasswordInputError = confirmPasswordInputError;
    }

    public WebElement getRegisterButton() {
        return registerButton;
    }

    public void setRegisterButton(WebElement registerButton) {
        this.registerButton = registerButton;
    }
}
