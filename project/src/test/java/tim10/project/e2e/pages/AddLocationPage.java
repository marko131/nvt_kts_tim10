package tim10.project.e2e.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AddLocationPage {

    private WebDriver driver;

    @FindBy(name="name")
    WebElement nameInput;

    @FindBy(xpath = "/html/body/app-root/app-add-location/form/mat-card/mat-form-field[1]/div/div[3]/div/mat-error")
    WebElement nameInputError;

    @FindBy(name="address")
    WebElement addressInput;

    @FindBy(xpath = "/html/body/app-root/app-add-location/form/mat-card/mat-form-field[2]/div/div[3]/div/mat-error")
    WebElement addressInputError;

    @FindBy(name="latitude")
    WebElement latitudeInput;

    @FindBy(xpath = "/html/body/app-root/app-add-location/form/mat-card/mat-form-field[3]/div/div[3]/div/mat-error")
    WebElement latitudeInputError;

    @FindBy(name="longitude")
    WebElement longitudeInput;

    @FindBy(xpath = "/html/body/app-root/app-add-location/form/mat-card/mat-form-field[4]/div/div[3]/div/mat-error")
    WebElement longitudeInputError;

    @FindBy(name="picture")
    WebElement pictureInput;

    @FindBy(xpath = "/html/body/app-root/app-add-location/form/mat-card/mat-form-field[5]/div/div[3]/div/mat-error")
    WebElement pictureInputError;

    @FindBy(id="submitLocation")
    WebElement addLocationButton;

    public AddLocationPage(WebDriver driver) {
        this.driver = driver;
    }

    public void ensureWebElementIsDisplayed(WebElement element) {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(element));
    }

    public WebElement getNameInput() {
        return nameInput;
    }

    public void setNameInput(String value) {
        WebElement element = getNameInput();
        element.clear();
        element.sendKeys(value);
    }

    public WebElement getAddressInput() {
        return addressInput;
    }

    public void setAddressInput(String value) {
        WebElement element = getAddressInput();
        element.clear();
        element.sendKeys(value);
    }

    public WebElement getLatitudeInput() {
        return latitudeInput;
    }

    public void setLatitudeInput(String value) {
        WebElement element = getLatitudeInput();
        element.clear();
        element.sendKeys(value);
    }

    public WebElement getLongitudeInput() {
        return longitudeInput;
    }

    public void setLongitudeInput(String value) {
        WebElement element = getLongitudeInput();
        element.clear();
        element.sendKeys(value);
    }

    public WebElement getPictureInput() {
        return pictureInput;
    }

    public void setPictureInput(String value) {
        WebElement element = getPictureInput();
        element.clear();
        element.sendKeys(value);
    }

    public WebElement getAddLocationButton() {
        return addLocationButton;
    }

    public void setAddLocationButton(WebElement addLocationButton) {
        this.addLocationButton = addLocationButton;
    }

    public WebElement getNameInputError() {
        return nameInputError;
    }

    public void setNameInputError(WebElement nameInputError) {
        this.nameInputError = nameInputError;
    }

    public WebElement getAddressInputError() {
        return addressInputError;
    }

    public void setAddressInputError(WebElement addressInputError) {
        this.addressInputError = addressInputError;
    }

    public WebElement getLatitudeInputError() {
        return latitudeInputError;
    }

    public void setLatitudeInputError(WebElement latitudeInputError) {
        this.latitudeInputError = latitudeInputError;
    }

    public WebElement getLongitudeInputError() {
        return longitudeInputError;
    }

    public void setLongitudeInputError(WebElement longitudeInputError) {
        this.longitudeInputError = longitudeInputError;
    }

    public WebElement getPictureInputError() {
        return pictureInputError;
    }

    public void setPictureInputError(WebElement pictureInputError) {
        this.pictureInputError = pictureInputError;
    }
}
