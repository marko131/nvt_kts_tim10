package tim10.project.e2e.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
    private WebDriver driver;

    @FindBy(name = "email")
    private WebElement emailInput;

    @FindBy(name = "password")
    private WebElement passwordInput;

    @FindBy(xpath = "/html/body/app-root/app-login/mat-card/form/mat-form-field[1]/div/div[3]/div/mat-error")
    private WebElement emailError;

    @FindBy(xpath = "/html/body/app-root/app-login/mat-card/form/mat-form-field[2]/div/div[3]/div/mat-error")
    private WebElement passwordError;

    @FindBy(xpath = "/html/body/app-root/app-login/mat-card/form/button")
    private WebElement loginButton;

    @FindBy(xpath = "/html/body/app-root/app-navbar/mat-toolbar/mat-toolbar-row/button[2]")
    private WebElement userProfileButton;

    @FindBy(xpath = "/html/body/app-root/app-navbar/mat-toolbar/mat-toolbar-row/button[5]")
    private WebElement adminProfileButton;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    public void ensureWebElementIsDisplayed(WebElement element) {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(element));
    }

    public WebElement getLoginButton() {
        return loginButton;
    }

    public void setLoginButton(WebElement loginButton) {
        this.loginButton = loginButton;
    }

    public WebElement getEmailInput() {
        return emailInput;
    }

    public void setEmailInput(String email) {
        this.emailInput.clear();
        this.emailInput.sendKeys(email);
    }

    public WebElement getPasswordInput() {
        return passwordInput;
    }

    public void setPasswordInput(String password) {
        this.passwordInput.clear();
        this.passwordInput.sendKeys(password);
    }

    public WebElement getUserProfileButton() {
        return userProfileButton;
    }

    public WebElement getAdminProfileButton() {
        return adminProfileButton;
    }

    public WebElement getEmailError() {
        return emailError;
    }

    public WebElement getPasswordError() {
        return passwordError;
    }
}
