package tim10.project.e2e.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ReservePage {

    private WebDriver driver;

    @FindBy(xpath = "/html/body/app-root/app-event-details/div/mat-card/mat-card-content/table/tbody/tr[1]/td[5]/button")
    private WebElement selectStandingArea;

    @FindBy(xpath = "/html/body/app-root/app-event-details/div/mat-card/mat-card-content/table/tbody/tr[2]/td[5]/button")
    private WebElement selectSittingArea;

    @FindBy(xpath = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-select-ticket-dialog/mat-dialog-content/div[1]/form/div[1]/mat-form-field/div/div[1]/div/input")
    private WebElement numberOfSeats;

    @FindBy(xpath = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-select-ticket-dialog/mat-dialog-content/div[1]/form/div[2]/button")
    private WebElement reserveButtonStanding;

    @FindBy(xpath = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-select-ticket-dialog/mat-dialog-content/div[2]")
    private WebElement totalPrice;

    @FindBy(xpath = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-select-ticket-dialog/mat-dialog-content/div[1]/form/div[1]/mat-form-field/div/div[3]/div/mat-error")
    private WebElement numberOfSeatsError;

    @FindBy(xpath = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-select-ticket-dialog/mat-dialog-content/div[1]/seating-layout/div/ol/li[1]/ol/li[1]")
    private WebElement seat1A;

    @FindBy(xpath = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-select-ticket-dialog/mat-dialog-content/div[1]/seating-layout/div/ol/li[1]/ol/li[2]")
    private WebElement seat1B;

    @FindBy(xpath = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-select-ticket-dialog/mat-dialog-content/div[1]/div[2]/mat-error")
    private WebElement seatsError;

    @FindBy(xpath = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-select-ticket-dialog/mat-dialog-content/div[1]/div[1]/button")
    private WebElement reserveButtonSitting;

    @FindBy(xpath = "/html/body/div[2]/div/div/snack-bar-container/simple-snack-bar")
    private WebElement snackBar;

    public ReservePage(WebDriver driver) {
        this.driver = driver;
    }

    public void ensureWebElementIsDisplayed(WebElement element) {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(element));
    }

    public WebElement getSnackBar() {
        return snackBar;
    }

    public void setSnackBar(WebElement snackBar) {
        this.snackBar = snackBar;
    }

    public WebElement getReserveButtonSitting() {
        return reserveButtonSitting;
    }

    public void setReserveButtonSitting(WebElement reserveButtonSitting) {
        this.reserveButtonSitting = reserveButtonSitting;
    }

    public WebElement getNumberOfSeatsError() {
        return numberOfSeatsError;
    }

    public WebElement getSeatsError() {
        return seatsError;
    }

    public void setSeatsError(WebElement seatsError) {
        this.seatsError = seatsError;
    }

    public WebElement getSeat1A() {
        return seat1A;
    }

    public void setSeat1A(WebElement seat1A) {
        this.seat1A = seat1A;
    }

    public WebElement getSeat1B() {
        return seat1B;
    }

    public void setSeat1B(WebElement seat1B) {
        this.seat1B = seat1B;
    }

    public void setNumberOfSeatsError(WebElement numberOfSeatsError) {
        this.numberOfSeatsError = numberOfSeatsError;
    }

    public WebElement getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(WebElement totalPrice) {
        this.totalPrice = totalPrice;
    }

    public WebElement getReserveButtonStanding() {
        return reserveButtonStanding;
    }

    public void setReserveButtonStanding(WebElement reserveButtonStanding) {
        this.reserveButtonStanding = reserveButtonStanding;
    }

    public WebElement getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(String numberOfSeats) {
        this.numberOfSeats.clear();
        this.numberOfSeats.sendKeys(numberOfSeats);
    }

    public WebElement getSelectStandingArea() {
        return selectStandingArea;
    }

    public void setSelectStandingArea(WebElement selectStandingArea) {
        this.selectStandingArea = selectStandingArea;
    }

    public WebElement getSelectSittingArea() {
        return selectSittingArea;
    }

    public void setSelectSittingArea(WebElement selectSittingArea) {
        this.selectSittingArea = selectSittingArea;
    }
}
