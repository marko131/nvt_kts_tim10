DELETE FROM user;
DELETE FROM layout;
DELETE FROM standing_area;
DELETE FROM sitting_area;
DELETE FROM ticket;
DELETE FROM event;
DELETE FROM price_list;

INSERT INTO user (id, email, is_admin, last_name, name, password) VALUES (1, 'admin@gmail.com', 1, 'nikolic', 'nikola', '$2a$10$4/UPWTscRULDXAFDbxP8OeYDWRDk.ji2Yy8LWA1kvKT9oyIwJ1plu');
INSERT INTO user (id, email, is_admin, last_name, name, password) VALUES (2, 'user@gmail.com', 0, 'petrovic', 'petar', '$2a$10$DhnvByFeBNJS7K/Yh3gziucI0Y/47q5PPJjwa8LzseMuT8Qe.F3Oi');
INSERT INTO user (id, email, is_admin, last_name, name, password) VALUES (3, 'user2@gmail.com', 0, 'markovic', 'marko', '$2a$10$DhnvByFeBNJS7K/Yh3gziucI0Y/47q5PPJjwa8LzseMuT8Qe.F3Oi');

INSERT INTO location (id, address, latitude, longitude, name, picture) VALUES (1, 'Address1', 1.000000, 1.000000, 'Location123', 'https://upload.wikimedia.org/wikipedia/commons/2/2d/Spens.jpg');
INSERT INTO location (id, address, latitude, longitude, name, picture) VALUES (2, 'Address2', 1.000000, 1.000000, 'Location321', 'https://upload.wikimedia.org/wikipedia/commons/2/2d/Spens.jpg');

INSERT INTO layout (id, layout_name, stage_position, location_id) VALUES (1, 'layout1', 'E', 1);
INSERT INTO layout (id, layout_name, stage_position, location_id) VALUES (2, 'layout2', 'W', 1);

INSERT INTO standing_area(id, max_number, layout_position, layout_id) VALUES (1, 50, 'E', 1);
INSERT INTO standing_area(id, max_number, layout_position, layout_id) VALUES (2, 50, 'W', 2);

INSERT INTO sitting_area(id, num_rows, num_seats, layout_position, layout_id) VALUES (1, 7, 7, 'E', 1);
INSERT INTO sitting_area(id, num_rows, num_seats, layout_position, layout_id) VALUES (2, 2, 2, 'W', 2);

INSERT INTO event (id, days_reservation_is_valid, description, event_category, max_reservation_number, name, picture, reservations_available, layout) VALUES (1, 1, 'Description', 'OTHER', 3, 'Event123', 'https://www.spens.rs/wp-content/uploads/2015/10/velika_dvorana_Spens11024x768.jpg', '2020-10-10', 1);

INSERT INTO price_list(id, area_id, area_type, date, price, event) VALUES (1, 1, 1, '2020-10-15', 555, 1);
INSERT INTO price_list(id, area_id, area_type, date, price, event) VALUES (2, 1, 0, '2020-10-15', 55, 1);
INSERT INTO price_list(id, area_id, area_type, date, price, event) VALUES (3, 2, 0, '2020-10-16', 50, 1);

INSERT INTO ticket(dtype, id, section, ticket_status, timestamp, price_list, user_id) VALUES ('Ticket', 1, 'standingarea1', 'IS_SOLD', '2020-02-02', 1, 2);
INSERT INTO ticket(dtype, id, section, ticket_status, timestamp, price_list, user_id) VALUES ('Ticket', 2, 'standingarea1', 'IS_RESERVED', '2020-02-03', 1, 2);
INSERT INTO ticket(dtype, id, section, ticket_status, timestamp, price_list, user_id) VALUES ('Ticket', 22, 'standingarea1', 'IS_RESERVED', '2020-02-03', 1, 2);
INSERT INTO ticket(dtype, id, section, ticket_status, timestamp, price_list, user_id) VALUES ('Ticket', 23, 'standingarea1', 'IS_RESERVED', '2020-02-03', 1, 2);
INSERT INTO ticket(dtype, id, section, ticket_status, timestamp, price_list) VALUES ('Ticket', 300, 'standingarea1', 'AVAILABLE', '2020-02-04', 1);
INSERT INTO ticket(dtype, id, section, ticket_status, timestamp, price_list) VALUES ('Ticket', 301, 'standingarea1', 'AVAILABLE', '2020-02-04', 1);
INSERT INTO ticket(dtype, id, section, ticket_status, timestamp, price_list) VALUES ('Ticket', 302, 'standingarea1', 'AVAILABLE', '2020-02-04', 1);
INSERT INTO ticket(dtype, id, section, ticket_status, timestamp, price_list) VALUES ('Ticket', 303, 'standingarea1', 'AVAILABLE', '2020-02-04', 1);
INSERT INTO ticket(dtype, id, section, ticket_status, timestamp, price_list) VALUES ('Ticket', 304, 'standingarea1', 'AVAILABLE', '2020-02-04', 1);
INSERT INTO ticket(dtype, id, section, ticket_status, timestamp, price_list) VALUES ('Ticket', 305, 'standingarea1', 'AVAILABLE', '2020-02-04', 1);
INSERT INTO ticket(dtype, id, section, ticket_status, timestamp, price_list) VALUES ('Ticket', 306, 'standingarea1', 'AVAILABLE', '2020-02-04', 1);
INSERT INTO ticket(dtype, id, section, ticket_status, timestamp, price_list) VALUES ('Ticket', 307, 'standingarea1', 'AVAILABLE', '2020-02-04', 1);
INSERT INTO ticket(dtype, id, section, ticket_status, timestamp, price_list) VALUES ('Ticket', 308, 'standingarea1', 'AVAILABLE', '2020-02-04', 1);
INSERT INTO ticket(dtype, id, section, ticket_status, timestamp, price_list) VALUES ('Ticket', 309, 'standingarea1', 'AVAILABLE', '2020-02-04', 1);

INSERT INTO ticket(dtype, id, section, ticket_status, timestamp, row_num, seat_num, price_list, user_id) VALUES ('TicketSittingArea', 10, 'sittingarea1', 'IS_SOLD', '2020-02-02', 5, 4, 2, 2);
INSERT INTO ticket(dtype, id, section, ticket_status, timestamp, row_num, seat_num, price_list, user_id) VALUES ('TicketSittingArea', 20, 'sittingarea1', 'IS_RESERVED', '2020-02-03', 2, 1, 2, 2);
INSERT INTO ticket(dtype, id, section, ticket_status, timestamp, row_num, seat_num, price_list, user_id) VALUES ('TicketSittingArea', 220, 'sittingarea1', 'IS_RESERVED', '2020-02-03', 2, 2, 2, 2);
INSERT INTO ticket(dtype, id, section, ticket_status, timestamp, row_num, seat_num, price_list, user_id) VALUES ('TicketSittingArea', 230, 'sittingarea1', 'IS_RESERVED', '2020-02-03', 2, 3, 2, 2);
INSERT INTO ticket(dtype, id, section, ticket_status, timestamp, row_num, seat_num, price_list) VALUES ('TicketSittingArea', 30, 'sittingarea1', 'AVAILABLE', '2020-02-04', 1, 1, 2);

INSERT INTO ticket(dtype, id, section, ticket_status, timestamp, row_num, seat_num, price_list) VALUES ('TicketSittingArea', 31, 'sittingarea2', 'AVAILABLE', '2020-02-04', 1, 1, 3);
INSERT INTO ticket(dtype, id, section, ticket_status, timestamp, row_num, seat_num, price_list) VALUES ('TicketSittingArea', 32, 'sittingarea2', 'AVAILABLE', '2020-02-04', 1, 2, 3);
INSERT INTO ticket(dtype, id, section, ticket_status, timestamp, row_num, seat_num, price_list) VALUES ('TicketSittingArea', 33, 'sittingarea2', 'AVAILABLE', '2020-02-04', 2, 1, 3);
INSERT INTO ticket(dtype, id, section, ticket_status, timestamp, row_num, seat_num, price_list) VALUES ('TicketSittingArea', 34, 'sittingarea2', 'AVAILABLE', '2020-02-04', 2, 2, 3);