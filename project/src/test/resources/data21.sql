DELETE FROM user;
DELETE FROM layout;
DELETE FROM standing_area;
DELETE FROM sitting_area;
DELETE FROM ticket;
DELETE FROM event;
DELETE FROM price_list;
DELETE FROM layout_sitting_areas;
DELETE FROM layout_standing_areas;

INSERT INTO user (id, email, is_admin, last_name, name, password) VALUES (1, 'admin@gmail.com', 1, 'nikolic', 'nikola', '$2a$10$4/UPWTscRULDXAFDbxP8OeYDWRDk.ji2Yy8LWA1kvKT9oyIwJ1plu');
INSERT INTO user (id, email, is_admin, last_name, name, password) VALUES (2, 'user@gmail.com', 0, 'petrovic', 'petar', '$2a$10$DhnvByFeBNJS7K/Yh3gziucI0Y/47q5PPJjwa8LzseMuT8Qe.F3Oi');
INSERT INTO user (id, email, is_admin, last_name, name, password) VALUES (3, 'user2@gmail.com', 0, 'markovic', 'marko', '$2a$10$DhnvByFeBNJS7K/Yh3gziucI0Y/47q5PPJjwa8LzseMuT8Qe.F3Oi');

INSERT INTO location (id, address, latitude, longitude, name, picture) VALUES (1, 'Address1', 1.000000, 1.000000, 'Location123', 'https://upload.wikimedia.org/wikipedia/commons/2/2d/Spens.jpg');
INSERT INTO location (id, address, latitude, longitude, name, picture) VALUES (2, 'Address2', 1.000000, 1.000000, 'Location321', 'https://upload.wikimedia.org/wikipedia/commons/2/2d/Spens.jpg');

INSERT INTO layout (id, layout_name, stage_position, location_id) VALUES (1, 'layout1', 'E', 1);
INSERT INTO layout (id, layout_name, stage_position, location_id) VALUES (2, 'layout2', 'W', 1);

INSERT INTO standing_area(id, max_number, layout_position, layout_id) VALUES (1, 5, 'S', 1);
INSERT INTO standing_area(id, max_number, layout_position, layout_id) VALUES (2, 50, 'N', 2);

INSERT INTO sitting_area(id, num_rows, num_seats, layout_position, layout_id) VALUES (1, 2, 2, 'N', 1);
INSERT INTO sitting_area(id, num_rows, num_seats, layout_position, layout_id) VALUES (2, 6, 6, 'S', 2);

INSERT INTO layout_sitting_areas(layout_id, sitting_areas_id) VALUES (1, 1);
INSERT INTO layout_sitting_areas(layout_id, sitting_areas_id) VALUES (2, 2);

INSERT INTO layout_standing_areas(layout_id, standing_areas_id) VALUES (1, 1);
INSERT INTO layout_standing_areas(layout_id, standing_areas_id) VALUES (2, 2);

INSERT INTO event (id, days_reservation_is_valid, description, event_category, max_reservation_number, name, picture, reservations_available, layout) VALUES (1, 1, 'Description', 'OTHER', 3, 'Event123', 'https://www.spens.rs/wp-content/uploads/2015/10/velika_dvorana_Spens11024x768.jpg', '2020-10-10', 1);

INSERT INTO price_list(id, area_id, area_type, date, price, event) VALUES (1, 1, 1, '2020-10-15', 555, 1);
INSERT INTO price_list(id, area_id, area_type, date, price, event) VALUES (2, 1, 0, '2020-10-15', 55, 1);

INSERT INTO ticket(dtype, id, section, ticket_status, price_list) VALUES ('Ticket', 1, 'Standing area: 1', 'AVAILABLE', 1);
INSERT INTO ticket(dtype, id, section, ticket_status, price_list) VALUES ('Ticket', 2, 'Standing area: 1', 'AVAILABLE', 1);
INSERT INTO ticket(dtype, id, section, ticket_status, price_list) VALUES ('Ticket', 3, 'Standing area: 1', 'AVAILABLE', 1);
INSERT INTO ticket(dtype, id, section, ticket_status, price_list) VALUES ('Ticket', 4, 'Standing area: 1', 'AVAILABLE', 1);
INSERT INTO ticket(dtype, id, section, ticket_status, price_list) VALUES ('Ticket', 5, 'Standing area: 1', 'AVAILABLE', 1);

INSERT INTO ticket(dtype, id, section, ticket_status, row_num, seat_num, price_list) VALUES ('TicketSittingArea', 6, 'Sitting area: 1', 'AVAILABLE', 1, 1, 2);
INSERT INTO ticket(dtype, id, section, ticket_status, row_num, seat_num, price_list) VALUES ('TicketSittingArea', 7, 'Sitting area: 1', 'AVAILABLE', 1, 2, 2);
INSERT INTO ticket(dtype, id, section, ticket_status, row_num, seat_num, price_list) VALUES ('TicketSittingArea', 8, 'Sitting area: 1', 'AVAILABLE', 2, 1, 2);
INSERT INTO ticket(dtype, id, section, ticket_status, row_num, seat_num, price_list) VALUES ('TicketSittingArea', 9, 'Sitting area: 1', 'AVAILABLE', 2, 2, 2);
