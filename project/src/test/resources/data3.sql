DELETE FROM location;
DELETE FROM layout;
DELETE FROM sitting_area;
DELETE FROM event;

INSERT INTO user (id, email, is_admin, last_name, name, password) VALUES (1, 'admin@gmail.com', 1, 'admin', 'admin', '$2a$10$RG1Ax2LFvLIXHAo5s0ci..eXna1L/uDJ7SDTdFSuX3USfEZhIZ2Bi');
INSERT INTO user (id, email, is_admin, last_name, name, password) VALUES (2, 'user@gmail.com', 0, 'user', 'user', '$2a$10$RG1Ax2LFvLIXHAo5s0ci..eXna1L/uDJ7SDTdFSuX3USfEZhIZ2Bi');

-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-- Test podaci za Location
INSERT INTO location (id, name, address, latitude, longitude, picture) VALUES (1, 'SPENS', 'Sutjeska 2, Novi Sad 21000', 45.246937, 19.845355, 'https://rs.n1info.com/Picture/231909/jpeg/SPENS-1-.jpg');
INSERT INTO location (id, name, address, latitude, longitude, picture) VALUES (2, 'Srpsko narodno pozoriste', 'Pozorišni trg 1, Novi Sad 21000', 45.255009, 19.843016, 'https://barinfo.me/wp-content/uploads/2019/08/snp-1.jpg');
INSERT INTO location (id, name, address, latitude, longitude, picture) VALUES (3, 'Test', 'Test address 1, Novi Sad 21000', 45.123456, 19.654321, 'picture/path/test.jpg');

INSERT INTO layout (id, location_id, layout_name, stage_position) VALUES (1, 2, 'Layout1', 'M');
INSERT INTO location_layouts (location_id, layouts_id) VALUES (2, 1);

INSERT INTO sitting_area (id, num_rows, num_seats, layout_id, layout_position) VALUES (1, 15, 20, 1, 'E');
INSERT INTO layout_sitting_areas (layout_id, sitting_areas_id) VALUES (1, 1);

-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-- Test podaci za PriceList
INSERT INTO layout (id, location_id, layout_name, stage_position) VALUES (2, 1, 'Layout2', 'M');
INSERT INTO location_layouts (location_id, layouts_id) VALUES (1, 2);

INSERT INTO standing_area (id, max_number, layout_id, layout_position) VALUES (1, 500, 2, 'E');
INSERT INTO layout_standing_areas (layout_id, standing_areas_id) VALUES (2, 1);

INSERT INTO sitting_area (id, num_rows, num_seats, layout_id, layout_position) VALUES (2, 20, 70, 2, 'W');
INSERT INTO layout_sitting_areas (layout_id, sitting_areas_id) VALUES (2, 2);

INSERT INTO event (id, name, description, event_category, picture, days_reservation_is_valid, reservations_available, max_reservation_number, layout) VALUES (1, 'Đorđe Balašević',
                'Sada već tradicionalni, četvrti po redu novosadski koncert Đorđa Balaševića na Bogojavljensku noć biće održan u Velikoj sali SPC Vojvodina (SPENS), u nedelju 19. januara 2020." +
                " godine sa početkom u 20 časova.', 'CONCERT', 'picture/path/balasevic.jpg', 2, '2020-01-17', 5, 2);

-- AreaType -> SITTING_AREA = 0, STANDING_AREA = 1
INSERT INTO price_list (id, area_id, area_type, date, price, event) VALUES (1, 2, 0, '2020-01-19', 2100, 1);
INSERT INTO price_list (id, area_id, area_type, date, price, event) VALUES (2, 1, 1, '2020-01-19', 1600, 1);
INSERT INTO price_list (id, area_id, area_type, date, price, event) VALUES (3, 1, 1, '2020-01-20', 1600, 1);

INSERT INTO standing_area (id, max_number, layout_id, layout_position) VALUES (2, 500, 1, 'W');
INSERT INTO layout_standing_areas (layout_id, standing_areas_id) VALUES (1, 2);
